once	again	,	david	mccullough	's	treatment	of	an	american	icon	is	the	best	of	its	kind	.
this	biography	reads	like	a	fascinating	novel	,	and	mcculloughs	narrative	ability	is	unmatched	.
no	one	seems	to	be	able	to	tell	a	story	the	way	he	can	.
the	great	theodore	roosevelt	,	known	throughout	the	world	and	across	the	ages	as	an	incredibly	strong	,	tough	,	larger	than	life	figure	actually	began	as	somewhat	of	a	weakling	.
the	book	covers	his	total	transformation	from	a	sickly	child	to	a	man	of	great	resolve	and	courage	,	the	man	who	would	become	president	of	the	united	states	.
best	of	all	is	that	mccullough	likes	and	admires	his	subject	.
this	is	not	always	the	case	with	biographies	.
roosevelt	and	his	family	are	treated	with	the	dignity	and	respect	they	deserve	.
highly	recommended	,	as	are	all	of	mccullough	's	books