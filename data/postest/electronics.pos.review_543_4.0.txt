until	an	accident	caused	one	bud	to	stop	emitting	sound	,	these	were	my	favorite	pair	of	earphones	.
they	were	comfortable	(	i	often	left	them	in	when	i	went	to	sleep	)	,	affordable	,	lightweight	,	durable	,	and	had	good	sound	quality	.
i	used	them	with	my	sony	cfd-v5	boombox	,	my	oritron	op5034	portable	cd	player	,	and	my	rca	rp2410	mp3-cd	player	.
on	all	three	,	bass	and	detail	was	great	,	and	there	was	no	distortion	at	a	higher	volume	.
all	in	all	,	these	earphones	were	a	great	value	;	the	only	possible	downside	is	a	bit	of	fuzziness	in	the	sound	,	which	i	got	used	to	rather	quickly	and	began	to	prefer	over	my	other	earphones	(	it	lent	a	bit	of	warmth	to	the	music	)	.
i	plan	to	buy	another	pair	in	the	near	future	,	and	i	highly	recommend	that	you	do	so	as	well