as	the	authors	state	in	the	preface	of	the	book	,	the	main	focus	of	the	text	is	to	show	the	reader	how	to	write	high	quality	contracts	.
this	is	not	a	text	intended	to	convince	readers	of	the	veracity	of	design	by	contract	,	but	if	you	are	a	developer	with	an	academic	knowledge	of	assertions	,	then	this	book	with	its	extensive	examples	will	likely	convert	you	to	a	design	by	contract	advocate	.
most	of	the	examples	presented	in	the	book	are	written	in	eiffel	,	an	object	oriented	language	that	fully	supports	preconditions	,	postconditions	and	invariants	without	need	for	preprocessing	.
if	you	have	not	written	code	in	eiffel	i	highly	recommend	that	you	download	eiffelstudio	and	deploy	the	examples	in	this	book	.
although	there	are	tools	that	offer	assertion	facilities	,	none	provide	the	ease	of	use	as	that	seen	in	the	eiffel	language	.
the	text	covers	a	preprocessor	that	imparts	assertions	to	java	,	but	the	extra	steps	and	unique	keywords	required	to	incorporate	and	activate	the	assertions	are	likely	to	prevent	wide	scale	use	of	the	tool	in	a	software	development	environment	.
some	aspects	of	object	oriented	languages	such	as	design	by	contract	and	multiple	inheritance	are	often	taught	through	a	language	and	an	integrated	developer	's	environment	that	minimally	supports	the	functionality	.
as	such	developers	form	a	poor	opinion	of	the	concept	and	not	the	tool	.
the	keyword	support	of	contracts	in	eiffel	makes	the	language	a	perfect	learning	tool	which	will	impart	a	more	than	academic	understanding	of	the	value	of	assertions	.
the	authors	state	ardently	that	developers	have	a	choice	between	spending	hours	hunting	down	the	causes	of	runtime	errors	or	instead	allocating	time	to	write	thorough	contracts	.
as	a	sometimes	eiffel	developer	i	can	state	that	this	assertion	is	accurate	.
since	contracts	also	assign	responsibility	for	the	runtime	errors	,	they	are	invaluable	for	debugging	.
i	recall	an	instance	when	i	encountered	a	postcondition	violation	while	using	a	linked	list	from	the	eiffel	library	.
as	a	client	of	the	class	,	i	did	not	spend	any	time	tracking	down	the	source	of	the	error	other	than	to	report	it	along	with	the	condition	that	triggered	the	error	to	the	supplier	.
in	industry	significant	time	is	spent	debugging	code	prior	to	delivery	and	maintaining	code	afterwards	.
any	concept	or	tool	that	aids	in	this	function	is	indispensable	to	the	project	.
apparently	many	people	understood	this	notion	since	for	several	years	the	proposal	to	broaden	java	to	include	keyword	support	of	programming	by	contract	was	purportedly	the	most	frequent	non-bug	report	requested	by	java	developers	.
the	book	is	well	organized	providing	a	set	of	step	by	step	instructions	on	how	to	write	robust	contracts	applicable	to	a	wide	range	of	examples	from	simple	data	structures	to	more	advanced	cases	such	as	the	implementation	of	the	observer	pattern	.
i	considered	myself	well	versed	in	design	by	contract	but	the	six	principles	and	guidelines	as	well	as	the	frame	rules	,	introduced	successively	via	examples	,	provided	a	more	rigorous	approach	to	developing	contracts	than	i	had	employed	in	the	past	.
in	chapter	NUMBER	,	the	distinction	between	basic	and	derived	queries	is	discussed	and	all	six	principles	are	introduced	through	a	stack	example	.
each	principle	is	unveiled	in	conjunction	with	designing	methods	and	their	contracts	for	the	data	structure	.
in	the	development	of	the	dictionary	class	in	chapter	NUMBER	,	the	authors	present	a	case	for	a	new	query	which	asks	if	the	dictionary	already	contains	a	specific	key	.
the	query	arises	from	the	specific	application	of	the	principles	pertaining	to	postconditions	.
this	is	an	excellent	example	of	how	contracts	result	in	the	development	of	new	and	needed	features	in	a	class	.
in	chapter	NUMBER	,	the	authors	present	a	clear	rational	for	weakening	inherited	preconditions	and	strengthening	postconditions	and	invariants	in	accordance	with	other	literature	pertaining	to	the	same	topic	.
in	particular	,	the	example	of	a	delivery	service	reads	like	a	case	of	retail	agreement	in	plain	language	but	is	complemented	by	examples	of	code	and	uml	diagrams	.
the	concept	of	guarding	postconditions	with	preconditions	to	facilitate	possible	redefinition	of	methods	is	also	introduced	and	well	explained	.
chapter	NUMBER	covers	the	benefits	of	design	by	contract	and	if	the	reader	has	compiled	and	executed	the	examples	presented	in	the	book	,	they	will	have	experienced	some	of	these	benefits	firsthand	.
of	course	there	are	considerations	like	better	documentation	and	more	reliable	code	,	but	the	debugging	assistance	is	the	most	overt	benefit	of	contracts	.
the	stack	traces	associated	with	assertion	exceptions	are	an	invaluable	tool	during	both	development	and	maintenance	.
anyone	who	has	developed	quality	software	under	a	tight	schedule	understands	the	benefits	of	precision	bug	identification	and	resolution	.
analysis	by	contract	,	introduced	in	the	final	chapter	,	presents	the	concepts	of	design	by	contract	abstractly	removed	from	the	code	specifics	.
the	authors	present	an	analysis	of	a	person	withdrawing	money	from	a	bank	.
in	doing	so	they	codify	the	sequence	of	events	based	on	the	contract	stating	that	the	design	is	derived	from	the	specification	rather	than	being	a	design	that	is	also	being	used	as	a	specification	.
the	obvious	implication	of	this	is	that	the	final	product	will	likely	fulfill	the	specification	implicitly	.
this	is	a	book	packed	with	knowledge	that	is	essential	for	all	developers	regardless	of	the	current	support	of	design	by	contract	in	conventional	languages	.
in	the	future	,	as	the	more	popular	languages	incorporate	additional	design	by	contract	concepts	,	there	will	be	an	increased	number	of	authors	producing	texts	on	these	topics	as	well	as	a	growing	number	of	developers	annotating	their	resumes	accordingly	.
i	suspect	that	all	of	these	texts	will	reference	design	by	contract	by	example	and	many	of	the	developers	that	already	understand	the	concept	will	be	well	acquainted	with	the	pages	of	this	book	.