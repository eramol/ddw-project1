oh	,	spike	in	san	antonio	.
it	's	so	hard	to	believe	that	sexism	exists	,	is	not	it	?
i	am	not	sure	where	your	numbers	come	from	,	as	your	logic	seems	tenuous	,	at	best	,	but	according	to	that	left-wing	,	liberal	think	tank	known	as	the	us	census	bureau	,	rowe-finkbeiner	is	right	.
and	it	's	getting	worse	.
the	census	bureau	reported	in	august	,	based	on	the	current	population	survey	,	that	women	's	real	median	earnings	fell	by	$	NUMBER	,	or	NUMBER	percent	,	from	NUMBER	to	NUMBER	,	while	men	's	increased	by	$	NUMBER	,	or	NUMBER	percent	.
(	the	increase	for	men	was	not	statistically	significant	,	but	the	decrease	for	women	was	.	)
the	gender	wage	ratio	thus	fell	to	NUMBER	,	compared	with	NUMBER	in	NUMBER	.
in	addition	,	a	u.s.	government	accountability	office	(	gao	)	report	released	in	october	of	NUMBER	,	controlling	for	changes	in	education	and	work	experience	over	time	,	concludes	that	womens	earnings	have	remained	stagnant	,	relative	to	men	's	,	for	an	even	longer	period-17	years-with	a	gap	that	can	not	be	explained	by	measurable	differences	in	education	and	experience	.
if	you	actually	read	the	f-word	,	you	might	find	cited	cases	like	this	:	although	women	make	up	nearly	3/4	of	wal-mart	's	workforce	,	men	hold	NUMBER	%	of	management	positions-despite	female	employee	's	receiving	better	performance	evaluations	overall-and	female	employees	are	paid	lower	wages	than	male	employees	for	the	same	jobs	.
none	of	this	is	disputed	by	the	corporation	.
why	?
according	to	wal-mart	,	because	men	are	there	for	a	career	,	while	women	are	working	for	pocket	money	.
i	wish	i	could	say	your	attitude	was	merely	naive-but	the	truth	is	it	's	far	more	dangerous	and	insidious	.
thanks	to	rowe-finbeiner	for	telling	it	how	it	really	is	.