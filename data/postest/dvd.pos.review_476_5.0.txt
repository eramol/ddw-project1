art	history	has	always	been	an	interest	of	mine	,	and	i	am	a	big	fan	of	other	art	movies	such	as	girl	with	a	pearl	earing	and	merchant	of	venice	.
so	when	i	found	out	about	modigliani	i	was	happy	to	take	the	time	to	go	watch	it	.
not	only	was	it	worth	my	time	,	but	it	was	a	beautiful	filmed	movie	that	depicted	the	life	of	a	very	passionate	man	in	a	time	when	passion	was	not	looked	fondly	on	.
everything	about	the	movie	is	amazing	,	from	the	sets	to	the	acting	.
i	feel	this	is	one	of	andy	garcia	's	best	films	in	some	time	,	not	only	is	he	convincing	,	but	he	fits	the	part	of	modigliani	better	than	any	other	actor	could	.
they	focused	the	movie	around	his	rivalry	with	picasso	,	which	i	find	to	be	one	of	the	most	interesting	things	about	it	.
you	do	not	only	get	an	insight	into	one	artist	,	but	many	.
you	see	the	life	the	artists	were	living	and	the	obstacles	they	had	to	overcome	to	get	their	art	out	into	the	world	.
modigliani	also	had	a	tragic	romance	with	a	young	catholic	girl	,	who	's	father	did	not	want	her	with	the	struggling	painter	.
the	story	is	the	perfect	depiction	of	the	religious	standards	in	that	time	period	.
overall	this	was	a	great	movie	,	and	i	would	strongly	suggest	that	you	take	the	time	to	check	it	out	.
you	wo	not	regret	it	,	it	's	probably	one	of	the	best	movies	of	the	year