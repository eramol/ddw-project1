i	am	a	d-link	refugee	and	this	card	has	been	just	great	.
my	home	connection	has	been	very	good	,	i	am	in	a	basement	and	have	no	idea	where	the	router	is	!	swapping	wireless	networks	is	a	breeze	as	i	travel	around	-	it	can	be	easily	configured	to	automatically	find	one	wherever	you	are	.
very	reliable	,	quality	construction	,	easy	to	install	and	no	problems	with	w2k	.
this	is	my	ORDINAL	netgear	card	;	also	have	the	fa511	wired	adapter	about	which	i	can	say	the	same	things	(	except	the	wireless	bits	)	.definitely	recommended	and	worth	the	extra	cost	if	there	is	any