i	ca	not	go	on	enough	about	this	book	.
it	should	be	required	reading	in	colleges	and	for	those	in	all	levels	of	government	,	wall	street	and	industry	ceos	.
the	writing	is	excellent	.
many	of	its	passages	read	like	prayers	.
i	ca	not	think	of	topics	and	subjects	more	important	than	those	jensen	writes	about	-	our	cultures	diconnection	from	the	natural	world	and	from	ourselves	and	how	it	has	impacted	our	mental	and	spiritual	health	and	the	health	of	the	earth	.
jensen	is	so	passionate	about	these	topics	that	he	does	repeat	certain	ideas	,	but	instead	of	ruining	it	,	i	think	the	non-linear	trajectory	of	his	book	actually	enhances	his	message	.
the	first	half	of	the	book	where	he	discusses	problems	can	be	hard-hitting	and	difficult	to	get	through	certain	parts	of	it	,	but	please	hold	fast	through	the	end	.
it	is	worth	it	.
jensen	's	eloquence	and	wisdom	are	best	when	he	is	also	discussing	answers	,	or	possibilities	,	as	it	were.www.corimorenberg.co