ever	wonder	where	col.	kurtz	would	've	ended	up	if	he	had	survived	the	end	of	apocalypse	now	?
well	,	now	we	know	:	he	exiled	himself	to	a	deserted	island	to	create	humanimals	-	the	horror	,	the	horror	.
in	light	of	all	the	bad	press	one	would	really	think	this	is	a	lousy	movie	.
and	i	can	see	why	some	people	do	not	like	it	,	the	main	portion	of	the	flick	is	really	insane	.
however	,	if	you	are	a	sucker	for	lost-on-an-island	type	yarns	and	appreciate	the	heavy	mood	&	insanity	of	apocalypse	now	,	you	will	probably	like	it	.
i	am	not	saying	it	's	anywhere	near	as	great	as	apocalypse	now	but	marlon	brando	and	the	jungle	insanity	make	the	comparison	inevitable	.
what	works	:	the	title	sequence	is	great	,	perhaps	one	of	the	best	in	all	of	cinema	(	another	amazon	reviewer	noted	this	,	so	i	am	not	alone	)	;	the	soundtrack	is	phenomenal	(	'nuff	said	)	;	the	plot	is	intriguing	;	the	humanimal	make-up	is	fine	(	'hyena	looks	especially	horrific	)	;	the	film	possesses	an	undeniable	creative	pizzazz	(	i.e	.
it	's	moody	,	atmospheric	and	insane	)	;	and	marlon	brando	's	NUMBER	minute	stint	in	the	film	is	as	captivating	as	always	(	you	can	probably	tell	i	am	a	huge	brando	fan	-	in	particular	,	one-eyed	jacks	,	mutiny	on	the	bounty	(	YEAR	)	,	the	young	lions	,	superman	and	apocalypse	now	[	not	redux	]	)	.
what	does	not	work	:	the	third	act	of	the	film	is	almost	total	insanity	,	except	for	the	final	ten	minutes	or	so	;	as	a	result	the	story	will	lose	the	interest	of	those	who	have	no	appreciation	for	such	artistic	flair	.
closing	thoughts	:	i	really	enjoyed	seeing	brando	in	his	old	age	in	this	flick	;	it	proves	he	had	his	magnetic	charm	until	the	end	.
if	you	enjoyed	brando	as	'kurtz	in	apocalypse	now	you	will	appeciate	him	in	island	;	in	fact	,	as	already	noted	,	dr.	moreau	is	a	variation	of	kurtz	in	his	old	age	(	i.e	.
nutjob	in	the	jungle	)	.if	you	are	predisposed	for	such	a	flick	,	dr.	moreau	is	weird	,	but	certainly	worthwhile	.