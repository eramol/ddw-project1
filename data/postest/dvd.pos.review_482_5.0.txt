this	is	one	of	the	best	photography	documentary	i	have	ever	seen	.
i	love	photojournalism	and	this	is	as	good	as	it	gets	.
i	love	the	story	narration	that	this	dvd	succeeds	in	.
james	nachtwey	is	the	greatest	ever	to	have	photographed	all	the	atrocities	up	close	and	personal	.
you	can	feel	the	pain	,	agony	and	raw	human	emotion	in	his	photographs	.
he	is	right	in	the	middle	of	shooting	with	mostly	a	wide	angle	just	a	few	feet	away	from	danger	.
it	needs	guts	and	commitment	to	do	a	job	which	has	constant	fear	of	getting	killed	.
i	salute	you	DOTS	..james	nachtweyyou	inspire	me.-	uday