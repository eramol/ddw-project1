i	had	to	replace	my	video	with	this	dvd	version	.
this	movie	is	spectacular-beautiful	photography	,	excellent	acting	,	and	most	importantly	a	tremendous	comment	on	the	social	restrictions	placed	on	women	of	the	time	.
it	is	based	on	the	true	life	of	one	of	the	first	commissioned	female	artists	in	europe	.
artemesia	,	whose	father	is	an	artist	,	is	forbidden	to	draw	or	paint	the	human	body	.
her	efforts	to	do	so	lead	her	into	difficulties	.
her	father	is	supportive	but	even	he	must	fight	the	social	structure-especially	when	trying	to	get	her	into	the	male-only	art	school	.
he	finally	gets	her	apprenticed	to	another	talented	artist	and	is	devastated	when	he	finds	they	have	engaged	in	an	unlawful	relationship	.
he	files	rape	charges	and	everyone	suffers	.
a	brilliant	social	commentary