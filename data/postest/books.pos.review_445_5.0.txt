george	dewolfe	sees	the	photographer	as	an	artist	,	not	a	technician	,	yet	his	book	provides	the	technical	knowledge	and	shows	how	to	use	photoshop	to	create	consistently	high	quality	fine	art	prints	.
writing	for	the	experienced	photographer	with	a	basic	knowledge	of	photoshop	,	dewolfe	begins	by	showing	the	photographer	,	with	many	examples	,	how	to	evaluate	an	image	and	decide	what	adjustments	will	bring	out	its	special	qualities	.
the	experienced	photographer	has	always	used	his	or	her	understanding	of	how	light	,	luminosity	and	color	give	life	to	an	image	,	and	has	traditionally	used	various	techniques	in	the	darkroom	to	enhance	them	.
dewolfe	begins	by	training	the	eye	of	the	artist	to	see	these	elements	and	understand	how	they	work	.
after	showing	the	reader	how	to	truly	see	the	image	and	decide	how	to	fine-tune	it	,	dewolfe	then	gives	us	a	very	detailed	digital	workflow	which	,	once	mastered	,	allows	the	photographer	to	produce	excellent	prints	on	a	consistent	basis	.
the	workflow	is	clarified	by	many	screen	shots	showing	photoshop	techniques	and	before/after	image	adjustments	.
those	who	think	visually	will	find	these	particularly	helpful	.
dewolfe	's	approach	is	to	use	the	simplest	techniques	that	can	produce	consistently	fine	results	.
(	some	photoshop	users	will	be	surprised	by	his	use	of	the	history	brush	instead	of	masking	;	as	a	photographer	who	uses	this	tool	,	i	appreciate	its	simplicity	and	controlled	results	.	)
his	focus	is	not	on	tips-and-tricks	in	photoshop	,	but	on	how	to	get	the	best	possible	prints	in	the	simplest	,	most	straightforward	way	.
while	a	first	reading	gives	valuable	insights	,	the	book	's	true	value	becomes	evident	when	the	photographer	uses	the	workflow	on	his	or	her	own	images	,	and	practices	to	become	proficient	.
the	proof	,	as	they	say	,	is	in	the	print