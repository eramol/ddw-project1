i	checked	this	book	out	of	our	local	library	when	i	was	teaching	a	unit	on	oceans	to	my	preschool	class	.
the	children	listened	closely	as	the	suspense	built	in	this	story	and	the	surprise	ending	was	wonderful	.
the	children	understood	the	food	chain	concept	and	have	asked	me	several	times	to	read	it	again	!
it	has	also	triggered	a	renewed	interest	in	the	children	for	using	their	own	imaginations	to	create	stories	of	their	own	creation	.
i	am	ordering	a	copy	for	our	own	personal	library	.
this	one	is	a	keeper