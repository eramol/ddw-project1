the	story	of	anne	continues	with	this	work	and	the	author	stays	true	to	her	style	,	story	line	and	character	developement	.
this	,	like	the	other	books	in	this	series	,	have	a	rather	timeless	nature	about	them	and	a	comforting	charm	.
the	reader	,	of	course	,	must	remember	the	time	they	were	written	and	the	style	and	syntax	used	at	that	time	.
from	my	own	point	of	view	,	this	is	great	.
i	enjoy	this	type	of	writing	and	certainly	enjoy	ms.	montgomery	's	story	telling	abilities	.
in	this	work	,	anne	goes	off	to	redman	college	and	her	adventure	continues	.
recommend	these	books	for	readers	of	all	ages	.
wish	there	were	more	works	out	there	like	it	.