a	lot	of	these	reviews	say	that	they	've	read	this	book	and	it	's	done	wonders	for	them	and	everyone	they	've	recommended	it	to	.
the	book	itself	will	not	do	wonders	unless	you	work	on	the	things	gawain	suggests	.
it	's	not	a	miracle	cure-all	book	-	it	's	about	giving	you	the	motivation	to	do	something	about	your	life	instead	of	sitting	back	and	watching	the	trail	roll	out	behind	you	.
i	read	this	book	in	college	when	i	was	going	through	a	little	bit	of	a	rough	patch	,	figuring	out	what	i	wanted	,	etc	.
now	that	i	've	graduated	and	am	slowly	becoming	wary	of	the	corporate	world	,	i	need	more	positivity	in	my	life	so	i	am	re-reading	it	in	the	hopes	that	i	can	zero	in	on	my	goals	and	start	achieving	them	again	.
i	highly	recommend	it	to	those	who	are	open-minded	enough	to	give	it	a	chance