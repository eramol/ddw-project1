i	have	not	watched	the	first	season	-	i	just	started	watching	the	show	so	i	am	a	very	brand	new	viewer	-	but	i	've	seen	two	episodes	of	season	NUMBER	and	this	is	really	a	good	show	.
its	subject	matter	is	grim	,	yes	,	but	there	are	light	moments	and	the	cast	has	nice	chemistry	,	especially	david	boreanaz	(	buffy	the	vampire	slayer	,	angel	)	and	emily	deschanel	.
the	stories	are	interesting	w/out	being	too	scientific	so	anyone	can	follow	along	.
and	i	never	realized	how	much	information	a	person	can	get	just	by	examining	someone	's	bones	(	okay	,	maybe	only	in	tv	land	)	.