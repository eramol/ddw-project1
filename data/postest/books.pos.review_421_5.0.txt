if	you	do	not	know	what	you	are	doing	then	you	need	to	get	this	book	.
if	you	think	you	know	what	you	are	doing	,	then	you	definitely	need	this	book	because	you	probably	have	no	clue	.
contrary	to	what	one	reviewer	states	,	this	text	does	provide	a	good	balance	between	application	and	theoretical	example	.
i	use	this	book	all	of	the	time	as	a	graduate	student	.
it	is	a	wonderful	reference	.
if	you	are	studying	econometrics	,	then	this	is	the	first	book	you	should	buy	.