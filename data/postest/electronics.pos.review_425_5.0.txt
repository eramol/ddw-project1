this	has	helped	me	significantly	in	drawing	in	the	computer	,	for	programs	such	as	adobe	illustrator	,	adobe	photoshop	and	macromedia	flash	.
drawing	with	a	mouse	is	like	drawing	with	a	brick	-	so	buy	this	piece	of	equipment	if	you	need	to	draw	in	the	computer	!	it	also	works	well	as	an	overall	mouse	for	your	computer	.
it	even	comes	with	a	mouse	if	you	prefer	to	use	that	instead	of	the	pen	.
plugs	right	into	your	usb	drive	and	works	almost	instantly