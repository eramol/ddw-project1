if	you	've	got	a	treo	NUMBER	,	you	do	not	need	a	gps	device	.
simple	as	that	.
the	smaller-than-a-small-filet-mignon	gps	device	talks	via	bluetooth	to	your	NUMBER	,	which	attaches	to	your	windshield	via	a	well-engineered	holster	.
the	charger	charges	'em	both	,	or	not	,	as	you	see	fit	.
the	instructions	are	clear	and	to	the	point	.
the	installation	instructions	are	slightly	out	of	date	-	you	do	not	need	a	card	reader	to	make	it	happen	,	after	all	-	and	the	user	experience	is	,	in	my	case	,	outstanding	.
if	you	've	got	a	NUMBER	and	you	ever	drive	somewhere	unfamiliar	,	buy	it