marc	singer	is	a	bloke	from	england	who	moved	to	new	york	city	,	saw	all	these	homeless	sleeping	on	the	streets	,	and	became	friends	with	them	.
they	in	turn	trusted	him	,	accepted	him	as	one	of	their	own	,	and	showed	him	their	secret	living	quarters	.
deep	underground	in	abandoned	train	tunnels	,	the	homeless	erected	huts	out	of	anything	scavenged	off	the	streets	.
intelligent	and	resourceful	,	they	had	hooked	up	to	free	electricity	and	had	working	tv	's	and	stoves	.
what	makes	this	documentary	five	stars	instead	of	four	is	the	making	of	documentary	.
only	there	do	you	learn	that	the	movie	crew	were	the	tunnel	folks	.
only	there	do	you	learn	how	singer	made	this	movie	with	no	money	,	equipment	,	or	knowledge	of	filmmaking	.
this	movie	took	six	years	from	start	to	finish	.
unfortunately	,	it	was	not	the	financial	success	its	producers	hoped	for	.
but	it	resonates	unbelievably	.
the	deleted	scenes	are	not	to	be	missed	,	although	some	are	much	better	than	others	.
the	one	where	a	man	talks	about	how	his	cousin	killed	a	cat	for	food	,	and	the	scene	where	two	young	men	talk	in	x-rated	detail	about	recent	sexual	activities	lent	nothing	to	the	film