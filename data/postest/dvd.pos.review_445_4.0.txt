li	is	no	heavy-weight	pianist	.
neither	is	bunin	or	even	zimerman	.
and	do	not	foeget	that	chopin	himself	weighed	less	than	a	hundred	pounds	after	all	.
and	judging	from	the	steam	and	progess	li	has	made	,	lang	lang	's	position	is	certainly	under	threat	.
and	any	viewer	who	is	interested	in	li	or	how	chinese	pianists	nowadays	are	doing	should	perhaps	go	for	this-	here	we	have	to	thank	martha	for	doing	a	good	job	in	picking	him	as	the	winner	of	the	chopin	competition	.
now	in	his	mid-twenties	,	and	musically	out	of	almost	nowhere	,	li	is	already	a	full	fledged	musician	.
of	course	,	viewers	should	not	compare	him	with	arrau	's	fine	balance	between	the	extreme	high	and	rather	low	register	;	or	his	scales	with	richter	's	.
this	is	not	fair	,	for	arrau	and	richter	are	such	great	artists	.
li	is	already	so	close	to	zimerman	now	.
furthermore	,	what	we	hear	may	have	more	to	do	with	the	accoustics	of	the	hall	,	something	which	has	more	to	do	with	the	engineers	than	the	performer	himself	.
and	i	say	so	because	i	am	a	bit	concerned	about	the	quality	of	the	picture	of	this	dvd	.
it	appears	to	me	more	like	a	vcd	than	dvd	production	(	or	somewhere	in	between	)	:	it	is	slightly	below	average	dg	production	and	the	camerawork	is	also	average	only	.
this	is	obviously	not	fair	to	a	blossoming	career	that	li	is	now	having	.
one	star	is	taken	for	want	of	a	better	production	.