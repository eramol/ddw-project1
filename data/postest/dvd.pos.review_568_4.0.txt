i	became	interested	in	this	movie	after	reading	the	book	ishmael	by	daniel	quinn	and	learning	from	a	friend	that	this	movie	was	based	on	the	book	.
it	was	a	very	good	movie	and	anthony	hopkins	is	wonderfully	persuasive	but	all	the	suggestion	from	ishmael	there	seems	to	be	is	just	that	living	in	nature	is	better	than	living	in	civilization	.
there	was	nothing	that	indicated	the	precariousness	of	civilization	-	takers	are	mentioned	but	it	comes	across	more	as	you	just	do	not	know	what	you	are	missing	by	being	one	-	not	that	all	of	nature	has	had	it	.
heck	,	the	movie	makers	would	be	shooting	their	own	business	in	the	foot	if	they	were	really	to	try	and	convince	us	to	head	back	to	the	trees	.
but	of	course	that	was	the	main	point	of	ishmael	.
if	we	do	not	give	up	being	takers	and	go	back	to	being	part	of	nature	,	then	nature	has	had	it	and	so	have	we