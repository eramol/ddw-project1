i	had	been	storing	my	music	on	a	2gb	card	in	my	palm	pilot	.
i	was	having	a	lot	of	trouble	transferring	the	music	,	with	many	errors	,	not	to	mention	how	long	it	took	to	transfer	.
i	finally	decided	to	get	an	mp3	player	.
a	friend	kept	telling	me	about	the	sandisk	sanso	.
there	were	some	great	features	,	but	the	bad	far	,	far	outweighed	the	good	.
too	many	of	the	same	problems	over	and	over	,	and	many	people	returning	them	.
i	got	my	apple	ipod	last	night	and	have	hardly	put	it	down	!
i	did	a	lot	of	research	first	and	was	hard	pressesd	to	find	negative	feedback	about	this	item	.
this	thing	is	great	!
i	was	up	till	3am	listening	to	my	music	.
the	sound	quality	is	so	much	better	than	i	had	expected	!
i	really	like	the	case	i	got	(	tuffwrap	accent	by	xtrememac-	walmart	and	radio	shack	)	because	it	is	really	tough	rubber	and	all	of	the	holes	are	in	the	right	place	for	the	new	nano	.
the	only	thing	is	that	the	unit	does	not	slide	up	into	it	completely	because	of	the	square	corners	of	the	nano	.
the	case	has	more	rounded	corners	in	the	top	,	so	the	nano	does	not	line	up	just	right	in	the	case	.
it	is	good	enough	and	you	can	still	use	the	controls	.
it	is	only	about	a	1/4	of	an	inch	at	most	.
i	am	just	picky	about	it	lining	up	correctly	so	this	is	a	bit	annoying	.
but	the	case	is	still	great	as	far	as	protection	.
i	also	bought	the	belkin	tunebase	fm	transmitter	for	the	car	.
it	is	similar	to	the	monster	version	.
i	did	not	like	the	belkin	version	at	all	.
i	ended	up	going	and	getting	the	monster	fm	transmitter	at	radio	shack	.
it	works	great	!
i	like	the	fact	that	my	ipod	lays	in	the	console	rather	than	sitting	in	a	dock	(	some	of	you	may	prefer	the	dock	)	.
i	like	that	i	can	pick	up	the	ipod	and	use	it	sort	of	like	a	remote	to	the	radio	because	i	can	operate	it	easily	without	looking	down	at	the	dock	.
i	can	even	hold	it	while	driving	if	i	want	to	flip	through	the	songs	while	driving	rather	than	having	to	reach	over	to	the	dock	.
the	sound	of	this	transmitter	is	great	.
some	areas	you	might	get	a	bit	of	static	or	station	bleed	through	while	driving	,	but	this	is	minimal	and	is	usually	not	a	problem	.
i	find	this	happened	a	lot	less	with	monster	than	with	the	belkin	.
the	belkin	had	a	nasty	hiss	over	all	of	the	songs	.
people	are	talking	about	this	unit	not	having	a	radio	on	it	,	but	i	find	that	i	have	so	many	favorite	songs	on	it	that	i	do	not	need	a	radio	.
when	i	do	have	a	radio	,	all	i	do	is	channel	surf	for	songs	i	like	anyway	.
this	way	,	i	already	have	all	the	songs	i	like	at	my	fingertips	!	i	think	these	headphones	really	make	this	nano	great	!
i	was	so	surprised	when	i	first	heard	this	little	unit	play	!
for	something	so	tiny	,	it	rocks	!
i	am	really	glad	i	followed	my	gut	and	went	with	this	unit	instead	of	dealing	with	frustration	first	by	getting	the	sanso	.
this	new	nano	is	a	must	have	!