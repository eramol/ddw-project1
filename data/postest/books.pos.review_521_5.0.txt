if	you	are	just	starting	out	on	photography	,	i	highly	recommend	this	book	.
it	begins	immediately	on	the	first	chapter	on	picture	making	!
for	this	reason	alone	,	i	would	give	it	NUMBER	stars	.
here	in	the	first	chapter	,	the	authors	wasted	no	time	to	tell	you	what	makes	good	pictures	:	the	photographer	,	not	the	equipment	.
they	then	proceed	to	explain	why	the	majority	of	our	pictures	(	at	least	,	the	rest	of	us	non-professionals	)	turn	out	unflattering	despite	our	best	efforts	and	high-tech	cameras	.
if	you	read	nothing	else	but	the	first	chapter	alone	,	i	would	bet	that	your	photography	will	already	begin	to	improve	.
or	if	not	,	the	first	chapter	will	at	least	inspire	you	to	try	the	suggestions	and	see	the	result	yourself	.
in	fact	,	even	if	all	you	have	is	a	humble	point	and	shoot	film	camera	,	the	things	they	teach	in	the	first	chapter	are	still	totally	valid	,	as	the	principles	of	good	photography	remains	the	same	whether	you	use	film	or	digital	.
the	next	chapters	give	details	about	camera	operations	,	how	to	creatively	use	camera	controls	,	and	another	great	topic	,	how	to	tackle	different	photographic	subjects	.
just	what	i	needed	!
i	like	the	chapter	on	how	to	process	you	own	film	in	black	and	white	.
although	i	will	probably	not	dive	into	it	,	this	chapter	gives	you	good	grounding	on	the	grassroots	of	photography	.
other	good	stuff	inside:1	.
projects	at	the	end	of	most	every	chapter	.
i	dare	you	to	do	all	the	project	at	chapter	one	and	tell	me	if	it	will	not	improve	your	understanding	of	what	makes	a	good	picture	!	NUMBER	.
information	on	digital	aspect	of	photography	.
although	i	shoot	film	slr	,	i	know	that	one	day	,	i	will	want	a	digital	slr	so	the	info	on	it	is	nice.3	.
all	the	illustative	pictures	are	ones	that	can	be	done	by	amateurs	!
in	keeping	with	the	beginner	's	purpose	,	this	is	intentional	on	the	part	of	the	authors	.
if	you	are	looking	for	award	winning	photos	or	famous	photographers	,	you	wont	find	it	here	.
but	the	pictures	in	the	book	are	beautiful	in	their	own	right	and	they	drive	home	the	points	taught	.
modest	studio	lighting	you	can	create	yourself	!
the	principles	on	the	very	important	aspect	of	light	taught	here	are	very	revealing	to	me	.
the	diagram	of	how	to	photograph	a	subject	using	only	your	own	flash	and	any	old	white	card	to	reflect	some	light	is	already	a	good	starting	point	.
however	,	i	am	tempted	to	give	this	book	NUMBER	stars	since	i	felt	that	despite	it	's	generous	information	,	attractive	layout	,	highly	readable	text	,	and	wide	pages	,	the	authors	skimmed	the	topic	on	tricky	exposure	problems	.
the	use	of	grey	cards	,	spotmeters	,	and	handheld	lightmeters	to	solve	these	tough	light	problems	were	only	briefly	explained	.
on	the	other	hand	,	they	provided	easily	applied	solutions	to	our	most	common	exposure	problems	.
so	i	give	them	the	benefit	of	the	doubt	that	such	issues	correctly	belong	to	advanced	photography	topics	which	might	only	discourage	the	wobbly	beginner	if	mentioned	here	.
now	that	you	are	aware	of	it	,	you	will	probably	do	more	research	on	it	.
meanwhile	,	a	lot	of	my	pictures	are	already	much	improved	by	the	application	of	the	principles	in	the	book	alone	.
i	therefore	,	highly	recommend	this	book	to	fellow	beginners	in	photography