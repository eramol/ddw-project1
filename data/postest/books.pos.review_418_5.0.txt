mr	hargrave	's	book	contains	practical	guidance	to	developing	a	sound	,	realistic	and	executable	strategic	business	plan	.
drawing	from	his	wealth	of	business	experience	from	working	&	amp	;	leading	various	corporations	and	running	his	own	consultancy	practice	,	the	book	provides	a	detailed	framework	from	which	managers	can	follow	to	draw	up	their	own	plans	.
on	top	of	that	,	explainations	are	given	throughout	the	book	on	why	certains	steps	are	needful	while	others	are	superfluous	,	what	key	issues	to	consider	and	what	pitfalls	to	avoid	in	formulating	the	plan	.
three	words	that	describe	the	content	of	the	book	will	have	to	be	:	impactful	,	illuminating	and	practical	.
a	must-read	for	managers	and	aspiring	mbas	in	strategic	management	.
a	second	edition	to	bring	in	the	role	of	it	in	strategic	planning	would	be	great