woodward	and	bernstein	,	famous	reporters	for	the	washington	post	during	the	watergate	scandal	,	chronicle	their	experience	investigating	and	exposing	the	deceptive	actions	of	president	nixon	and	his	cronies	during	watergate	.
this	book	takes	the	reader	through	woodward	and	bernstein	's	efforts	at	uncovering	the	truth	,	internal	dilemmas	about	the	information	they	were	uncovering	,	and	covert	meetings	with	confidential	sources	.
they	tell	a	tale	of	mystery	and	intrigue	that	stands	up	to	some	of	the	world	's	best	fiction	except	that	this	story	actually	happened	and	signaled	the	end	of	an	era	where	the	us	presidency	was	respected	above	all	else	.
this	book	is	a	fascinating	look	at	how	investigative	journalism	once	was	.
instead	of	printing	first	and	asking	questions	later	,	woodward	and	bernstein	,	along	with	their	editors	,	took	painstaking	efforts	at	checking	,	double	checking	,	and	even	triple	checking	their	sources	and	information	before	putting	something	derogatory	in	print	.
it	seems	a	sharp	contrast	to	journalism	today	that	appears	to	be	publish	now	,	ask	questions	later	,	and	file	a	retraction	in	small	print	even	later	.
this	book	also	tells	the	tale	of	two	very	opposite	reporters	and	how	they	pulled	together	to	write	about	the	crime	spree	of	the	century	and	topple	a	corrupt	presidency	.
it	gets	better	with	each	reading