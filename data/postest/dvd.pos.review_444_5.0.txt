i	have	heard	both	arrau	and	richter	playing	liszt	's	b	minor	sonata	and	i	am	afraid	i	very	much	prefer	the	way	yundi	li	had	played	this	piece	.
perhaps	it	is	not	fair	to	compare	their	techniques	as	arrau	's	technique	was	always	not	that	impressive	.
yundi	li	is	not	just	a	virtuoso	.
you	are	impressed	not	only	by	the	shattering	way	he	played	for	the	more	emotional	part	of	this	piece	(	when	liszt	was	supposed	to	struggle	with	his	other	side	of	character	)	,	you	will	also	be	impressed	by	his	every	subtle	nuance	for	the	lyrical	part	of	the	music	which	you	seldom	hear	from	others	.
you	will	also	appreciate	all	the	smooth	transitions	he	made	from	one	theme	to	another	.
will	i	be	able	to	hear	a	better	and	a	more	logical	liszt	's	b	minor	sonata	than	this	?	i	believe	yundi	li	is	capable	of	very	deep	feeling	when	he	plays	.
you	seldom	find	one	pianist	more	deeply	immersed	in	the	music	during	performance	than	yundi	li	.
music	just	flow	out	from	his	fingers	in	a	most	natural	way	as	a	result	of	such	deep	feeling	.
i	particularly	agree	with	this	description	of	yundi	li	:	what	distinguishes	this	uniquely	gifted	young	artist	from	most	of	his	contemporaries	is	his	ability	to	put	his	personal	stamp	on	a	work	without	resorting	,	even	for	a	single	gesture	,	to	eccentricity	.
although	richter	was	technically	more	capable	compared	with	arrau	,	i	have	always	found	him	not	expressive	enough	for	pieces	that	require	more	rubati	.
you	will	be	impressed	by	his	technique	for	some	beethoven	sonata	.