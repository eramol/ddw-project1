for	someone	totally	unfamiliar	with	authorware	,	i	thought	this	book	rather	confusing	.
i	typically	learn	best	by	playing	with	software	and	using	the	manual	to	construct	my	own	exercises	.
however	,	i	knew	authorware	was	pretty	complex	so	i	thought	this	book	would	help	.
well	,	it	did	help	a	little	but	overall	i	became	quite	frustrated	with	following	the	book	's	exercises	.
as	i	use	authorware	i	imagine	i	will	keep	this	book	on	my	desk	as	a	reference	tool	.
but	until	then	,	i	will	content	myself	with	playing	on	my	own