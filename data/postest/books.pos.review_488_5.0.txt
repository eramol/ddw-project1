this	book	was	a	gift	given	to	me	by	a	friend	years	ago	.
i	own	and	have	read	about	twelve	books	on	the	topic	.
out	of	all	of	them	,	this	has	been	by	far	the	best	.
by	the	end	it	feels	like	a	journey	inside	yourself	has	taken	place	and	you	arrive	to	a	new	destination	on	your	insides	.
the	feel	of	the	book	is	one	of	love	and	support	.
not	the	self-centered	type	,	or	author	as	guru	type	that	i	have	read	in	other	books-this	one	has	the	experience	of	two	professional	therapists	that	shows	itself	in	each	chapter	.
it	made	a	large	difference	in	my	struggle	with	an	eating	disorder	that	started	at	age	NUMBER	.