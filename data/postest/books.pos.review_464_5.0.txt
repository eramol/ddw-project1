stimulating	,	compelling	primer	on	the	fallacies	of	naturalistic	evolution	.
in	a	philosophical	wrestling	match	,	phillip	johnson	bends	richard	dawkins	into	a	pretzel	and	pins	his	shoulders	to	the	mat	in	three	seconds	or	less	.
macroevolutionary	darwinism	is	living	on	borrowed	time	.
within	NUMBER	years	it	will	join	the	ranks	of	geocentric	universe	theories	,	phlogistons	,	and	phrenologies	.
a	lot	of	guys	with	letters	after	their	names	are	going	to	have	to	look	for	new	work	,	or	else	be	honest	enough	to	convert	from	their	naturalistic	religion	to	a	real	form	of	science	,	that	pursues	truth	no	matter	where	it	leads