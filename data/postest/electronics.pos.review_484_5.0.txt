the	sennheiser	hd600s	have	only	one	thing	wrong	with	them	:	they	will	change	you	from	someone	who	enjoys	good	audio	DOTS	into	an	audiophile	!
i	've	been	known	as	a	videophile	for	many	years	over	on	one	of	the	top	av	forums	,	but	i	've	always	been	satisfied	with	merely	decent	audio	.
until	now	.
i	'd	previously	ripped	all	my	cds	into	the	(	previously	quite	impressive	)	windows	media	format	at	192k	bps	,	but	that	's	now	useless	.
with	the	phenomenal	ability	of	the	sennheiser	hd600s	to	reveal	all	the	subtle	nuances	of	your	music	,	listening	to	compressed	discs	is	like	listening	to	your	music	through	a	thick	layer	of	vaseline	.
here	we	go	again	DOTS	it	's	time	to	rip	everything	into	lossless	format	.
i	can	not	recommend	these	headphones	more	highly	-	they	are	simply	extraordinary	.
when	your	tastes	become	accustomed	to	this	level	of	quality	,	though	,	life	's	going	to	get	a	lot	more	expensive	,	as	you	try	to	update	everything	to	match	your	new	level	of	sonic	differentiation	.
still	,	this	is	a	bloody	miracle	.
my	ears	have	finally	been	opened	.