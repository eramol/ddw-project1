we	got	this	book	out	from	our	library	recently	and	within	a	week	i	had	ordered	a	copy	for	us	.
my	nearly	NUMBER	year	old	demands	poems	from	it	all	the	time	(	as	only	NUMBER	year	olds	can	demand	!	)
and	her	older	brothers	have	also	enjoyed	them	.
the	book	has	so	many	poems	that	struck	a	chord	with	us	;	just	one	small	example	:	with	recent	cold	weather	i	ended	up	sharing	dragon	smoke	(	about	day	being	so	cold	you	can	see	your	own	breath	)	with	my	oldest	son	's	class	at	school	.
i	have	also	suggested	that	our	local	pre-school	buys	a	copy	for	the	adult	library	so	that	the	poems	can	be	shared	with	the	children	when	relevant	themes	/	moments	arise	.
the	pictures	are	perfect	for	the	poems	creating	a	completely	wonderful	package	.
it	is	just	an	absolutely	superb	book	and	one	i	will	be	buying	as	a	present	for	many	other	families	.
i	think	in	a	way	the	title	is	a	bit	of	a	pity	,	(	although	completly	reasonable	)	,	as	these	poems	will	last	a	lot	longer	than	just	for	the	very	young