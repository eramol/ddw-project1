there	's	no	need	to	take	anybody	else	's	word	for	specs	on	this	sony	dvd	player	when	you	can	view	or	download	,	for	free	,	the	user	's	manual	at	http	:	//esupport.sony.com/us/perl/model-documents.pl	?	mdl=dvpns90v	&	loc=3	.
i	've	been	running	this	sony	dvp-ns90v	player	through	a	sony	widescreen	hdtv	and	a	pioneer	amplifier/receiver	for	several	days	now	,	and	i	find	almost	none	of	the	problems	and	limitations	mentioned	by	previous	reviewers	.
one	,	for	example	,	states	that	there	is	no	bass	management	,	yet	you	can	(	a	)	route	more	or	less	signal	to	the	subwoofer	by	selecting	small	or	large	for	the	front	speaker	size	(	see	page	NUMBER	of	the	user	's	manual	)	,	and	also	(	b	)	increase	or	decrease	the	bass	from	-15	db	to	+5	db	(	see	page	NUMBER	of	the	manual	)	.
whether	you	prefer	analog	to	digital	audio	,	or	you	think	sacd	's	are	tops	,	is	a	matter	of	opinion	,	taste	and	choice	and	not	an	objective	evaluation	of	a	playback	unit	.
i	,	for	one	,	prefer	dvd-audio	,	and	i	mention	this	for	an	important	reason	:	some	product	ads	and	the	user	's	manual	clearly	warn	that	this	unit	will	not	play	dvd-audio	discs	.
happy	surprize	:	it	most	certainly	will	!
for	my	dvd-a	's	that	have	optional	dolby	NUMBER	and/or	dts	tracks	,	the	player	simply	ignores	the	primary	dvd-a	format	(	normally	output	via	the	NUMBER	analog	cable	connectors	)	but	still	plays	the	dolby	or	the	dts	via	the	digital	audio	(	optical	or	coaxial	)	connectors	.
for	pure	dvd-a	's	(	without	dolby	NUMBER	or	dts	tracks	)	,	it	conveniently	outputs	the	audio	both	via	the	NUMBER	analog	and	the	digital	connectors	.
and	in	all	cases	above	,	the	audio	is	full	multi-channel	surround	,	and	not	downmixed	to	2-channel	pcm	,	nor	prologic	pseudo-surround	.
the	only	dvd-a	function	it	lacks	is	the	video	slideshow	often	included	on	dvd-a	's	,	and	so	displays	only	the	jacket	image	onscreen	.
the	quick	setup	allows	any	dvd	newbie	to	be	up	and	running	in	a	short	time	.
and	the	custom	setup	is	so	detailed	with	so	many	fine	tweaks	and	trouble-shooters	that	it	's	bound	to	please	even	the	high-end	audio-video	geek-freaks	.
in	short	,	you	will	find	this	to	be	a	superior	universal	player	,	provided	you	read	the	user	's	manual	(	all	NUMBER	pages	of	it	!
)	,	follow	it	,	and	connect	equally	high-quality	,	compatible	equipment	capable	of	exploiting	its	many	fine	features