three	months	later	,	i	am	still	amazed	at	how	cheap	this	amazing	piece	of	hardware	was	.
granted	,	it	does	not	vibrate	,	nor	have	a	zillion	buttons	,	but	it	definitely	performs	admirably	.
it	is	comfortable	and	the	buttons	respond	well	.
the	unit	feels	well-built	,	and	connects	to	the	computer	easily	via	usb	.
and	as	a	bonus	,	it	looks	pretty	too	.