i	am	new	to	the	world	of	pilates	and	this	dvd	is	very	helpful	.
the	instructor	explains	everything	clearly	so	that	you	can	be	sure	of	doing	each	pose	correctly	.
the	great	thing	about	this	dvd	is	that	now	that	i	am	getting	comfortable	with	pilates	,	i	can	continue	using	this	video	because	it	lets	you	chose	workouts	for	beginners	,	intermediates	,	and	advanced	.
there	's	no	need	to	buy	any	additional	dvds	!
and	days	that	i	do	not	have	time	to	fit	in	a	whole	workout	,	it	also	gives	you	time	ranges	to	choose	from	.
this	video	is	worth	buying	for	those	wanting	to	try	pilates	.
i	've	recommended	it	to	my	friends	,	and	they	all	use	it	now