{	my	review	of	the	village	mentions	the	sixth	sense	more	than	it	does	the	village	,	so	here	it	is	.
}	i	do	not	really	want	to	talk	a	lot	about	this	movie	because	i	could	spoil	the	effect	.
instead	,	i	will	talk	about	this	filmmaker	's	first	movie	.
the	sixth	sense	,	also	by	m.	night	shyamalan	.
i	first	saw	it	in	december	YEAR	,	on	a	20-hour	flight	from	chicago	to	hong	kong	.
my	only	flight	out	of	the	usa	.
the	film	really	pulled	me	in	,	which	i	suspect	is	particularly	difficult	on	an	airplane	.
then	,	the	ending	.
based	on	the	article	you	just	read	,	we	can	say	that	some	viewers	were	not	surprised	.
me	,	i	was	blown	the	heck	away	and	wanted	to	see	it	again	.
years	later	i	finally	did	.
i	taught	movie	classes	,	this	is	a	film	i	chose	,	and	i	saw	it	about	a	dozen	times	,	fully	aware	of	the	ending	.
it	had	a	bit	more	going	for	it	than	that	.
my	only	complaint	is	with	people	who	think	bruce	willis	was	the	star	.
the	star	is	haley	joel	osment	!
i	may	have	missed	out	on	the	surprise	ending	because	i	was	watching	the	kid	instead	of	the	shrink	.
bruce	willis	could	probably	star	in	a	few	more	movies	after	he	died	and	we	'd	never	notice	.
but	i	digress	.
the	village	is	about	a	pilgrim-style	village	.
some	place	quaint	,	rural	,	close	knit	and	low	tech	.
nobody	from	the	village	goes	into	the	surrounding	woods	lest	they	encounter	those	we	do	not	speak	of	.
the	acting	was	flawless	and	the	scenery	most	certainly	created	a	mood	.
shyamalan	is	a	masterful	writer	,	director	and	producer	.
but	i	had	trouble	caring	about	what	happened	to	his	characters	.
that	could	've	been	my	fault	.
saturday	morning	at	NUMBER	,	jan	was	working	,	and	my	goal	for	the	day	was	to	pack	for	our	impending	move	but	i	was	too	drained	to	begin	.
the	neighbor	had	loaned	us	this	dvd	back	in	february	,	so	i	figured	watching	it	and	returning	it	to	him	was	a	good	start	.
(	the	same	bad	attitude	that	i	brought	to	the	sixth	sense	,	right	?
one	of	these	days	i	am	going	to	watch	a	shyamalan	movie	in	the	right	frame	of	mind	.	)
the	ending	.
how	would	you	like	to	be	a	filmmaker	judged	solely	on	the	ending	?
in	the	case	of	the	village	,	it	worked	for	me	.
i	thought	about	this	film	for	days	after	seeing	it	.
actually	,	i	was	trying	to	decide	what	to	write	in	this	review	.
how	about	this	?
shyamalan	is	a	filmmaker	of	bold	originality	surviving	in	a	commercialized	medium	.
that	's	a	good	thing	,	no	matter	what	your	opinion	of	each	individual	film	is	.
i	believe	some	critics	are	panning	him	,	but	i	am	not	panning	this	one	.
i	am	glad	i	saw	it	.
i	've	missed	a	few	films	between	the	sixth	sense	and	the	village	,	and	there	will	be	more	after	the	village	.
i	will	watch	them	all	.