this	is	a	very	involved	book	,	bringing	to	light	all	the	past	and	present	characters	.
i	loved	it	.
it	was	nice	to	see	the	history	of	o'connor	and	his	mentor	jack	.
i	thought	ms.	burke	pulled	this	one	off	nicely	with	the	mystery	being	spread	over	a	40+	yr	timeframe	.
nice	job	.
it	was	nice	to	see	new	characters	introduced	also	,	i	hope	to	see	them	in	future	books	.
if	you	have	not	read	this	series	,	you	could	probably	get	away	with	starting	out	with	this	book	.
i	have	to	tell	you	though	that	you	are	missing	a	lot	by	not	reading	all	of	the	other	books	in	the	irene	kelly	series	.