as	i	read	this	book	it	was	easy	to	tell	that	it	was	not	written	by	mr.	cussler	,	the	story	was	slow	and	the	plot	was	transparent	.
this	book	just	did	not	have	the	zing	of	the	older	cussler	stories	.
if	you	are	just	laying	around	with	nothing	else	to	do	it	will	give	you	something	to	read