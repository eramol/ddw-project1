the	keyboard	with	this	set	is	flimsy	and	cheap	feeling	.
worse	,	the	layout	is	nonstandard	:	they	've	messed	up	the	key	cluster	above	the	arrows	(	insert/delete/home/end/etc	.	)	.
and	the	right	hand	ctrl	is	not	below	the	right	end	of	the	shift	key	,	it	is	under	the	left	end	of	the	shift	key	,	where	one	of	the	windows	keys	(	the	context	menu	one	)	normally	is	.
they	've	taken	out	one	of	the	windows	menu	keys	,	too	.
the	wireless	features	worked	fine	(	i	've	used	it	two	days	)	and	worked	even	without	installing	their	driver	software	(	something	i	am	always	reluctant	to	do	)	.
i	think	some	of	the	extra	buttons	(	multimedia	controls	)	on	the	keyboard	do	not	work	for	me	,	because	of	this	.
i	use	the	computer	it	's	on	for	work	,	not	screwing	around	,	so	this	does	not	bother	me	.
(	i	have	it	installed	on	a	thinkpad	laptop	so	i	can	keep	the	screen	farther	from	my	eyes	,	and	my	employer	bought	the	thing	;	it	would	not	have	been	my	first	choice	.
)	i	've	given	it	two	stars	only	because	it	worked	great	,	out	of	the	box	,	with	no	driver	software	.
(	this	is	on	a	thinkpad	running	winxp	.	)
i	am	already	dreading	the	battery	usage	(	based	on	another	review	here	)	.
thankfully	,	it	's	not	my	main	keyboard