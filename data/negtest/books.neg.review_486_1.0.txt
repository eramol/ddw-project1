this	is	perhaps	the	most	disappointing	king	book	i	have	ever	read	(	and	considering	that	i	have	read	desperation	and	insomnia	that	is	saying	a	lot	)	.
the	storyline	was	okay	until	the	author	lost	me	by	inserting	himself	into	the	story	.
i	have	no	problem	with	characters	from	other	stories	popping	up	but	for	the	author	to	become	a	character	was	over	the	top	.
the	only	thing	i	found	interesting	about	king	as	a	character	was	the	diaries	that	appear	as	the	last	chapter	of	the	book	.
i	thought	this	was	a	blatant	attempt	to	market	his	other	books	by	having	them	referenced	throughout	the	book	.
i	would	guess	that	anybody	that	reaches	song	of	susannah	has	read	is	a	faithful	reader	of	king	and	does	not	need	to	be	reminded	of	all	his	previous	works	(	which	as	of	late	have	been	bad	with	the	exception	of	the	green	mile	:	hey	,	steve	,	if	i	wanted	to	read	an	infomercial	i	will	read	my	junk	mail	.
i	can	not	wait	to	read	the	last	dark	tower	book	so	i	can	be	put	out	of	my	misery