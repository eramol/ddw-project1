i	searched	amazon.com	for	a	multi-region	dvd	player	so	i	could	view	dvds	that	i	have	purchased	overseas	.
this	dvd	player	came	up	.
there	are	no	instructions	within	the	packaging	that	state	how	to	watch	non-region	NUMBER	dvds	.
i	called	the	manufacturer	and	they	told	me	how	to	rigg	it	to	play	region	NUMBER	dvds	.
also	,	the	remote	control	did	not	work	from	the	very	beginning	and	the	manufacturer	said	i	would	need	to	pay	money	to	replace	it	.
they	also	let	me	know	that	the	dvd	player	will	not	work	without	the	remote	control	(	not	compatible	with	universal	remotes	)	.
this	is	not	a	good	product