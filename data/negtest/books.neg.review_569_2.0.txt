i	found	stale	recounting	of	very	well-known	facts	about	ORDINAL	century	technologies	and	their	economics	,	with	no	insights	.
since	it	covers	the	gamut	from	plastics	to	jet	engines	to	microprocessors	,	and	it	's	only	NUMBER	pages	in	a	fairly	large	typeface	,	i	was	not	expecting	historical	depth	.
but	i	was	expecting	at	least	one	fresh	idea	.
i	bought	it	on	the	strength	of	a	much	earlier	book	by	nathan	rosenberg	(	about	technology	in	the	economy	of	the	ORDINAL	century	)	.
i	was	disappointed	.
i	get	the	feeling	the	book	is	intended	as	a	brief	survey	for	people	who	just	came	down	in	the	last	shower	-	college	freshmen	born	in	the	YEAR	.
i	will	bet	they	find	it	kinda	stodgy