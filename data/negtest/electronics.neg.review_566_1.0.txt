this	may	be	one	of	the	cheapest	routers	out	there	,	but	it	's	cheap	in	every	way	.
i	spent	hours	either	on	hold	or	talking	with	poorly	trained	overseas	tech	support	whose	expertise	consisted	of	(	barely	)	reading	a	script	.
eventually	these	computer	whizzes	determined	that	my	d-link	router	did	not	work	.
they	told	me	to	send	it	back	(	on	my	nickel	,	of	course	)	,	and	they	sent	me	another	one	,	which	also	did	not	work	,	at	least	not	with	my	computer	system	.
after	more	wasted	hours	,	i	was	referred	to	the	one	guy	at	d-link	who-supposedly-has	mastered	this	router	's	bugs	.
the	problem	?
he	never	answered	the	phone	.
and	despite	all	my	calls	to	him	and	all	my	voicemails	asking	him	either	to	call	me	at	work	or	to	set	a	time	for	us	to	talk	,	he	repeatedly	returned	my	calls	at	3:00	in	the	afternoon	,	the	time	when	most	people	who	can	afford	routers	are	working	.
finally	i	had	an	epiphany	:	life	was	too	short	to	%	*	&	#	@	with	d-link	.
i	packed	up	the	router	and	sent	it	back	to	the	ceo	,	stephen	joe	,	with	a	letter	.
then	i	went	out	and	bought	a	netgear	router	,	and	i	was	up	and	running	in	minutes	.
mr.	joe	never	responded	to	my	letter	,	but	what	else	do	you	expect	from	a	company	that	so	clearly	does	not	value	its	customers