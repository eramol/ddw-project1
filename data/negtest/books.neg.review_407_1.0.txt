this	has	been	a	story	of	an	american	soldier	whose	involvement	in	the	making	of	the	world	's	greatest	dictionary	was	singular	,	astonishing	,	memorable	and	laudable	-	and	yet	at	the	same	time	wretchedly	sad	.
-	prologue	.
well	,	that	certainly	could	have	been	the	story	.
but	what	it	was	was	a	history	of	the	dictionary	(	as	in	the	history	of	the	horse	)	with	w.c.	minor	's	contributions	finally	gotten	to	on	page	NUMBER	or	so	.
winchester	,	the	author	of	several	other	books	i	will	avoid	,	also	seems	to	suffer	from	the	common	malady	-	or	unwillingness	-	of	not	knowing	how	to	end	a	book	.
so	the	last	fifty	pages	sink	the	poor	reader	into	DOTS	the	history	of	insanity/dimentia/alzeihmer	's	,	whatever	you	want	to	call	what	minor	suffered	.
whereas	i	should	have	shed	a	tear	when	the	main	character	-	assuming	it	was	not	a	dictionary	-	was	laid	to	rest	at	age	NUMBER	,	i	was	fairly	glad	to	see	him	go	.