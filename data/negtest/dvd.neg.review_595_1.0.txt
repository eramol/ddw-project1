well	,	this	was	about	as	formulaically	politically	correct	as	a	movie	can	get	,	unfortunately	:	the	female	teenage	protagonist	(	played	by	witherspoon	,	whose	acting	powers	have	increased	dramatically	since	her	stilted	acting	here	)	acts	masculine	and	heroic	,	the	male	teenage	protagonist	acts	craven	and	flippant	.
at	one	ridiculous	point	,	the	girl	runs	around	acting	like	a	commando	,	a	veritable	one-gal	army	,	blowing	up	the	bad	guys	,	while	the	boy	hides	in	a	cave	sniveling	.
later	,	when	the	boy	tries	to	assert	himself	by	offering	to	carry	the	gal	's	backpack	as	they	cross	a	desert	,	the	gal	snaps	at	him	,	bella	abzug-style	,	and	walks	quickly	ahead	,	leaving	him	in	the	dust	.
clearly	a	modern	radical	feminist	thing	going	on	here	,	and	what	is	most	unfortunate	is	that	this	movie	was	made	for	a	young	audience	and	therefore	is	in	keeping	with	the	modern	radical	feminist	propaganda	which	is	brainwashing	young	women	into	unnaturally	behaving	like	men	.
of	course	,	in	the	real	world	,	the	result	for	such	manly	girls	,	once	they	become	women	,	is	that	men	do	not	choose	them	and	they	remain	alone	,	and	therefore	become	bitter-ironically	and	inevitably	blaming	men	,	of	course	.
in	this	movie	,	the	wimpy	teenage	guy	falls	for	the	brassy	,	masculine	gal-but	that	's	why	they	call	them	movies	.
when	are	these	post-modern	writers	,	artists	,	and	other	culture-shapers	with	an	agenda	going	to	realize	that	when	they	tout	modern	radical	feminism	,	they	are	touting	something	unnatural-men	simply	do	not	prefer	masculine	females	.
men	want	someone	to	complement	them	,	not	compete	with	them	.
men	crave	femininity	in	females	,	not	feminism	.
wow	,	is	there	ever	a	difference	in	those	two	forms	of	comportment	!
this	poor	teenage	guy	depicted	in	this	movie	,	at	least	his	character	does	progress	a	little	bit	:	by	the	end	of	the	movie	,	instead	of	craven	and	flippant	,	he	graduates	to	something	akin	to	a	metrosexual-type	with	a	wild-boy	haircut	,	if	that	is	even	a	step	upward	.
but	the	plucky	gal-she	's	still	the	one	in	charge	in	this	fictitious	world	of	upside-down	gender	relations	.
uh	,	another	problem	with	this	movie-and	there	are	many-is	when	the	kids	are	going	through	the	kalahari	desert	,	something	which	should	be	,	and	for	a	time	is	here	depicted	as	,	a	foreboding	and	life-threatening	task	.
yet	we	see	witherspoon	in	this	desert	frolicking	with	a	dog	;	and	we	see	the	boy	's	flippancy	and	joviality	emerge	in	a	totally	out-of-place	manner	,	considering	the	dire	situation	they	are	in	.
the	sense	of	urgency	which	had	been	built	up	is	totally	excreted	away	by	these	light-hearted	,	inane	scenes	.
look	,	i	dig	disney	kid	movies-more	realistic	ones	.
you	know	,	old	style	disney	movies	.
you	take	old	yeller	for	example	.
now	there	was	a	quality	kid	movie	,	and	quite	true	to	life	.
this	movie	was	not	.
it	had	a	plastic	,	spoiled-american-materialistic-teenybopper	feel	throughout	.
disney	has	gone	down	the	tubes	.
in	any	number	of	ways	,	they	've	been	subverted	by	radical	marxist	agendas	(	note	:	one	more	of	these	was	the	fact	that	an	african	bushman	was	allowed	to	evince	a	pagan	ritual	from	his	pc	pagan	religion-something	called	tapping	,	while	the	caucasians	were	of	course	not	shown	evincing	any	sign	of	their	un-pc	judeo-christian	religion-this	is	bias	,	folks	,	leftwing	,	pc	bias	,	same	as	the	aforementioned	feminist	bilge	)	.
i	am	learning	to	run	,	not	walk	,	away	from	disney	stuff	,	and	this	movie	was	further	cause	for	me	to	do	so	.