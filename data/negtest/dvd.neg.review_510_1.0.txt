george	sluizer	's	original	dutch-french	version	of	the	vanishing	(	aka	the	man	who	wanted	to	know	)	offers	one	of	european	cinema	's	most	quietly	disturbingly	anonymous	and	everyday	sociopaths	,	feeling	his	way	one	step	at	a	time	towards	murder	.
if	you	've	seen	that	version	,	you	probably	still	ca	not	get	the	final	revelation	out	of	your	head	,	but	the	film	had	plenty	more	to	offer	than	that	,	playing	with	chronology	,	subverting	the	usual	cliches	of	its	'lady	vanishes	plot	(	the	hero	wants	to	know	what	happened	to	his	missing	lover	far	more	than	he	wants	her	to	be	alive	)	and	throwing	in	some	excellent	characterization	.
i	can	only	assume	that	for	this	YEAR	us	remake	sluizer	was	so	determined	that	no-one	else	was	going	to	get	the	chance	to	ruin	his	film	when	he	was	perfectly	capable	of	doing	it	himself	,	but	few	people	could	have	anticipated	how	comprehensively	he	trashes	his	own	work	.
his	career	never	recovered	.
chief	culprit	is	an	astonishing	performance	by	jeff	bridges	that	has	been	overthought	through	in	every	detail	to	a	truly	disastrous	level	.
a	friend	who	produced	one	of	his	earliest	movies	noted	that	bridges	was	a	great	instinctive	actor	as	long	as	you	stopped	him	thinking	about	what	he	was	doing	,	and	this	film	is	the	proof	of	the	pudding	.
every	movement	is	overly	mechanical	in	its	precision	,	making	him	look	like	a	rusty	clockwork	toy	,	while	his	voice	is	a	bizarre	mixture	of	tootsie	,	latka	gravas	from	taxi	and	a	dalek	who	have	all	been	taking	elocution	lessons	from	dok-tah	e-ville	.
no	banality	of	evil	here	,	just	a	looney	walking	around	with	an	invisible	sign	over	his	head	saying	please	.
let	.
me	.
kill	.
you	.
thank	you	.
for	your	.
consideration	.
but	the	blame	really	needs	to	be	shared	out	here	.
none	of	the	performances	are	good	:	often	,	they	do	not	even	look	good	-	keifer	sutherland	looks	more	like	a	baby	hamster	than	a	distraught	man	at	his	wits	end	in	the	hurried	scenes	at	the	gas	station	,	nancy	travis	flounders	badly	and	sandra	bullock	makes	no	impression	at	all	as	the	object	of	his	obsession	.
not	that	they	are	given	any	help	by	either	director	or	writer	todd	graff	.
the	script	is	particularly	weak	.
the	chronology	has	been	altered	to	put	the	focus	firmly	on	bridges	at	the	expense	of	the	couple	at	the	opening	of	the	film	.
worse	is	the	rush	the	film	is	in	,	draining	the	life	and	character	from	each	scene	in	its	race	to	get	to	the	next	.
rather	than	the	high/low	mood	shifts	in	the	couple	's	relationship	or	the	apparently	casual	but	careful	establishing	of	the	feel	of	the	location	,	we	just	get	a	couple	of	arguments	that	give	you	the	impression	that	he	's	probably	better	off	without	her	.
as	for	the	new	and	improved	happy	ending	-	standard	woman	chased	by	nutter	in	the	woods	jeopardy	stuff	complete	with	lame	`let	's	end	on	a	joke	like	a	tv	cop	show	moment	-	best	not	go	there	DOTS	which	is	advice	that	holds	for	this	entire	trainwreck	of	a	movie	.
even	a	shockingly	bland	and	uninspired	jerry	goldsmith	score	ca	not	do	anything	for	this	one	.