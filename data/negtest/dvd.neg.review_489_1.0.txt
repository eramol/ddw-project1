this	program	was	boring	!
as	much	as	billy	tried	to	keep	the	kid	on	the	program	motivated	,	about	NUMBER	minutes	into	the	workout	they	were	showing	how	bored	they	were	,	and	of	course	my	kids	at	home	,	and	i	am	sure	any	other	kid	picked	up	on	this	,	and	soon	lost	interest	.
several	things	made	this	program	boring	:	one	,	you	ca	not	understand	a	thing	billy	is	saying	,	he	would	mumble	unclear	phrases	thoughout	the	entire	program	,	and	his	demeanor	was	very	un-natural	,	to	the	point	of	being	fake	.
he	just	looked	&	sounded	very	out	of	place	working	with	kids	.
it	came	across	like	he	&	his	crew	were	not	really	sure	how	to	interact	with	the	kids	,	making	them	appear	to	be	very	fake	.
another	boring	point	was	the	movements	,	there	was	absolutely	no	creativity	here	,	these	were	some	of	the	exact	same	movements	from	the	adult	tae-bo	videos	.
now	,	i	do	applaud	billy	for	slowing	the	movements	down	(	unlike	the	adult	tae-bo	which	goes	at	a	break	neck	pace	)	,	these	movements	are	performed	in	a	controlled	manner	.
but	,	they	are	still	adult	type	movements	,	and	extremely	boring	for	kids	.
i	would	have	liked	to	have	seen	some	creativity	with	these	movements	to	make	them	more	kid	friendly	,	and	more	exciting	!	to	be	honest	my	kids	were	turned	off	&	bored	with	this	program	after	the	first	NUMBER	minutes	.
something	billy	needs	to	realize	is	,	kids	are	smarter	than	people	give	them	credit	for	being	.
-	they	can	tell	when	someone	is	being	fake	&	not	truly	having	fun	,	and	they	also	realize	that	when	kids	on	t.v	.
are	bored	&	not	having	fun	,	then	this	is	not	something	they	want	to	do	.
kids	want	to	have	fun	,	and	want	to	watch	the	instructor	&	other	kids	having	fun	.
that	is	how	you	motivate	children