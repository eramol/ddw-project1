all	of	us	have	a	dark	side	.
unfortunately	auto	focus	focuses	on	this	small	subset	of	actor	bob	crane	's	life	and	expands	it	out	of	proportion	.
in	doing	so	the	movie	reduces	the	other	voluminous	and	interesting	aspects	of	crane	's	life	and	career	as	window	dressing	.
even	the	screenplay	is	,	at	best	,	an	uneducated	guess	at	the	evening	escapades	of	bob	crane	and	john	h.	carpernter	-	since	both	are	deceased	with	the	former	murdered	and	the	latter	the	twice-accused	murder	suspect	.
in	viewing	this	film	i	knew	that	it	would	involve	some	of	crane	's	extracurricular	activities	.
my	interest	in	the	film	was	as	a	hogan	's	heroes	fan	and	hopeful	that	the	movie	would	spend	more	time	on	the	back	stage	politics	and	production	of	the	successful	six-year	series	.
bob	crane	's	professional	credits	alone	would	make	for	a	good	film	.
auto	focus	unfortunately	revolves	around	crane	's	so-called	secret	life	and	is	little	more	than	a	product	for	the	risque	film	crowd	.
facts	are	facts	and	there	are	established	truths	about	bob	crane	's	private	life	.
auto	focus	,	however	,	should	be	approached	as	a	fictionalized	story	straight	out	of	a	grocery	store	scandal	magazine	.
you	are	better	off	spending	your	time	and	money	buying	the	dvd	releases	of	the	hogan	's	heroes	television	series	.