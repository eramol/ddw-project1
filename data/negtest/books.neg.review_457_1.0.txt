hunt	this	book	down	.
read	the	bet	(	pp	.
131-134	)	.put	this	book	down	.
leave	the	store	satisified	knowing	that	you	have	read	the	only	humorous	story	in	this	collection	.
sure	you	could	read	some	other	stories	in	the	book	,	but	they	are	so	terribly	unfunny	that	you	will	invariably	come	to	hate	yourself	and	everyone	around	you	for	being	in	a	store	that	carries	this	dreadful	book	.
the	only	way	these	stories	could	possibly	be	funny	is	if	they	were	somehow	adapted	for	a	series	of	snl	skits	,	in	which	mcsweeney	's	unfunniness	were	somehow	reversed	by	the	lens	of	snl	's	unfunniness	.
come	to	think	of	it	,	this	book	is	just	as	awful	as	snl	,	except	that	you	ca	not	change	the	channel	.
you	are	just	stuck	there	on	the	subway	wishing	you	'd	picked	up	that	am	metro	,	desperately	trying	the	read	the	wall	street	journal	(	or	even	the	daily	news	)	over	the	shoulder	of	the	person	next	to	you	.
anything	-	just	anything	-	to	avoid	the	tears	of	boredom	that	come	from	reading	this	garbage	.
so	,	if	you	like	late-model	snl	and	you	can	read	,	you	may	like	this	.
but	i	doubt	it