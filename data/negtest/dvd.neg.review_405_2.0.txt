i	was	optimistic	for	greg	kinnear	once	.
he	was	very	funny	ragging	on	soap	soup	.
he	usually	plays	the	nice-guy	second	banana	in	film	or	a	man	dying	of	aids	.
auto	focus	gave	him	a	chance	to	portray	a	YEAR	's	tv	icon	,	hogan	,	in	hogan	's	heroes	.
the	replication	of	the	original	cast	is	a	bit	eerie	.
after	all	,	these	actors	dress	up	as	german	prison	guards	.
a	chill	goes	down	my	spine	,	but	hogan	's	was	a	comedy	,	which	made	the	series	kind	of	a	curiosity	beyond	the	silly	caricatures	of	those	funny	ss	guys	.
anyhow	,	bob	crane	played	hogan	for	six	years	and	kinnear	badly	imitated	.
yes	,	i	am	sorry	to	say	,	kinnear	is	awful	as	crane	,	which	puts	a	damper	on	the	bio-flick	.
here	's	a	disc	jockey	with	semi-hip	NUMBER	's	patter	on	the	air	with	an	all	american	family	,	a	loving	wife	and	cute	kids	.
they	live	like	ozzie	and	harriet	in	the	la	burbs	,	going	to	catholic	church	on	sunday	.
crane	lands	his	big	break	,	falling	for	the	trappings	of	fame	.
most	people	fall	for	the	trappings	of	fame	.
read	the	supermarket	check	out	magazine	racks	.
so	all	of	a	sudden	,	crane	's	garage	porn	collection	is	not	enough	.
women	,	very	beautiful	,	hollywood	loose-morals	women	find	crane	and	his	buddy	william	defoe	irresistible	.
the	birth	of	the	video	camera	comes	into	play	.
the	trouble	with	the	script	or	maybe	it	's	the	cardboard	way	kinnear	acts	,	we	watch	,	but	in	the	end	,	we	do	not	care	.