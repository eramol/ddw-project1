i	commute	via	public	transportation	,	the	majority	of	that	on	a	noisy	train	.
earbuds	and	over	ear	headphones	do	not	block	out	enough	of	the	noise	to	enjoy	the	music	.
i	received	these	philips	hn110	headphones	as	a	gift	.
when	i	first	got	these	i	was	pretty	disappointed	at	how	flimsy	they	appeared	.
i	have	some	koss	over	ear	head	phones	which	are	nice	and	beefy	.
these	are	scrawny	but	on	the	other	hand	take	up	less	space	.
they	use	a	single	aaa	batt	.
using	them	on	the	train	i	was	able	to	turn	down	the	volume	on	my	music	player	a	tad	but	a	lot	of	the	noise	still	gets	through	.
i	am	not	an	expert	on	these	but	i	guess	this	noise	cancelling	works	mainly	on	lower	frequencies	.
alot	of	the	noise	on	the	train	is	higher	in	pitch	and	thus	gets	through	.
the	next	thing	i	noticed	was	that	with	a	cord	coming	out	of	both	the	left	and	right	earphone	you	are	limited	in	how	you	can	put	these	on	and	take	them	off	.
my	koss	and	sony	headphones	and	earbuds	use	the	single	cord	to	one	phone	and	the	connecting	cord	from	there	(	or	it	runs	through	the	headphone	band	)	.
i	realize	i	like	this	single	cord	feature	very	much	.
again	these	philips	hn	NUMBER	do	not	have	this	.
i	also	noticed	that	when	the	noise	cancelling	is	turned	on	and	the	wind	is	blowing	you	can	hear	the	wind	as	if	it	were	amplified	.
after	about	a	month	and	a	half	the	battery	compartment	door	latch	broke	inside	.
now	i	ca	not	open	the	battery	compartment	without	doing	some	damage	to	these	headphones	.
i	checked	philips	website	for	info	on	these	phones	but	i	could	not	find	any	.
i	called	their	customer	support	and	they	told	me	i	had	to	call	a	different	#	.
i	called	the	different	number	and	got	the	return	merchandise	authorization	#	but	was	told	i	need	to	send	the	receipt	in	.
i	got	them	as	a	gift	so	i	do	not	have	the	receipt	.
in	summary	,	their	noise	cancelling	ability	is	pretty	week	.
better	than	my	hefty	non-noise	cancelling	koss	over	ears	or	my	stethascope	like	sony	ear	buds	but	only	by	about	NUMBER	%	or	so	(	just	to	put	a	number	in	their	)	.
flimsy	design	.
cords	from	both	earphones	.
hear	the	wind	amplified	.
battery	compartment	latch	broke	after	a	month	and	a	half	and	their	warrenty	sucks