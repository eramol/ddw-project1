short	answer	:	the	microphone	volume	on	this	is	set	too	low	relative	to	the	speaker	volume	,	and	can	not	be	adjusted	independently-either	through	the	device	or	at	the	o/s	level	.
i	spoke	with	tech	support	,	and	they	said	this	was	to	keep	the	echo-cancellation	software	from	having	issues	.
so	as	a	speakerphone	,	this	device	is	useless	.
i	'd	have	given	it	one	star	,	but	the	rest	of	the	device	is	actually	very	well	designed	,	easy	to	setup	,	and	so	on	.