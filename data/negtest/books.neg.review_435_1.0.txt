i	bought	this	book	NUMBER	years	ago	-	almost	to	the	day	DOTS	i	have	the	receipt	stuck	in	the	book	DOTS	i	can	only	seem	to	force	myself	to	get	to	page	NUMBER	and	then	i	just	ca	not	stomach	anymore	and	i	put	it	down	for	NUMBER	more	years	.
maybe	this	is	a	piece	of	art	.
something	to	be	treasured	and	something	that	should	change	your	life	.
but	pleasure	reading	should	not	be	this	painful	.
i	wish	the	store	would	take	it	back	-	but	i	think	i	've	probably	had	it	for	too	long