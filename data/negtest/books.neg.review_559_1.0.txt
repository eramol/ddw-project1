this	book	is	an	excellent	example	of	how	creationists	approach	science	:	write	a	book	with	a	lot	of	bafflegab	that	sounds	scientific	but	is	actually	devoid	of	any	meaningful	content	,	and	then	finish	up	by	concluding	that	evolution	is	impossible	.
what	a	bunch	of	baloney	.
first	of	all	,	evolution	,	in	the	sense	of	common	descent	,	is	not	impossible	.
even	id-iot	superstars	michael	behe	,	stephen	meyer	,	and	jonathan	wells	have	admitted	as	much	.
(	see	a	summary	of	their	recent	testimony	in	the	kansas	biology	curriculum	hearings	in	the	evolution/creationism	forum	at	the	west	virginia	gazette	.
(	wvgazettemail	)	(	put	/forums	after	com	)	so	any	id-iot	who	thinks	all	that	bafflegab	about	irreducible	complexity	(	ic	)	and	the	nonsense	in	this	film	is	the	death	knell	of	evolution	is	simply	ignorant	.
man	evolved	from	non-human	ancestors	.
get	over	it	.
thaxton	in	particular	ought	to	read	that	summary	,	since	he	himself	was	one	of	the	witnesses	and	looked	particularly	id-iotic	.
in	any	case	,	the	empirical	data	discussed	in	this	book	is	indeed	nothing	but	bafflegab	.
talking	about	the	allegedly	empirical	foundation	for	intelligent	design	makes	it	sound	deliciously	scientific	,	especially	to	poorly	educated	id-iots	,	who	generally	are	clueless	about	real	science	;	but	even	if	the	empirical	data	is	accurate	,	it	's	nothing	but	a	facade	,	because	it	has	nothing	to	do	with	the	book	's	main	conclusion	.
for	example	,	imagine	your	neighbor	told	you	that	his	dog	could	play	beethoven	concertos	on	the	piano	.
skeptical	,	you	ask	for	proof	.
simple	,	your	neighbor	replies	.
see	,	here	's	my	dog	,	and	there	's	my	piano	.
only	a	moron	would	accept	that	as	proof	,	and	yet	id-iot	christians	are	more	than	happy	to	accept	exactly	that	kind	of	proof	from	id-iotic	films	like	this	.
the	problem	with	your	neighbor	's	proof	is	that	it	proves	only	the	existence	of	the	dog	and	the	existence	of	the	piano	.
neither	of	those	issues	,	however	,	was	the	real	cause	of	your	skepticism	,	so	the	proof	was	entirely	meaningless	.
the	critical	issue	,	of	course	,	was	not	the	mere	existence	of	the	dog	and	the	piano	,	but	rather	the	ability	of	the	dog	to	play	the	piano	,	and	your	neighbor	's	proof	was	completely	silent	on	that	issue	.
again	,	only	a	moron	would	accept	evidence	of	mere	existence	as	proof	of	piano-playing	ability	.
in	this	book	,	there	's	a	lot	of	talk	about	how	complex	certain	biological	systems	are	.
that	is	nothing	but	meaningless	bafflegab	,	because	the	mere	existence	of	complex	systems	is	not	a	disputed	issue	.
the	disputed	issue	is	how	those	systems	came	into	being	,	and	on	that	issue	the	book	's	evidence	is	completely	silent	.
this	book	was	designed	to	fool	poorly	educated	,	easily	impressed	id-iots	.
judging	from	the	five-star	reviews	,	it	seems	to	have	succeeded	.
well	,	there	's	a	sucker	born	every	minute	,	and	apparently	a	lot	of	them	are	found	in	evangelical	churches	and	seminaries	.
in	one	of	the	very	first	evolution	debates	,	thomas	huxley	accused	bishop	wilberforce	of	using	his	intellectual	and	oratorical	gifts	to	mislead	and	bamboozle	people	.
things	have	not	changed	much	since	then	.
instead	of	meaningful	evidence	,	this	book	substitutes	a	god-of-the-gaps	argument	.
gee	,	look	how	complex	it	is	.
it	must	have	been	created	by	an	intelligent	designer	.
maybe	,	just	maybe	,	it	was	god	.
wink	,	wink	.
god-of-the-gaps	arguments	are	dumb	.
only	ignorant	suckers	are	taken	in	by	them	.
so	if	any	of	those	folks	who	wrote	five-star	reviews	for	this	book	would	like	to	buy	a	nice	bridge	in	brooklyn	or	a	dog	who	can	play	beethoven	concertos	on	the	piano	,	please	give	me	a	call	.
i	will	be	happy	to	provide	evidence	!