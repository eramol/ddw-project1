i	've	had	a	set	of	these	batteries	for	a	year	or	two	now	.
the	last	few	times	i	've	taken	them	out	for	vacation	use	in	camera	,	i	've	had	the	same	problem	:	i	charge	two	sets	(	one	kodak	and	one	these	energizers	)	when	i	put	the	camera	away	.
a	few	weeks	later	,	i	go	off	on	vacation	.
the	kodak	1600mah	set	will	last	me	all	day	on	the	charge	from	a	month	or	so	before	.
the	energizer	2500mah	set	might	last	me	half	a	day	,	having	lost	most	of	its	charge	from	sitting	for	the	same	number	weeks	before	use	.
after	missing	out	on	some	pics	on	my	latest	trip	due	to	dead	batteries	,	i	'd	had	enough	.
the	energizers	are	going	in	the	bin	,	and	i	just	followed	the	good	reviews	to	buy	a	set	of	duracells	to	replace	them	.