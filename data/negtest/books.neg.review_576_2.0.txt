gail	's	writing	is	again	wonderful	in	this	in	book	,	but	i	found	the	book	disappointing	due	to	a	lack	of	closure	.
i	felt	the	book	led	you	up	to	a	particular	point	in	the	life	of	the	character	's	and	then	left	you	wondering	in	the	end	what	the	message	was	meant	to	be	.
i	almost	felt	as	if	the	writer	was	not	brave	enough	enough	to	tell	you	the	ending	to	a	tragic	,	yet	hopeful	story	.
i	can	only	compare	this	book	to	the	samurai	's	garden	since	i	have	not	read	a	third	book	by	this	author	yet	,	and	i	loved	the	samari	's	garden	!
so	i	was	surprised	that	this	book	lacked	closure	at	the	end	.
i	am	not	sorry	that	i	read	this	book	due	to	the	fact	that	i	enjoy	the	author	's	style	of	writing