i	am	very	surprised	that	this	product	has	as	high	a	rating	as	it	does	,	i	suppose	most	are	simply	glad	the	show	is	available	on	dvd	.
however	,	the	transfer	to	dvd	is	absolutely	horrible	.
on	an	opening	night	shot	on	disc	NUMBER	,	there	is	so	much	junk	on	the	film	that	one	would	think	one	was	seeing	lightning	bugs	-	inside	the	house	!
the	colors	on	these	prints	are	not	even	as	good	as	what	is	shown	on	tvland	.
this	show	might	be	NUMBER	years	old	,	but	they	can	not	be	using	the	best	prints	available	.
there	is	another	irritating	tip-off	of	the	cheapness	of	this	package	-	the	music	on	the	closing	credits	is	wrong	-	or	is	this	my	imagination	?
for	those	who	are	defending	this	package	,	the	upcoming	daniel	boone	dvd	set	is	a	good	item	to	compare	it	with	.
that	show	aired	in	YEAR	and	the	remastered	print	is	fantastic	.
studios	and	re-issue	companies	can	do	a	good	job	when	they	choose	to	.
it	's	a	real	shame	that	,	in	this	age	of	big-screen	tvs	,	this	program	will	look	so	shoddy	.
it	's	apparent	that	with	such	a	popular	show	,	they	could	have	recouped	the	investment