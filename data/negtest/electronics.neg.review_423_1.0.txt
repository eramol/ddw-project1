pros*	fits	like	a	glove	(	goes	without	saying	)	*	um	DOTS	peace	of	mind	?	cons*	this	case	makes	pushing	the	buttons	harder	than	it	already	is	.
*	there	is	nothing	to	protect	the	screen	.
*	if	you	are	left-handed	(	or	like	to	operate	gadgets	with	your	left	hand	)	note	that	it	is	impossible	to	push	the	menu	button	.
it	would	take	a	double-jointed	freak	of	nature	to	comfortably	push	it	.
*	the	case	is	a	dust	and	hair	magnet	.
every	time	you	pull	it	out	of	your	pocket	,	it	's	encrusted	with	lint	you	did	not	even	know	you	had	.
dust	manages	to	sneak	under	every	surface	,	and	hair	magically	sticks	to	it	.
this	case	is	a	dna-sample	paradise	for	forensic	experts	.
one	of	the	features	of	a	flash-based	memory	player	is	the	fact	that	they	wo	not	break	as	easily	when	you	drop	them	.
unless	you	are	paranoid	about	getting	scratches	on	your	shiny	new	sansa	,	do	not	buy	this