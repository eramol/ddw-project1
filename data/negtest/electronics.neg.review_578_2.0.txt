i	got	these	batteries	just	over	a	few	months	ago	for	my	portable	gps	unit	that	i	use	often	on	my	travels	.
in	fact	,	i	bought	eight	of	them	(	two	packs	of	NUMBER	)	.
i	was	upgrading	from	my	previous	set	of	1600mah	rayovac	nimh	aa	batteries	that	were	still	going	strong	after	NUMBER	years	.
when	i	saw	the	new	energizer	2500mah	rechargeables	i	was	excited	to	purchase	them	thinking	they	would	last	longer	than	my	old	1600mah	rayovacs	.
boy	was	i	wrong	!	for	2-3	months	,	the	2500mah	energizers	worked	well	.
a	charge	lasted	about	6-7	hours	in	my	portable	gps	unit	.
i	was	getting	roughly	5.5-6	hours	on	my	old	1600mah	rayovac	aa	's	.
my	gps	unit	is	spec'ed	for	NUMBER	hours	on	a	pair	of	aa	's	so	the	new	2500mah	energizers	seemed	to	work	flawlessly	.
now	they	work	okay	and	seem	to	be	getting	worse	each	time	i	charge	them	,	appearing	to	hold	less	of	a	charge	each	time	.
for	the	last	month	or	two	(	ORDINAL	&	ORDINAL	month	after	their	initial	purchase	)	i	will	be	lucky	if	i	can	get	4-5	hours	of	use	out	of	them	in	my	portable	gps	unit	.
i	now	get	roughly	3.5-4	hours	of	use	out	of	them	.
i	pulled	out	and	tried	my	old	1600mah	rayovac	aa	's	and	once	again	got	approximately	5.5-6	hours	of	use	out	of	them	in	my	gps	unit	-	and	my	trusty	rayovacs	are	just	over	NUMBER	years	old	!	i	thought	i	might	have	had	some	defective	energizers	but	i	ca	not	seem	to	get	any	pair	of	combinations	to	hold	a	decent	charge	.
charges	just	do	not	seem	to	last	-	it	's	really	disappointing	.
i	am	going	to	look	at	another	brand	for	high	capacity	2500+	mah	nimh	batteries	to	replace	my	1600mah	rayovacs	.
would	be	great	if	the	energizer	could	hold	its	charge	.
but	because	they	do	not	,	i	do	not	recommend	the	energizers	to	anyone