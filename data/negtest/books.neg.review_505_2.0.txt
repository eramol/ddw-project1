the	author	had	all	the	elements	for	a	good	mystery/suspense	except	for	one	thing	,	the	lack	of	an	end	or	resolution	to	the	mystery	.
i	find	it	difficult	to	believe	the	author	of	such	noted	works	as	bag	of	bones	and	the	stand	would	leave	his	readers	hanging	in	such	a	manner	.
fiction	should	be	about	conflict	and	resolution	and	we	have	the	basis	for	the	conflict	.
a	body	turns	up	on	the	beach	and	the	only	clue	comes	from	a	pack	of	cigarettes	found	on	the	body	.
we	learn	the	man	was	from	colorado	but	very	little	else	.
if	the	author	had	brought	the	story	to	a	meaning	conclusion	it	might	well	have	been	a	classic	mystery	or	crime	thriller	but	unfortunately	the	reader	is	left	to	wonder	what	really	happened	.
i	would	recommend	a	mary	higgins	clark	mystery	in	place	of	this	boo