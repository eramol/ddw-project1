if	you	are	going	to	write	about	the	city	that	is	paris	,	you	'd	better	be	up	to	the	task	.
gopnik	may	have	the	writing	and	observation	skills	that	would	have	served	this	assignment	well	,	but	this	collection	of	essays	generally	falls	flat	.
for	someone	who	resided	in	the	city	of	light	for	NUMBER	whole	years	,	gopnik	gives	us	a	surprisingly	myopic	picture	constrained	by	his	own	self-absorption	.
those	who	are	interested	in	paris	and	its	natives	will	be	sorely	disappointed	in	the	smug	chronicles	of	a	new	yorker	who	regards	himself	and	his	son	to	be	the	most	interesting	elements	of	this	city	.
i	have	zero	interest	in	my-toddler-does-paris	stories	and	gopnik	's	relentless	insertion	of	his	look-how-cute-my-kid-is	accounts	should	have	been	saved	for	a	family	re-union	instead	of	wasting	precious	space	that	could	have	been	used	for	more	interesting	topics	.
for	a	better	book	on	paris	cuisine	and	culture	,	check	out	the	collection	of	gourmet	essays	from	the	last	NUMBER	years	titled	'remembrance	of	things	paris	.
it	provides	a	wider	variety	of	perspectives	and	a	more	expansive	and	interesting	view	of	paris	-	and	it	's	blessedly	free	of	all	that	baby's-scrapbook	stuff	.