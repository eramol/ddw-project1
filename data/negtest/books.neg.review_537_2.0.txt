the	book	's	appearance	and	story	line	are	both	lighthearted	and	gentle	with	the	underlying	message	that	acceptance	is	the	highest	of	all	virtues	.
this	approach	will	probably	work	for	a	typical	NUMBER	year	old	child	in	a	lesbian	led	household	-	especially	a	girl	child	.
if	the	child	is	a	boy	and/or	approachng	NUMBER	or	NUMBER	years	old	,	the	hard	questions	start	coming	and	this	book	pretends	these	questions	will	never	be	asked	.
does	not	everyone	have	a	father	,	somewhere	?
who	is	my	father	?
what	is	he	like	?
where	is	he	?
does	he	know	about	me	?
does	he	care	about	me	?
why	do	not	i	know	?
why	do	not	you	know	,	mommy	?	mommy	,	when	you	grew	up	you	lived	with	your	daddy	,	why	do	not	i	?	when	i	grow	up	,	should	i	live	with	a	man	or	a	woman	?
why	?
how/when	will	i	know	?
does	it	matter	?
is	our	family	really	a	complete	family	if	my	father	is	unknown	?	i	want	to	be	a	father	some	day	.
how	do	i	learn	what	good	fathers	do	?	i	love	my	friends	and	my	dog	;	does	that	make	us	a	family	?	think	about	it	.