i	bought	this	printer	because	i	wanted	high	quality	photo	printing	and	i	did	not	need	any	of	the	bells	and	whistles	that	come	with	more	expensive	printers	.
it	's	a	canon	,	so	i	figured	i	could	count	on	the	photo	quality	.
i	was	terribly	disappointed	though	.
i	generally	buy	staples	brand	photo	paper	for	general	photo	printing	,	and	it	always	worked	fine	with	my	epson	printer	.
but	my	first	prints	on	this	printer	looked	dull	and	muddy	-	like	i	was	looking	at	the	images	though	a	mild	but	annoying	grey/brown	haze	.
so	i	went	out	and	bought	canon	paper	instead	.
the	quality	was	somewhat	improved	,	but	still	not	what	i	had	expected	.
so	yes	,	i	got	a	printer	for	under	$	NUMBER	,	but	now	to	get	even	passably	ok	prints	,	i	need	to	spend	a	fortune	on	canon	brand	inks	and	paper	.
not	much	of	a	bargain	.