absolute	dog	poop	-	with	corn	.
so	there	is	this	joke	that	all	the	comedians	know	about	,	and	there	are	a	million	ways	to	tell	it	,	and	they	get	really	raunchy	.
there	,	that	's	the	movie	.
i	saved	you	two	hours	.
oh	but	there	's	a	kicker	,	the	joke	is	not	funny	and	everyone	knows	it	.
yet	,	of	course	,	some	of	the	comedians	tell	it	like	it	's	the	funniest	thing	in	the	world	-	sarah	silverman	,	gilbert	godfried	,	bob	sagat	,	and	south	park	are	definite	highlights	.
and	i	love	sarah	silverman	.
however	,	the	awful	cinematography	,	horrific	editing	,	and	terrible	production	value	of	this	film	make	impossible	for	me	to	fathom	why	people	did	not	burn	down	the	theaters	when	they	realized	this	film	was	not	getting	any	better	.
i	would	have	at	least	demanded	a	refund	,	and	$	100/hr	billed	in	NUMBER	minute	increments	.
i	think	sarah	silverman	saved	the	day	again