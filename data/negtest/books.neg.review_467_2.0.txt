when	i	traveled	in	japan	with	a	friend	,	this	was	the	book	we	brought	with	us	.
within	a	week	we	were	at	a	foriegn	language	bookstore	in	tokyo	looking	for	a	better	one	.
there	are	some	things	this	guide	does	quite	well	-	it	helped	us	out	finding	reasonably	priced	places	to	stay	throughout	the	country	,	and	the	advice	to	get	a	jr	pass	was	invaluable	.
however	,	the	book	has	a	very	negative	,	immature	view	of	japan	as	a	country	and	travel	in	general	.
it	seems	to	be	written	by	bitter	expatriates	with	a	fascination	with	gaijin	bars	and	the	sex	trade	.
did	i	pay	$	YEAR	for	plane	tickets	to	go	drink	with	americans	?
the	authors	express	a	bias	against	culturally	interesting	sites	,	ancient	and	modern	,	that	borders	on	the	anti-intellectual	.
the	cultural	notes	are	dated	,	inaccurate	,	and	shallow	.
after	a	while	,	we	began	to	think	,	if	lonely	planet	does	not	recommend	it	,	it	's	probably	interesting	.
the	maps	,	as	many	have	mentioned	,	are	almost	useless	.
ironically	for	a	series	which	fetishizes	getting	off	the	beaten	path	,	it	's	practically	useless	once	you	get	out	of	the	main	tourist	areas	.
there	are	much	better	guides	available	.
this	entry	has	sworn	me	off	the	lp	series	for	life	.