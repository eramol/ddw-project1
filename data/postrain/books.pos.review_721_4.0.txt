first	off	,	i	have	to	admit	:	i	am	NUMBER	years	old	and	read	the	entirety	of	this	book	in	one	NUMBER	hour	sitting	on	my	way	to	paris	.
i	picked	up	the	novel	out	of	sheer	curiosity	and	personal	research	for	my	job	.
i	work	at	barnes	&	noble	and	am	in	charge	of	both	the	teen	and	children	's	sections	.
when	our	store	sold	out	of	new	moon	within	a	week	of	initially	getting	the	book	,	my	curiosity	was	piqued	-	i	felt	i	had	to	give	its	predecessor	a	try	so	i	knew	what	the	hype	was	about	and	could	give	an	honest	opinion	about	the	book	.
now	here	i	sit	and	i	have	finished	both	twilight	and	new	moon	.
i	have	to	admit	-	meyer	has	a	talent	.
both	books	i	read	at	breakneck	speed	,	barely	stopping	to	eat	or	sleep	.
her	prose	is	smooth	and	easy	,	accompanied	by	a	plot	that	i	found	engrossing	.
i	went	back	after	reading	twilight	and	re-read	my	favorite	sections	more	than	once	.
the	love	story	was	hypnotizing	and	surprisingly	seductive	.
i	definitely	plan	on	recommend	this	book	,	mainly	to	teenage	girls	who	will	swoon	over	edward	.
however	,	i	only	gave	the	book	four	stars	.
while	i	enjoyed	it	immensley	as	i	read	the	novel	,	a	few	things	bothered	me	upon	looking	back	:	NUMBER	)	while	meyer	deinitely	has	a	talent	for	writing	an	engaging	novel	full	of	beautiful	descriptions	,	the	character	descriptions	at	times	felt	extremely	lazy	.
if	i	had	to	read	one	more	time	about	edward	's	marble	,	angelic	face	(	what	,	exactly	,	does	that	mean	?
)	,	his	liquid	topaz	eyes	,	or	how	bella	glared	at	edward	,	i	thought	i	was	going	to	pitch	the	book	down	the	aisle	of	the	plane.2	)	i	am	not	sure	i	buy	their	romance	.
it	was	heartstopping	and	climactic	during	the	read	,	but	honestly	,	what	does	he	see	in	her	?
if	bella	keeps	asking	that	question	to	herself	,	maybe	her	audience	shoud	seriously	consider	it	.
NUMBER	)	bella	herself	bothered	me	.
i	found	bella	to	be	horribly	dependent	on	edward	to	the	point	that	i	got	irritated	with	her	.
and	her	constantly	having	to	catch	her	breath	around	edward	-	puh-leeze	.
be	a	woman	!
stand	on	your	own	feet	!
she	never	fully	developed	into	a	character	for	me	since	she	revolved	solely	around	edward	,	something	that	i	,	as	a	self-sustained	woman	,	can	not	connect	with	.
and	lastly	,	NUMBER	)	bella	wants	so	desperately	to	become	a	vampire	,	but	that	seems	like	it	could	pose	a	problem	between	herself	and	edward	.
since	i	have	not	seen	any	evidence	that	edward	likes	bella	for	any	other	reason	than	that	she	smells	wonderful	(	a	scent	that	she	has	because	she	is	human	and	has	blood	coursing	through	her	veins	,	mind	you	)	,	what	happens	when	she	no	longer	smells	good	?
meyer	is	really	going	to	have	to	prove	to	me	that	edward	actually	loves	bella	for	other	reasons	than	her	smell	,	otherwise	it	will	be	completely	unbelievable	if	bella	turns	into	a	vampire	and	edward	is	still	attracted	to	this	ordinary	girl	DOTS	although	the	plot	has	its	shortcomings	,	i	still	thoroughly	enjoyed	it	and	that	is	why	i	give	it	four	stars	.
i	will	read	the	next	one	when	it	comes	out	,	and	any	other	subsequent	after	that	,	for	the	simple	reason	that	the	books	are	entertaining	and	edward	is	so	dang	alluring