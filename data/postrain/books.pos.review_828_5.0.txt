i	remember	my	father	giving	me	this	book	when	i	was	young	.
he	was	a	junior	high	school	english	teacher	and	used	this	book	in	class	.
together	we	read	the	stories	and	i	loved	them	.
now	i	am	a	college	professor	and	use	the	book	in	class	myself	.
while	some	of	the	summaries	are	dated	,	they	are	still	useful	in	communicating	the	basic	action	of	the	play	to	students	and	the	very	fact	that	they	are	dated	allows	the	book	to	serve	as	an	illustration	of	how	interpretations	of	shakespeare	's	plays	have	changed	since	the	lambs	time	.
i	recommend	this	book	heartily	.