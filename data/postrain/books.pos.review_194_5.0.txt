every	now	and	then	a	pivotal	moment	in	history	is	witnessed	and	recorded	by	a	master	communicator	.
the	mid-first	century	of	rome	was	such	a	time	and	tacitus	was	such	a	communicator	.
the	histories	will	forever	be	a	benchmark	of	good	history	with	its	observations	on	human	nature	and	behaviour	along	with	their	impact	on	history	.
the	historian	will	do	well	to	read	tacitus	not	just	for	the	historical	lessons	but	for	his	approach	to	history	as	a	record	of	human	activity	.
while	observing	and	commenting	on	the	human	element	in	history	,	tacitus	avoids	making	moral	judgements	and	remains	as	objective	as	possible	in	the	midst	of	turmoil	,	wars	,	and	rumors	of	wars	.
his	beloved	nation	and	people	were	suffering	under	the	barbarity	of	fratricidal	war	yet	he	remains	above	the	madness	and	records	the	events	with	passion	tempered	with	objectivity	.
his	example	is	one	that	has	remained	difficult	for	others	to	follow	.
a	word	on	this	translation	in	particular	-	i	found	mr.	wellesley	's	translation	very	readable	and	poetic	.
he	seems	to	have	captured	the	literature	value	of	the	text	as	well	as	the	content	.
well	done