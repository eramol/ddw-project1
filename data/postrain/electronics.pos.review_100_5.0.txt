i	have	been	trying	to	find	a	way	to	easily	carry	a	lot	of	camera	equipment	for	quite	some	time	.
i	have	tried	backpacks	,	shoulder	bags	,	duffel	bags	,	but	none	of	them	really	worked	all	that	well	.
then	i	saw	the	lowepro	street	and	field	system	with	completely	customizable	pouches	,	vests	,	belts	,	and	accessories	.
it	works	great	,	you	only	get	the	pouches	you	need	so	there	is	no	waisted	space	and	you	can	organize	them	however	you	want	according	to	how	often	they	are	used	(	put	frequently	used	items	in	easier	to	access	places	.	)
the	variety	alone	of	this	system	is	worth	the	investment	,	but	the	quality	and	ease	of	use	is	also	very	high	which	is	a	big	plus	.
you	can	get	a	case	for	any	lens	,	camera	body	,	or	accessory	you	have	.
i	even	have	cases	for	my	cell	phone	and	water	bottle	.
one	word	of	caution	,	be	sure	to	measure	your	lens	with	the	lens	hood	on	backwards	if	you	use	them	to	make	sure	that	the	entire	thing	will	fit	into	the	pouch	,	because	sometimes	the	lens	will	fit	,	but	then	the	lens	with	the	lens	hood	will	not	.
i	often	carry	5-6	lenses	,	extension	tubes	,	teleconverters	,	extra	memory	cards	and	batteries	,	and	a	bunch	of	cleaning	supplies	and	other	gadgets	,	which	weights	a	ton	.
but	when	they	are	evenly	distributed	on	this	vest/belt	lowepro	system	it	is	actually	very	manageable	.
the	only	down	side	is	that	everyone	will	be	staring	at	you	because	you	have	thousands	of	dollars	of	camera	equipment	strapped	to	your	body	.
but	if	you	can	overlook	that	,	this	is	the	way	to	go	.
i	have	used	this	system	at	many	zoos	without	ever	having	problems	,	in	fact	just	unzipping	a	single	lens	and	switching	seems	safer	to	me	than	opening	an	entire	bag	of	equipment	every	time	you	want	to	switch	lenses	.
many	of	the	cases	also	come	with	a	built	in	rain/snow	cover	so	if	the	weather	changes	all	of	your	equipment	is	not	destroyed	.
you	can	also	put	just	a	few	pouches	on	a	normal	belt	when	you	are	just	going	to	take	photos	in	the	back	yard	or	do	not	need	to	take	all	of	your	equipment	,	which	is	nice	.
i	really	ca	not	say	enough	about	how	much	easier	this	makes	traveling	with	a	lot	of	equipment	,	if	you	have	been	hopelessly	searching	for	a	better	solution	,	consider	this	,	you	wo	not	regret	it	.