a	comprehensive	,	entertaining	and	plainly	written	introduction	to	critical	thinking	.
it	should	be	in	every	school	and	public	library	(	preferably	next	to	the	biology	books	that	have	been	stickered	with	pro-creationist	caveats	)	.my	only	complaint	is	the	title	.
perhaps	it	should	have	been	called	the	critical	thinkers	reference	so	as	not	to	confuse	those	poor	mites	who	can	not	differentiate	between	rational	thought	and	cartesian	skepticism	.
of	course	,	if	your	world-view	is	not	based	on	rational	thinking	and	evidence	,	then	the	knowledge	contained	in	this	book	may	make	you	feel	uncomfortable	.
you	may	even	feel	threatened	to	the	point	that	you	negatively	review	it	.
guys	-	as	robert	park	once	said	:	alas	,	to	wear	the	mantle	of	galileo	it	is	not	enough	that	you	be	persecuted	by	an	unkind	establishment	,	you	must	also	be	right	.
please	read	this	book	(	or	even	read	the	faq	on	carroll	's	website	)	and	learn	how	to	reason	.