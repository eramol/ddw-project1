this	book	was	originally	conceived	of	and	designed	in	the	manner	of	an	illuminated	manuscript	.
the	images	are	combined	with	the	text	to	convey	meaning	.
in	the	paperback	most	of	the	images	are	gone	and	the	ones	remaining	are	converted	to	black	and	white	and	shoved	into	the	middle	of	the	book	where	they	lose	their	context