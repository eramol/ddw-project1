good	books	on	popular	music	are	,	frankly	,	few	and	far	between	.
this	is	one	of	those	precious	few	.
journalist	dan	leroy	has	done	a	remarkable	job	of	piecing	together	the	details	of	the	creation	of	this	album	.
even	better	,	he	has	written	an	engaging	story	.
it	might	be	a	cliche	,	but	i	could	not	put	it	down	.