lost	in	the	funhouse	can	be	a	very	bewildering	and	irritating	collection	if	you	are	not	in	the	right	mood	for	it	.
if	you	are	not	well-versed	in	post-modern	fiction	(	barthelme	,	calvino	,	etc	are	good	reference	points	)	you	might	want	to	start	somewhere	else	first	.
even	barth	's	novels	are	more	immediately	digestible	.
with	that	said	,	though	,	this	collection	does	not	really	operate	on	one	consistent	level	.
perhaps	this	is	because	many	of	these	stories	were	written	by	barth	much	earlier	in	his	career	.
the	three	stories	concerning	ambrose	's	birth	and	development	are	very	straightforward	and	enjoyable	on	a	surface	level	until	the	whole	series	goes	flying	into	left-field	with	the	titular	lost	in	the	funhouse	story	(	which	barth	is	probably	most	known	for	)	.
from	that	point	on	,	most	of	the	stories	are	more	about	the	process	of	writing	and	the	relationship	between	the	reader	,	writer	,	and	the	characters	.
stories	like	title	and	life-story	work	more	as	essays	on	the	nature	of	fiction	than	actual	works	of	fiction	,	and	were	(	for	me	at	least	)	a	little	tedious	.
the	best	moments	occur	when	barth	combines	his	thoughful	analysis	on	the	nature	of	writing	and	art	with	a	really	good	ground-situation	,	typically	based	on	greek	mythology	.
the	best	of	these	are	the	utterly	raunchy	petitition	and	the	labyrinthine	menelaiad	.taken	as	a	whole	,	though	,	lost	in	the	funhouse	is	greatly	satisfying	,	even	if	(	like	me	)	you	really	only	understood	about	NUMBER	%	of	what	barth	was	talking	about	on	your	first	read-through	.
it	's	the	sort	of	book	i	will	go	back	to	again	and	again	to	try	and	delve	deeper	into	the	mystery	of	the	funhouse	while	appreciating	all	over	the	hilarious	bawdy	humor	.
oh	,	and	make	sure	to	read	barth	's	seven	additional	notes	at	the	front	of	the	book	(	though	maybe	only	after	you	've	read	the	story	that	is	being	discussed	in	each	note	,	so	as	not	to	ruin	the	initial	experience	)	-	they	really	help	to	clarify	some	of	barth	's	intentions	.
i	ca	not	even	imagine	appreciating	a	story	like	glossolalia	without	having	read	the	note	concerning	it