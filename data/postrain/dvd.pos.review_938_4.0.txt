joseph	campbell	would	be	proud	of	this	coming	of	age	hero	story	.
haru	(	chris	farley	)	is	a	ninja	school	dropout	.
he	still	maintains	a	positive	attitude	.
he	gets	an	opportunity	to	help	a	sally	,	a	lady	in	distress	(	nicollette	sheridan	)	.
his	mission	will	eventually	take	him	to	the	hills	of	beverly	.
sensei	(	soon	tek	oh	)	is	like	a	father	to	him	.
sensei	realizing	he	can	not	sway	haru	from	his	mission	sends	another	ninja	gobei	(	robin	shou	)	who	is	haru	's	spiritual	brother	to	quietly	help	.
on	this	mission	haru	,	gobei	,	and	sensei	learn	more	about	themselves	.