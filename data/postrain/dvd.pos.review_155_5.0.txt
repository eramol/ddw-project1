watch	this	and	be	inspired	by	the	awesome	herbie	hancock	.
the	recording	sessions	are	great	.
the	music	and	the	movie	flow	along	like	a	terrific	jazz	solo	,	riffing	on	all	kinds	of	themes	but	herbie	brings	it	together	into	a	whole	.
just	watching	him	play	on	the	piano	is	a	treat	.
if	you	are	not	that	familiar	with	this	living	jazz	legend	,	check	this	out	and	you	will	admire	both	the	man	and	his	music