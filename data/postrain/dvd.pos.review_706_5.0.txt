what	can	one	add	to	what	's	already	been	written	about	the	movie	many	consider	to	be	the	greatest	film	of	all	time	?
citizen	kane	has	been	analyzed	more	than	any	other	movie	,	in	part	because	the	story	behind	the	film	is	as	interesting	as	kane	itself	.
it	's	therefore	appropriate	that	this	special	edition	also	includes	the	documentary	the	battle	over	citizen	kane	and	the	hbo	film	rko	NUMBER	.
i	am	amazed	at	how	many	people	i	encounter	who	have	watched	citizen	kane	once	and	thought	it	was	just	ok	.
but	seeing	it	once	is	simply	not	enough	.
this	is	true	of	any	great	film	,	but	more	so	for	kane	,	a	multifaceted	work	that	can	not	be	fully	digested	upon	first	viewing	.
in	many	ways	,	kane	is	the	bible	of	modern	cinema	:	you	will	find	something	new	each	time	you	watch	it	.
orson	welles	astonishing	directorial	debut	is	a	tour	de	force	of	cinematic	technique	,	narrative	innovation	and	ensemble	acting	.
the	story	of	charles	foster	kane	,	a	newspaper	tycoon	based	more	or	less	on	william	randolph	hearst	,	unfolds	in	imaginatively	designed	flashbacks	.
welles	and	cinematographer	gregg	toland	employ	chiaroscuro	lighting	and	deep	focus	photography	to	create	an	evocative	and	rich	tapestry	that	rewards	repeated	viewing	.
welles	,	as	kane	,	ages	NUMBER	years	over	the	course	of	the	film	,	aided	by	makeup	artist	maurice	seiderman	's	wizardry	.
welles	mercury	players	,	including	joseph	cotten	,	agnes	moorehead	,	ruth	warrick	,	ray	collins	,	everett	sloane	,	and	erskine	sanford	,	round	out	an	impressive	cast	.
the	expertly	produced	documentary	,	the	battle	over	citizen	kane	,	recounts	the	factors	that	nearly	prevented	welles	film	from	seeing	the	light	of	day	.
but	it	belabors	the	comparison	between	welles	and	hearst	.
both	were	certainly	ambitious	men	,	and	charles	foster	kane	is	based	as	much	on	welles	as	on	hearst	,	but	the	similarities	end	shortly	thereafter	.
by	repeating	the	popular	wisdom	that	welles	career	tanked	after	kane	,	the	documentary	glosses	over	welles	achievements	.
welles	went	on	to	make	several	great	films	,	two	of	which	(	touch	of	evil	and	chimes	at	midnight	)	are	often	cited	by	welles	scholars	as	superior	to	kane	.
while	the	documentary	overdoes	the	comparisons	between	welles	and	hearst	,	rko	NUMBER	distorts	the	relationship	between	welles	(	liev	schrieber	)	and	kane	co-writer	herman	mankiewicz	(	john	malkovich	)	.
mankiewicz	was	holed	up	with	welles	mercury	theatre	partner	john	houseman	-	not	welles	-	at	victorville	,	and	houseman	was	welles	confidant	throughout	the	kane	controversy	,	not	mankiewicz	.
if	the	film	simplifies	real-life	relationships	,	it	remains	an	enjoyable	-	if	somewhat	lightweight	-	re-enactment	of	the	controversy	,	with	great	performances	by	james	cromwell	as	the	imperious	hearst	and	melanie	griffith	as	the	ditsy	marion	davies	.
back	to	the	inevitable	question	:	is	citizen	kane	the	greatest	movie	of	all	time	?
quite	possibly	,	though	touch	of	evil	might	be	even	better	among	welles	own	films	.
but	kane	is	more	inexhaustible	,	and	more	conducive	to	repeated	viewing	and	in-depth	study	.
citizen	kane	has	topped	the	highly	regarded	sight	and	sound	polls	since	YEAR	,	and	the	american	film	institute	reinforced	kane	's	#	NUMBER	status	with	its	YEAR	top	NUMBER	list	.
can	all	the	critics	,	experts	,	and	film	buffs	all	be	wrong	?
watch	citizen	kane	-	not	once	,	but	twice	or	more	-	and	decide	for	yourself