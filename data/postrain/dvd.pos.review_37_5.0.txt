the	good	earth	must	be	considered	a	milestone	movie	.
as	yet	,	there	is	no	dvd	but	one	packed	with	out-takes	about	the	making	of	this	film	would	be	especially	welcome	.
there	was	no	oscar	given	for	outstanding	visual	effects	until	YEAR	;	otherwise	,	the	good	earth	would	have	'taken	home	that	award	!
the	lovely	luise	ranier	did	win	best	actress	award	.
the	YEAR	movie	was	based	on	pearl	buck	's	novel	that	won	the	pulitzer	prize	in	YEAR	.
interest	in	the	good	earth	was	revived	in	fall	NUMBER	when	it	was	selected	for	oprah	's	book	club	.
perhaps	women	's	rights	advocates	should	also	give	it	a	boost	?	the	story	is	about	china	in	the	YEAR	.
a	simple	farmer	,	wang	lung	,	is	'given	(	by	his	father	)	the	slave	o-lan	,	for	his	bride	.
i	do	not	remember	much	from	childhood	about	this	film	,	but	i	definitely	do	not	recall	paul	muni	having	such	a	mouthful	of	teeth	!
(	special	effecrs	?	!	)
luise	ranier	had	won	an	oscar	the	previous	year	for	her	role	in	the	ziegfield	follies	-	what	a	contrast	!
here	she	is	the	completely	docile	,	loyal	wife	;	as	someone	wrote	,	she	was	bound	to	her	husband	in	eternal	servitude	.
and	her	devotion	was	betrayed	.
partly	through	the	machinations	of	uncle	.
at	every	time	of	crisis	in	the	story	along	comes	(	groan	!	)
the	crafty	,	manipulatve	uncle	(	walter	connolly	)	who	whines	&	wheedles	-	-	inserting	humor	(	?	)
into	the	script	and	at	the	same	time	destroying	family	amity	.
from	the	internet	movie	database	are	the	prophetic	words	of	former	slave	o-lan	:	when	i	go	back	in	that	house	,	it	will	be	with	my	son	in	my	arms	.
i	will	have	a	red	coat	on	him	DOTS	and	red	flower	trousers	DOTS	and	a	hat	with	a	gilded	buddha	and	tiger-faced	shoes	,	and	i	will	go	into	the	kitchen	where	i	spent	my	days	as	a	slave	and	into	the	great	hall	where	the	old	mistress	sits	with	her	pipe	,	and	i	will	show	myself	and	my	son	to	all	of	them	.
(	smiles	,	contented	)	wang	lung	:	well	DOTS	now	,	i	DOTS
i	have	not	heard	you	speak	so	many	words	since	you	came	to	this	house	.
is	not	the	photography	impressive	for	having	been	shot	nearly	seventy	years	ago	?
i	found	the	quality	of	framed	camera	shots	surprising	&	the	production	of	many	scenes	quite	amazing	.
read	susan	wong	's	wonderfully	perceptive	review	on	amazon.com	.
today	's	movie-goers	do	not	seem	to	care	about	true	life	documentation	in	films	,	but	animation	could	not	convince	as	the	YEAR	true-life	locusts	do	!
that	tired	word	awesome	truly	fits	this	vintage	film	:	the	choking	drought	&	famine	,	the	beautiful	'condensed	story	of	harvesting	when	they	must	survive	the	fury	of	a	storm	,	and	the	terrifying	scenes	of	revolutionary	uprisings	.
reviewer	mchaiku	hails	this	film	,	and	others	that	made	an	impact	on	my	childhood	!