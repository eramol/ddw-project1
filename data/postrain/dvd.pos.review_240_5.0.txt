it	is	nice	to	see	bebe	neuwirth	in	something	other	than	cheers	.
oscar	(	aaron	stanford	)	is	too	sophisticated	for	girls	his	age	but	not	for	his	stepmother	(	sigourney	weaver	)	and	bebe	neuwirth	(	the	seductress	)	.
women	are	all	over	him	.
his	stepmom	ignores	him	and	chiropractor	thinks	she	finds	the	romantic	she	's	always	dreamed	of	.
the	joking	innuendos	are	quite	hilarious	reminding	me	of	the	graduate	.
gary	winnick	(	the	director	)	shot	this	movie	on	a	digital	camera	in	two	weeks	and	won	raves	at	the	festivals	.
the	little	captions	before	or	after	a	scene	kinda	tell	you	how	oscar	is	working	or	getting	worked	over	.
john	ritter	(	dad	,	apparently	serious	but	funny	)	and	the	whole	cast	gives	strong	performances	certainly	earning	their	street	creed	.
you	will	find	charming	nuggets	of	truth	in	this	movie	,	quite	a	bit	of	wit	,	and	hilarity	.
sure	,	oscar	is	NUMBER	and	,	rather	than	opening	his	eyes	to	girls	,	they	open	his	.
his	daydreams	about	his	stepmom	are	electrifyingly	filmed	.
as	for	the	morality	,	i	think	people	are	a	little	paranoid	over	this	taut	,	striking	film	.
if	you	are	openminded	,	you	will	find	this	film	is	more	about	coming	of	age	than	what	narrow	minded	people	think	.
it	is	a	light	film	to	the	touch	and	men	will	probably	be	reliving	high	school	fantasies	about	the	older	woman	.
women	will	like	it	because	it	shows	how	men	are	nothing	more	than	putty	in	their	hands	.
it	is	just	amazing	how	much	depth	this	film	has	in	NUMBER	minutes	!	rachel	alice	hunterchairwomanteardropfilms	entertainment	corporatio