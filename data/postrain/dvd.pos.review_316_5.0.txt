as	i	have	found	with	most	baby	einstein	movies	,	this	one	engages	my	very	active	10-month-old	baby	enough	for	her	to	sit	for	a	period	of	time	to	enjoy	.
while	i	know	that	watching	tv	too	much	is	unhealthy	,	she	will	not	even	give	regular	television	a	second	gland	(	which	i	am	glad	about	)	.
however	,	there	are	times	when	i	need	her	to	be	more	calm	like	when	i	change	a	diaper	,	try	feeding	new	foods	,	and	right	before	and	after	naps	.
these	movies	have	been	lifesavers	to	me	.
this	one	in	particular	is	nice	because	it	is	introducing	the	body	by	identifying	the	same	parts	on	babies	and	animals	.
i	love	to	give	her	a	bath	and	do	the	head	and	shoulders	,	knees	and	toes	song	with	her	and	point	to	the	different	parts	.
she	also	loves	to	stand	in	front	of	a	mirror	and	we	will	point	out	our	eyes	,	mouths	,	etc	.
this	movie	is	a	great	introduction	for	babies	and	toddlers	to	understand	their	body	parts	.