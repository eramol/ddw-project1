there	are	lots	of	reviews	here	and	most	appear	to	be	on-target	relative	to	my	experience	with	these	headphones	.
but	here	are	my	NUMBER	cents	:	i	got	these	to	replace	some	aging	sony	mdr-v2	's	that	i	purchased	maybe	NUMBER	years	ago	.
the	ear	cushions	of	the	mdr-v2	's	had	begun	to	deteriorate	,	and	i	figured	it	was	time	to	see	what	had	improved	in	NUMBER	years	.
i	just	compared	them	to	the	older	v2s	and	i	can	say	that	the	v6	is	definitely	better	.
they	fully	surround	the	ears	(	v2s	just	sat	on	your	ear	,	pressing	on	them	)	.
the	v6s	also	seem	to	offer	more	detailed	sound	.
i	concur	that	they	sound	a	little	bright	,	but	i	can	take	care	of	this	on	my	ipod	by	setting	the	eq	to	reducetreble	(	but	i	like	the	brightness	,	so	sometimes	i	do	not	do	that	)	.
i	do	not	concur	with	other	review	comments	about	weak	bass	.
they	appear	to	have	the	right	amount	.
(	imho	-	it	is	a	shame	that	bass	has	been	overemphasized	in	the	past	decade	,	at	the	expense	of	flatness	and	non-coloring	of	sound	)	.
i	also	concur	that	the	sensitivity	of	these	is	quite	good	(	plenty	loud	for	me	with	the	ipod	-	and	i	listen	at	good	levels	-	albeit	not	ear	damaging	ones	)	.as	a	last	comment	,	i	notice	amazon	has	these	for	about	$	NUMBER	.
i	paid	$	NUMBER	at	a	local	store	.
buy	from	amazon	!
(	no	-	i	do	not	work	for	or	invest	in	them	;	-