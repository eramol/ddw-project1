these	are	the	best	portable	headphones	in	this	price	range	.
good	bass	and	smooth	,	natural	sound	.
in	fact	they	would	probably	still	be	the	best	bang	for	the	buck	at	three	times	the	price	.
if	you	can	afford	more	,	consider	the	sennheiser	px100	for	about	40-50	dollars	.
but	if	you	are	on	a	budget	or	do	not	want	to	worry	about	losing	or	breaking	a	more	expensive	pair	of	headphones	,	these	are	definitely	the	ones	to	get