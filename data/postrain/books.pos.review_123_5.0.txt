i	highly	recommend	this	book	.
it	has	changed	the	way	i	look	at	life	and	spirituality	.
nearly	every	line	in	the	book	is	filled	with	profound	meaning	.
additonally	,	the	style	is	clear	and	the	ideas	accessible	for	anyone	who	is	truly	interested	in	spiritual	fulfillment	.
i	am	at	greater	peace	with	the	world	since	having	read	this	book	.