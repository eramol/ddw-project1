this	is	the	masterpiece	of	atheist	spanish	film	director	lu�s	bu�uel	.
an	unrelentless	,	devastating	,	unmerciful	criticism	of	religion	.
yes	,	it	can	be	viewed	as	a	social	satire	,	a	political	vehicle	,	etc	.
that	's	what	makes	it	a	great	film	,	that	you	can	view	it	from	different	perspectives	.
to	me	the	most	valuable	aspect	of	this	film	is	the	opportunity	to	see	,	as	in	a	wide	canvass	,	the	soul	and	mind	of	this	great	atheist	man	.
it	is	revealing	that	bu�uel	took	to	the	task	of	proving	religion	wrong	and	absurd	with	so	intense	passion	and	-i	would	even	say-	desperation	.
because	no	one	is	more	desperate	than	he	who	searches	and	ca	not	find	.
but	,	as	unamuno	would	say	to	explain	the	angst	of	the	author	,	it	comes	from	seeking	to	believe	with	the	reason	and	not	with	the	life	.
the	film	depicts	spain	's	social	condition	pretty	accurately	in	those	mid-century	years	.
unamuno	explains	this	aspect	(	though	he	wrote	decades	before	bu�uel	's	work	)	as	follows	:	in	france	and	spain	there	are	multitudes	who	have	proceeded	from	rejecting	popery	to	absolute	atheism	,	because	'the	fact	is	,	that	false	and	absurd	doctrines	,	when	exposed	,	have	a	natural	tendency	to	beget	scepticism	in	those	who	received	them	without	reflection	.
none	are	so	likely	to	believe	too	little	as	those	who	have	begun	by	believing	too	much	.
another	important	aspect	of	the	film	is	the	way	he	depicts	the	poor	.
helplessly	incorrigible	,	lacking	as	much	in	money	as	in	human	virtues	,	and	extremely	ugly	and	repealing	.
it	is	very	curious	how	bu�uel	did	not	even	allow	any	little	space	for	sentimentality	or	candidness	;	i	mean	,	there	is	no	hero	or	anything	even	close	to	it	.
it	's	a	terrible	portrait	of	the	human	soul	.
i	would	say	pessimistic	,	but	i	do	not	think	bu�uel	would	agree	.
he	would	probably	prefer	sincere	or	realistic	.
the	symbolism	is	so	evident	that	any	private	school	kid	could	figure	it	out	,	so	forcibly	paired	are	the	scenes	with	their	objects	of	criticism	.
and	trying	to	make	it	so	evident	(	and	not	as	difficult	as	other	of	his	surrealistic	films	)	evidences	his	intensity	of	feeling	,	his	anger	,	his	hatred	of	all	that	bu�uel	despises	,	religion	.
and	not	only	catholicism	-it	just	happened	to	be	spain-	but	universal	religion	.
going	back	to	the	the	poor	in	the	film	,	bu�uel	being	a	communist	sympathizer	,	it	is	ironic	that	he	does	not	show	even	a	little	mercy	with	his	poor	.
i	think	the	rich	young	man	is	a	more	admirable	figure	than	any	of	his	other	characters	.
the	female	protagonist	,	the	nun	who	gives	up	her	vows	being	used	only	as	a	tool	,	can	not	be	expected	to	represent	all	nuns	or	all	religious	persons	-it	would	be	preposterous-	.
a	case	that	in	real	life	might	happen	isolatedly	should	not	be	used	to	prove	a	general	rule	.
it	is	dishonest	.
but	there	is	a	correlation	between	his	view	of	the	economically	poor	and	the	christian	view	:	for	the	poor	you	have	with	you	always	,	but	me	you	do	not	have	always	.
john	12:8is	not	there	something	of	the	same	nature	?
so	how	can	one	conception	of	the	poor	lead	christians	to	love	them	nevertheless	(	knowing	that	they	will	always	be	around	)	,	and	the	furious	atheist/socialist	to	despise	them	(	maybe	not	to	condemn	them	,	because	surely	society	or	the	rich	would	take	the	blame	)	.
well	,	that	question	is	for	everybody	to	meditate	on	.
i	wish	people	like	bu�uel	would	give	christ	a	chance	.
unamuno	,	another	great	spaniard	,	says	:	note	the	greater	part	of	our	atheists	and	you	will	see	that	they	are	atheists	from	a	kind	of	rage	,	rage	at	not	being	able	to	believe	that	there	is	a	god	.
they	are	the	personal	enemies	of	god	.
but	-i	say-	how	can	you	be	an	enemy	of	someone	who	does	not	exist	?
however	,	this	is	a	great	and	splendid	work	of	art	.
but	now	,	ending	with	a	quote	from	rousseau	:	where	is	the	philosopher	who	would	not	willingly	deceive	mankind	for	his	own	glory	?
with	believers	he	is	an	atheist	;	with	atheists	he	would	be	a	believer	.
the	essential	thing	is	to	think	differently	from	others	.
nevertheless	,	it	moves	!