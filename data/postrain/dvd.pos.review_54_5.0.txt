a	great	behind-the-scenes	look	at	decisions	that	shaped	history	-	and	nearly	killed	all	of	us	.
cross-reference	this	with	the	more	recent	kennedy	biographies	for	a	wider	,	sobering	context	.
this	is	the	sort	of	thing	that	the	bush	administration	should	have	watched	before	deciding	to	invade	iraq	.