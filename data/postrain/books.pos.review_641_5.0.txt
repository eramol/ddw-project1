i	enjoyed	this	book	.
it	focuses	mainly	on	franklin	's	time	abroad	in	england	and	france	and	gives	an	overview	on	who	franklin	was	and	how	he	interacted	on	the	world	stage	during	the	revolutionary	period	.
i	believe	it	's	very	accessible	and	would	be	a	good	starting	place	for	anyone	interested	in	franklin	's	life	.
he	's	funny