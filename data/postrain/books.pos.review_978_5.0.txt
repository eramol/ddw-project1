dark	nature	cuts	to	the	very	core	of	the	will	to	survive	.
mr.	watson	takes	you	on	an	easy	to	read	journey	up	the	food	chain	to	the	homo	sapiens	:	who	is	neither	man	nor	kind	.
for	me	,	dark	nature	removed	judgment	from	the	word	evil	,	giving	a	deeper	,	behind	the	scene	look	at	what	drives	life	.
a	must	read	.
this	copy	was	purchased	a	gift	.
in	my	judgment	to	continue	to	grow	i	needed	to	understand	this	piece	of	myself	and	the	world	around	me