haunted	castle	and	alien	adventure	are	great	,	but	the	concept	and	dimensional	enjoyment	of	encounter	in	the	third	dimension	makes	it	better	than	the	other	two	.
the	film	is	an	excellent	way	of	showing	how	3d	started	and	how	it	works	with	lots	of	prime	examples	thrown	in	,	other	than	the	entire	movie	itself	.
i	do	wish	the	mine	ride	was	longer	though	(	the	one	which	ends	with	the	big	,	round	,	stone	ball	slamming	into	the	tunnel	)	and	there	was	more	shown	of	the	t3	experience	.
sure	,	the	jules	verne	journey	to	the	center	of	the	earth	ride	certainly	makes	up	for	it	,	but	the	effects	in	the	shorter	example	were	awesome	,	like	the	bats	.
best	of	all	,	the	film	does	not	look	all	that	dated	cgi	wise	for	a	YEAR	production	.
now	i	've	definitely	got	to	get	the	sequel	misadventures	in	3d	dvd	.
noticed	something	stupid	in	the	credits	for	encounter	.
instead	of	saying	cassandra	peterson	as	elvira	,	mistress	of	the	dark	,	it	redundantly	reads	,	elvira	as	elvira	,	mistress	of	the	dark	.
unlike	the	other	3d	imax	films	,	this	one	is	good	either	in	stereo	depth	form	or	2d	wise	.
of	course	,	the	best	is	3d	since	that	is	the	whole	point	of	the	story	anyway	.
i	highly	recommend	this	movie	to	all	3d	collectors