these	speakers	are	not	what	i	had	expected	from	their	size	,	price	,	and	looks	.
their	quality	,	even	at	high	volumes	,	exceeds	many	other	speakers	i	've	seen	at	double	the	price	tag	.
the	reason	i	bought	these	was	because	of	the	other	reviews	and	the	size	.
another	reason	was	the	proposed	battery	life	,	NUMBER	hours	on	NUMBER	aaa	batteries	,	not	bad	at	all	.
so	far	its	been	very	close	.
i	have	heard	the	other	speaker	(	systems	)	and	yes	the	jbl	onstage	and	the	bose	sounddock	are	both	very	good	and	do	live	up	to	their	reputations	,	but	your	stuck	with	using	them	only	when	there	is	an	outlet	nearby