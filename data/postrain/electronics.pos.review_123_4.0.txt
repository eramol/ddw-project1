i	will	try	to	keep	this	short	and	sweet	.
if	you	have	been	frustrated	trying	to	get	am	reception	on	the	go	,	then	you	've	found	the	right	radio	for	you	.
i	ca	not	tell	you	how	many	pieces	of	junk	i	've	bought	that	barely	pulled-in	even	relatively	close	am	stations	unless	i	constantly	twisted	the	radio	to	the	perfect	angle	needed	to	get	reception	.
luckily	(	?	)
those	other	radios	also	died	pretty	quickly	.
this	sangean	product	solves	the	am	reception	problem	and	seems	very	durable	.
i	also	own	their	ccradio	and	this	pocket	product	lives	up	to	its	reputation	.
i	love	the	fact	that	it	also	gives	you	the	option	to	broadcast	through	the	built-in	speaker	,	just	like	transistor	radios	of	the	NUMBER	's	and	60's-a	seemingly	forgotten	feature	.
as	for	listening	through	the	supplied	earphones	,	forget	it	.
sangean	should	be	ashamed	to	ship	this	product	with	the	utter	junk	earphones	that	are	supplied	.
never	have	i	heard	tinnier	sound	,	even	for	am	listening	.
not	not	mention	that	the	cord	is	annoyingly	short	.
i	immediatey	invested	another	$	NUMBER	in	decent	pair	of	koss	earbuds	and	stored	the	supplied	earphones	in	the	neareast	trash	bin	.
now	i	happily	and	easily	listen	to	am	and	fm	stations	even	over	the	roar	of	the	lawnmower	.
there	might	be	alternatives	for	those	of	you	who	plan	on	listening	to	fm	exclusivey	,	but	if	you	spend	even	a	little	time	on	the	am	dial	,	i	really	do	not	think	you	have	a	choice	but	to	pick	up	this	radio