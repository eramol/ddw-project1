the	sound	quality	is	pretty	darn	good	,	except	that	if	you	pace	around	while	talking/listening	,	there	are	these	annoying	clicking/scratching	sounds	.
they	are	quite	,	and	,	if	you	stand	still	they	go	away	.
i	've	had	other	phones	that	click/scratch	while	pacing	around	,	and	they	actually	dropped	some	of	the	sound	during	the	clicking	-	at	least	this	phone	does	not	do	that	.
my	NUMBER	mhz	dss	uniden	did	not	have	these	sounds	(	while	pacing	or	otherwise	)	,	and	still	had	greater	range	.
i	do	not	know	why	the	new	technology	ca	not	measure	up	.
still	though	,	this	is	the	best	NUMBER	i	've	tried	.
(	yet	i	'd	rather	buy	another	900mhz	dss	if	there	were	any	on	the	market	anymore	.
all	of	today	's	900mhz	are	analog	instead	of	digital	.
)	another	annoying	thing	is	that	you	ca	not	recharge	the	phone	with	the	belt	clip	attached	,	so	the	clip	goes	on	and	off	(	and	on	and	off	)	.
(	many	(	cell	)	phones	have	a	charger	that	allows	this	.
)	the	plastic	is	a	bit	cheap	.
do	not	drop	this	phone	.
my	friend	has	one	and	it	has	not	worked	since	it	was	dropped	.
(	i	have	not	tried	the	bluetooth	yet	,	and	have	yet	to	see	how	good	the	battery	life	is	.