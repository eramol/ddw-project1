this	is	not	a	book	about	management	.
it	is	a	wonderful	exploration	about	how	and	why	groups	of	individuals	can	and	do	make	better	decisions	than	individuals	.
it	sets	out	some	necessary	conditions	for	this	wisdom	to	be	able	to	take	place	and	then	does	some	excellent	analysis	of	why	that	happens	.
what	i	got	out	of	this	book	was	some	very	important	ideas	about	why	collaborative	decision	making	is	important	and	how	to	assure	that	it	is	done	well	(	if	you	want	the	other	side	irving	janis	old	classic	on	groupthink	is	a	good	counterpoint	.	)
unlike	a	lot	of	other	big	idea	books	this	one	is	very	literate	and	readable	.
it	is	a	joy	to	read	but	it	also	offers	some	very	substantive	ideas