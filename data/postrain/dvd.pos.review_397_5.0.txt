hello	vietnam	,	and	greetings	!
,	says	bruno	kirby	as	lt.	steven	hauk	in	one	scene	of	this	film	.
it	was	this	scene	where	he	filled	in	for	radio	personality	adrian	cronauer	(	robin	williams	)	who	was	on	reportedly	on	special	assignment	.
adrian	arrives	in	vietnam	from	america	via	aircraft	.
at	first	the	military	brass	was	delighted	to	have	him	grace	the	vietnam	radio	airwaves	.
but	they	discover	that	adrian	's	brand	of	humor	is	outrageous	,	very	much	like	howard	stern	in	real	life	.
they	also	disagree	with	the	rock	'n	roll	music	he	is	playing	.
hey	,	this	was	(	set	in	)	YEAR	and	rock	'n	roll	was	dominating	the	music	world	especially	with	the	beatles	and	the	rolling	stones	from	the	united	kingdom	.
when	adrian	and	lt.	hauk	first	met	,	the	lieutenant	told	adrian	that	he	was	sort	of	a	comedian	himself	.
when	the	lieutenant	first	heard	adrian	on	the	air	,	the	lieutenant	was	not	pleased	at	all	with	adrian	's	stint	.
you	oughta	stick	to	playing	'normal	modes	of	music	,	not	wild	stuff	,	lt.	hauk	told	adrian	in	a	meeting	.
the	'normal	modes	of	music	refer	to	easy	listening	artists	like	andy	williams	,	perry	como	,	mantovani	,	jim	nabors	,	lawrence	welk	,	frank	sinatra	and	percy	faith	.
would	bob	dylan	be	out	of	line	?
,	adrian	asks	lt.	hauk	and	the	lieutenant	replies	,	way	,	way	,	way	out	of	line	!	.
adrian	becomes	popular	with	the	fighting	men	in	vietnam	and	they	hoped	that	adrian	would	permanently	do	his	morning	show	there	.
the	brass	decides	to	suspend	adrian	,	bringing	heartbreak	to	fans	.
when	lt.	hauk	did	his	stint	,	many	listeners	wrote	and	telephoned	about	how	they	wanted	adrian	back	on	the	air	.
one	listener	said	in	a	letter	,	captain	hauk	sucks	the	sweat	off	a	dead	man	's	balls	.
another	wrote	to	lt.	hauk	,	you	suck	,	eat	a	bag	of	s-t	!	.
so	adrian	is	back	on	the	air	but	not	for	long	because	sgt	.
dickerson	(	the	late	j.t	.
walsh	)	,	the	top	man	of	the	brass	,	demanded	that	adrian	be	cancelled	.
sgt	.
dickerson	's	superior	loves	adrian	and	even	demanded	his	reinstatement	.
sgt	.
dickerson	is	transferred	to	guam	since	his	superior	grew	tired	of	the	seargeant	's	abusive	attitude	.
the	title	of	this	film	is	how	adrian	began	his	morning	show	.
on	the	very	last	show	done	by	adrian	's	friend	lt.	ed	garlick	(	forrest	whittaker	)	,	ed	began	the	show	saying	,	good	morning	,	vietnam	,	just	like	adrian	.
ed	tells	listeners	about	adrian	's	departure	from	vietnam	.
we	hear	adrian	saying	,	goodbye	,	vietnam	in	a	pre-recorded	message	.
adrian	also	left	vietnam	because	of	his	association	with	a	criminal	who	's	the	brother	of	a	vietnamese	woman	adrian	befriended	.
other	funny	scenes	include	adrian	teaching	the	vietnamese	how	to	use	profanity	,	adrian	doing	a	parody	of	the	late	richard	m.	nixon	on	the	air	and	adrian	doing	a	live	performance	for	a	group	of	fighting	soldiers	while	they	were	in	a	parked	truck	enroute	to	a	battlefield	.
this	film	was	a	megahit	for	both	williams	and	director	barry	levinson	.
they	were	both	at	least	nominated	for	academy	awards	.
levinson	did	win	a	best	director	oscar	for	his	next	film	,	rain	man	.
this	film	was	released	theatrically	in	late	YEAR	just	like	three	men	and	a	baby	.
both	films	were	made	by	touchstone	pictures	,	a	division	of	buena	vista	pictures	.
three	men	and	a	baby	was	a	megahit	as	well	.
both	films	were	still	in	theatres	by	spring	YEAR	.
three	men	and	a	baby	inspired	a	sequel	,	why	did	not	gmv	?
obviously	nobody	had	any	ideas	,	not	even	mitch	markowitz	who	composed	the	screenplay