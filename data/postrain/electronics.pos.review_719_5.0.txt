i	've	gone	through	a	ton	of	headphones	in	recent	years	.
they	've	included	koss	the	plug	,	sennheiser	px100	,	koss	ur29	,	koss	sportapro	and	a	few	others	.
all	had	their	strong	and	weak	points	.
so	far	,	this	has	been	the	closest	i	've	had	to	a	good	balance	.
for	my	tastes	(	not	necessarily	anybody	else	's	)	,	strong	bass	is	one	of	the	most	important	things	.
the	px100	felt	very	weak	in	this	area	,	despite	what	everybody	else	seemed	to	say	-	weaker	than	my	old	sony	mdr-nc20	(	which	are	still	going	strong	after	NUMBER	years	and	remain	my	backup	headphone	of	choice	thanks	to	their	decent	sound	and	folding	ability	)	.
i	had	problems	keeping	the	plug	sealed	in	my	ears	,	despite	the	hold	the	outer	ear	while	the	plug	seats	advice	.
they	are	also	a	very	a	bad	choice	for	ipods	because	their	very	low	impedance	overdrives	the	amplifier	circuit	.
the	sportapros	had	great	bass	,	but	leaked	way	too	much	sound	.
the	ur29	had	good	bass	and	very	good	isolation	,	but	were	a	little	uncomfortable	,	warm	,	heavy	and	too	bulky	to	use	on	the	go	.
going	back	a	few	years	,	i	had	a	pair	of	akg	k240s	that	had	disappointing	bass	.
ditto	for	a	pair	of	sennheiser	hd-420s	.
the	k26p	seems	to	combine	the	better	aspects	of	all	these	phones	.
the	bass	is	among	the	strongest	.
when	i	listen	to	the	YEAR	overture	,	the	cannon	blasts	come	through	loud	and	clear	,	even	at	moderately	low	volumes	,	with	no	distortion	at	all	.
isolation	is	not	as	good	as	the	ur29	,	but	not	too	bad	.
certainly	a	lot	more	portable	,	easy	to	stow	in	my	bag	at	all	times	.
the	px100s	fold	smaller	,	but	they	are	a	pain	in	the	neck	to	stow	in	their	hard	case	.
the	higher	efficiency	of	the	k26p	means	i	can	set	my	ipod	at	a	lower	volume	.
instead	of	55-60	%	of	max	volume	with	the	ur29	or	the	stock	earbuds	,	i	now	run	at	35-40	%	comfortably	.
in	terms	of	comfort	,	they	are	comfortable	enough	for	hours	of	wearing	,	after	a	couple	of	slight	modifications	.
if	they	press	too	hard	on	your	hears	,	first	extend	the	headband	all	the	way	,	then	gently	straighten	each	half	a	little	bit	by	gripping	a	couple	of	inches	of	steel	band	at	a	time	and	unbending	it	.
after	a	few	tries	,	it	should	be	perfectly	fitted	to	your	head	.
do	not	bend	too	hard	or	grip	the	plastic	parts	or	you	can	break	the	phones	.
be	patient	.
so	it	takes	a	couple	of	days	of	trial	and	error	,	but	in	the	end	,	you	get	good-sounding	phones	that	are	wonderfully	comfortable	.
you	'd	be	surprised	how	little	pressure	is	needed	to	produce	a	good	seal	.
you	will	know	if	you	've	unbent	too	far	because	either	the	phones	will	slide	down	or	you	lose	bass	.
here	's	an	easy	test	:	with	a	finger	on	each	side	,	press	the	earcups	harder	onto	your	ears	.
if	the	bass	does	not	get	stronger	as	you	press	(	the	way	it	does	with	all	open-air	headphones	)	,	that	's	as	good	a	seal	as	you	need	.
i	hated	the	little	,	square	foam	pads	on	the	plastic	sliders	and	quickly	peeled	them	off	.
all	they	seem	to	do	is	create	two	pressure	points	on	top	of	the	head	.
other	phones	with	similar	headbands	(	like	the	koss	sportapro/portapro	)	do	not	use	these	pads	,	so	i	do	not	think	they	are	really	necessary	.
better	to	let	the	headband	rest	directly	on	the	scalp	and	distribute	the	pressure	over	the	entire	headband	,	and	so	far	my	suspicions	seem	to	have	been	correct	.
it	has	been	far	more	comfortable	for	me	without	the	pads	,	especially	after	more	than	half	an	hour	.
in	fact	,	without	the	pads	,	i	barely	feel	the	headband	at	all	.
overall	,	i	am	satisified	,	especially	at	its	low	price	point	.
i	hope	to	use	these	for	many	years	to	come	until	either	they	break	or	something	better	comes	along	.
by	the	way	,	do	not	be	put	off	by	comments	about	the	warranty	being	invalid	in	the	us	and	canada	.
read	the	whole	warranty	sheet	!
a	separate	section	on	the	sheet	covers	a	limited	warranty	for	these	countries	,	so	yes	,	it	is	covered