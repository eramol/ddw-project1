this	book	provides	a	straight-forward	approach	to	a	raw-food	diet	,	and	the	changes	required	to	maintain	the	health	of	your	pet	through	the	process	.
it	also	helps	to	have	someone	who	can	assist	you	in	locating	the	ingredients	locally	,	but	schultze	provides	all	the	requisite	information	that	you	will	need	.
a	great	resource	to	go	to	when	you	get	a	nagging	question	,	or	have	a	concern	.
try	not	to	let	people	talk	you	out	of	trying	the	diet	,	as	you	will	see	changes	in	the	health	of	your	pet	;	many	veterinarians	that	i	have	spoken	with	are	not	particularly	supportive	.
my	NUMBER	y/o	lab	has	a	softer	coat	,	clearer	eyes	,	and	better	muscle	tone	since	i	have	started	him	on	the	diet	.
this	diet	is	slightly	more	expensive	than	feeding	dog	chow	,	but	the	reward	of	having	a	healthy	,	happy	pet	far	outweigh	the	associated	costs	.
good	luck	,	and	enjoy	your	pet