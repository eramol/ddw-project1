this	book	is	not	only	interesting	but	gives	a	great	deal	of	insight	into	life	on	death	row	,	at	least	in	the	state	of	mississippi	,	and	the	appeals	process	involved	with	stays	of	execution	and	commutation	of	sentences	.
it	touches	on	activities	of	the	ku	klux	klan	.
it	was	my	impression	that	the	author	leans	toward	those	who	favor	the	abolition	of	the	death	penalty	.
however	,	some	sympathy	is	found	for	the	victims	of	crime	.
the	story	centers	on	sam	cayhall	who	is	sentenced	to	death	after	several	trials	over	a	number	of	years	following	the	crime	.
sam	's	grandson	becomes	his	attorney	during	the	last	months	of	appeal	from	death	row	.
the	latter	part	of	the	book	leaves	one	wondering	whether	or	not	there	will	be	an	execution	.
meanwhile	sam	shows	repentance	and	response	to	the	prison	's	chaplain	.
i	found	it	to	be	one	of	the	better	john	grisham	books	.