i	am	a	lutheran	church	librarian	and	i	am	glad	i	bought	this	very	satisfactory	book	for	our	church	library	.
its	text	stresses	that	children	are	the	same	the	world	over	.
they	have	the	same	feelings	and	needs	,	and	have	the	same	hopes	and	dreams	.
the	illustrations	are	a	nice	change	from	the	usual	.
each	page	is	surrounded	by	a	gold	picture	frame	in	which	faux	jewels	are	embedded	.
the	illustrations	appear	to	be	tempera	or	opaque	watercolor	,	drawn	in	persian	style	.
the	colors	are	very	saturated	.
the	book	's	cover	gives	you	an	idea	of	what	they	look	like	.
the	message	of	this	book	is	one	of	diversity-that	even	though	children	live	in	different	parts	of	the	world	and	may	have	different	nationalities	,	races	,	ethnicities	,	languages	or	faiths	,	they	still	have	the	same	hopes	,	dreams	and	daily	needs	.
this	is	a	very	important	message	for	children	to	hear	in	today	's	world	where	there	is	so	much	suspicion	of	those	we	perceive	to	be	different	.
the	more	children	realize	that	kids	are	the	same	everywhere	,	the	more	tolerant	they	will	be	.
children	pick	up	prejudice	by	the	time	they	are	three	years	of	age	,	say	researchers	,	so	you	have	to	nip	it	in	the	bud	very	early	,	and	this	book	does	that	in	a	wonderful	way	.