in	order	to	properly	understand	the	jesus	of	the	new	testament	,	instead	of	making	a	jesus	of	our	own	liking	,	it	is	necessary	to	understand	jesus	as	he	himself	indicated	.
jesus	himself	,	according	to	the	new	testament	,	made	constant	reference	and	allusion	to	the	hebrew	scriptures	,	(	old	testament	)	,	as	he	sought	to	explain	himself	,	his	actions	,	his	teachings	and	his	significance	.
understanding	the	old	testament	is	therefore	of	paramount	importance	for	understanding	what	jesus	was	and	is	about	.
this	means	far	more	than	knowing	some	of	the	messianic	proof	texts	or	knowing	about	noahs	ark	or	the	temple	and	sacrificial	system	of	the	ancient	hebrews	.
understanding	the	old	testament	involves	knowing	the	overall	aim	and	purpose	of	it	,	and	how	it	all	is	held	together	by	connected	themes	that	form	a	unified	whole	.
christopher	wright	's	book	,	knowing	jesus	through	the	old	testament	,	is	an	absolute	godsend	towards	getting	the	drift	of	the	old	testament	and	how	it	carries	forward	to	the	jesus	of	the	new	testament	.
this	book	is	a	little	bit	technical	at	times	,	it	is	not	a	devotional	work	,	but	reading	this	book	will	educate	a	person	to	accurately	understand	what	the	old	testament	is	about	and	how	it	flows	into	the	person	of	jesus	,	thereby	expositing	the	true	meaning	and	intent	of	jesus	according	to	the	background	that	jesus	himself	referred	to	.
thanks	chris	wright	for	this	fabulous	book	.
also	see	his	forthcoming	book	,	the	mission	of	god	,	due	out	in	fall	of	NUMBER	by	intervarsity	press	.
it	looks	to	be	another	bullseye	!
for	some	other	great	little	books	on	jesus	that	are	sane	and	sober	,	see	:	jesus	and	his	world	by	peter	walker	,	the	original	jesus	by	n.t	.
wright	,	and	for	a	bit	of	a	larger	work	see	jesus	and	the	gospels	by	craig	blomberg