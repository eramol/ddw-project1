the	danny	boyle	directed	trainspotting	was	a	somber	view	with	some	comedic	overtones	into	a	group	of	scottish	heroin	addicted	losers	.
while	the	film	garnered	accolades	,	i	found	it	to	be	profoundly	disturbing	as	the	seemier	side	of	addiction	was	vividly	depicted	.
ewan	mcgregor	playing	mark	renton	and	his	crew	which	included	full	monty	star	robert	carlyle	,	as	violent	psycho	begbie	,	swing	back	and	forth	between	kicking	the	habit	and	falling	back	in	love	with	allure	of	a	scag	high	.
congregating	in	a	filthy	,	deplorable	shooting	gallery	the	gang	is	shown	in	graphic	manner	getting	high	.
as	a	group	they	are	abhorent	of	authority	and	employment	and	motivated	only	by	scoring	their	next	fix	.
mcgregor	attempts	to	kick	the	habit	by	locking	himself	into	an	apartment	with	needed	supllies	.
remembering	the	constipation	from	a	previous	attempt	he	goes	out	to	score	suppositories	.
in	a	humorous	scene	,	cramps	force	him	to	use	the	filthiest	toilet	in	scotland	into	which	he	drops	his	precious	suppositories	.
he	then	dives	into	the	disgusting	toilet	to	retrieve	them	.
mcgregor	eventually	does	clean	up	complete	with	cold	turkey	induced	horrific	hallucinations	,	escaping	to	london	and	working	as	a	realtor	.
his	buddies	follow	him	there	and	reel	him	back	into	the	life	.
mcgregor	and	carlyle	and	two	other	buddies	eventually	have	their	dreams	come	true	,	DOTS	DOTS	..the	big	drug	deal	.
they	manage	to	complete	the	deal	for	NUMBER	pounds	which	mcgregor	decides	to	steal	.
he	intends	to	use	the	loot	to	start	a	straight	and	clean	life	anew	.
fat	chance