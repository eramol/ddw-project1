this	fiction	of	a	plot	against	american	jews	is	very	interesting	.
it	is	well	written	and	it	holds	your	attention	.
the	little	boy	telling	the	story	and	the	intricacy	of	politics	attached	to	the	story	make	this	novel	a	book	you	can	not	put	aside	.
this	is	about	an	era	of	american	history	,	and	it	is	well	documented	.
i	recommend	it	to	everyone	:	the	older	generation	who	lived	thru	it	and	particularly	to	the	younger	generation	of	people	who	are	not	familiar	with	this	period	;	it	teaches	that	history	is	constantly	in	the	remake	and	cultural	differences	are	still	a	problem	in	our	world	.