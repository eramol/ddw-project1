this	is	really	sweet	adventurous	romp	with	plenty	of	action	and	humor	&	i	really	enjoyed	watching	it	.
i	think	it	has	great	replay	value	&	as	one	of	the	other	reviewers	noted	you	can	just	switch	your	brain	into	neutral	and	enjoy	the	silliness	.
i	found	it	more	reminiscent	of	the	old	NUMBER	's	films	like	'monte	carlo	or	bust	or	'those	magnificent	men	DOTS	.	.
jackie	chan	rather	than	trying	to	take	a	leading	role	delights	as	the	action-packed	foil	to	'fillius	fogg	played	with	brilliant	english	reserve	&	self-deprecating	humor	by	'steve	coogan	.
there	's	no	comparison	to	the	original	&	the	film	does	not	even	try	to	copy	.
rather	,	it	takes	a	silly	story	with	silly	character	's	puts	them	in	silly	situations	and	with	quite	considerable	imagination	and	verve	makes	one	of	the	nicest	family	action/adventure/comedy	movies	you	will	see	.
because	this	is	not	a	typical	'martial	arts	vehicle	for	'jackie	chan	i	found	that	the	fight	scenes	and	stunts	worked	a	whole	lot	better	given	the	storyline	.
check	out	for	instance	the	fight-scene	in	the	art-gallery	which	goes	to	show	why	'jackie	chan	is	probably	the	best	action/comedy	star	in	the	world	right	now	.
there	's	much	that	's	chaplain/keaton-like	in	his	performance	&	all	the	stunts	are	more	impressive	because	you	know	that	he	does	most	all	his	own	stunts	.
the	guest-stars	are	a	nice	treat	to	spot	during	the	travels	and	again	harkens	back	to	the	older	comedies	which	used	similar	devices	,	most	notably	'it	's	a	mad	,	mad	,	mad	,	mad	world	(	what	i	think	is	the	best	comedy	ever	&	this	is	by	no	means	a	comparison	)	.
anyway	'arnold	swarzenneger	in	his	awesomely	bad	wig	was	a	hoot	as	a	turkish	prince	no	less	(	possibly	the	poorest	arabian	impression	ever	,	which	is	of	course	why	it	works	so	well	)	and	'rob	schneider	has	the	funniest	guest	spot	as	the	smellier	than	smelly	bum	with	poor	career	advice	.
miss	'cecile	de	france	is	a	beautiful	comedy	foil	&	manages	to	overcome	a	limited	role	to	portray	a	luminous	comic	talent	&	i	hope	to	see	more	from	her	.
you	might	expect	the	comedy	in	the	film	to	be	crude	or	obvious	but	i	found	a	lot	of	it	to	be	nicely	restrained	,	this	was	in	part	due	to	great	casting	and	what	appeared	to	be	great	chemistry	on	set	,	check	out	the	extras	.
all	in	all	a	wonderfully	attractive	and	relaxing	family	comedy/action/adventure	with	ne'er	a	slow	moment	and	a	well	paced	story	that	however	familiar	still	manages	to	please	on	repeated	viewing	.