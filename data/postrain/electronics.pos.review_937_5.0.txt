i	tried	both	microsoft	's	lifecam	vx-3000	and	vx-6000	.
had	'em	both	home	and	ran	trials	with	my	intel	pentium	d	dual	core	3.00ghz	cpu	with	NUMBER	gigs	of	ram	and	,	with	both	of	those	,	the	video	lagged	the	audio	(	distractingly	so	)	.
spent	the	better	part	of	an	hour	on	the	phone	with	a	friendly	microsoft	customer	service	rep	one	evening	(	got	through	on	their	toll	free	line	)	and	he	pointed	me	to	new	software	for	the	vx-6000	.
downloaded	the	NUMBER	meg	plus	file	,	installed	it	,	but	the	vx-6000	still	produced	out-of-sync	video	vs.	audio	when	i	created	a	simple	NUMBER	by	NUMBER	video	file	for	viewing	on	media	player	or	editing	in	windows	movie	maker	.
so	,	i	talked	to	some	technicians	at	our	company	and	then	talked	to	the	webcam	guru	at	the	local	assembler	that	built	my	,	i	believe	,	excellent	computer	.
all	recommended	logitech	when	buying	webcams	.
i	then	marched	back	to	best	buy	and	traded	my	$	NUMBER	microsoft	vx-6000	for	a	logitech	quickcam	orbit	mp	for	bb	's	price	of	$	NUMBER	.
you	can	find	this	logitech	unit	cheaper	all	over	the	place	,	including	,	of	course	,	here	at	amazon	.
the	package	i	bought	included	an	inexpensive	headset	,	which	i	have	not	used	so	far	.
although	i	have	not	tried	the	orbit	in	a	video	phone	call	yet	,	from	the	moment	i	turned	it	on	i	had	that	satisfing	good	feeling	that	i	bought	a	winner	.
installed	fairly	easily	with	a	couple	of	bumps	.
got	a	usb	composite	device	has	not	passed	windows	logo	testing	screen	a	couple	of	times..but..i	received	the	same	screen	on	,	get	this	,	the	microsoft	cams	!
i	just	continued	installing	and	have	not	experienced	system	problems	so	far	.
the	audio	sync	with	video	in	making	a	recording	is	right	on	(	remember	,	that	was	primary	reason	for	taking	back	the	microsoft	cameras	)	and	the	picture	clarity	on	my	lcd	flat	panel	monitor	at	the	NUMBER	webcam	capture	setting	produced	a	gorgeous	picture	.
excellent	detail	and	smoothness	of	video	.
besides	the	superior	audio	sync	with	video	,	the	orbit	mp	is	remarkably	better	than	the	vx-6000	and	way	above	the	$	NUMBER	microsoft	vx-3000	in	all	other	criteria	too	,	in	my	opinion	after	comparison	testing	.
the	orbit	's	special	effects	are	interesting	and	fun	.
the	pan	,	tilt	,	etc.	,	excellent	.
the	face	tracking	is	vigorous	and	alive	-	fun	to	watch	the	little	orbit	camera	turn	-	but	i	have	not	tested	in	actual	live	conferencing	yet	.
all	in	all	-	for	webcams	,	i	am	now	saying	what	more	experienced	webcam	users	told	me	-	head	for	logitech	.
they	've	certianly	hooked	me	with	their	apparent	superior	performance	.
the	orbit	mp	is	a	keeper	!
not	going	to	waste	anymore	time	searching	and	testing	.
and	,	my	advice	to	microsoft	,	you	'd	better	talk	to	the	outfit	that	bought	your	name	for	those	lifecams	.
i	've	relied	on	the	microsoft	name	on	peripherals	,	keyboards	,	etc.	,	and	i	've	generally	been	very	satisfied	.
but	,	in	webcams	,	with	me	,	the	microsoft	brand	now	takes	a	back	seat	to	logitech	.