unlike	marshall	fine	,	who	wrote	the	amazon	review	,	i	do	not	find	the	subplot	in	this	movie	hard	to	swallow	at	all	!
sadly	,	politically	i	believe	many	of	our	fine	military	personnel	have	been	sacrificed	through	the	years	for	political	agenda	,	and	i	am	not	just	talking	about	recently	.
i	am	sure	there	are	flaws	in	the	movie	,	but	i	was	not	watching	the	movie	for	flaws	.
truthfully	,	military	movies	are	not	my	favorite	genre	.
having	said	that	,	i	found	myself	up	very	late	one	night	and	happened	upon	this	fine	movie	.
i	was	mesmerized	.
tommy	lee	jones	and	samuel	l.	jackson	were	brilliant	,	but	it	was	the	short	segment	with	the	former	officer	in	the	north	vietnamese	army	that	riveted	my	attention	more	than	any	other	scene	.
just	brilliant	!
i	ca	not	say	more	or	i	will	spoil	it	for	anyone	who	has	not	seen	it	.
needless	to	say	,	i	give	it	NUMBER	stars	because	it	is	a	movie	that	made	me	think	,	one	i	will	talk	about	in	years	to	come	.
that	's	a	barometer	for	my	star-meter	!