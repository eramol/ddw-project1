i	just	finished	reading	the	handyman	's	dream	,	by	nick	poff	.
i	was	completely	blown	away	!
this	is	a	great	book	.
its	about	NUMBER	guys	who	meet	one	another	in	small	town	indiana	.
after	the	first	chapter	you	will	want	to	have	these	fellas	over	for	dinner	.
the	story	is	very	well	written	and	has	alot	of	innovative	things	in	it	that	i	havent	seen	in	any	other	gay	novels	.
i	love	the	way	the	author	integrates	music	and	songs	into	the	story	.
from	the	character	norma	(	ed	's	mother	)	and	her	quirky	comebacks	to	the	wonderful	mrs.	penfield	,	the	novel	will	have	you	laughing	and	will	touch	a	place	deep	inside	you	that	few	books	do	.
this	story	also	tells	of	the	fears	and	struggles	a	gay	couple	had	in	the	past	,	and	one	many	still	have	today	.
i	love	the	fact	that	its	very	g	rated	and	a	good	read	for	not	only	gay	folks	but	also	straight	folks	DOTS	anyone	who	believes	in	the	power	of	love	.
i	wish	while	growing	up	i	would	have	had	a	book	like	this	one	to	show	me	that	there	is	such	a	thing	as	good	,	healthy	gay	relationships	.
i	highly	recommend	the	handyman	's	dream	.