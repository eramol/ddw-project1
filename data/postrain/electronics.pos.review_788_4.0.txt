as	you	may	already	know	,	this	is	an	accessory	handset	which	will	only	work	if	you	have	one	of	these	panasonic	models	:	kx-tg2700/kx-tg2720	,	kx-tg2730/kx-tg2740	.
the	panasonic	kx-tga271v	,	does	not	need	(	or	use	)	a	telephone	jack	.
it	simply	plugs	into	any	standard	electrical	socket	and	works	off	the	base	unit	with	one	of	the	models	mentioned	above	.
this	phone	was	also	very	easy	to	register	.
(	it	had	to	be	charged	for	about	NUMBER	hours	before	it	could	be	registered	DOTS	)	i	simply	had	to	press	the	mute	button	on	my	base	unit	and	then	press	the	play	button	on	the	phone	(	the	registration	directions	were	even	displayed	on	the	lcd	display	on	the	phone	!	)
please	know	that	i	did	find	the	panasonic	instaltion	manual/registration	directions	to	be	a	little	confusing	.
some	features	that	are	not	mentioned	in	this	description	which	are	included	are	:	finger	grips/ergonomic	sizing	on	the	sides	of	the	phone	,	the	opportunity	to	play	your	answering	machine	messages	off	this	phone	(	the	base	unit	to	the	kx-tg2730	has	a	digital	answering	machine	,	and	i	suspect	the	other	panasonic	models	do	as	well	)	-	this	was	the	main	reason	why	i	purchased	this	phone	,	yes	the	kx-tga271v	will	play	your	answering	machine	messages	off	this	phone	through	the	speaker	on	the	back	of	it	!	,	a	separate	call	waiting	dedicated	button	is	also	included	.
also	,	this	phone	will	use	any	standard	headset	,	it	does	not	need	a	panasonic	branded	headset	.
this	was	very	important	for	me	because	i	ofen	use	a	headset	and	i	do	not	want	to	purchase	another	one	.
i	have	since	learned	that	all	cordless	phone	headsets	can	be	used	on	any	type	of	cordless	phone	!
please	know	that	the	batteries	on	this	phone	are	not	interchangeable	with	the	kx-tg2730	system	(	that	is	the	model	that	i	have	.	)
also	,	the	kx-tg2730	phone	ca	not	be	charged	in	this	charger	,	and	vice	versa	.
that	is	(	likely	)	because	the	kx-tga271v	phone	is	slightly	smaller	and	weighs	less	.
the	face	of	the	phone	is	white	with	a	purple	trim	.
the	back	of	the	phone	is	also	purple	on	the	bottom	and	white	on	the	top	half	.
the	white	in	the	phone	is	sort	of	a	shiny	metalic	white	,	and	the	purple	is	not	shiny	at	all	.
some	of	the	buttons	are	also	slightly	smaller	than	the	buttons	on	the	kx-tg2730	model	.
and	the	lcd	screen	is	not	flat	,	it	is	indented	slightly	,	and	bordered	in	silver	.
i	purchased	this	phone	from	an	amazon.com	merchant	and	am	very	pleased	with	it	.
i	noticed	that	another	amazon.com	merchant	is	selling	the	same	phone	that	i	purchased	for	less	(	$	NUMBER	.
)	,	however	that	merchant	has	horrific	feedback	and	i	was	only	too	glad	to	pay	a	little	extra	for	the	confidence	of	knowing	that	i	would	receive	a	phone	in	a	timely	manner	that	would	work	!
panasonic	makes	very	good	phones	.
the	only	(	big	)	problem	is	the	battery	life	.
the	batteries	often	die	after	less	than	NUMBER	minutes	.
i	have	solved	this	problem	by	purchasing	a	second	set	of	batteries	.
however	,	this	is	still	not	a	perfect	solution	.
so	,	i	had	to	purchase	another	set	of	cordless	phones	(	uniden	.	)
i	now	keep	two	phones	in	each	of	the	rooms	that	i	had	each	of	the	panasonic	phones	,	and	so	far	have	not	run	in	to	any	problems	.
i	hope	panasonic	will	offer	batteries	with	longer	lives	soon	.
all	in	all	this	is	an	excellent	phone	.
i	hope	the	extra	details	that	i	provided	will	help	a	prospective	buyer	make	the	right	decision	.
amazon.com	rarely	provides	specific	details	to	these	types	of	questions	(	does	amazon.com	only	employ	braindead	dimwits	?	)
and	the	panasonic	website	does	not	offer	this	info