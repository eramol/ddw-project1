just	a	note	to	future	readers	-	if	you	take	gemstar	up	on	their	offer	to	upgrade	this	model	you	will	receive	some	nice	new	features	,	however	what	they	do	not	tell	you	is	that	your	machine	will	become	proprietary	!
all	the	books	you	had	before	will	become	unreadable	and	your	ebook	will	only	be	able	to	read	gemstar	books	in	the	future	.
this	means	you	are	force	to	buy	all	future	books	from	the	gemstar	catalog	only	!
the	new	features	that	come	with	this	upgrade	to	the	YEAR	model	are	hardly	worth	being	locked	into	one	ebook	vendor	forever	.
the	openness	of	this	little	model	is	it	's	beauty	-	it	's	the	last	one	of	it	's	kind	,	as	all	future	models	are	proprietary