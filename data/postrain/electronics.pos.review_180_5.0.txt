i	was	paranoid	about	having	buyer	's	remorse	from	this	unit	.
for	such	a	cheap	price	i	was	sure	i	would	end	up	regretting	the	purchase	.
what	is	the	tradeoff	?
shoot	if	know	,	but	i	have	not	regretted	buying	it	one	bit	.
the	receiver	is	perfect	for	home	theater	,	particularly	dd	NUMBER	shows	.
there	is	absolutely	no	discernable	speaker	hiss	with	digital	content	,	which	is	important	for	tv	watching	that	has	a	high	range	of	both	loud	and	silent	moments	.
as	for	power	,	it	's	not	an	even	match	with	some	of	the	more	power	hungry	and	expensive	analog	amplifiers	out	there	,	but	it	easily	crosses	the	threshold	of	annoying	the	neighbors	.
digital	is	something	to	get	used	to	.
on	conventional	receivers	,	you	turn	the	sound	up	high	and	it	becomes	somewhat	sloppy	,	or	what	some	call	warm	.
not	so	with	the	digital	panny	.
it	's	distortion	free	up	to	very	high	levels	.
i	would	not	call	it	bright	,	but	the	response	is	very	linear	and	precise	.
i	like	hearing	all	the	details	in	music	and	cinema	,	even	at	loud	volume	.
others	do	not	.
the	weaknesses	are	very	minor	:	-the	fm	tuner	is	nothing	special.-it	lacks	fancy	dsp	modes	of	expensive	receivers.-no	fancy	on	screen	display	(	must	do	most	of	the	tweaking	with	knobs	)	-it	does	not	upconvert	video.-the	remote	control	is	an	el	cheapo	.
for	this	price	,	though	,	who	cares