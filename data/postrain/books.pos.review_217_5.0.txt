were	someone	to	write	a	biography	of	carl	hiassin	in	fifty	years	it	is	hard	to	imagine	that	he	or	she	would	not	focus	on	the	author	's	latest	triumph	,	skinny	dip	.
it	is	one	of	his	seminal	achievements	.
the	book	simply	shines	with	humor	,	well	drawn	characters	,	action	and	as	usual	everyone	gets	their	just	desserts	.
it	is	also	one	of	his	smoothest	rides	.
one	gets	the	feeling	that	hiassin	was	very	confident	in	every	sentence	that	he	wrote	.
here	is	an	overview	that	will	stimulate	readers	interest	hiassin	's	.
the	book	opens	with	chaz	trying	to	kill	his	wife	by	flinging	her	over	the	rail	of	their	cruise	ship	,	but	by	some	twists	and	turns	and	luck	joey	survives	long	enough	to	be	rescued	.
much	of	the	rest	of	the	book	circles	around	the	characters	interactions	and	relationships	as	joey	and	her	team	pursue	vengeance	.
by	the	midpoint	of	the	novel	chaz	is	already	an	emotional	wreck	and	it	does	not	look	like	joey	is	about	to	let	up	any	time	soon	.
the	moral	backbone	of	the	novel	is	twofold	.
one	is	the	pitting	of	the	everglades	forest	and	her	defenders	against	big	business	and	her	pollutants	.
the	everglades	is	quickly	turning	from	pristine	swamp	into	another	type	of	ecosystem	entirely	and	many	floridians	want	to	prevent	the	remaining	land	from	being	destroyed	,	which	means	millions	of	dollars	loss	to	the	big	agricultural	business	.
hiassin	's	two	segments	on	the	real	life	plight	of	this	swamp	standout	a	bit	apart	from	the	book	,	but	also	lends	it	a	feeling	of	seriousness	.
and	two	is	the	pitting	of	criminals	against	victims	who	are	not	going	to	take	it	anymore	.
hiassin	is	at	his	when	he	reapetedly	refers	to	the	attempted	murder	as	an	insult	to	joey	.
as	most	survivors	of	violent	crime	can	ascertain	there	is	a	severe	indignity	attached	to	one	's	helplessness	.
there	are	six	main	characters	in	the	book	:	joey	and	her	horny	husband	chaz	,	joey	's	rescuer	mick	who	is	a	kind	and	eccentric	ex-policeman	who	lives	alone	on	an	island	,	joey	's	brother	,	a	rich	expatriate	who	raises	sheep	in	new	zealand	,	mr.	hammernut	,	the	owner	of	a	large	agricultural	empire	and	tool	,	the	books	muscled	goon	who	undergoes	a	moral	transformation	.
the	small	group	of	sub-characters	is	entertainingly	populated	in	part	with	a	hermit	who	lives	in	the	swamp	,	a	street-tough	hairdresser	,	a	slap-happy	senior	and	a	cowardly	lawyer	.
though	similar	elements	live	in	many	of	hiassin	's	works	his	books	never	feel	like	he	has	used	a	formula	to	create	them	more	easily	.
this	ca	not	be	said	for	all	or	most	of	crime	writers	and	should	be	accredited	to	hiassin	's	love	for	storytelling	,	his	skill	as	a	plot	weaver	and	his	ability	to	evoke	so	many	different	emotions	.
in	this	line	of	thought	this	reviewer	took	note	of	the	characters	subtle	(	except	for	tool	)	character	changes	throughout	the	book	.
joey	for	example	seemed	learn	a	bit	more	restraint	as	she	calmed	down	a	little	and	the	same	can	be	said	even	for	characters	that	have	appeared	in	some	of	hiassin	's	previous	works	(	mick	)	.
hiassin	has	never	been	a	fan	of	superficial	materialism	,	but	the	issue	is	slightly	complicated	by	skinny	dip	and	does	not	simply	fall	into	place	along	character	axes	.
while	the	villains	all	crave	money	some	of	the	heroes	are	loaded	too	.
joey	maintains	a	very	frugal	existence	considering	her	$	NUMBER	million	trust	,	but	her	brother	spends	lavishly	.
joey	's	rescuer	on	the	other	hand	lives	on	a	disability	check	.
one	may	wonder	if	the	only	difference	between	chaz	and	his	wife	is	that	she	never	had	to	work	or	hustle	for	a	buck	,	but	there	is	one	other	way	in	which	the	enemies	contrast	and	that	is	the	way	they	treat	their	associates	.
chaz	is	continuously	scrambling	for	something	and	in	his	desperation	-	be	it	for	sex	,	money	,	or	escape	-	uses	other	people	as	tools	rather	than	relating	to	them	as	equals	.
on	the	other	hand	,	joey	has	a	good	sense	of	humor	and	she	enjoys	the	companionship	of	her	pals	.
like	most	of	hiassin	's	work	this	book	is	not	an	overly	violent	one	.
although	it	begins	with	an	attempted	murder	,	the	event	is	quite	peaceful	.
there	is	no	struggle	,	no	blood	and	the	victim	,	joey	,	does	not	die	.
after	reading	skinny	dip	i	immediately	lent	it	to	my	best	friend	katie	who	has	not	been	able	to	put	it	down	.
so	,	potential	readers	be	warned	:	like	katie	you	may	not	be	able	to	get	any	work	done	for	a	day	or	two	,	but	the	time	is	not	wasted	on	hiassin	's	best	work	to	date	.
one	comes	away	gaining	not	only	pleasant	memories	of	laughs	and	smiles	,	but	also	the	feeling	that	one	has	poured	one	more	drop	into	the	bucket	of	our	minds	that	we	must	fill	in	order	to	make	this	world	a	better	place	.
in	short	,	carl	hiassin	puts	us	in	touch	with	the	good	guys	in	ourselves