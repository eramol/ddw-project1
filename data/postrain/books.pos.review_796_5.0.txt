i	have	spent	a	lot	of	time	and	money	researching	into	mormon	history	.
the	big	problem	is	that	,	as	henry	ford	once	said	history	is	bunk	.
with	joseph	smith	and	the	history	of	the	mormon	church	,	there	are	hundreds	of	accounts	that	argue	that	he	was	an	inspired	prophet	and	just	as	many	,	an	ingenious	charlatan	.
there	are	even	accounts	that	avoid	making	a	judgement	either	way	and	leave	it	up	to	the	reader	to	decide	.
hence	,	using	history	as	a	basis	for	discovering	the	truth	about	the	history	of	joseph	smith	can	be	a	very	frustrating	(	and	sometimes	expensive	)	exercise	.
the	church	itself	avoids	encouraging	the	study	of	historical	events	to	gain	testimony	,	but	instead	promotes	the	individual	member	to	gain	a	spiritual	testimony	.
it	is	easy	to	see	from	my	statement	above	,	why	.
this	book	takes	a	completely	different	approach	in	investigating	the	truthfulness	of	what	joseph	smith	produced	.
forget	about	what	might	have	happened	in	the	early	YEAR	's	and	instead	,	focus	on	the	actual	record	contained	in	the	book	of	mormon	itself	.
let	current	day	investigation	be	the	basis	for	the	search	of	truth	instead	of	historical	personal	accounts	.
this	removes	any	bias	of	the	source	material	and	hence	allows	for	an	objective	study	.
thomas	stuart	ferguson	starts	out	as	the	book	of	mormon	's	most	enthusiastic	proponent	.
he	sacrifices	a	large	portion	of	his	life	both	petitioning	the	first	presidency	for	financial	support	,	and	performing	extensive	archaeological	expeditions	to	find	evidence	of	the	civilisations	documented	in	the	book	of	mormon	(	bofm	)	.
while	he	expects	an	abundance	of	material	to	be	discovered	,	he	finds	none	.
nothing	.
no	evidence	of	the	language	or	writings	contained	within	the	bofm	,	the	animals	and	crops	mentioned	,	artefacts	,	city	names	,	weapons	,	metals	,	glass	etc	.
nothing	for	the	period	documented	within	the	bofm	(	NUMBER	bc	-	NUMBER	ad	)	.
there	are	few	discoveries	outside	this	timeline	,	but	none	within	.
all	he	wanted	(	and	expected	)	to	find	were	actual	artefacts	or	at	least	murals/pictorials	showing	the	above	documented	items	.
the	turning	point	came	when	fragments	of	the	joseph	smith	papyri	were	rediscovered	in	the	late	YEAR	's	and	found	to	contain	nothing	in	common	with	the	book	of	abraham	.
this	is	a	completely	separate	quest	in	itself	(	see	by	his	own	hand	upon	papyrus	)	.while	ferguson	may	have	lost	his	faith	,	he	never	left	the	lds	church	and	continued	to	attend	and	support	it	-	he	even	sent	a	son	on	a	mission	.
he	believed	the	mormon	church	was	the	best	fraternity	on	earth	.
in	public	,	he	was	a	normal	member	,	in	private	circles	he	believed	joseph	smith	was	a	fraud	and	as	a	result	,	spent	the	remainder	of	his	life	researching	why	.
this	book	is	both	a	document	detailing	ferguson	's	quest	for	the	truth	,	and	his	way	of	dealing	with	it	from	the	result	.