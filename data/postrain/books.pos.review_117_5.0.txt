this	thing	sat	on	my	shelf	,	half-read	for	the	longest	time	.
only	the	notice	of	the	upcoming	release	this	november	of	pynchon	's	next	got	me	motivated	enough	to	dig	into	it	again	.
it	's	not	that	it	is	not	brilliant	.
no	one	else	around	can	dazzle	you	with	so	much	wit	and	wonder	.
the	first	encounter	with	the	talking	dog	is	as	magical	as	anything	you	will	ever	read	.
and	it	's	not	like	this	is	the	only	pynchon	novel	that	takes	some	effort	to	get	into	.
there	are	plenty	of	folks	who	have	had	to	to	take	a	couple	cracks	at	v	or	gravity	's	rainbow	before	catching	the	wave	.
but	mason	and	dixon	is	a	lot	of	work	,	if	for	no	other	reason	than	the	effort	it	takes	dealing	with	the	mid-18th	century	prose	style	.
(	can	you	imagine	the	effort	it	took	to	produce	it	?	)
john	barth	's	sotweed	factor	is	similar	,	and	yet	somehow	infinitely	more	accessible	(	and	highly	recommended	!	)	.
pynchon	's	gift	for	rapid	exposition	is	not	necessarily	suited	to	the	constraint	on	verbal	glibness	.
especially	in	a	work	this	voluminous	.
and	yet	the	darn	thing	is	consistently	challenging	,	if	one	has	the	patience	and	energy	to	put	into	it	.
it	seemed	to	me	that	the	beginning	and	ending	were	the	best	parts	,	but	this	could	very	well	have	everything	to	do	to	the	enthusiasm	one	brings	to	a	new	book	,	and	the	emotional	satisfaction	one	gain	's	when	reaching	towards	the	conclusion	.
one	thing	for	sure	,	for	once	pynchon	truly	has	plotted	out	and	delivers	a	conclusion	worthy	of	the	whole	work	,	as	opposed	to	suddenly	rushing	out	a	trap	door	and	leaving	the	reader	in	a	state	of	suspension	(	which	of	course	is	also	one	of	the	many	delights	of	his	first	three	novels	)	.
this	time	one	gets	the	sense	that	the	author	has	a	good	deal	of	affection	for	his	featured	players	.
this	book	is	a	great	as	you	want	it	to	be	,	if	you	are	willing	to	work	at	it	.
i	am	just	looking	forward	to	the	next	one	being	a	little	more	nimble	.
(	meanwhile	,	i	've	got	a	couple	months	to	see	if	i	can	make	more	of	a	dent	into	the	recognitions	.