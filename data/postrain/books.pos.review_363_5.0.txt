i	thought	that	the	two	books	previous	to	this	in	the	duncan	kincaid/gemma	james	series	were	slight	disappointments	.
kincaid	seemed	relegated	to	a	side	character	with	gemma	taking	the	lead	.
in	a	dark	house	is	an	excellent	mystery	that	brings	the	two	back	to	equal	ground	and	re-establishes	them	as	one	of	the	best	partnerships	in	mystery	fiction