every	fiber	of	my	being	is	incorporated	into	this	review	for	jack	&	quot	;	ti	jean	&	quot	;	kerouac	!
to	actually	see	most	of	kerouac	's	fellow	writer	's	and	poet	's	giving	his	or	her	true	and	poignant	interpretation	of	the	unmitigated	&	quot	;	genius	&	quot	;	that	was	this	brother	lowellian	is	in	itself	a	magnificent	production	!
i	bought	this	dvd	today	at	a	mall	in	new	hampshire	and	could	have	bought	it	for	less	on	amazon.com	!
just	a	few	minutes	ago	i	did	order	the	dvd	&	quot	;	jack	kerouac-king	of	the	beats	&	quot	;	on	amazon.com	and	ca	not	wait	for	its	arrival	!
if	it	's	anything	like	&	quot	;	what	happened	to	kerouac	?	&	quot	;	then	it	will	surely	be	another	masterful	production	!
after	i	write	this	review	i	am	going	to	watch	this	dvd	again	and	experience	the	same	chills	as	i	did	when	initially	watching	it	for	the	first	time	!
anyone	that	is	a	true	fan	of	kerouac	and	his	works	will	be	smitten	with	this	production	because	you	are	reliving	pure	literary	genius	!
to	hear	the	poetry	and	prose	from	the	lips	and	soul	of	kerouac	himself	is	truly	awe-inspiring	!
this	production	is	a	brutal	and	factual	statement	of	a	man	that	was	rarely	understood	but	truly	adored	by	those	that	have	read	the	majestic	prose	and	verse	that	was	and	still	is	:	jack	kerouac