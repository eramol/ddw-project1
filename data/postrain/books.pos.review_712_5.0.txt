william	manchester	,	the	greatest	narrative	historian	of	or	time	,	provides	a	vivid	account	of	his	experiences	in	the	pacific	theatre	during	world	war	ii	.
as	with	all	of	his	books	,	we	see	,	hear	,	smell	,	taste	,	and	feel	the	experiences	with	him	,	as	if	we	were	there	.
although	more	personal	,	of	course	,	than	his	other	narrative	histories	,	goodbye	darkness	provides	the	same	factual	details	,	and	each	sentence	,	paragraph	,	and	chapter	is	crafted	with	the	same	beauty	and	skill	that	is	manchester	's	hallmark	.
an	amazing	memoir	by	a	truly	great	writer	.