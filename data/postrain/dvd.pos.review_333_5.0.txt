i	bought	this	as	a	dvd	for	home	use	.
i	wo	not	repeat	the	story	as	others	have	done	so	already	.
i	loved	the	story	.
the	plot	was	well	put	together	and	solid	.
the	acting	was	wonderful	.
i	have	watched	it	a	couple	of	times	now	and	enjoy	it	more	each	time	i	see	it	.
i	really	am	enjoying	the	fun	of	this	film	.
i	'd	suggest	to	folks	to	just	relax	and	watch	a	terrific	and	humorous	film	.
it	is	made	to	be	light	with	family	values	of	what	counts	in	life	.
tim	did	an	outstanding	job	as	he	always	does	.
the	chasing	scenes	with	tim	on	all	fours	becoming	a	dog	;	the	dog	behaviors	as	a	human	had	me	rolling	.
it	is	one	of	the	funniest	films	i	've	seen	in	a	long	time	.
as	an	adult	i	loved	it	and	would	buy	it	again	.
i	am	thrilled	to	see	disney	making	such	terrific	and	wonderful	films	.
it	was	tastefully	done	.
the	caged	scenes	were	carefully	done	.
no	bad	language	just	a	wonderful	film	that	can	be	enjoyed	as	another	eventual	classic	among	the	disney	films	.
and	no	i	did	not	perceive	this	as	a	film	geared	toward	small	children	.
perhaps	pre-teens	and	up	would	be	my	guess	.
to	demean	the	film	based	on	the	expectation	that	tiny	children	would	appreciate	the	story	is	not	realistic	nor	does	it	do	justice	for	what	the	film	is	.
it	is	a	wonderful	film	with	a	great	storyline	!