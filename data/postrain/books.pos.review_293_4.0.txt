book	number	three	in	the	outlander	trilogy	,	the	fantasy	historical	romance	of	the	time	traveling	claire	fraser	,	has	claire	re-unite	with	her	ORDINAL	century	husband	,	jamie	,	who	she	left	before	the	battle	of	culloden	in	dragonfly	in	amber	.
a	time	travel	trip	through	the	stones	is	once	more	necessary	and	claire	has	to	leave	her	beloved	daughter	brianna	behind	.
this	trip	is	successful	and	claire	is	now	plunged	into	the	complicated	life	of	jamie	,	who	has	survived	the	battle	but	now	is	dealing	with	the	post-uprising	difficulties	of	the	scottish	.
while	claire	is	involved	with	the	difficulties	of	her	re-union	with	jamie	,	there	is	also	a	series	of	mysterious	and	violent	murders	in	edinborough	,	and	the	kidnapping	of	jamie	's	nephew	.
claire	and	jamie	sail	the	high	seas	and	encounter	pirates	,	voodoo	and	slave	uprisings	in	the	caribbean	.
this	colorful	third	installment	in	the	series	at	times	seems	like	it	tries	a	little	too	hard	to	have	claire	and	jamie	caught	up	in	every	possible	adventure	of	the	time	period	.
nevertheless	the	foundation	of	the	solid	characters	created	and	nurtured	by	gabaldan	weather	the	slightly	torrid	escapades	of	this	timeless	romantic	couple	.
one	of	the	new	characters	is	a	bit	flimsy	and	caricatured	;	the	giggling	,	bouncy	,	foot-worshipping	chinaman	mr.	willoughby	.
although	he	later	proves	to	be	more	substantial	than	his	intial	portrayal	,	the	appearance	of	this	character	is	contrived	and	jarring	.
a	note	i	think	the	story	would	have	been	better	without	.
despite	its	flaws	,	voyager	continues	the	thread	of	a	passionate	romance	and	fascinating	premise	.
the	time	travel	fantasy	is	portrayed	well-in	particular	the	notion	of	what	one	would	bring	back	to	the	ORDINAL	century	if	all	you	could	carry	was	in	your	pockets	.
combining	historical	romance	and	fantasy	was	a	genius	stroke	that	makes	this	series	of	continuing	interest	.
gabaldon	's	superior	narrative	,	likeable	characters	and	excellent	research	continue	and	keep	her	audience	hooked	.
in	light	of	the	inherent	difficulty	of	maintaining	reader	interest	in	a	character	and	premise	over	three	books	,	this	is	a	highly	sucessful	sequel	.