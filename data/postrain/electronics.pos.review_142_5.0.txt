(	despite	some	of	the	negative	reviews	of	this	item	,	it	continues	to	perform	well	with	my	regular-sized	ipod	.
in	fact	,	it	has	more	volume	,	depth	,	and	presence	than	three	other	portable	speakers-each	costing	NUMBER	to	NUMBER	%	more-to	which	i	've	compared	it	.
the	only	downside	is	that	the	item	is	no	longer	sold	postage-free	from	amazon	,	diminishing	its	bargain	appeal	somewhat	.
)	these	modest-sized	and	priced	speakers	perform	as	advertised	:	the	sound	is	more	than	adequate	;	they	are	small	enough	that	i	can	take	the	stephen	colbert	report	with	me	on	walks	;	when	not	in	use	,	they	fold	up	into	a	mini	porta-john	.
my	only	misgiving	is	whether	i	should	have	spent	2-3x	more	for	a	unit	with	a	dock	connector	.
but	for	under	twenty	bucks	you	simply	wo	not	find	a	better	sounding	,	more	compact	unit