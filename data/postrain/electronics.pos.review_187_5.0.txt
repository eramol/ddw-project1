i	've	had	these	headphones	for	just	over	two	years	,	and	i	am	happy	to	see	that	they	are	one	of	the	few	pairs	marketed	as	surround	sound	that	have	done	quite	well	.
the	reason	behind	that	is	because	they	are	truly	excellent	headphones	.
the	range	is	spectacular	and	the	inputs	even	more	so	.
you	are	given	the	option	of	rca	and	toslink	(	optical	)	,	which	is	all	you	really	need	.
movies	really	do	sound	like	movies	DOTS	you	hear	things	behind	,	in	front	,	and	to	your	left	and	right	when	you	are	connected	via	optical	.
take	note	that	dts	also	sounds	'boomier	and	a	tad	crisper	than	dolby	d	does	,	especially	in	gladiator	.
honestly	,	i	've	nothing	to	complain	about	.
as	a	college	guy	,	i	could	not	bring	my	YEAR	watt	system	from	home	.
which	,	i	might	add	,	never	sounded	as	good	as	my	headphones	,	mostly	because	headphones	will	always	have	the	advantage	of	being	closer	to	you	physically	.
listening	to	music	at	extremely	high	bit	rates	and	using	the	optical	connection	from	my	macbook	pro	truly	does	some	incredible	things	.
you	'd	be	amazed	at	coldplay	's	the	scientist	if	you	heard	it	on	these	headphones	.
it	's	indescribable	.
and	by	the	way	,	i	am	a	slight	audiophile	.
you	seriously	ca	not	go	wrong	with	these	headphones	.
and	$	NUMBER	for	each	additional	pair	of	headphones	,	allowing	you	to	add	as	many	as	you	want	to	one	base	station	?
even	better	DOTS	good	idea	for	families	in	apartments	.
incredibly	advanced	technology	,	incredible	sound	,	and	incredible	results	.
ca	not	go	wrong	here	.
oh	,	and	they	are	extremely	comfy