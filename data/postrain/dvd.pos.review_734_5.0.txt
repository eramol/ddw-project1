there	is	not	one	wasted	second	in	this	film	.
not	one	!
you	will	be	sucked	in	,	and	surprised	,	within	the	first	two	minutes	,	and	there	's	very	little	dialogue	so	you	need	not	worry	about	too	much	subtitle	reading	.
you	can	tell	there	's	a	lot	of	influence	from	american	film	noir	in	this	film	,	but	le	samourai	takes	it	to	a	level	of	perfection	.
i	wish	some	of	the	folks	that	throw	their	good	money	towards	bad	modern	action	flicks	would	take	the	time	to	watch	le	samourai	they	just	might	discover	a	thing	or	two	about	how	it	could	really	be	done	.