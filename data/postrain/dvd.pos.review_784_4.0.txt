samuel	jackson	and	kevin	spacey	turn	two	distinct	yet	strong	performances	.
jackson	is	always	great	to	watch	for	his	intensity	,	and	this	role	suits	that	to	a	tee	.
spacey	continues	to	impress	with	a	solid	and	believable	performance	.
this	film	and	its	settings/supporting	cast	are	*slightly*	dated	by	today	's	standards	,	but	the	two	main	actors	and	the	good	story	more	than	make	up	for	it	.
highly	worthwile