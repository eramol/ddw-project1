i	recently	received	the	zen	microphoto	as	a	gift	.
since	i	travel	fairly	often	,	i	decided	to	purchase	the	traveldock	after	reading	the	reviews	.
it	's	great	.
the	microphoto	fits	in	easily	and	comes	out	just	as	simply	.
the	sound	is	rich	,	loud	enough	,	and	better	than	any	other	travel	speakers	that	i	own	.
many	people	comment	on	the	bass	issue	.
for	travel	speakers	,	the	traveldock	has	the	best	bass	to	date	,	in	my	opinion	.
true	bass	enthusiasts	could	purchase	the	subwoofer	(	for	an	additional	$	NUMBER	from	creative	's	website	)	.
i	am	very	satisfied	with	the	sound	and	the	convenience	.
the	jacks	are	easy	to	reach	and	use	,	which	are	on	the	back	of	the	unit	.
also	,	the	microphoto	charges	while	docked	,	whether	it	's	playing	or	not	.
there	's	even	an	attachable	fm	antenna	that	plugs	in	to	give	you	reception	when	docked	(	since	you	are	not	using	the	earphones	which	serve	as	an	antenna	also	)	.
by	the	way	,	it	comes	with	a	remote	control	which	allows	you	to	switch	between	fm	and	mp3	's	,	adjust	volume	,	and	play/pause/ff/rew	.
overall	,	this	is	a	very	nice	addition	to	the	zen	microphoto	.