this	is	a	great	multi-function	printer/scanner/copier/fax	machine	for	a	small	office	or	home	business	that	has	a	light	to	moderately	light	document	workload	.
print	quality	is	excellent	,	scanning	quality	is	excellent	,	copying	quality	is	excellent	,	faxing	is	faxing	.
the	control	software	is	fine	;	not	great	,	but	it	does	the	job	.
the	networking	features	,	including	built-in	wireless	,	makes	this	printer	a	snap	to	integrate	into	your	office	/	business	using	a	wireless	network	.
some	concerns	:	after	heavy	usage	the	top	document	feeder	,	used	for	scanning	,	faxing	,	or	photocopying	a	stack	of	loose	documents	,	begins	to	jam	.
the	rollers	need	to	be	cleaned	to	avoid	this	,	and	getting	jammed	paper	out	of	the	top	document	feeder	is	very	difficult	.
however	,	the	fact	that	this	piece	of	equipment	even	has	a	document	feeder	for	this	sort	of	functionality	is	pretty	good	.
the	fax	log	keeps	a	record	of	outgoing	faxes	and	you	can	re-send	them	easily	enough	,	but	you	ca	not	re-print	faxes	received	;	the	fax	log	merely	logs	their	reception	,	but	not	the	fax	itself	.
overall	it	's	a	good	multi-function	at	a	reasonable	price	with	a	lot	of	functionality	built	into	it	.
would	recommend	to	anyone	who	only	has	a	light	document	load	,	with	maybe	the	occasional	large	document	scanning	/	copying	requirement