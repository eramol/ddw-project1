i	am	glad	i	bought	this	book	and	i	will	continue	to	refer	to	it	.
the	remit	of	the	dummies	series	is	to	provide	a	guide	to	its	subject	matter	without	any	great	fuss	.
the	text	focuses	on	practical	techniques	without	unnecessary	diversion	into	the	detail	of	molecular	biology	or	computer	science	.
in	this	respect	it	would	have	been	a	difficult	book	to	author	,	readers	having	come	from	one	discipline	or	the	other	.
i	agree	with	previous	reviewers	that	this	is	well	worth	reading	before	doing	a	bioinformatics	course	or	degree	.
bioinformatics	is	a	new	field	,	and	this	book	has	delivered	a	useful	introduction	to	it	without	recourse	to	expensive	textbooks	full	of	unreadable	filler