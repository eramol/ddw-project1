lemon	and	curtis	are	a	great	team	-	some	like	it	hot	and	the	great	race	.
in	this	movie	we	have	great	melodrama	and	the	ageless	story	of	boy	meets	girl	(	natalie	wood	)	and	she	catches	him	.
the	star-studded	cast	is	enhanced	by	the	superb	acting	of	peter	falk	in	one	of	his	great	comic	roles	,	the	hapless	maximillian	always	catching	the	short	end	of	the	stick	by	his	perpetually	out-of-sorts	boss	,	the	permanently	crabby	professor	fate	(	lemon	)	.
curtis	does	what	he	does	best	-	looks	dashing	and	debonoir	in	a	role	so	familiar	to	him	he	could	phone	it	in	.
this	dvd	is	a	collector	and	one	of	my	all-time	top	ten	favorites