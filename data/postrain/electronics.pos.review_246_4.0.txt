notebooks	pro	is	the	highest	rated	webcam	(	for	notebooks	)	on	the	market	and	for	good	reason	.
audio	and	video	quality	is	excellent	.
you	can	talk	normal	and	be	NUMBER	or	NUMBER	feet	away	with	no	problem	.
the	echo	cancellation	feature	is	a	true	must	for	webcams	,	especially	with	the	use	of	video	.
on	the	negative	side	the	usb	cable	is	too	short	which	limits	interchangeability	with	your	desktop	.
also	,	i	have	the	quickcam	fusion	for	my	desktop	which	is	a	NUMBER	star	rated	product	and	without	doubt	the	best	of	the	best	webcams	overall	.
logitech	ships	that	product	with	quickcam	NUMBER	software	which	is	probably	a	nice	update	to	quickcam	NUMBER	the	notebook	pro	uses	.
unfortunately	,	logitech	provides	little	detail	and	info	on	these	technical	and	software	issues	.