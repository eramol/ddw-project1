i	listen	to	a	wide	range	of	music	,	from	hard	rock	,	techno/electronic	,	to	classical	,	and	i	've	found	it	difficult	to	locate	reasonably	priced	speakers	that	can	handle	all	of	this	-	at	least	until	i	ordered	this	set	.
i	was	apprehensive	about	buying	online	(	and	also	about	the	sony	brand	for	speakers	)	,	but	i	am	very	happy	with	this	purchase	.
i	've	been	very	impressed	with	the	sound	both	at	the	high	and	low	ends	-	the	highs	are	crisp	and	the	bass	is	tight	,	not	boomy	.
the	soundstage	is	expansive	(	of	course	,	this	depends	a	lot	on	your	receiver	as	well	)	and	imaging	good	.
i	'd	definitely	recommend	this	speaker	set