i	found	airframe	to	be	a	great	book	to	read	on	my	summer	vacation	.
i	was	expecting	it	to	be	more	of	a	science	fiction	novel	but	it	is	really	a	book	about	corporate	intrigue	and	social	commentary	(	i.e	.
the	media	)	.
that	being	said	,	i	was	not	the	least	disappointed	.
in	his	typical	style	,	crichton	takes	an	aerospace	engineering	course	and	boils	it	down	to	plain	english	.
i	learned	so	much	about	the	construction	of	airplanes	and	the	airline	industry	-	it	was	fascinating	.
he	also	does	a	great	job	in	raking	the	mass	media	over	the	coals	-	which	was	fun	(	and	somewhat	ironic	from	the	creator	of	er	)	!	the	book	is	fast	paced	and	reads	like	a	movie	-	which	i	am	sure	is	no	coincidence	by	crichton	;	)	the	main	character	is	very	likeable	and	you	find	yourself	rooting	for	her	.
there	are	a	number	of	plot	twists	and	surprises	and	it	certainly	keeps	you	turning	the	pages	.
in	summary	i	would	not	say	that	this	is	crichton	's	best	work	,	but	it	is	surely	a	very	enjoyable	book	that	i	would	recommend	to	everyone