one	of	the	best	movies	i	have	seen	!
the	subject	itself	forces	you	to	look	at	your	own	limitations	on	anything	that	is	out	of	your	'normal	boundaries	.
after	watching	this	movie	,	you	have	a	better	insight	on	how	limited	most	of	us	are	and	how	this	causes	a	lot	more	harm	than	we	think	it	does	.
in	terms	of	film	quality	,	it	is	also	one	of	the	best	i	have	seen	.
the	actors	are	tremendous	,	the	characters	are	so	accurately	portrayed	that	you	actually	feel	the	pain	of	each	one	of	them	.
of	course	,	ludovic	's	innocence	is	rendered	beautifully	,	but	the	changes	that	the	mother	goes	through	are	very	emotionally	charged	(	maybe	because	i	am	a	mother	myself	?	?	)	.
she	seems	at	first	to	be	the	most	understanding	of	all	when	it	comes	to	her	son	's	differences	,	then	goes	through	a	confusion	stage	,	and	then	tries	really	hard	to	understand	him	.
but	when	her	family	's	whole	life	setting	(	house	,	husband	's	job	,	social	life	)	goes	havoc	,	she	closes	down	emotionally	on	her	own	son	which	is	extremely	painful	to	watch	.
after	one	final	emotionally	violent	confrontation	with	ludovic	,	she	realizes	that	she	's	loosing	him	and	finally	sees	him	the	way	it	should	be	:	her	beautiful	child	,	nothing	more	,	nothing	less	.
it	is	also	filmed	in	a	very	colorful	manner	(	especially	during	ludovic	's	escapes	into	dreamland	)	,	which	accentuate	the	notion	of	ludovic	's	innocence	.
the	neighborhood	setting	seems	to	be	a	bit	of	a	caricature	of	american	suburbia	(	green	front	lawns	,	barbecue	parties	)	with	an	european	flavor	(	adults	dancing	at	neighborhood	parties	)	.
in	any	case	,	the	movie	seems	to	make	a	clear	statement	that	intolerance	seems	to	be	most	prominent	in	middle-class	white	surburban	populations	,	which	many	could	find	this	to	be	a	bit	of	a	stereotype	.
in	any	case	,	i	highly	recommend	this	movie	to	anyone	who	wants	something	more	out	of	movie	than	what	is	presented	in	mainstream	movies	.