any	movie	that	begins	with	a	civil	war	ironclad	going	down	a	river	with	guns	blazing	is	going	to	appeal	to	me	(	although	my	wife	thought	i	have	started	playing	the	wrong	movie	)	.
then	again	,	it	was	really	upsetting	at	the	film	's	climax	to	see	the	c.s.s	.
texas	getting	shot	up	.
yes	,	i	know	it	is	just	a	film	,	but	the	thought	of	finding	such	a	civil	war	artifact	intact	only	to	have	it	immediately	start	getting	shot	up	momentarily	took	me	back	to	the	real	world	,	which	is	a	mistake	because	realism	is	not	the	name	of	the	game	here	.
sahara	begins	at	the	end	of	the	civil	war	,	as	the	confederate	ironclad	texas	escapes	from	a	burning	richmond	carrying	the	gold	supply	of	the	confederacy	.
it	disappears	into	history	,	where	the	lost	ship	becomes	the	latest	obsession	of	dirk	pitt	(	matthew	mcconaughey	)	,	who	believes	the	texas	crossed	the	atlantic	and	ended	up	somewhere	in	africa	(	do	not	ask	how	,	because	an	explanation	is	not	forthcoming	and	you	know	from	the	title	of	the	film	that	dirk	is	right	on	this	score	)	.
dirk	is	partnered	with	al	giordino	(	steve	zahn	)	,	and	if	you	pay	attention	to	the	title	sequence	you	get	a	visual	tour	of	their	career	resumes	.
technically	the	boys	work	for	admiral	(	ret	.	)
jim	sandecker	(	william	h.	macy	)	,	who	has	a	hard	time	reining	them	in	once	dirk	finds	another	clue	about	the	texas	.
meanwhile	,	dr.	eva	rojas	(	penelope	cruz	)	of	the	world	health	organization	has	arrived	in	africa	with	a	team	to	find	the	source	of	a	dangerous	plague	that	has	been	spreading	across	the	region	.
however	,	there	is	another	problem	in	that	rojas	is	getting	too	close	to	the	truth	,	which	puts	here	in	danger	from	the	local	dictator	,	general	kazim	(	lennie	james	)	,	and	his	partner	,	smarmy	french	rich	guy	,	yves	massarde	(	lambert	wilson	)	.
dirk	becomes	involved	in	this	because	he	is	making	a	habit	of	rescuing	eva	,	and	because	he	is	looking	for	the	legendary	ironclad	in	the	same	part	of	the	continent	.
al	is	along	for	the	ride	,	as	if	rudi	gunn	(	rainn	wilson	)	for	part	of	the	way	.
these	narrative	threads	are	used	to	connect	the	action	sequences	that	are	the	main	set	pieces	of	this	NUMBER	film	from	director	breck	eisner	(	thoughtcrimes	)	.
the	best	of	these	is	the	speedboat	one	,	involving	the	panama	,	which	comes	relatively	early	in	the	film	.
i	want	to	ask	,	whatever	happened	to	saving	the	best	for	last	?
in	a	film	like	this	,	but	it	is	not	like	they	did	that	in	raiders	of	the	lost	arc	,	so	why	should	that	happen	here	?
granted	,	sahara	is	not	as	much	fun	as	that	classic	film	romp	,	but	it	was	certainly	more	enjoyable	than	national	treasure	,	probably	because	it	is	not	as	burdened	by	american	history	and	national	landmarks	as	that	one	(	and	mcconaughey	and	zahn	both	look	buff	enough	to	pull	this	stuff	off	in	the	world	of	movie	reality	)	.
besides	,	i	like	a	film	where	the	chemistry	between	the	two	guys	is	better	than	the	chemistry	between	the	hero	and	the	heroine	,	mainly	because	if	this	film	is	the	start	of	a	dirk	pitt	franchise	,	you	know	al	will	be	back	but	you	have	to	expect	a	different	female	lead	in	each	subsequent	film	.
i	especially	liked	sandecker	's	third	demand	at	the	end	of	the	film	and	to	tell	the	truth	,	knowing	nothing	about	the	original	novels	,	i	would	much	rather	see	the	team	in	action	than	dirk	teaming	up	with	somebody	new	in	each	film	.
for	me	the	end	result	is	that	sahara	is	an	enjoyable	action	romp	of	the	brainless	variety	.
but	if	you	want	more	,	then	look	elsewhere