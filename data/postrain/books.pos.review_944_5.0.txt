while	on	maternity	leave	from	cambridge	university	and	with	her	husband	away	handling	a	legal	matter	in	los	angeles	,	professor	cassandra	james	meets	her	neighbor	actress	melissa	meadows	and	her	spouse	keith	kingleigh	.
melissa	and	cassandra	share	an	affinity	since	both	recently	gave	birth	prematurely	.
kevin	is	going	to	direct	a	production	starring	melissa	.
he	asks	cassandra	to	adapt	the	victorian	drama	east	lynn	,	which	she	agrees	to	do	.
as	opening	night	beckons	,	cassandra	notices	the	cast	seems	extra	nervous	.
later	an	anxious	melissa	calls	cassandra	believing	that	someone	is	stalking	her	.
cassandra	tries	to	calm	her	down	.
the	next	day	melissa	fails	to	appear	for	a	rehearsal	so	cassandra	goes	to	visit	her	.
melissa	is	gone	without	a	word	,	but	abandoned	her	six	month	old	baby	that	she	had	shown	so	much	love	.
with	her	own	fussy	infant	at	her	side	,	cassandra	wonders	what	happened	to	her	new	friend	while	unable	to	contact	stephen	,	dealing	with	the	abrupt	arrival	of	her	first	husband	,	and	detective	sergeant	vickers	suspecting	the	professor	killed	the	actress	.
stage	fright	,	the	second	professor	james	tale	(	see	murder	is	academic	)	,	is	a	fabulous	amateur	sleuth	mystery	that	builds	up	the	suspense	slowly	so	that	the	reader	like	the	heroine	suspects	everyone	's	motives	.
either	foul	play	occurred	or	melissa	had	played	the	role	of	a	loving	mother	,	but	cassandra	can	not	find	a	motive	for	either	scenario	while	the	evidence	points	towards	her	so	that	even	the	audience	will	believe	she	harmed	or	murdered	the	actress	.
this	is	a	fantastic	english	mystery	in	the	christie	tradition	.
harriet	klausner