boy	,	did	this	movie	suck	at	the	beginning	!
and	boy	,	did	it	just	get	better	and	better	from	there	on	in	.
even	the	oriental	humor	,	which	is	usually	so	awful	,	was	actually	funny	at	times	.
the	action	sequences	,	while	a	little	too	often	fanciful	,	were	very	impressive	,	even	innovative	.
chow	is	a	talented	guy	.
having	seen	this	i	am	interested	to	see	what	his	earlier	work	is	like	.
if	you	like	martial	arts/action/weird	comedy	,	you	will	love	this