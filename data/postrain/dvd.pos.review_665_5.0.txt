if	you	are	a	fan	of	vampires	,	werewolves	,	action	,	or	gothic	suspense	,	this	is	probably	a	movie	you	'd	be	interested	in	checking	out	.
while	it	's	not	a	groundbreaking	film	,	it	's	a	highly	entertaining	one	,	and	one	that	,	for	me	,	is	something	i	can	rewatch	several	times	per	year	.
the	filming	is	very	attractive	.
everything	is	dark	,	yet	not	so	dark	you	ca	not	see	what	's	going	on	.
the	movie	has	a	gothic	look	to	it	-	from	the	sets	,	to	the	costumes	,	to	the	lighting	.
(	please	do	not	mistake	the	meaning	of	gothic	in	terms	of	entertainment	as	the	same	thing	as	the	gothic	lifestyle	.	)
the	music	,	mostly	heavy	rock	,	fits	in	well	with	the	movie	.
and	the	action	sequences	are	expertly	shot	and	exciting	to	watch	.
the	movie	uses	several	techniques	made	popular	by	the	matrix	,	but	who	cares	?
it	works	perfectly	within	this	movie	.
the	only	downfall	of	the	movie	is	the	complete	lack	of	humor	DOTS	there	's	not	a	single	joke	in	the	entire	thing	!
everything	is	so	serious	and	dark	that	unless	you	are	in	a	pretty	good	mood	the	movie	could	really	bring	you	down	DOTS	that	's	how	heavy	it	is	.
as	for	the	set	.
the	added	scenes	provide	more	insight	,	and	the	special	features	are	a	treat	,	especially	the	45-minute	documentary	fang	vs.	fiction	,	which	talks	about	vampires	and	werewolves	.
it	was	very	intriguing	.
if	you	are	a	fan	of	this	movie	and	you	own	the	dvd	already	,	i	would	recommend	selling	it	and	buying	this	version	,	it	's	worth	the	money