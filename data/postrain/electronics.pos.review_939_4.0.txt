garmin	ique	m5i	am	very	pleased	with	my	little	ique	m5	gps/pda	.
while	it	's	not	perfect	,	it	's	pretty	darn	amazing	.
i	had	actually	purchased	a	lowrence	iway	NUMBER	gps	,	but	then	my	husband	discovered	the	ique	on	sale	at	costco	for	about	[	DOTS	]	more	.
with	the	ique	offering	so	many	more	features	,	i	returned	the	lowrence	and	purchased	the	ique	.
with	my	husband	and	i	both	having	the	same	machines	,	we	can	beam	information	to	each	other	or	send	them	over	bluetooth	.
it	has	taken	many	hours	of	playing	around	with	it	to	get	to	learn	the	device	,	as	it	is	not	very	intuitive	and	the	manuals	,	especially	for	the	pda	,	are	limited	.
my	husband	has	spent	a	lot	of	time	learning	to	use	the	gps	,	and	he	's	learning	its	idiosyncrasies	as	far	as	planning	routes	and	getting	it	to	avoid	routing	through	construction	areas	.
it	's	very	sophisticated	and	the	maps	are	great	-	better	detail	than	the	lowrence	.
we	purchased	a	NUMBER	gig	memory	card	and	downloaded	the	entire	map	database	onto	it	,	and	there	were	still	NUMBER	gigs	to	spare	.
i	had	no	trouble	downloading	the	maps	or	the	active	sync	software	.
the	basic	gps	functions	are	easy	to	use	once	you	get	the	hang	of	it	,	the	voice	directions	are	clear	and	concise	,	and	it	re-routes	quickly	if	you	go	off	course	.
you	can	plan	your	route	in	advance	without	having	the	gps	activated	.
this	is	a	good	idea	,	since	it	can	take	up	to	NUMBER	minutes	for	the	unit	to	locate	the	satellites	,	and	you	can	be	on	your	way	if	you	've	viewed	your	route	before	starting	out	.
i	used	the	gps	on	a	trip	stratford	,	canada	,	and	it	was	wonderful	having	the	canadian	maps	as	well	as	the	us	maps	available	.
i	even	used	it	handheld	while	walking	to	my	bed-and-breakfast	from	the	theater	.
every	once	in	awhile	there	is	a	glitch	(	it	said	the	theater	was	on	the	left	,	which	would	have	taken	me	into	the	avon	river	!
)	,	but	it	's	generally	right	on	target	and	i	find	most	of	the	problems	are	operator	error	,	such	as	turning	too	soon	.
the	route	to	home	feature	is	very	convenient	.
granted	,	the	ique	does	not	always	take	you	on	the	shortest	route	,	but	it	will	get	you	where	you	are	going	and	if	you	are	directionally	challenged	like	i	am	,	it	becomes	indispensible	.
the	car	mounting	bracket	works	fine	.
it	's	popped	off	occasionally	in	hot	weather	,	but	generally	stays	put	.
the	bracket	would	be	easy	to	move	to	another	vehicle	.
the	gps	would	work	in	a	cup-holder	if	you	needed	to	use	it	in	a	rental	car	.
i	've	spent	more	time	learning	the	pda	portion	,	which	uses	windows	mobile	NUMBER	ORDINAL	edition	.
the	ique	uses	outlook	as	its	interface	,	so	you	will	have	to	have	it	installed	on	your	computer	.
not	having	used	outlook	before	,	i	installed	outlook	on	my	laptop	and	spent	some	time	getting	familiar	with	it	,	which	was	helpful	.
the	pda	has	so	many	functions	that	it	's	hard	to	even	describe	what	this	little	machine	can	do	.
it	will	organize	all	of	your	calendar	functions	and	contacts	,	checks	e-mail	,	accesses	the	internet	if	you	get	a	wi-fi	card	,	recognizes	handwriting	,	has	a	voice	recorder	,	interfaces	with	other	bluetooth	units	,	sends	alarms	,	has	word	and	excel	,	even	has	a	couple	of	good	games	.
i	got	a	separate	sd	card	for	music	and	downloaded	a	lot	of	songs	,	and	the	sound	through	headphones	is	quite	acceptable	(	it	has	bass	boost	)	.
it	uses	windows	media	player	so	you	can	organize	all	of	your	playlists	.
you	can	download	any	programs	that	are	available	for	windows	mobile	NUMBER	,	including	tv	shows	and	videos	.
i	never	thought	i	'd	find	much	use	for	a	pda	but	i	am	using	it	every	day	.
no	more	little	notes	on	scraps	of	paper	or	a	separate	pocket	calendar	,	address	book	or	even	an	ipod	.
everything	is	in	one	handy-dandy	little	machine	.
and	even	though	it	is	not	that	intuitive	,	once	you	've	learned	the	basic	conventions	they	translate	to	most	of	the	programs	.
we	've	called	garmin	tech	support	a	few	times	and	,	although	they	are	only	open	during	regular	business	hours	,	we	found	american	staff	who	are	very	knowledgeable	about	the	product	.
since	a	good	gps	will	run	at	least	[	DOTS	]	,	this	was	a	great	bargain	at	around	[	DOTS	]	we	've	purchased	screen	protectors	and	are	hoping	it	wo	not	wear	out	too	quickly	,	as	i	've	heard	palm	pilots	tend	to	do	.
i	am	really	enjoying	the	ique	.