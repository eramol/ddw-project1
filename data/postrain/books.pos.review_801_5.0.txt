one	of	the	most	interesting	books	i	've	ever	read	!
i	could	not	put	this	one	down	.
if	all	economics	books	were	written	this	well	,	everyone	would	want	to	be	an	economist	!
the	author	has	a	unique	perspective	on	the	analysis	of	data	that	is	very	compelling	.
he	will	make	you	question	every	fact	you	've	ever	heard	and	observe	the	world	around	you	from	a	different	angle