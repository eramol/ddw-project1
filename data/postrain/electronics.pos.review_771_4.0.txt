this	review	concerns	the	v3	hardware	and	firmware	v3.0_20	-	i	did	not	purchase	from	amazon	.
for	the	most	part	,	everything	works	pretty	well	.
i	have	discovered	a	bug	where	the	log	is	not	cleared	after	a	scheduled-email	out	,	which	leads	to	strange	behavior	for	the	log-emailing	function	.
i	can	always	go	into	the	web-interface	to	view	or	clear	it	with	no	problem	.
another	thing	to	note	regarding	the	log	is	that	vpn	(	not	pass-thru	)	has	its	own	separate	log	area	,	which	does	not	get	emailed	out	.
as	expected	,	netgear	support	is	mediocre	.
level	NUMBER	support	can	only	handle	very	simple	issues	and	level	NUMBER	support	is	very	slow	.
i	recommend	you	use	the	incident-submission	area	of	the	my	netgear	website	,	so	you	can	more	easily	track/respond	to	support	incidents	.
for	those	of	you	using	the	fr114p	model	,	this	is	basically	the	same	thing	with	8-connection	vpn	service	minus	the	printer	port	.
i	figure	,	after	a	few	more	firmware	revisions	,	this	will	be	a	really	good	unit