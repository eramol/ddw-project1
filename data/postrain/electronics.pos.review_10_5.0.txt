after	going	through	the	reviews	,	i	bought	this	cf	card	for	my	canon	digital	rebel	.
so	far	it	has	worked	fine	,	though	i	do	not	pretend	to	be	an	expert	digital	photographer	.
when	the	card	is	empty	,	it	shows	NUMBER	shots	available	.
it	seems	to	read	reasonably	fast	,	though	it	takes	a	bit	longer	than	i	was	used	to	with	my	point-and-shoot	olympus	and	its	smartmedia	card	.