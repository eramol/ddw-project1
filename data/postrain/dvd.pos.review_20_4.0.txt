i	grabbed	this	flick	from	the	shelf	on	a	very	wet	and	miserable	day	at	my	local	target	DOTS	.	craving	the	summer	sun	and	needing	an	island	vacation	DOTS	.
the	cover	appealled	to	me	for	these	reasons	and	i	admit	i	was	not	at	all	disappointed	in	the	movie	.
the	scenery	was	indeed	spectacular	.
the	underwater	diving/snorkling	scenes	were	clear	,	crisp	,	and	just	what	the	doctor	ordered	.
even	the	sharks	and	rays	made	it	all	quite	beautiful	.
ok	,	enough	of	the	shallow	stuff	DOTS	.	the	movie	was	indeed	much	like	the	NUMBER	's	flick	the	deep	which	i	enjoyed	back	then	as	well	.
the	characters	of	jared	and	sam	were	pretty	believable	-	poor	,	honest	,	and	happy	folks	(	most	of	us	would	love	to	really	be	this	happy	-	let	's	face	it	!	)
the	introduction	of	bryce	(	played	by	scott	caan	)	really	created	an	interesting	balance	of	characters	and	how	each	reacted	to	the	many	dangers	,	twists	,	etc	.
overall	,	the	bikinis	and	the	buff	guys	added	to	the	adventure	many	of	us	would	love	to	have	in	our	generally	boring	lives	.
would	i	want	to	deal	with	tiger	sharks	?
no	,	but	a	little	excitement	would	beat	the	heck	out	of	my	boring	9-5	job	every	day	.