i	got	one	of	these	a	few	years	ago	.
it	's	been	to	several	countries	,	on	camping	trips	,	bicycle	trips	,	subways	,	etc	etc	and	held	together	very	well	.
i	pop	this	and	the	handspring	into	my	pocket	and	off	i	go	,	not	lugging	around	a	huge	laptop	anymore	.
the	typing	is	comfortable	,	this	is	just	like	a	laptop	keyboard	,	and	while	i	too	have	worried	about	the	little	popup	part	where	you	connect	the	handspring	,	it	's	taken	a	beating	so	far	.
i	am	buying	another	preemptively	because	i	will	be	so	bummed	when	i	do	eventually	manage	to	break	the	current	one	.
the	hinge	in	the	middle	is	annoying	-	you	have	to	put	the	thing	on	a	book	or	other	flat	surface	to	hold	it	in	your	lap	.
i	think	that	when	the	new	one	comes	,	i	will	try	to	create	some	kind	of	deadbolt	mechanism	to	hold	the	middle	hinge	when	it	's	open	and	then	it	will	be	the	single	most	perfect	gadget	ever