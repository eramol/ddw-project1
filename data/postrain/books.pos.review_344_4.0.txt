only	my	second	james	patterson	book	,	when	the	wind	blows	,	will	keep	me	coming	back	for	more	.
i	was	not	disappointed	and	look	forward	to	reading	many	more	of	his	novels	.
frannie	,	a	veterinarian	,	and	kit	,	an	fbi	agent	find	themselves	thrown	together	in	the	hopes	of	saving	a	few	very	special	children	.
surrounded	by	some	mysterious	deaths	in	the	past	few	years	,	including	her	own	husbands	,	frannie	now	is	trying	to	stop	yet	another	murder	.
it	all	begins	when	she	thinks	she	sees	a	girl	with	wings	in	the	forest	near	her	home	in	colorado	.
from	here	we	learn	of	genetic	experiments	and	more	heinous	crimes	than	those	.
this	book	was	written	prior	to	the	much	acclaimed	and	popular	lakehouse	with	many	of	the	same	characters	.