like	pretty	much	anything	in	which	neil	's	involved	,	this	a	fine	and	useful	work	.
the	reviews	of	teddy	millon	's	axis	ii	taxonomic	extensions	are	worth	most	of	the	considerable	price	of	admission	here	all	by	themselves	.
but	after	NUMBER	years	in	the	field	,	i	will	warn	any	newcomers	that	getting	any	too	rigid	with	treatment	plan	rollouts	will	make	you	as	nuts	at	the	people	you	are	treating	.
as	-guidelines-	with	notions	about	starting	points	and	signposts	along	the	way	,	however	,	these	are	very	useful	.
neil	's	six-step	treatment	outline	seems	a	bit	oddly	ordered	,	and	i	was	not	able	to	find	a	rationale	for	determining	diagnosis	in	the	final	step	(	when	you	have	to	lay	one	on	the	client	's	payment	guarantor	at	the	outset	)	.
but	otherwise	,	the	six	steps	are	another	handy	construction	among	several	now	available	.
if	you	are	fairly	new	in	the	field	,	this	will	be	a	real	handy	reference	,	but	do	not	stake	the	clients	lives	(	or	your	own	serenity	)	on	this	when	they	start	to	go	refractive	.