annie	hall	is	one	of	the	most	acclaimed	romantic	comedies	of	all	time	.
the	film	not	only	holds	a	place	on	the	american	film	institute	's	top	NUMBER	(	#	NUMBER	)	,	as	well	as	the	american	film	institute	's	top	NUMBER	comedies	(	#	NUMBER	)	.
the	film	took	home	NUMBER	academy	awards	including	best	picture	and	best	director	;	the	film	is	also	without	a	doubt	one	of	the	best	romantic-comedies	ever	made	.
it	really	nails	that	awkwardness	of	romance	.
i	think	few	of	us	are	like	the	neurotic	woody	allen	and	i	do	not	so	much	mean	the	way	the	characters	act	,	but	the	way	they	think	and	stuff	like	that	is	totally	realistic	.
woody	allen	plays	alvy	singer	,	a	comeddian	who	always	knocks	it	out	of	the	park	when	it	comes	to	comedy	but	ca	not	really	get	a	grip	on	love	.
he	's	been	through	two	marriages	and	is	a	little	bit	neurotic	,	which	turns	people	off	.
then	his	friend	rob	(	tony	roberts	)	introduces	him	to	annie	hall	(	the	wonderful	diane	keaton	)	.
annie	is	a	little	bit	ditzy	and	not	quite	as	smart	as	alvy	,	but	the	two	click	and	begin	a	relationship	that	(	to	say	the	least	)	has	it	's	share	of	ups	and	downs	.
now	,	not	only	is	the	plot	and	the	dialogue	and	everything	true-to-life	but	this	film	is	also	absolutely	hilarious	.
when	i	see	a	movie	on	afi	's	top	NUMBER	comedies	,	i	tend	to	shy	away	from	the	films	on	that	list	.
not	because	they	are	not	good	,	it	's	just	that	they	do	not	really	pick	by	what	's	the	funniest	film	ever	.
example	,	'the	NUMBER	year	old	virgin	was	funnier	than	their	number	NUMBER	pick	some	like	it	hot	.
but	anyway	,	annie	hall	actually	is	very	funny	.
the	dialogue	is	quick-witted	,	some	the	scenes	have	become	classics	(	allen	sneezing	on	the	cocaine	)	.
in	an	era	where	romantic	comedies	run	rampant	into	movie	theatres	,	it	's	nice	to	go	back	and	see	the	films	that	really	made	the	genre	what	it	is	.
this	is	a	great	film	,	a	great	romantic-comedy	,	a	woody	allen	masterpiece	.
grade	:	a