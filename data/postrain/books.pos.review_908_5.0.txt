i	admire	an	author	who	deviates	from	a	standard	formula	and	the	interweaving	of	two	cases	really	works	in	piece	of	my	heart	.
di	stanley	chadwick	faces	a	murder	case	in	YEAR	against	the	backdrop	of	a	rock	'n	roll	concert	.
his	teenage	daughter	has	facts	to	hide	and	di	chadwick	walks	a	fine	line	.
dci	alan	banks	is	seeking	a	murderer	in	NUMBER	which	eventually	connects	with	the	YEAR	murder	.
the	plot	has	already	been	outlined	,	so	i	will	just	say	it	's	a	delight	to	visit	dci	banks	again	.
i	am	never	disappointed