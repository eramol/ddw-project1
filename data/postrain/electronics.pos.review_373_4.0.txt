this	is	the	ultimate	ears	super	fi	model	especially	designed	for	bassheads	,	featuring	a	disproportionately	large	bass	driver	complemented	by	a	more	conventional	driver	for	the	middle	and	high	frequencies	.
i	do	not	think	ue	would	ever	claim	that	what	results	is	a	balanced	sound	signature	.
yes	,	the	mids	and	highs	are	present	and	the	quality	is	clean	and	reasonably	detailed	,	but	these	frequencies	are	definitely	recessed	and	with	some	kinds	of	music	this	can	result	in	an	unnatural	kind	of	reproduction	.
on	the	bright	side	,	however	,	the	bass	reproduction	is	marvelous	.
not	only	is	the	quantity/extent	of	the	bass	frequencies	as	powerful	as	any	headphones	or	earphones	i	've	ever	used	,	the	quality	is	excellent	.
the	bass	is	tight	and	pleasant	,	neither	boomy	nor	thumpy	.
this	overbalance	toward	bass	frequencies	renders	the	ue	NUMBER	eb	's	ideal	for	use	when	listening	to	music	in	which	the	bass	is	presented	front	and	center	,	including	techno	bass	(	aka	car	audio	bass	)	,	drum	n	bass	,	and	many	types	of	rap/hip-hop	.
for	music	in	which	the	basslines	are	prominent	but	comprise	part	of	a	more	balanced	presentation	overall	,	opinions	will	vary	.
with	rock	music	,	for	example	,	the	strong	bass	is	certainly	a	plus	,	but	i	have	found	that	sometimes	the	bass	overpowers	the	middle	frequencies	too	strongly	,	and	some	detail	is	lost	.
it	should	be	noted	that	this	bass-dominated	sound	signature	renders	the	eb	's	a	good	choice	for	people	who	own	ipods	and	feel	that	their	music	player	is	weak	on	bass	.
without	any	eq	adjustment	,	the	ipod	suddenly	is	transformed	into	a	player	where	the	bass	is	present	.
in	terms	of	build	,	the	eb	's	superficially	seem	well-constructed	and	designed	.
i	have	heard	complaints	,	however	,	that	the	thinness	of	the	wires	and	earpieces	render	them	vulnerable	to	breakage	.
be	careful	out	there	!
thoughtfully	,	eb	has	includes	a	variety	of	ear	tips	with	these	iem	's	(	including	foam	,	flanged	,	and	plain	)	so	that	users	with	different	sized	ear	canals	can	get	a	good	fit	.
the	eb	's	do	not	extend	deeply	into	the	ear	canal	,	which	is	a	mixed	blessing	.
on	the	one	hand	,	this	means	that	if	you	get	a	good	fit	of	the	earpiece	into	the	entrance	to	the	ear	canal	,	comfort	is	good	and	isolation	decent	.
however	,	if	the	fit	is	not	quite	right	both	comfort	and	isolation	will	prove	frustrating	.
making	the	right	choice	of	ear	tip	is	absolutely	essential	when	using	the	eb	's	.
oh	,	and	some	people	think	that	the	eb	's	look	kind	of	strange	when	worn	.
larger	than	the	higher-priced	super	fi	NUMBER	pro	model	,	the	eb	's	stick	out	a	bit	.
your	call	as	to	whether	this	is	a	problem	.
like	all	of	the	ue	iem	's	in	this	series	,	the	eb	's	are	packaged	with	a	nice	set	of	accessories	,	including	a	1/4	adapter	plug	,	a	sound	attenuator	,	a	soft	travel	case	,	an	earwax	loop	,	and	a	silvery	metal	storage	case	which	includes	a	winder	that	helps	head	off	what	is	always	a	problem	with	iem	's	,	cord	tangling	.
it	's	unlikely	that	the	eb	's	will	prove	satisfactory	as	your	everyday	earphone	unless	you	listen	solely	to	bass-dominant	types	of	music	.
the	truth	is	,	however	,	that	even	for	bass-heavy	music	the	overall	sound	signature	of	the	super	fi	NUMBER	pros	is	likely	to	be	more	satisfactory	for	most	users	(	i	own	both	)	.
but	as	a	second	pair	specifically	for	use	with	bass-y	music	,	the	eb	's	are	well-designed	and	definitely	fun