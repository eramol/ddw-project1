we	know	some	things	about	saul	of	tarsus	(	later	named	the	apostle	paul	)	but	a	complete	biography	is	always	lacking	because	of	sketchy	and	incomplete	information	.
most	everything	we	know	from	paul	is	from	the	dr.	luke	in	the	book	of	acts	or	paul	's	epistles	themselves	.
ii	peter	chapter	NUMBER	makes	some	very	intriguing	statements	.
peter	,	in	some	of	his	last	earthly	words	before	martyrdom	,	validates	paul	's	apostleship	and	states	that	some	of	his	writings	and	teachings	are	difficult	but	that	we	should	follow	them	.
pollock	's	book	is	presented	as	documentary-style	reading	since	paul	's	complete	biography	is	not	entirely	clear	.
there	is	much	the	author	states	is	inference	or	speculation	.
if	you	can	respect	that	and	not	build	doctrine	and	facts	on	where	the	evidence	is	fuzzy	,	i	think	this	is	an	excellent	book	.
one	example	of	fuzzy	facts	that	pollock	considers	is	the	age-old	controversy	over	the	authorship	of	the	book	of	hebrews	.
the	author	of	hebrews	does	not	identify	himself	and	scholars	argue	who	wrote	it	.
pollock	speculates	that	paul	,	while	imprisioned	in	acts	ch	.
NUMBER	for	two	years	may	have	had	time	and	motivation	to	write	the	book	to	the	people	he	so	desparately	wanted	to	reach	.
with	his	imprisonment	and	his	controversial	style	among	the	jews	,	he	may	have	left	his	identity	anonymous	so	he	himself	would	not	be	a	stumbling	block	to	reach	his	jewish	brothers	.
eternity	will	reveal	who	wrote	the	book	of	hebrews	but	it	is	certainly	a	plausible	theory	.
overall	,	the	greatest	benefit	in	reading	this	book	is	to	provide	a	better	framework	for	understanding	paul	as	he	travels	among	the	early	churches	and	wrote	letters	to	them	correcting	the	various	false	teachings	and	problems	that	developed	.