i	think	one	of	my	major	problems	was	the	level	of	opinion	being	shotat	the	reader	in	the	book	.
but	then	at	times	that	made	the	book	moreentertaining	-	its	acerbic	opinion	of	the	right	,	how	reagan	gettingelected	made	her	feel	stupid	about	being	american	,	a	funny	british	couple	.
the	description	of	small	details	from	each	of	the	characters	childhood	was	something	icould	relate	to	because	i	think	of	that	myself	.
there	were	clevertidbits	from	anthropology	,	and	economics	,	and	generally	all	fields	ofintellectual	inquiry	,	all	somehow	connected	with	the	patchwork	of	loveand	relationships	,	and	thus	brain	meets	heart	and	you	end	up	withsomething	almost	like	the	bible	for	our	times	.
the	focus	on	the	maincharacter	made	it	so	that	every	detail	about	him	was	described	.
anyway	,	i	am	rambling	,	so	,	a	note	about	the	ending	:	i	did	find	myself	thinking	this	is	getting	very	strange	as	the	bookgot	towards	the	ending	.
it	might	have	made	more	sense	if	nelsonactually	was	interested	in	the	new	girl	the	narrator	finds	for	him	,	and	the	story	then	comes	full	circle	.
it	would	end	in	a	nice	ironicsort	of	way	.
however	,	his	rejecting	her	makes	in	my	mind	for	whatwould	be	a	bad	traditional	ending	.
i	do	not	mind	this	,	since	the	onetime	i	wrote	a	story	for	a	creative	writing	course	i	ended	up	with	aterrible	traditional	ending	.
now	,	could	he	have	been	anything	otherthan	a	mystic	?
i	felt	that	there	was	some	hint	towards	hinduism	andspirituality	building	up	towards	the	end	.
he	mentioned	india	andhinduism	unexpectedly	and	repeatedly	at	one	point	in	the	book	.
so	hisbecoming	a	mystic	was	alluding	perhaps	to	the	ashramas	-	and	histransition	from	intellectual	worker	to	a	mystical	worker	to	eventuallya	sage	,	as	he	attains	nirvana	and	becomes	the	buddha	of	our	times	.
soin	this	way	an	ending	emphasizing	the	spiritual	,	is	perhaps	the	best	.
in	line	with	the	book	,	the	spiritual	is	the	eternal	,	and	onlyaccessible	through	life-changing	(	near-death	)	experience	.
what	you	dofrom	25-50	(	making	a	model	village	in	rural	africa	)	is	transient	(	cannever	be	perfect	;	infighting	;	hunting	;	guns	;	the	dark-side	of	humannature	will	come	back	even	in	utopia	;	i	like	the	flexible	hours	;	workwhen	you	want	;	accumulate	credits	;	do	manual	labor	once	in	a	while	;	start	the	day	with	reading	,	not	the	end	when	you	are	tired	;	siesta	,	fiesta	etc	.	)
no	matter	how	good	you	are	.
the	girl	herself	is	secondaryto	the	book	-	ironic	since	the	book	promotes	feminism	,	is	written	by	aguy	with	a	female	narrator	but	a	male	protagonist	,	who	she	is	fixatedon	and	has	doubts	about	.
but	in	the	end	the	jesus	character	is	stillmale	.
while	the	female	goes	back	to	palo	alto	and	takes	an	average	jobin	service	of	the	savior	.
the	man	has	the	final	laugh	.
do	you	favor	a	better	ending