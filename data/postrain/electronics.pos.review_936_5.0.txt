this	does	exactly	what	it	is	supposed	to	do	.
it	is	light	,	easy	to	pack	,	easy	to	use	,	and	probably	difficult	to	crack	.
(	though	would-be	pc	pilferers	have	not	tried	taking	a	hacksaw	to	my	cable	lock	so	i	can	not	comment	as	to	its	ability	to	withstand	saws	,	blow-torches	,	or	any	other	criminal	's	tool	.
)	other	reviewers	baffled	me	as	mine	is	as	described	by	amazon	.
there	is	neither	an	alarm	nor	batteries	.
why	bother	with	an	alarm	if	the	pc	is	locked	with	steel	cable	?
seems	like	overkill