i	recently	began	upgrading	my	home	audio	by	buying	at	ebay	NUMBER	pairs	of	jbl	e20s	and	NUMBER	pair	e30s	,	an	e25	center	speaker	and	a	harmon	kardon	avr	.
i	got	the	first	pair	of	e20s	a	week	ago	and	the	ORDINAL	pair	of	e20s	and	the	e30	yesterday	and	the	remaining	stuff	should	be	delivered	by	the	weekend	.
the	e20s	and	e30s	are	listed	as	bookshelf	speakers	.
the	e20s	are	bookshelf	speakers	.
as	i	wrote	in	my	review	of	the	e20s	,	the	e30s	can	only	be	thought	of	as	bookshelf	speakers	if	you	last	name	is	kong	and	your	first	name	is	king	!
the	e20s	are	heavy	at	7lbs	each	,	not	flimsy	little	boxes	as	some	cheapo	bookshelves	are	.
the	e30s	are	heavy	.
heavy	!
NUMBER	lbs	each	.
(	that	is	what	my	NUMBER	qt	lodge	cast	iron	dutch	oven	weighs	!	)
they	are	NUMBER	inches	high	and	NUMBER	inches	deep	.
they	need	to	be	placed	on	small	end	tables	or	speaker	stands	NUMBER	to	NUMBER	inches	off	the	floor	.
i	am	very	pleased	with	both	the	e20s	and	e30s	.
for	small	speakers	,	the	e20s	have	some	oomph	and	can	be	cranked	up	.
the	e30s	have	ooommphhhh	.
they	want	to	be	cranked	up	.
yesterday	i	tried	them	out	by	listening	to	my	mp3	files	of	the	animals	singles	and	paul	revere	and	the	raiders	kicks	.
when	cranked	up	,	the	e30s	sound	at	least	YEAR	%	better	than	the	the	e20s	.
i	hooked	them	up	to	my	vintage	jvc	av	receiver	circa	YEAR	that	has	NUMBER	watts	front	and	NUMBER	watts	rear	power	.
at	a	little	over	half	volume	my	ears	began	to	hurt	.
you	can	really	feel	the	bass	and	the	drums	kick	.
i	also	listened	to	about	NUMBER	hours	of	the	doors	via	rhapsody	with	the	audio	out	from	my	computer	to	the	audio	in	on	my	receiver	.
i	have	been	listening	to	this	music	since	it	was	first	played	on	the	airwaves	back	in	the	YEAR	.
the	e30s	are	simply	incredible	!
they	brought	some	very	familiar	tunes	to	life	and	made	them	new	and	fresh	.
my	biggest	surprise	with	the	e30s	is	that	they	sound	very	good	at	lower	volumes	,	not	as	good	as	the	e20s	,	but	still	very	good	.
as	i	write	,	i	am	watching/listening	to	a	dvd	of	vivaldi	's	NUMBER	seasons	concertos	performed	by	herbert	von	karajan	,	the	bpo	and	anne-sophie	mutter	on	fiddle	and	am	running	the	e30s	and	e20s	at	about	NUMBER	%	volume-pretty	low	since	the	NUMBER	speakers	are	sharing	the	front	NUMBER	watts	of	my	receiver	.
yet	i	can	still	hear	everything	at	low	volumes	including	the	resonating	violins	and	the	harpsacord	.
since	we	normally	do	not	rock	the	house	down	all	the	time	and	we	do	often	listen	at	lower	levels	,	i	am	very	pleased	at	how	good	they	sound	without	having	to	blow	out	my	eardrums	.
i	bought	the	e30s	for	under	$	NUMBER	including	s/h	.
back	in	the	early	YEAR	or	late	YEAR	i	spent	more	than	four	times	this	amount	(	adjusted	for	inflation	)	for	NUMBER	large	,	heavy	advent	loudspeakers	with	NUMBER	woofers	.
the	e30s	clearly	sound	better	than	i	ever	remember	the	advents	sounding	including	better	bass	response	,	and	i	liked	the	advents	!
over	time	,	our	audio	choices	have	gotten	better	and	cheaper	.
you	still	can	spend	mega	bucks	and	buy	a	$	NUMBER	mcintosh	system	(	or	even	drop	that	much	on	a	turntable	magnetic	cartridge	as	i	saw	at	one	website	!	)
it	all	depends	on	what	your	budget	is	.
the	jbls	are	a	great	value	offering	tremendous	perfomance	for	those	who	are	budget	minded