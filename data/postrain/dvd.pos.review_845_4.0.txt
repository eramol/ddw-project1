this	dvd	shows	shania	's	potential	as	a	serious	musician	.
with	alison	krauss	and	union	station	as	her	back	up	here	,	she	is	simply	riveting	.
too	bad	she	is	more	interested	in	making	a	quick	buck	these	days	marketing	herself	as	a	pop	star	and	making	purely	pop	albums	like	up	!
instead	of	making	enduring	popular	country	classics	like	the	woman	in	me	and	come	on	over	.
maybe	she	's	living	too	well	nowadays	to	make	soulful	music	.
get	this	dvd	if	you	want	to	hear	shania	doing	something	good.4	stars	is	for	the	short	length	of	this	dvd	.
we	need	at	least	NUMBER	hours	of	this	awesome	music	.
not	the	NUMBER	minutes	which	is	on	this	dvd	.
that	's	only	enough	time	to	drink	NUMBER	beers