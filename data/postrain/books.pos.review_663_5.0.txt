phyllis	chesler	is	a	most	unusual	feminist	.
she	really	does	not	fit	in	with	the	equity	feminist	crowd	and	is	a	complete	dissenter	from	the	female	superiority	,	gender	feminist	crowd	.
the	one	thing	that	makes	her	consistently	worth	reading	is	her	passion	for	telling	the	truth	.
this	seems	to	be	her	most	pervasive	(	and	endearing	)	feature	as	an	intellectual	,	and	the	trait	was	ubiquitously	on	display	throughout	her	recent	death	of	feminism	release	.
here	she	tackles	a	verboten	subject	which	is	whether	or	not	women	are	the	unbridled	source	of	goodness	that	many	feminists	assume	them	to	be	.
numerous	examples	of	misapplied	and	unprovoked	indirect	aggression	are	identified	and	elucidated	in	the	text	.
on	the	whole	,	it	makes	for	compelling	reading	.
we	find	that	women	are	regular	human	beings	just	like	everyone	else	.
they	have	many	of	the	same	,	yet	subtlety	different	,	characteristics	as	do	men	.
in	these	pages	,	chesler	illustrates	the	magnitude	of	inter-woman	hostility	as	their	strong	social	skills	can	often	be	applied	in	a	negative	manner	.
many	mother-daughter	conflicts	are	projected	into	adult	situations	.
why	feminists	would	not	have	wanted	this	published	seems	rather	silly	to	me	.
we	should	regard	women	as	they	are	and	not	as	objects	of	fantasy	.
chesler	never	implies	any	inter-sex	superiority	or	inferiority	,	she	is	content	to	describe	what	is	,	and	it	's	great	stuff	indeed	.