i	thought	i	'd	weigh	in	,	as	the	reviews	suggesting	noncompliance	with	apple	products	seemed	ominous	.
i	bought	a	passport	,	and	the	usb	outlet	on	my	macbook	powers	it	perfectly	well	.
i	do	not	need	an	external	power	supply	.
btw	,	i	get	a	transfer	rate	of	about	NUMBER	mb/s	,	or	NUMBER	gb/5	minutes	.
this	seems	a	little	slow	,	but	it	does	not	bother	me	much	.
the	passport	is	small	,	has	a	nice	rubber	coating	to	prevent	shock	,	and	overall	is	worth	the	purchase	price