i	found	this	series	to	be	fun	,	gritty	,	and	lifelike	.
the	characters	in	the	precinct	remind	me	so	much	of	guys	i	used	to	work	with	on	the	job	.
certainly	some	artistic	license	is	taken	,	but	for	the	most	part	,	this	is	a	slice	of	life	which	most	people	will	never	experience	in	their	day	to	day	jobs	.
denis	leary	has	risen	to	the	top	of	my	list	as	not	only	a	comedian	,	but	a	really	talented	character	actor	.
his	supporting	cast	fits	well	,	and	all	in	all	i	would	recommend	this	series	to	anyone	who	is	a	fan	of	rescue	me	,	or	denis	leary