luc	besson	's	the	professional	raises	the	stakes	of	besson	's	playful	women-with-guns	theme	by	making	the	heroine	a	12-year-old	,	played	by	a	then	unknown	natalie	portman	.
in	the	making	of	the	professional	besson	says	if	i	imagine	somebody	in	the	street	try	to	knock	on	my	daughter	,	i	kill	the	guy	,	in	five	seconds	.
i	kill	him	,	and	i	think	it	's	in	me	,	i	am	a	beast	!
on	this	part	we	ca	not	forget	that	a	part	of	us	,	the	genetic	things	inside	are	much	,	much	older	than	the	ten	commandments	.
he	certainly	uses	visceral	scenes	to	create	very	strong	emotion	in	the	movie	-	the	blood	running	from	mathilda	's	nose	or	stansfield	's	unforgettable	everyone	!
are	just	a	couple	of	examples	.
the	music	and	the	sound	are	good	and	are	used	in	a	masterly	fashion	-	you	can	hear	fat	man	's	heart	beating	desperately	or	a	low	claustrophobic	sound	when	stansfield	turns	to	look	at	mathilda	's	father	.
leon	however	does	not	work	only	on	this	primary	level	;	it	also	has	an	intelligent	story	.
it	may	seem	to	be	almost	a	fairy-tale	,	but	do	not	be	fooled	-	just	like	his	character	besson	is	serious	.
this	movie	has	a	message	:	without	love	we	are	dead	,	even	if	we	do	not	see	it	.
only	true	love	give	meaning	to	our	lives	:	everything	else	reminds	me	a	big	yogurt	:	warm	and	rancid	as	mathilda	says	in	the	original	script	,	which	is	available	on	the	net	under	the	name	leon	version	NUMBER	.
is	this	true	in	real	life	?
i	do	not	know	but	this	movie	can	make	you	wonder	.
then	of	course	there	's	the	sensuality	.
it	's	hypocritical	to	deny	it	;	the	camera	interacts	with	mathilda	in	a	mesmerizing	fashion	.
it	's	not	sick	and	it	's	not	degrading	:	its	art	,	subtle	and	beautiful	.
leon	is	not	perfect	but	it	has	so	many	great	moments	that	all	its	flaws	can	be	forgiven	.
it	's	a	movie	that	really	should	not	be	missed	,	unless	you	are	concerned	with	its	amorality	.
and	do	not	be	-	leon	is	less	violent	than	many	action	movies	and	the	unusual	relationship	between	the	main	characters	is	handled	mostly	with	genuine	feeling	and	tact	.