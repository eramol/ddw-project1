ok	folks	,	this	is	by	far	one	of	the	best	dvds	i	've	bought	in	quite	awhile	.
you	get	so	much	for	so	little	.
first	you	get	the	original	black	and	white	movie	.
reefer	madness	is	like	the	three	stooges	,	black	and	white	just	adds	to	its	charm	.
but	to	see	it	in	color	is	quite	a	hoot	.
the	smoke	is	purple	and	pink	,	the	suits	and	dresses	are	loud	,	and	the	color	scheme	for	mae	's	apartment	is	just	mind	boggling	.
what	more	could	you	ask	for	?
well	you	get	more	anyway	.
when	i	discovered	that	mike	nelson	from	mst3000	fame	actually	had	narrating	duty	,	i	just	had	to	buy	reefer	madness	again	.
he	's	hilarious	.
sure	it	would	have	been	great	for	his	two	robot	buddies	to	join	in	,	but	who	knows	,	maybe	someday	.
mike	was	right	on	the	ball	and	i	laughed	out	loud	quite	a	few	times	.
i	never	noticed	before	how	much	time	the	dope	dealer	spent	making	meals	and	eating	.
well	if	that	was	not	enough	,	we	also	get	a	short	NUMBER	minute	film	starring	grandpa	ganja	.
he	's	a	marijuana	expert	who	's	been	smoking	pot	for	over	NUMBER	years	.
he	quotes	weed	from	the	bible	,	convincingly	explains	how	moses	was	a	major	dopehead	,	and	also	the	disciples	.
not	only	do	we	get	an	indepth	history	on	marijuana	,	he	demonstrates	how	to	roll	a	joint	,	smoke	a	bong	,	and	other	helpful	hints	on	how	to	administer	medical	thc	.
the	old	man	is	hilarious	on	helpful	hints	as	to	buying	and	growing	your	marijuana	.
when	they	showed	the	real	old	dude	throwing	marijuana	seeds	over	his	fence	and	then	harvesting	pot	in	his	neighbor	's	yard	i	lost	it	.
low	brow	pot	humor	true	,	but	in	any	case	i	laughed	my	butt	off	.
if	you	already	have	a	black	and	white	copy	of	reefer	madness	,	it	's	worth	buying	again	to	have	this	edition	.
it	's	well	worth	it