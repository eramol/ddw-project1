i	evolved	into	a	lower	carb	nutrition	by	finding	first	that	i	needed	more	protein	than	i	had	thought	and	that	i	reacted	strongly	to	sugars	.
i	bought	this	book	when	i	decided	to	lose	a	few	pounds	,	but	wanted	to	understand	any	health	implications	related	to	a	low	carb	diet	.
i	was	already	having	gall	bladder	and	liver	issues	.
i	bought	this	book	first	and	NUMBER	years	later	,	it	is	still	the	best	reference	i	have	.
the	chapter	on	hormones	is	a	bit	technical	,	but	this	chapter	alone	is	worth	the	price	of	the	book	.
within	three	days	of	eliminating	grains	from	my	diet	,	i	no	longer	cared	whether	or	not	i	lost	weight	.
my	awful	blood	sugar	drops	and	painful	bloating	were	gone	,	my	energy	increased	significantly	and	within	weeks	i	noticed	that	the	gall	bladder	pain	was	gone	(	and	it	has	not	come	back	)	.
within	three	months	my	intense	hot	flashes	disappeared	and	i	started	sleeping	better	.
the	book	first	describes	what	low	carb	nutrition	is	,	then	goes	on	to	discuss	hormones	and	how	insulin	impacts	the	overall	balance	of	the	body	's	hormones	in	different	ways	.
it	addresses	overall	energy	,	diabetes	,	heart	disease	,	cancer	,	gastrointestinal	disorders	,	vitamins	and	minerals	,	and	weight	control	(	one	chapter	-	the	rest	is	on	health	issues	)	.