we	bought	this	unit	from	sam	's	club	right	before	a	trip	to	disney	world	.
(	NUMBER	hours	in	the	car	with	a	toddler-eek	!	)
however	,	it	was/is	perfect	!
we	still	use	it	for	trips	around	town	when	we	are	in	massive	traffic	or	our	son	is	being	really	fussy	.
the	case	that	keeps	the	dvd	player	on	the	back	of	the	seat	needs	a	little	work	,	but	we	figured	out	a	way	to	make	it	work	for	us	.
also	,	the	remote	does	not	work	well	,	but	we	do	not	really	need	to	use	it	as	a	dvd	usually	lasts	long	enough	between	stops