introduction3	games	to	glory	iii	is	a	two-disc	video	compilation	of	the	NUMBER	new	england	patriots	playoffs	,	including	their	super	bowl	victory	over	the	philadelphia	eagles	24-21	in	super	bowl	xxxix	.
this	super	bowl	victory	was	the	patriots	third	in	four	years	,	marking	them	as	the	team	of	`00s	.
for	new	england	patriots	fans	it	's	a	chance	to	relive	the	glory	of	back-to-back	super	bowl	victories	and	three	championships	in	four	years	.
non-new	england	patriots	fans	probably	would	not	particularly	enjoy	this	in-depth	video	.
main	features3	games	to	glory	iii	provides	extensive	highlights	of	both	new	england	patriots	playoff	wins	and	super	bowl	victory	after	the	NUMBER	season	.
these	playoffs	found	new	england	once	again	schooling	the	indianapolis	colts	in	yet	another	year	where	peyton	manning	and	the	vaunted	colts	offensive	choke	under	playoff	pressure	.
the	high	flying	colts	offense	was	throttled	by	the	superior	schemes	and	tough	play	of	the	patriots	in	a	20-3	afc	divisional	playoff	win	at	gillette	stadium	in	foxborough	,	massachusetts	.
this	is	followed	by	new	england	's	dramatic	defeat	of	the	pittsburgh	steelers	,	41-27	,	in	the	afc	championship	game	at	heinz	field	in	pittsburgh	.
the	patriots	entered	this	game	as	decided	underdogs	to	the	15-1	steelers	after	being	throttled	by	pittsburgh	earlier	in	the	season	.
finally	,	you	get	to	enjoy	the	extensive	highlights	of	new	england	's	impressive	24-21	win	over	the	nfc	champion	philadelphia	eagles	.
this	is	yet	another	game	where	new	england	's	superior	execution	and	stamina	in	the	later	stages	of	the	game	brought	them	ultimate	glory	.
the	super	bowl	was	played	in	jacksonville	,	florida	on	february	NUMBER	,	NUMBER	.
the	highlights	of	each	game	are	very	extensive	and	seem	to	cover	nearly	every	play	.
they	are	also	mostly	well	done	,	but	frankly	the	highlights	often	fail	to	capture	the	real	drama	of	the	game	.
of	course	no	highlight	reel	can	match	the	excitement	and	drama	of	a	live	contest	,	but	there	are	times	when	key	turning	points	of	the	game	really	just	become	another	quick	hit	in	the	video	.
there	are	a	few	disappointments	in	this	video	though	and	it	's	not	quite	as	well	done	as	the	preceding	NUMBER	games	to	glory	ii	(	covering	the	patriots	playoff	run	and	super	bowl	victory	after	the	NUMBER	season	)	.
first	,	there	is	not	as	much	pre-game	drama	and	preparation	presented	in	these	videos	compared	to	last	year	.
secondly	,	there	are	not	as	many	close	up	,	on	the	field	shots	that	bring	the	game	right	into	your	living	room	.
it	's	much	more	distant	and	not	as	close	up	as	previous	editions	in	this	series	.
despite	these	drawbacks	,	it	's	still	great	to	relive	a	great	season	through	these	videos	.
bonus	featuresthe	bonus	features	on	this	edition	of	NUMBER	games	to	glory	are	not	nearly	as	good	as	the	ones	on	NUMBER	games	to	glory	ii	.
the	bonus	features	are	nice	they	just	did	not	live	up	the	standard	in	the	previous	edition	.
following	is	a	brief	rundown	of	what	you	will	get	on	the	dvd	.
there	are	some	bill	belichick	breakdowns	of	key	plays	for	each	game	but	there	are	very	few	of	them	and	really	leaves	the	viewer	wanting	more	.
there	's	also	a	points	after	:	sounds	of	the	game	which	shows	post-super	bowl	press	conference	features	,	but	again	,	it	's	really	short	and	leaves	you	wanting	more	.
the	patriot	's	all	access	which	appears	along	with	each	of	the	playoff	and	super	bowl	wins	shows	locker	room	scenes	and	practices	,	team	meetings	,	and	other	shots	of	players	as	they	prepare	for	games	.
these	are	pretty	well	done	short	features	that	add	a	great	deal	to	the	video	.
the	all	access	section	for	the	super	bowl	of	course	included	pre-game	plane	trip	,	post	game	party	and	the	parade	.
i	find	these	a	bit	boring	frankly	,	but	i	guess	it	's	nice	to	see	the	players	up	close	on	their	daily	routines	.
the	super	bowl	highlights	are	shown	twice	,	once	with	the	regular	commentary	and	once	with	commentary	from	rosevelt	colvin	(	lb	)	,	ted	johnson	(	lb	)	,	and	matt	light	(	t	)	.
matt	light	is	a	funny	guy	and	this	was	more	entertaining	than	the	one	last	year	.
i	thought	the	best	bonus	feature	was	one	on	disc	two	called	views	from	the	past	with	interviews	with	three	great	players	from	previous	decades	-	gino	cappelletti	of	the	YEAR	's	,	michael	haynes	of	the	YEAR	's	,	and	andre	tippett	of	the	YEAR	's	and	YEAR	's	.
this	conversation	was	mostly	about	comparing	this	current	patriots	team	with	teams	of	the	past	and	how	the	game	has	changed	over	the	decades	.
finally	,	the	video	has	the	NUMBER	playoff	roster	,	super	bowl	stats	,	and	NUMBER	game	by	game	results	-	just	like	you	can	get	in	any	book	,	it	just	shows	it	printed	on	the	screen	.
overall	,	while	all	these	bonus	features	are	nice	,	NUMBER	games	to	glory	iii	is	not	nearly	as	good	as	NUMBER	games	to	glory	ii	.
it	did	not	include	an	extensive	behind	the	scenes	segment	on	the	super	bowl	,	which	i	found	fascinating	on	the	previous	edition	,	nor	did	it	have	highlights	for	each	game	of	the	regular	season	like	the	previous	edition	.
i	found	the	lack	of	these	too	items	very	disappointing	as	i	thought	they	were	outstanding	editions	to	last	year	's	disc	.
nevertheless	,	the	bonus	features	overall	are	nice	,	just	nice	as	nice	as	last	time	.
bottom	lineall	new	england	fans	should	enjoy	reliving	a	third	super	bowl	win	through	this	highlights	package	.
it	's	mostly	well	done	and	certainly	brings	back	great	memories	of	a	great	season	.
those	who	are	not	patriots	fans	i	am	sure	would	find	the	video	quite	boring	.
disclaimer	:	i	am	a	die	hard	new	england	patriots	fan	and	am	totally	biased	in	this	review