if	you	are	looking	for	isolation	from	ambient	noise	,	these	are	great	.
i	use	them	as	in	ear	monitors	for	my	onstage	use	(	rock	band	)	,	and	if	i	use	the	foam	inserts	,	i	can	stand	right	in	front	of	the	horn	section	and	not	get	my	head	knocked	off	.
i	've	read	some	people	complain	that	the	bass	response	is	not	quite	what	they	expected	,	but	frankly	it	does	not	bother	me	a	bit	.
the	only	thing	that	's	awkward	is	removing	them	when	they	are	fully	inserted	for	a	while	.
they	are	rather	small	,	and	getting	hold	of	them	can	be	a	bit	tricky	.
i	am	using	small	bits	of	tape	for	pull	tabs	,	and	it	seems	to	work	pretty	well	.
overall	,	i	am	impressed	with	this	product	since	it	's	affordable	and	does	exactly	what	it	's	advertised	to	do