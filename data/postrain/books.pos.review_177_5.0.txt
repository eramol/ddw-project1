on	nation	,	underprivileged	is	a	timely	discussion	of	an	issue	that	impacts	us	all	.
in	his	book	,	professor	rank	carefully	crafts	a	compassionate	,	analytical	and	innovative	approach	for	addressing	poverty	in	our	country	.
this	is	a	must	read	for	all	(	especially	policy	makers	)	.