i	've	purchased	several	logitech	mice	(	cordless	optical	)	and	i	've	been	satisfied	with	all	of	them	,	except	for	the	battery	life	.
i	had	to	change	the	batteries	in	my	previous	mouse	once	a	month	.
this	new	logitech	mouse	is	terrific	.
it	's	been	several	months	and	i	have	not	had	to	change	the	batteries	yet	DOTS	and	i	am	not	even	using	the	on-off	switch	(	i	want	to	test	the	battery	life	without	the	switch	first	)	.
battery	issues	aside	,	the	mouse	is	smooth	,	light	and	accurate	.
it	was	simple	to	install	and	i	'd	buy	it	again	in	a	heartbeat