just	loved	this	book	!
enjoy	nora	roberts	books	but	this	one	is	now	at	the	top	of	my	list	.
it	is	so	suspenseful	you	can	hardly	wait	to	turn	the	next	page	to	see	what	happens	next	.
this	mystery/love	story	is	great	and	so	well	written	.
all	the	characters	are	so	well	developed	they	seem	to	come	to	life	and	just	about	jump	off	the	page	.
very	enjoyable	reading	!