i	was	stunned	at	how	much	i	enjoyed	this	book	.
i	had	avoided	it	for	years	since	i	was	not	a	big	fan	of	the	movie	.
this	book	really	explains	well	both	the	history	of	the	civil	war	(	troop	movements	,	etc	)	as	well	as	the	personalities	of	its	participants	.
it	is	an	intimate	portrayal	of	these	characters	,	especially	lee	and	jackson	.
i	found	it	to	be	quite	compelling	and	i	am	looking	forward	to	book	three	of	the	trilogy