engrossing	and	amusing	travelogue	by	world	's	crankiest	traveler	.
some	people	really	hate	theroux	,	accusing	him	of	snobbery	,	self	importance	,	and	bigotry	.
i	can	certainly	see	why	some	readers	would	not	be	able	to	deal	with	his	style	and	opinions	,	but	i	must	say	that	this	volume	might	be	a	pretty	good	litmus	test	of	readers	tolerance	for	the	author	's	travel	works	.
(	if	you	can	enjoy	this	one	,	you	will	probably	enjoy	his	other	non-fiction	works	)	.i	,	for	one	,	totally	enjoyed	hearing	about	grunge-y	has-been	towns	,	obnoxious	holiday	camps	,	and	seedy	hotels	.
to	me	,	the	book	was	a	real	page-turner	,	and	he	writes	so	vividly	of	scenery	.
i	felt	he	wrote	about	the	worst	bits	of	his	trip	with	true	humor	,	reminding	us	travelers	that	it	's	helpful	to	keep	a	sense	of	humor	during	the	rough	times	.
i	felt	the	book	was	a	pretty	decent	scouting	report	for	the	island	's	shoreline	,	and	i	now	know	what	to	avoid	there	.
i	can	definitely	thank	theroux	and	his	fussy	standards	!
if	you	like	theroux	,	it	's	a	good	bet	you	will	love	this	one	.