if	there	is	one	war	story	to	keep	on	your	shelf	besides	saving	private	ryan	,	this	is	it	.
the	story	starts	with	a	lawyer	visiting	england	as	a	tourist	years	after	war	's	end	.
he	discovers	a	cheap	,	ceramic	antique	which	the	store	owner	tells	him	is	of	little	value	.
value	?
replies	the	lawyer	.
wrap	it	very	carefully	he	adds	.
so	begins	his	mental	journey	back	in	time	to	the	english	airstrip	where	he	served	as	adjutant	of	the	ORDINAL	(	nine	,	eighteenth	)	bomb	group	.
this	hard	luck	group	is	taken	over	by	a	brigadier	general	from	a	colonel	who	has	over-identified	with	his	men	,	thus	putting	them	before	his	missions	.
the	general	,	frank	savage	,	must	restore	group	discipline	and	performance	before	the	group	disintegrates	as	an	effective	fighting	unit	.
general	savage	puts	mission	before	men	and	turns	the	disgruntled	men	who	despise	him	into	a	cohesive	unit	.
they	turn	into	a	group	that	will	do	anything	to	keep	from	being	left	behind	,	or	letting	down	their	new	leader	.
(	each	announcement	of	a	mission	for	the	following	day	is	characterized	by	the	operations	officer	going	to	the	mantel	piece	in	the	officers	club	,	and	turning	the	head	of	a	ceramic	pirate	,	face	outward	.
)	but	the	tough	general	will	not	make	the	same	mistake	that	the	last	group	commander	makes	.
he	will	always	keep	the	mission	first	,	and	will	not	over-identify	with	his	men	.
he	will	not	let	the	loss	of	his	men	affect	him	.
or	,	will	he	?	this	black	&	white	story	is	exceptional	and	superbly	acted	.
it	shows	the	mental	tug-of-war	a	leader	must	make	in	the	decisions	that	will	cost	the	lives	of	men	he	has	come	to	admire	and	respect	.
it	depicts	how	the	ugliness	of	war	brings	out	the	best	in	ourselves	,	and	creates	fraternal	bonds	that	last	a	lifetime	.
this	is	not	a	gory	story	,	but	it	is	one	that	will	leave	you	breathless	.
it	may	sadden	you	,	but	it	will	not	disappoint	you	.
it	does	not	end	with	everyone	living	,	or	happy	.
war	never	does	.
the	whole	nine	yards	refers	to	the	length	of	ammunition	linked	together	that	fed	into	the	machine	guns	aboard	each	bomber