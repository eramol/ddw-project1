(	just	as	a	note	,	that	's	the	japanese	title	)	there	's	something	about	this	movie	that	i	've	always	liked	.
i	am	not	sure	what	it	is	,	but	being	a	pokemon	fan	,	of	course	i	like	it	.
the	movie	have	always	been	great	,	and	this	one	has	something	that	i	really	like	about	it	.
the	movie	starts	out	in	the	north	pole	,	with	a	scientist	named	dr.	lund	,	his	friend	yuko	,	and	his	son	,	tory	.
there	is	suddenly	an	explosion	,	and	tory	is	trampled	by	a	frightened	pack	of	walrein	.
and	,	as	they	say	,	after	that	,	all	heck	broke	looks	.
a	meteor	is	the	reason	for	the	explosion	,	and	the	creature	inside	the	meteor	is	more	than	your	average	pokemon	.
deoxys	,	a	rare	pokemon	from	another	planet	(	woah	,	more	than	one	poke-planet	?
niiice	.	)
the	strange	creature	,	carrying	a	rock	with	a	gem	inside	,	is	suddenly	attacked	by	the	large	,	dragon-esque	rayquaza	,	a	frequenter	of	the	ozone	layer	.
so	,	what	the	heck	is	he	here	for	?	ah	,	so	rayquaza	feels	threatened	by	deoxys	,	and	so	,	both	pokemon	break	out	in	a	rather	spectacular	pokemon	battle	.
(	with	some	pretty	cool	background	music	,	might	i	add	.	)
so	,	professor	lund	flees	with	his	colleages	,	and	son	.
four	years	later	,	a	young	pokemon	trainer	named	ash	ketchum	,	along	with	some	friends	,	enter	the	high	tech	city	of	la	rousse	,	and	as	fate	would	have	it	,	tory	's	hometown	.
and	,	well	,	i	wo	not	spoil	it	from	there	.
the	plot	is	good	,	and	deoxys	'destructive	intentions	are	less	cruel	than	one	would	think	.
do	not	worry	,	folks	,	no	angsty	mewtwo-copy	here	.
also	,	rayquaza	brings	some	nice	battles	into	the	movie	.
his	fights	against	deoxys	are	totally	awesome	.
the	soundtrack	is	also	great	.
i	really	love	the	music	played	during	the	opening	scenes	of	the	movie	,	and	the	fights	between	rayquaza	and	deoxys	.
the	animation	is	very	spiffy	,	shiny	bright	and	new	:	p.	i	have	not	had	the	pleasure	of	watching	the	original	japanese	version	of	the	movie	,	but	when	i	do	,	i	will	tell	you	all	about	it	,	'kay	?	well	,	i	guess	that	pretty	much	wraps	things	up	.
oh	yes	,	tory	's	afraid	of	pokemon	,	due	to	the	whole	'get	attacked	by	a	herd	of	walrein	thing	.
heh	.
okay	then	,	sayanora	,	faithful	readers	!