i	've	had	this	controller	for	over	a	year	now	,	and	it	still	works	great	and	reliably	as	ever	.
after	many	months	,	the	analog	sticks	occasionally	drifted	a	little	,	but	a	quick	jiggle	and	they	'd	be	back	to	center	.
the	profiler	software	is	excellent	,	allowing	you	to	fully	map	the	controller	to	your	keyboard	and	even	mouse	.
this	allows	you	to	play	any	game	with	the	controller	,	even	ones	which	do	not	naturally	support	gamepads	like	splinter	cell	.
with	the	shift	profile	function	,	you	can	effectively	double	the	number	of	buttons	which	i	found	great	for	playing	a	complicated	flight	sim	like	x-wing	alliance	,	completely	keyboard	free	,	using	only	the	gamepad	.
only	downside	:	the	d-pad	is	hard	to	control	precisely	.
it	's	difficult	to	press	only	up	,	down	,	left	or	right	.
you	almost	always	end	up	pressing	a	diagonal	direction	instead	.
this	made	the	d-pad	more	or	less	unusable	for	games	like	street	fighter	,	but	then	the	analog	stick	was	a	perfect	alternative	anyway