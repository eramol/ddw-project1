ok..	here	's	the	deal	.
i	've	been	an	avid	magellan	user	for	the	past	NUMBER	years	.
i	love	my	magellan	.
i	've	never	liked	the	garmins	because	of	their	clumsy	interface	(	the	buttons	placed	above	the	display	never	made	sense	to	me	)	and	how	the	menu	structure	was	built	.
however	DOTS	.	we	are	planning	a	NUMBER	mile	road	trip	from	utah	up	to	the	oregon	coast	.
we	needed	something	with	expandable	memory	so	we	could	upload	more	than	one	state	map	at	a	time	.
my	wife	bought	me	the	magellan	explorist	NUMBER	for	fathers	day	.
i	forced	myself	to	use	it	for	two	days	(	and	believe	me	,	it	was	torture	)	.
i	could	go	on	and	on	about	why	i	sent	it	back	.
what	i	ordered	as	a	replacement	was	the	gpsmap	60cx	.
now	keep	in	mind	that	i	felt	like	someone	who	had	turned	to	the	dark	side	.
i	could	not	be	happier	with	my	decision	.
while	i	agree	with	most	of	the	other	reviews	about	the	lack	of	base	maps	,	and	the	expense	of	additional	ones	,	this	unit	rocks	!
it	acquires	satellites	within	seconds	.
it	has	expandable	memory	.
it	has	a	color	screen	.
it	manages	geocaches	.
it	auto-routes	.
it	stores	YEAR	waypoints	.
it	has	sirf	technology	.
it	is	a	usb	interface	.
it	has	a	belt	clip	.
it	changes	display	contrast	at	night	(	automatically	)	.
it	slices	DOTS
it	dices	DOTS	.suddenly	the	dark	side	is	not	so	dark	!	this	is	a	great	unit	that	i	would	recommend	to	anyone	.