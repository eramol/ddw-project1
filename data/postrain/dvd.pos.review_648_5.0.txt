i	have	no	idea	why	i	had	never	heard	of	this	movie	before	because	ronin	is	a	whale	of	a	good	film	.
i	can	take	or	leave	most	action	films	,	but	give	me	a	taut	,	intelligent	,	suspenseful	thriller	filled	with	great	action	,	wicked	twists	,	and	some	of	the	best	car-chasing	footage	you	will	ever	see	,	and	i	am	happy	.
throw	jean	reno	and	the	one	and	only	robert	de	niro	into	the	mix	,	as	well	,	and	i	am	happier	than	any	clam	has	a	right	to	be	.
director	john	frankenheimer	obviously	knows	that	action	alone	does	not	make	for	a	good	action	film	,	and	he	also	knows	that	the	only	way	to	make	a	high	speed	chase	scene	look	authentic	is	to	film	a	real	high	speed	chase	scene	.
the	title	is	derived	from	the	term	for	masterless	samurai	of	feudal	japan	-	warriors	who	failed	their	master	and	were	left	to	wander	in	the	shame	of	their	failure	.
in	modern	parlance	(	and	in	this	movie	)	,	the	term	applies	to	special	agents	of	various	governments	who	find	themselves	quite	on	their	own	thanks	to	the	end	of	the	cold	war	.
these	men	still	put	their	skills	to	good	use	by	selling	their	services	to	those	willing	to	pay	them	for	handling	sensitive	,	usually	dirty	,	jobs	.
in	this	case	,	sam	(	de	niro	)	,	vincent	(	reno	)	,	and	three	others	are	brought	together	in	france	by	an	irish	lass	named	deirdre	and	charged	with	acquiring	a	metal	case	well-protected	by	its	current	carrier	.
it	sounds	easy	-	ambush	the	target	and	his	security	force	,	snatch	the	case	,	exchange	it	for	cash	,	and	move	on	.
naturally	,	the	mission	turns	out	to	be	extremely	complicated	,	as	a	couple	of	powerful	parties	want	whatever	is	in	that	case	,	and	a	certain	individual	proves	less	than	trustworthy	.
what	's	the	world	coming	to	when	you	ca	not	even	trust	a	selfish	,	independent-minded	,	well-trained	gun-for-hire	?
de	niro	has	to	run	over	and	shoot	up	half	of	france	before	everything	gets	resolved	in	this	film	.
the	only	problem	i	had	with	this	movie	came	in	the	form	of	the	thick	accents	of	some	of	the	characters	,	especially	the	irish	ones	-	you	may	want	to	use	closed	captioning	at	times	to	really	understand	everything	that	is	said	.
the	actors	themselves	,	though	,	were	quite	good	,	especially	de	niro	and	reno	,	who	make	a	great	team	.
as	for	the	mix	of	bad	guys	,	how	about	three	-	count	'em	,	three	-	former	adversaries	of	james	bond	(	michael	lonsdale	,	sean	bean	,	and	jonathan	pryce	)	?
not	only	that	,	the	script	is	worthy	of	all	these	fine	actors	.
as	i	've	mentioned	already	,	though	,	the	chase	scenes	are	really	the	most	memorable	aspect	of	this	film	.
frankenheimer	does	not	have	any	use	for	these	fancy-schmancy	new	digital	means	for	manufacturing	this	kind	of	action	,	instead	treating	the	viewer	to	several	good	,	old-fashioned	thrill	rides	through	the	streets	of	paris	and	its	local	environs	.
besides	filming	an	incredible	number	of	scenes	live	,	he	also	chose	to	forego	the	accompaniment	of	a	musical	score	in	these	situations	,	letting	the	revving	engines	,	squealing	brakes	,	and	numerous	crashes	and	explosions	present	the	action	as	authentically	as	possible	.
they	had	to	have	had	an	incredible	number	of	stunt	drivers	and	stunt	men	involved	,	as	folks	were	constantly	crashing	or	jumping	out	of	the	path	of	oncoming	cars	-	not	to	mention	the	extended	high	speed	chase	down	one-way	streets	into	busy	oncoming	traffic	.
i	am	telling	you	-	these	chase	scenes	are	flat-out	impressive	.
obviously	,	i	think	ronin	has	it	all	.
without	de	niro	,	it	still	would	have	been	a	fantastic	film	;	with	him	,	it	's	a	must-see	in	my	book