i	took	the	NUMBER	tom	cover	's	classes	in	information	theory	at	stanford	last	year	.
we	used	this	book	as	the	text	for	both	classes	.
the	first	class	covered	roughly	the	first	NUMBER	chapters	and	the	second	class	the	remaining	ones	.
this	book	assumes	that	the	reader	has	a	clear	understanding	of	basic	probability	theory	.
tom	's	classes	were	among	the	most	enjoyable	i	took	last	year	and	i	recommend	this	text	to	anyone	interested	in	knowing	about	information	theory	.
if	you	just	want	to	know	the	basics	,	read	through	chapters	1-8	.
the	reader	interested	in	understanding	the	power	of	information	theory	is	adviced	to	read	the	whole	book	.
a	final	comment	,	it	is	clear	to	me	after	my	experience	with	tom	's	classes	that	mastering	information	theory	is	a	daunting	task	.
and	in	fact	,	this	book	provides	the	interested	researcher	just	with	the	basic	knowledge	needed	to	enter	the	field