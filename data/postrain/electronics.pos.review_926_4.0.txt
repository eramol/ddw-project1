it	is	remarkable	that	,	even	though	there	are	hundreds	of	ipod	cases	in	existence	,	none	of	them	are	near-perfect	.
i	tried	four	different	cases	,	both	hard	plastic	and	silicone	variants	,	and	the	iskin	case	comes	the	closest	,	protecting	the	ipod	but	at	the	same	time	not	rendering	the	ipod	less	enjoyable	to	use	.
the	case	is	basically	a	hybrid	of	hard	plastic	and	soft	silicone	.
the	ipod	first	slips	into	a	silicone	sleeve	,	which	covers	every	part	of	the	ipod	except	for	the	earphone	plug	and	the	screen	.
in	addition	,	there	is	a	hard	plastic	plate	that	is	then	inserted	into	seems	on	the	front	of	the	silicone	case	,	and	this	provides	the	screen	protection	.
due	to	the	double-seam	in	the	silicone	,	the	plastic	plate	never	comes	in	contact	with	the	ipod	,	so	you	do	not	have	to	worry	about	it	scratching	.
the	case	even	has	a	flap	that	covers	the	dock	connector	,	preventing	dust	from	entering	,	yet	you	can	still	use	the	universal	dock	without	removing	the	case	.
the	most	unique	part	of	this	case	is	that	it	covers	the	clickwheel	,	but	the	silicone	is	so	thin	that	it	hardly	impedes	the	scrolling	.
actually	,	i	think	it	provides	just	enough	resistance	so	that	you	will	overshoot	your	target	much	less	than	with	the	bare	clickwheel	.
the	only	real	downside	i	found	with	the	iskin	is	the	clip	that	comes	with	it-i	would	not	trust	it	at	all	,	so	i	took	it	off	.
it	would	be	nice	to	have	a	better	clip	that	you	can	actually	use	,	and	it	would	be	really	great	to	have	an	armband	available	for	it	as	well	,	but	as	i	said	above	,	i	have	yet	to	find	a	perfect	ipod	case	.
like	most	silicone	cases	,	this	one	does	also	attract	lint	in	your	pockets	,	etc.	,	but	it	is	not	as	bad	as	other	cases	.
unfortunately	,	even	though	this	is	a	great	case	,	it	is	not	readily	available	.
i	could	not	find	it	in	any	store	,	and	amazon	only	sells	it	through	marketplace	sellers	.
i	ordered	from	applelinks	,	and	they	delivered	it	to	me	in	four	days	.
whatever	you	do	,	do	not	order	from	the	iskin	company	directly	.
i	have	heard	countless	horror	stories	of	people	waiting	a	month	for	their	cases	.
they	should	re-dedicate	themselves	to	manufacturing	these	great	iskins	and	let	others	worry	about	selling	them