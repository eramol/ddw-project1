i	am	very	happy	with	this	model	.
i	use	it	mostly	as	a	vcr	replacement	to	record	broadcast	tv	shows	.
you	ca	not	beat	the	price	for	this	feature-set	.
i	will	try	to	describe	some	aspects	that	are	not	mentioned	in	the	other	reviews	.
the	owner	's	manual	can	also	be	found	online	in	pdf	format	.
it	can	schedule	upto	NUMBER	programs	.
it	does	not	support	vcr+	or	a	tvio-style	program	guides	.
you	can	enter	a	title	into	each	schedule	that	appears	both	on	the	schedule	list	and	direct	navigator	chapter	menus	.
it	even	sorts	the	main	schedule	listing	and	indicates	which	events	are	on	the	current	disk	.
every	on-screen	menu	shows	you	which	remote	control	buttons	are	functional	on	that	screen	.
the	commercial	skip	button	advances	about	NUMBER	seconds	which	is	not	quite	granular	enough	,	but	NUMBER	clicks	and	a	little	fast	forwarding	works	well	.
the	remote	operations	have	a	very	fast	feeling	response	compared	to	tape	players	.
my	only	complaint	:	the	remote	sensor	on	the	recorder	seems	to	be	all	the	way	on	right-hand	side	.
when	the	unit	is	in	our	video	cabinet	,	the	sensor	is	in	a	deadzone	for	half	the	room	.
we	ca	not	shift	the	position	much	because	the	disk	tray	slot	is	all	the	way	on	the	left	side	and	wo	not	open	if	it	's	too	close	to	the	wall	.
of	course	this	probably	means	nothing	to	you	.
at	the	highest	quality	setting	(	xp	)	it	made	a	very	good	(	near	perfect	i	'd	say	)	copy	from	a	8mm	video	camera	using	analog	inputs	.
at	that	setting	,	only	one	hour	of	content	fits	on	a	dvd	.
the	next	lower	setting	(	sp	)	which	can	hold	two	hours	per	disk	,	the	copy	from	the	video	camera	had	some	contrast	problems	.
most	tv	shows	recorded	in	ep	(	NUMBER	hrs	)	are	acceptable	quality	although	you	do	see	the	blockiness	and	some	motion	blur	at	times	.
this	is	especially	pronounced	on	animated	shows	.
the	four-hour	lp	setting	had	no	apparent	digital	artifacts	on	recorded	tv	.
it	also	has	a	flex-mode	option	i	have	not	tried	yet	in	which	you	tell	it	how	long	to	record	and	it	will	fit	the	recording	to	the	freespace	available	on	the	disk	at	the	highest	possible	quality	.
probably	would	be	good	for	recording	a	tv	movie	.
it	's	too	bad	other	vendors	have	not	adopted	the	-ram	format	because	most	of	the	features	on	this	box	only	work	on	that	kind	of	media	,	but	it	wo	not	play	on	most	other	equipment	.
the	other	formats	seem	lame	by	comparison	,	but	this	unit	can	play	and	record	on	them	.
watching	one	show	while	another	is	recording	(	or	the	same	show	that	's	recording	,	but	from	the	beginning	)	is	very	easy	to	do	.
most	of	the	operations	are	very	sensible	and	explained	on-screen	pretty	well	,	but	you	should	spend	some	time	in	the	manual	if	you	've	never	had	a	dvd	recorder	before	.
it	can	do	a	lot	of	stuff	.
one	interesting	feature	is	that	no	matter	what	input	you	are	using	,	the	signal	is	sent	through	the	s-video	output	.
it	ca	not	burn	music	cd	's	which	seems	like	it	would	have	been	a	trivially	easy	feature	to	add	.
that	and	a	battery	backup	(	so	i	wo	not	lose	my	schedule	)	and	wider	angle	of	view	for	the	remote	sensor	would	required	get	the	fifth	review	star