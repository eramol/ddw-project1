the	medium	instantly	became	my	favorite	show	on	television	in	NUMBER	.
i	usually	am	fascinated	by	shows	with	elements	of	the	paranormal	,	which	tuned	me	in	.
however	,	it	is	the	realism	of	the	family	relationship	that	is	most	fascinating	.
patricia	arquette	who	was	in	ed	wood	with	johnny	depp	plays	alison	dubois	.
her	emmy	win	for	best	actress	in	a	drama	attests	to	her	standing	within	the	industry	,	but	the	on-screen	chemistry	with	jake	weber	who	was	in	meet	joe	black	and	plays	her	husband	joe	is	riveting	.
they	fight	,	spat	,	try	to	mask	conflict	in	front	of	their	kids	,	juggle	an	impossible	number	of	real-life	tasks	,	and	have	layers	of	humor	and	romance	that	make	this	seem	like	a	real	family	that	loves	each	other	and	deals	with	the	unusual	circumstances	of	alison	's	psychic	ability	.
i	found	after	watching	the	initial	show	that	when	the	reruns	would	show	,	i	still	wanted	to	watch	them	over	episodes	of	csi	miami	that	i	had	not	seen	.
when	i	bought	the	dvd	,	i	was	hoping	there	might	have	been	an	episode	i	'd	missed	,	but	i	'd	seen	them	all	between	nbc	&	lifetime	reruns	several	times	.
i	am	impressed	by	how	entrancing	each	episode	is	and	the	quality	of	the	writing	.
alison	's	dreams	are	like	clues	that	she	struggles	to	fit	together	.
what	impresses	me	about	alison	is	that	she	sincerely	struggles	to	do	the	right	thing	.
in	a	world	of	violence	,	it	's	often	hard	to	be	clear	which	way	the	moral	compass	points	.
in	the	episode	in	sickness	&	adultery	her	tip	places	her	on	the	witness	stand	with	an	unethical	hostile	prosecutor	that	seeks	to	embarrass	her	.
as	she	comes	back	that	he	cheated	to	pass	his	law	exams	and	is	cheating	on	his	wife	,	she	finds	that	her	best	defense	is	the	truth	.
the	two	older	daughters	are	also	jewels	.
maria	lark	as	bridget	often	has	my	entire	family	in	stitches	with	her	offbeat	humor	like	shouting	at	her	mom	who	wakes	screaming	from	a	bad	dream	,	mom	,	please	stop	doing	that	!
sofia	vassilieva	as	the	older	aerial	keeps	the	arguments	at	the	breakfast	table	bouncing	.
david	cubitt	who	plays	the	reoccurring	role	of	detective	lee	scanlon	also	achieves	amazing	chemistry	with	arquette	.
miguel	sandoval	as	the	district	attorney	manuel	duvalos	has	such	a	focused	calmness	that	he	centers	the	crime	portion	of	the	show	.
(	perhaps	we	will	sometime	see	more	about	his	family	.	)
while	it	's	hard	to	pick	my	favorite	first	season	episode	,	the	complex	plot	of	a	priest	,	a	doctor	&	medium	walk	into	an	execution	chamber	is	a	jewel	,	with	the	police	chasing	a	ghost	who	apparently	has	just	committed	murder	.
the	dvd	commentaries	and	introduction	to	the	real-life	alison	dubois	really	give	excellent	background	for	fans	of	the	show	.
this	is	one	show	that	reaches	the	level	of	art	for	me	.
its	excellence	will	make	it	memorable	for	a	long	time	&	this	dvd	collection	a	valuable	addition	to	an	entertainment	collection	.