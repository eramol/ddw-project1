i	am	guessing	that	many	of	the	reviewers	are	watching	this	movie	through	a	haze	of	childhood	nostalgia	.
i	had	not	seen	this	movie	as	a	child	;	those	that	say	it	's	faithful	to	the	book	must	have	read	something	other	than	lewis	carroll	's	alice	.
however	,	the	kitsch	value	is	considerable	;	cheezy	music	and	costumes	,	pure	YEAR	.
that	being	said	,	the	jabberwock	was	pretty	cool	in	that	godzilla-man-in-the-rubber-suit	sort	of	way	.
also	,	the	set	design	was	very	handsome	.
it	's	too	bad	that	the	lighting	technology	of	the	time	could	not	have	been	more	varied	;	everything	looks	somewhat	flat	and	two	dimensional	.
the	film	that	comes	closest	to	tenniel	's	original	alice	illustrations	is	the	YEAR	paramount	b	&	w	version	;	it	's	not	available	on	dvd	but	occasionally	turns	up	on	turner	classics