i	saw	this	movie	in	the	theater	and	was	amazed	at	how	quiet	it	was	in	there	!
since	i	was	bored	,	i	found	myself	looking	at	other	people	there	to	see	if	i	could	get	a	read	on	what	they	were	thinking	,	and	after	it	was	over	i	asked	my	friend	what	he	thought	.
he	said	,	oh	,	it	was	okay	but	did	not	elaborate	.
so	let	me	:	it	was	painfully	unfunny	,	with	jokes	or	punchlines	that	fell	flat	,	horrible	acting	,	totally	predictable	,	and	very	forgetable	.
this	is	a	movie	you	might	want	to	rent	only	if	you	are	really	bored	and	nearly	every	other	movie	is	out	.
and	even	then	,	you	'd	be	better	off	to	go	home	empty	handed	.
there	's	not	even	much	eye	candy	to	recommend	,	which	is	a	nice	fallback	if	there	's	nothing	else	going	for	it	.
how	people	could	give	this	movie	anything	more	than	NUMBER	star	is	puzzling	.
just	because	you	like	a	movie	should	not	mean	you	give	it	NUMBER	stars	,	especially	when	it	's	as	bad	as	this	one	.
i	mean	,	i	liked	freddy	vs	jason	,	but	there	's	no	way	i	'd	give	that	movie	NUMBER	or	NUMBER	stars	.
this	is	bottom	of	the	barrel