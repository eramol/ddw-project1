i	purchased	this	router	(	not	from	amazon	)	about	NUMBER	months	ago	when	my	5-year-old	,	very	reliable	,	router	died	.
i	purchased	it	locally	and	someone	came	out	and	installed	it	.
i	have	had	numerous	problems	with	this	router	since	then	.
it	does	not	stay	connected	and	there	is	actually	no	option	for	always	on	.
after	having	some	other	problems	,	i	took	it	back	and	the	vendor	upgraded	the	firmware	.
after	that	,	it	lost	the	setup	information	twice	and	the	info	had	to	be	re-entered	.
then	today	,	after	being	instructed	to	reset	the	router	,	it	got	stuck	in	a	mode	where	it	only	wanted	a	firmware	upgrade	(	there	was	no	apparent	way	out	of	this	)	.
so	,	i	obtained	the	upgrade	and	according	to	the	router	message	,	it	was	updated	successfully	.
after	that	,	i	could	not	get	the	router	to	work	at	all	.
i	gave	up	and	ordered	an	entirely	different	router	(	the	same	brand	that	i	has	used	before	with	tremendous	success	)	.
my	summary	-	save	yourself	time	and	aggravation	and	do	not	buy	the	linksys	wrt54g