new	are	notorious	thieves	in	the	consumer	electronics	repair	industry	.
i	personally	submitted	for	repair	both	a	$	NUMBER	video	camera	(	sony	dcr-pc5	)	and	a	car	cd	player	(	a	sony	cdx-c90	,	a	$	NUMBER	in-dash	top	of	the	line	unit	.
if	you	've	heard	of	it	,	you	know	what	i	am	talking	about	)	.
i	was	told	that	the	warranty	did	not	cover	what	was	wrong	with	my	video	camera	(	some	ambiguous	statement	about	logic	board	,	power	unit	,	etc	.
failure	followed	)	,	and	would	have	to	pay	~	$	NUMBER	to	have	it	fixed	.
i	naturally	refused	,	and	was	forced	to	keep	my	dead	NUMBER	month-old	handycam	,	an	unfortunate	testament	to	some	of	sony	's	now	widely	publicized	qc	issues	.
the	reason	i	did	not	send	in	the	camera	initially	has	to	do	with	the	fiasco	involving	my	cd	player	.
i	was	told	,	explicitly	,	that	if	new	could	not	fix	it	,	they	would	either	return	the	unit	or	write	me	a	check	in	the	amount	of	$	NUMBER	(	not	even	enough	to	cover	the	cost	of	the	player	)	.
as	an	added	insult	,	when	they	indeed	could	not	fix	the	unit	,	they	kept	it	,	and	i	never	received	a	check	!
simply	outrageous	!
blatant	theft	,	and	i	am	not	the	only	one	who	will	tell	you	a	story	about	new	and	such	an	act	.
they	are	impossible	to	get	ahold	of	,	and	literally	take	your	money	and	run/disappear	.
do	not	expect	to	ever	hear	from	them	,	should	your	camera	stop	working	,	and	good	luck	getting	ahold	of	them	if	you	happen	to	send	it	in	beforehand	.
chances	are	,	you	will	have	to	eat	the	loss	,	as	i	did	.
this	product	deserves	a	negative	star	and	should	come	with	an	automatic	refund	.
i	seriously	thought	it	was	a	gag	when	i	saw	new	was	in	charge	of	the	coverage	.
what	a	joke	!
whoever	runs	your	company	,	new	,	ought	to	be	both	ashamed	of	themselves	and	held	liable	for	all	the	countless	damages	and	losses	they	have	caused	.
the	whole	lot	of	you	belong	in	an	unfriendly	prison