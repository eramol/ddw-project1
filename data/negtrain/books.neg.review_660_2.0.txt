without	question	a	book	is	needed	to	address	the	origin	and	history	of	late	night	television	and	steve	allen	's	pivitol	role	it	.
sadly	ben	alba	's	inventing	late	night	:	steve	allen	and	the	original	tonight	show	,	is	not	that	book	.
it	is	,	largely	,	a	poorly	edited	and	self-contradicting	pastiche	of	previously	published	memoirs	by	allen	himself	combined	with	some	excellent	interviews	with	allen	's	tv	contemporaries	.
these	interviews	could	form	the	basis	of	an	excellent	book	by	the	likes	of	biographers	scott	berg	(	sam	goldwin	)	or	neal	gabler	(	walter	winchle	)	.the	opening	chapter	,	which	addresses	allen	's	dysfuntional	up-bring	,	education	and	his	early	days	in	radio	and	tv	sets	the	stage	for	the	author	's	failure	to	create	a	full-blooded	,	well-rounded	analytical	portrait	of	allen	and	his	work	.
alba	draws	here	,	almost	exclusively	and	certainly	uncritically	,	from	allen	's	memiors	while	combining	these	words	with	one	quote	from	a	childhood	friend	and	two	early	reviews	.
taking	allen	and	the	reader	's	one	independent	witness	at	face	value	,	allen	had	a	rootless	,	violent	and	unstable	childhood	.
given	these	negative	conditions	alba	gives	no	hint	of	how	,	why	or	even	if	,	allen	really	remained	attached	to	his	drunken	and	generally	unstable	mother	and	her	equally	troubled	extended	family	.
the	author	gives	us	two	brief	stories	from	allen	's	adulthood	to	demonstarte	that	the	family	ties	endured	.
actually	,	all	the	stories	show	is	that	allen	attended	an	uncle	's	funeral	and	very	briefly	troted	his	mother	out	once	on	his	sunday	night	show	.
how	did	this	dysfunctional	world	of	allen	's	youth	effect	his	two	marriages	and	five	sons	?
why	,	after	a	childhood	on	the	fringes	of	a	perlious	show	business	existance	did	allen	choose	this	same	career	field	for	himself	?
alba	does	not	even	pose	such	questions	about	the	boy	's	effects	on	the	man	.
he	just	plows	ahead	in	a	haliographic	haze	.
throughout	inventing	DOTS	alba	maintains	this	pattern	of	unquestioning	acceptance	allen	's	words	.
no	other	witnesses	are	called	,	no	other	points	of	view	are	examined	.
steve	tells	a	story	;	alba	accepts	it	and	edits	it	into	his	text	.
why	not	read	allen	straight	?