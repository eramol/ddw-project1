leaving	the	racism	issue	aside	,	this	is	not	a	book	for	small	children	.
even	if	everything	turns	out	ok	in	the	end	,	i	am	not	prepared	to	expose	my	children	to	issues	of	capital	punishment	and	the	drowning	death	of	a	small	boy	.
small	children	forget	the	ending	and	focus	on	the	scary	parts	in	between	.
perhaps	i	am	a	softie	,	but	let	's	allow	them	to	be	children	and	not	worry	about	being	wrongfully	accused	of	murder	and	sentenced	to	death	by	beheading	.
just	because	it	's	a	classic	does	not	make	it	appropriate	reading	.