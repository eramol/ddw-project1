several	reviews	mention	the	improvement	when	these	cables	to	replace	a	composite	(	l/r	audio	+	video	)	cable	,	or	an	s-video	(	multi-pinned	plug	)	.
almost	all	of	that	improvement	is	due	to	the	fact	that	these	cable	connection	methods	are	completely	different	types	in	signal	format	and	the	tv	uses	different	internal	circuitry	for	each	one	.
virtually	any	s-video	cable	connection	will	give	a	better	image	than	a	composite	type	cable	,	and	any	component	(	individual	r-g-b	video	)	connection	will	be	better	than	s-video	.
cable	brand	has	absolutely	nothing	to	do	with	it	.
there	may	be	some	difference	between	one	brand	of	component	cable	and	another	,	but	its	almost	always	unnoticable