many	people	will	consider	giving	this	movie	such	a	low	rating	a	sacrilege	.
this	is	understandable	because	so	many	people	have	grown	up	watching	this	film	.
i	too	loved	this	movie	at	a	certain	period	of	my	life	.
the	period	between	my	birth	and	when	i	turned	ten	.
that	was	when	i	read	the	book	.
the	book	is	perhaps	one	of	the	greatest	literary	works	ever	created	.
it	has	now	been	awhile	since	i	first	read	it	,	but	i	still	remember	the	excitement	i	felt	with	each	new	chapter	.
i	finished	it	in	a	few	short	hours	.
since	then	,	i	have	reread	it	several	times	,	and	each	time	it	has	been	just	as	enjoyable	.
it	is	a	true	classic	that	people	of	all	ages	can	enjoy	.
my	brother	reads	as	little	as	possible	,	but	even	he	loved	it	.
the	movie	,	on	the	other	hand	,	has	gotten	worse	with	each	subsequent	viewing	.
it	deters	from	the	book	in	almost	every	way	possible	.
it	makes	a	mockery	of	each	character	through	a	combination	of	poor	writing	and	horrible	acting	.
judy	garland	is	an	absolutely	abominable	choice	to	play	the	plucky	heroine	,	dorothy	gale	.
in	the	book	,	dorothy	was	a	little	girl	of	about	eight	or	nine	and	she	was	brave	and	clever	.
in	the	film	,	she	is	whiny	,	cowardly	,	fretful	,	and	slightly	stupid	.
she	is	also	portrayed	as	being	around	fifteen	.
making	her	this	old	distorts	the	book	and	her	older	age	does	not	fit	with	her	childish	behaviour	and	appearance	.
besides	wrecking	the	book	,	this	movie	when	looked	at	clearly	is	a	terrible	film	.
do	not	misunderstand	me	,	i	am	a	fan	of	musicals	.
however	,	this	film	made	for	an	atrocious	musical	.
practically	every	single	song	was	annoying	!	also	,	the	somewhere	over	the	rainbow	number	did	not	suit	the	plotline	as	it	was	in	the	book	.
this	brings	me	to	the	biggest	,	most	awful	,	distortion	of	them	all	!	in	the	book	,	dorothy	really	went	to	oz	!	not	only	that	,	she	returned	to	oz	several	times	in	other	oz	books	.
eventually	,	she	even	moved	there	to	live	!	in	making	the	whole	experience	a	dream	,	the	entire	story	of	oz	was	destroyed	.
having	said	that	,	this	movie	receives	a	NUMBER	in	my	opinion	.
even	so	,	i	realize	that	many	people	will	persist	in	liking	this	movie	despite	my	words	of	wisdom	.
if	they	do	sill	consider	this	movie	a	favorite	after	reading	the	book	,	then	the	3-disc	dvd	is	the	edition	to	purchase	.
the	features	look	good	and	the	picture	is	restored	.
as	for	me	,	i	will	spend	my	money	on	better	things	.