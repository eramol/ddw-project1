i	like	looking	at	old	pictures	and	old	letters	,	but	i	thought	when	i	bought	the	book	that	i	would	actually	be	looking	in	the	history	of	someone	's	life	with	pictures	to	go	with	it	but	that	was	not	the	case	.
it	is	just	a	random	bunch	of	pictures	and	a	random	bunch	of	letters	put	together	in	a	book	.
the	pictures	are	not	necessarily	the	pictures	of	the	people	who	wrote	the	letters	,	so	you	know	nothing	of	the	authors	,	no	explanation	of	the	times	they	lived	in	or	the	place	they	lived	in	or	anything	.
in	other	words	,	no	story	behind	it	whatsoever	.
just	a	disappointing	collection