i	had	a	terrible	time	getting	a	station	to	come	in	even	close	to	fm	quality	.
there	was	always	a	bunch	of	static	and	the	stereo	buzzed	between	songs	because	i	had	to	keep	the	volume	too	high	.
the	high	band	of	frequencies	seemed	to	work	best	,	but	then	it	led	to	crackling	speakers	everytime	i	turned	something	electrical	on	in	the	car	.
it	also	seemed	to	kill	batteries	at	a	surprising	rate	.
on	a	positive	note	,	i	loved	the	fact	that	it	would	also	function	for	my	son	's	dvd	but	in	the	end	,	it	had	way	too	much	static	so	i	hunted	for	anothe