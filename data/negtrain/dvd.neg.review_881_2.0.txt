i	bought	this	dvd	because	amy	jo	johnsen	is	in	it	.
(	one	of	my	favorite	actresses	.	)
what	i	expected	was	a	decent	vampire	movie	,	with	some	blood	and	guts	,	and	a	good	storyline	.
what	i	got	was	a	movie	with	talking	,	talking	and	more	talking	,	and	a	boring	storyline	.
i	saw	hardly	any	action	,	hardly	any	fun	,	and	hardly	anything	interesting	.
i	was	extremely	disappointed	in	amy	for	starring	in	such	a	boring	,	unintriguing	movie	.
the	only	actual	action	was	toward	the	end	with	the	final	battle	between	seth	and	viki	's	ex	.
it	was	about	an	hour	and	fifteen	minutes	of	explanations	,	and	useless	talking	.
if	you	are	looking	for	a	movie	with	some	action	and	fun	,	look	elsewhere	.
as	much	as	i	hate	to	put	an	amy	jo	movie	down	,	i	am	doing	so	.