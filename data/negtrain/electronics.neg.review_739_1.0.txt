i	ordered	the	passport	x50	from	amazon	to	save	the	$	NUMBER	shipping	escort	radar	was	charging	.
i	was	told	by	someone	at	escort	radar	that	amazon	was	an	authorized	reseller	.
after	having	my	detector	on	for	ten	minutes	(	right	out	of	the	box	)	,	it	did	three	self	cal	s	,	then	said	service	required	.
i	called	escort	radar	and	was	told	if	you	let	the	car	power	up	the	unit	when	you	start	the	car	,	the	voltage	can	be	all	over	the	place	and	the	unit	gets	confused	.
you	must	first	start	the	car	,	and	then	use	the	thumbwheel	to	turn	on	the	detector	.
it	makes	some	sense	,	but	if	this	is	really	the	case	,	why	do	not	they	put	those	instructions	in	their	manual	?
after	trying	this	many	times	,	the	unit	was	still	confused	,	so	i	returned	it	for	a	replacement	.
my	second	detector	worked	for	maybe	thirty	minutes	before	the	same	thing	happened	.
this	time	escort	radar	told	me	if	you	leave	it	plugged	into	the	cigarette	lighter	over	night	,	this	has	been	found	to	cause	problems	.
well	my	lighter	power	goes	off	when	i	turn	off	the	ignition	,	so	this	does	not	apply	.
escort	radar	also	told	me	that	excessive	heat	has	been	causing	them	problems	.
now	we	have	had	a	few	days	of	100�	heat	,	and	i	am	sure	the	car	gets	hotter	parked	in	the	sun	,	but	the	design	engineers	should	have	accounted	for	these	conditions	plus	some	margin	when	you	design	something	that	is	supposed	to	work	in	a	car	.
it	gets	a	lot	hotter	in	other	parts	of	the	country	.
there	are	no	warnings	in	the	manual	about	not	using	it	above	90�	or	leaving	it	off	in	a	hot	car	.
you	would	not	design	a	ski	that	does	not	work	below	35�	,	would	you	?
?	the	most	troubling	part	of	my	conversation	with	escort	radar	service	department	was	they	told	me	amazon	is	not	an	authorized	reseller	,	and	they	have	no	idea	where	they	are	getting	the	units	from	.
furthermore	,	they	will	not	honor	any	warranty	nor	will	they	ever	repair	the	unit	,	not	even	for	money	.