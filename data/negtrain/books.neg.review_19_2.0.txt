while	reading	this	book	i	found	the	details	of	some	cases	to	be	unbelievable	.
also	,	the	photographs	of	the	murderers	and	detectives	all	looked	fuzzy	and	more	like	drawings	.
i	've	read	a	lot	of	true	crime	and	never	heard	of	catching	a	murderer	because	he	breathed	his	asthma	medication	on	the	murder	victim	's	hair	.
or	because	they	chemically	deduced	which	cologne	he	wore	.
and	who	leaves	their	backdoor	open	when	they	know	a	violent	doberman	pinscher	has	been	getting	through	their	backyard	fence	?
sure	,	just	let	that	dog	on	in	.
and	whose	place	of	employment	has	a	record	of	all	employee	's	blood	groups	(	not	their	types	,	just	their	groups	)	,	when	the	employee	does	not	even	know	himself	what	group	he	is	?	then	i	read	the	introduction	,	which	i	always	skip	,	and	found	out	this	book	is	fiction	,	not	true	crime	as	the	cover	announced	and	in	which	section	it	was	in	in	the	book	store	.
as	fiction	,	it	's	okay	,	(	i	prefer	agatha	christie	)	,	but	i	do	not	believe	the	forensic	work	in	this	book	is	even	based	on	any	fact	.
so	read	it	for	entertainment	,	but	do	not	be	so	gullible	as	to	believe	any	of	it