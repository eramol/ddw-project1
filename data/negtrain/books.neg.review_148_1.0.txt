a	very	disappointing	book	,	and	again	,	makes	me	very	wary	of	any	item	on	a	new	york	times	best	seller	list	or	any	other	list	,	and	really	any	big	name	reviewer	recommendation	.
this	book	has	one	NUMBER	page	chapter	which	is	excellent	-	the	recap	of	the	thompson	shot	heard	around	the	world	homerun	.
if	you	are	a	baseball	fan	as	i	am	,	that	(	barely	)	made	the	book	worthwhile	.
the	other	NUMBER	pages	are	disjoint	,	incomprehensible	,	and	really	trivial	.
you	come	away	thinking	i	really	do	not	care	bout	this	stuff	-	i	do	not	care	about	the	dynamics	of	trash	,	about	j	edgar	hoover	's	bathroom	habits	,	klara	sax	's	artistic	depression	and	annoying	infatuations	about	roof	tops	,	about	characters	that	just	do	not	have	much	depth	to	really	care	about	.
at	the	end	,	even	potentially	interesting	characters	like	albert	(	the	one	on	the	baseball	quest	)	become	hard	to	bear	.
the	whole	book	in	fact	had	the	feel	of	going	to	a	senior	citizen	home	.
i	read	this	book	,	as	perhaps	others	did	,	expecting	a	book	about	the	mafia	or	about	other	figures	in	the	underworld	.
what	i	got	was	a	book	with	a	good	first	chapter	,	some	interesting	tidbits	here	and	there	about	how	life	used	to	be	in	the	NUMBER	's	(	fear	of	nuclear	weapons	,	duck	and	cover	classroom	exercies	)	,	and	a	lot	of	knowledge	about	the	trash	business	.
the	masterful	epilogue	,	as	other	reviewers	call	it	,	really	does	no	justice	to	a	reader	who	has	spent	so	much	time	toiling	over	delillo	's	ragtag	story	telling	.
i	am	ok	with	stream	of	consciousness	writing	,	or	nonchronological	chapters	,	or	even	random	chapters	,	but	the	methods	used	in	thsi	book	served	no	purpose	other	than	to	further	make	the	reading	difficult	.
was	this	book	about	how	mundane	our	lives	are	versus	the	bigger	things	that	occur	around	us	?
or	how	each	of	us	is	on	an	individual	quest	?
or	that	the	50's-60's-70's-80	's	were	just	years	of	fear	,	and	the	NUMBER	's	are	years	of	greed	?
i	am	still	unsure	,	i	still	do	not	think	any	of	these	themes	came	across	well	.
please	-	not	pretending	to	be	a	sophisticate	,	intellectual	,	or	professional	critic	-	but	this	book	is	really	not	worth	the	time	or	effort	.
you	can	be	staisfied	by	a	number	of	other	books	,	including	the	the	corrections	by	franzen	or	the	cold	six	thousand	by	ellroy	which	cover	a	similar	period	or	similar	themes	and	do	not	make	you	so	frustrated	or	disappointed	at	the	end	.
read	the	first	chapter	,	then	put	the	book	down	or	you	will	be	sorely	disappointed	and	have	wasted	an	awful	lot	of	time	.