yesterday	was	the	annual	thanksgiving	sale	day	.
i	got	this	1g	cf	card	from	staples	by	$	NUMBER	after	easy	rebate	.
with	this	price	,	i	think	it	is	a	great	deal	.
however	,	it	worked	for	my	digital	rebel	only	half	an	hour	before	it	was	dead	.
all	my	photos	in	the	card	were	lost	.
i	am	a	graduate	student	majoring	in	computer	engineering	,	i	know	how	to	recover	data	if	a	hard	drive	fails	.
but	this	time	,	i	find	i	simply	ca	not	help	.
fortunately	,	staples	have	a	good	return	policy	and	i	just	returned	it	without	hassle	.
hopefully	,	this	is	only	my	case	.
my	suggestion	:	make	sure	your	dealer	has	a	good	return	policy	before	you	get	this	item	.