the	message	meant	more	to	me	than	to	my	young	children	.
they	didnt	understand	why	the	children	were	being	locked	in	a	box	.
they	think	playing	in	cardboard	boxes	is	fun	,	not	a	punishment	for	exercising	too	much	freedom	.
i	could	see	using	the	book	as	a	basis	for	discussion	,	but	i	think	it	may	just	scare	children	that	they	too	could	be	locked	up	by	their	parents