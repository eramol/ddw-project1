i	like	this	style	of	headphone	radio	(	especially	when	i	jog	)	,	and	have	not	found	any	similar	models	that	i	like	.
this	is	my	third	purchase	of	this	radio	.
each	of	the	three	have	had	problems	.
first	died	completely	(	although	it	was	used	a	lot	)	.
second	still	works	,	but	last	number	does	not	show	up	on	digital	display	.
i	wanted	a	backup	in	case	second	died	,	so	i	ordered	another	one	.
this	third	works	least	well	of	all	.
at	times	,	entire	display	is	off	,	radio	does	not	work	at	all	,	or	it	wo	not	change	channels	.
then	other	times	it	worked	.
seems	like	might	be	affected	by	heat	outside	.
however	,	i	know	have	another	persistent	problem	.
that	being	,	it	wo	not	let	me	program	the	preset	stations	.
again	,	i	am	limited	in	my	choices	,	and	i	use	this	type	headphone	a	lot	.
so	i	suffer	through	poor	quality