wright	presents	no	new	information	about	al-qaeda	,	leaves	out	plenty	of	information	about	al-qaeda	and	neglects	the	usa	side	of	the	road	to	9/11	.
understanding	qutb	's	radicalization	of	islam	is	important	to	understanding	why	ayman	al-zawahiri	and	usama	exist	and	also	why	they	failed	at	creating	theocracies	and	were	lame	ducks	until	the	usa	found	a	use	for	them	as	an	enemy	.
what	can	not	be	neglected	is	the	myths	spread	about	the	united	states	by	the	neoconservatives	and	followers	of	leo	strauss	.
the	united	states	is	not	a	unique	and	beautiful	snowflake	.
the	liberal	idea	of	individual	freedom	was	decried	by	strauss	as	destructive	of	the	society	of	the	usa	and	he	told	his	followers	to	keep	the	public	in	line	with	grand	myths	about	us	exceptionalism	.
after	the	the	soviet	union	was	kicked	out	of	afghanistan	,	both	qutb	and	strauss	followers	believed	they	were	the	cause	of	it	.
since	the	cold	war	the	us	has	been	working	off	of	the	friend	and	enemy	model	of	policy	formulated	by	carl	schmitt	and	9/11	presented	the	us	with	what	schmitt	called	a	state	of	exemption	.
if	you	think	jihad	was	the	cause	of	9/11	,	ask	yourself	why	richard	clarke	found	bush	and	his	advisors	saying	find	a	way	to	tie	this	to	iraq	hours	after	9/11	.
see	the	bbc	documentary	,	the	power	of	nightmares	or	from	secularism	to	jihad	:	sayyid	qutb	and	the	foundations	of	radical	islamism	by	adnan	a.	musallam	or	the	political	ideas	of	leo	strauss	by	shadia	drury	for	a	better	analysis	of	qutb	and	strauss	than	wright	has	in	his	book	.
if	you	prefer	to	stick	with	fiction	about	the	road	to	9/11	,	check	out	patrick	s.	johnston	's	novel	mission	accomplished	.