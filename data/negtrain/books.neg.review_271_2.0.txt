galbraith	's	insight	on	society	unfortunately	gets	lost	in	a	maze	of	double	negatives	and	confusing	sentence	structure	.
his	ideas	are	sound	,	though	he	has	a	very	off-putting	pompous	delivery	.
in	fact	,	his	verbosity	and	poor	structure	is	so	bad	,	that	it	turns	what	should	be	a	slim	read	into	a	fairly	mind-numbing	experience	.
the	book	really	needs	to	go	back	to	an	editor