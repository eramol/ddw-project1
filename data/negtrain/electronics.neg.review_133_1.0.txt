though	i	like	the	keyboard	,	i	've	found	two	serious	problems	with	the	mouse	.
first	,	it	is	very	sensitive	to	movement	.
i	've	tried	making	some	adjustments	but	it	is	still	difficult	to	control	.
finally	and	more	importantly	,	the	mouse	burns	a	set	of	aa	batteries	about	every	NUMBER	days	.
i	only	use	the	mouse	about	NUMBER	hours	a	week	so	i	should	be	getting	a	lot	more	battery	life	.
i	've	bought	other	logitech	products	and	have	been	pleased	with	them	but	in	this	case	i	wish	i	had	bought	another	brand	.