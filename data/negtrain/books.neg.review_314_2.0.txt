this	is	a	revolutionary	book	.
it	introduces	the	stage	metaphor	into	sociology	;	all	of	social	interaction	is	a	performance	on	a	stage	.
it	's	questionable	how	novel	this	metaphor	is	,	but	this	is	certianly	its	first	explicit	statement	.
the	language	used	to	present	the	ideas	of	the	author	is	,	like	most	sociological	writing	,	unecessarily	complicated	.
there	are	some	problems	with	clarity	and	structure	;	the	book	just	doesnt	have	a	holistic	feel	for	me	.
no	sociological	background	is	presupposed	,	there	is	extensive	quoting	from	other	sociological	sources	,	many	footnotes	,	and	various	other	ways	to	enter	sociological	literature	throught	the	book	.
so	if	you	are	interested	in	reading	sociology	,	this	is	probably	a	good	place	to	start	(	again	,	the	language	is	unecessicarily	thick	and	this	may	be	it	's	bane	as	an	introduction	)	.however	,	the	book	is	misguided	.
the	idea	of	viewing	social	interaction	as	a	performance	on	a	stage	is	unecessary	.
the	authors	motives	for	saying	this	will	make	my	position	clearer	.
(	the	following	is	a	charicature	of	the	author	's	argument	)	.
there	is	certianly	a	large	amount	of	social	behavior	which	is	directed	towards	other	people	(	the	audience	)	in	order	for	the	audience	to	build	an	impression	of	the	actor	.
well	,	if	it	can	happen	here	we	can	draw	a	parallel	with	other	situations	,	infact	all	situations	and	therfore	all	of	social	interaction	is	like	a	performance	on	a	stage	,	all	the	world	is	not	,	of	course	,	a	stage	,	but	the	crucial	ways	in	which	it	isnt	are	hard	to	specify	(	p	NUMBER	)	.
the	performances	can	either	be	with	individuals	,	or	groups	of	people	,	and	they	can	be	performing	conciously	or	unconciously	.
(	end	of	charicature	)	there	are	other	arguments	,	obviously	,	and	the	statement	we	can	draw	a	parallel	with	other	situations	is	most	of	the	content	of	the	book	,	which	i	dont	like	to	gloss	over	with	a	sentence	.
but	the	real	question	here	is	,	is	the	view	worthwhile	?
my	answer	:	no	.
social	interaction	is	a	complex	phenomena	that	cant	possibly	be	summed	up	with	a	signle	metaphor	.
yes	,	some	social	interaction	is	like	a	performance	,	where	indivudals	are	intentionally	making	impressions	on	one	another	.
yes	,	some	social	behavior	can	arise	from	unconcious	beliefs	.
however	,	most	social	interaction	is	exactly	the	way	we	see	it	,	conciously	controlled	,	meaningful	,	purposeful	interaction	with	individuals	;	it	is	not	some	kind	of	performance	with	the	purpose	of	creating	a	reality	.i	hope	my	view	of	the	book	is	clear	so	that	the	biases	on	the	final	part	of	my	review	can	be	sorted	out	:	the	book	is	a	waste	of	time	.
where	the	author	is	right	,	the	statement	is	little	more	than	common	sense	.
where	the	author	goes	outside	of	common	sense	,	he	's	wrong	,	sometimes	plainly	so	.
this	book	is	part	of	the	tradition	in	sociology	of	thinking	that	society	pulls	the	wool	over	each	individuals	eyes	,	that	reality	is	a	social	construct	with	no	purpose	other	than	social	control	(	here	the	wool	is	the	performance	,	and	the	control	has	to	do	with	impressions	DOTS	but	this	quickly	degenerates	into	an	obscure	mess	of	assertions	)	.
there	is	no	real	empirical	support	given	for	most	of	the	claims	.
where	evidence	is	given	,	the	evididence	is	so	heavily	intepreted	that	it	fails	to	correspond	to	facts	in	the	world	but	rather	to	facts	about	the	views	of	the	reporter	.
where	there	is	no	correspondence	to	the	world	there	is	no	truth	.
various	sources	of	sociology	are	cited	,	but	this	is	more	like	intellectual	bullying	.
saying	that	some	author	,	which	also	didnt	have	any	empirical	support	for	his	claims	,	agrees	with	you	is	just	to	bully	your	reader	into	acepting	your	claims	.
the	claims	in	the	book	have	no	practical	application	in	the	world	,	but	only	serve	to	intepret	situations	differently	,	and	in	personal	opinion	,	less	correctly	.
nothing	said	here	will	lead	to	better	predictions	about	social	behavior	or	a	better	understanding	of	psychology	.
in	short	,	this	isnt	a	scientific	study	at	all	.
my	final	qualm	with	the	book	deserves	its	own	paragraph	,	the	use	of	language	.
the	best	example	is	the	definitions	given	at	the	end	of	the	introduction	a	performance	may	be	defined	as	all	of	the	activity	of	a	given	participant	on	a	given	occasion	which	serves	to	influence	in	any	way	any	of	the	other	participants	(	NUMBER	)	.
this	is	a	curios	definition	.
me	breathing	while	i	work	is	a	performance	,	it	changes	the	percentages	of	o2	and	co2	in	the	atmosphere	near	my	co-workers	.
me	staring	into	space	is	a	performance	,	other	people	see	me	stare	into	space	and	are	therefore	influenced	.
infact	,	me	simply	existing	is	a	performance	,	since	my	existance	causes	a	gravitational	effect	on	the	other	participants	,	as	well	as	influencing	them	to	create	beliefs	about	me	existing	.
its	as	if	goffman	created	a	definition	for	performance	which	included	all	possible	actions	taken	by	any	person	,	and	then	wrote	a	book	about	how	all	possible	actions	taken	by	any	person	fall	under	the	category	of	performance	.
he	didnt	.
he	used	a	word	that	we	associate	with	controlled	behavior	(	performance	)	and	defined	it	in	a	way	no	one	is	used	to	.
then	he	used	alot	of	word	play	to	show	that	behavior	has	no	substance	,	its	all	for	the	purpose	of	maintaining	social	reality	.
i	hope	this	very	limited	example	shows	the	terrible	use	of	language	that	is	endemic	to	goffman	and	sociology	more	generally	.
so	an	ode	to	sociology	is	in	order	DOTS	actually	no	it	isnt	.
why	wont	this	subject	go	away	?
this	book	is	an	integral	part	of	sociology	,	and	it	espouses	the	methods	and	style	of	sociological	researchers	and	writers	by	being	an	archetypical	work	within	the	tradition	.
the	methods	are	questionable	;	the	style	is	obscure	.
like	it	or	hate	it	,	sociology	is	a	part	of	modern	thought	.
read	the	book	to	be	an	educated	person	and	keep	in	mind	its	intellectual	failings