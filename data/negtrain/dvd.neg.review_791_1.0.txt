delmer	daves	mediocre	direction	of	the	sequel	to	the	equally	pathetic	film	the	robe	starring	richard	burton	.
a	totally	contrived	film	that	demonstrates	no	competence	in	either	screenplay	,	direction	,	acting	,	or	historical	accuracy	of	any	sort	.
avoid	at	all	costs	unless	you	are	expecting	to	see	it	as	a	comedy	.
compared	to	kubrik	's	incredible	direction	for	'spartacus	a	few	years	later	,	the	gladiatorial	scenes	in	this	film	are	second	rate	.
even	the	low	budget	film	'barabas	with	anthony	quinn	had	better	direction	than	this	worthless	film	.
the	screenplay	is	contrived	,	the	script	is	worthless	,	and	the	acting	utterly	shallow	from	everyone	on	this	film	:	at	least	'the	robe	had	richard	burton	.
victor	mature	continues	to	act	as	if	he	just	came	out	of	a	cocktail	lounge	after	a	few	martinis	too	many	.
jay	robinson	continues	his	idiotic	and	ridiculous	performance	of	caligula	as	if	the	character	were	a	retarded	bufoon	instead	of	the	diabolical	genius	of	cruelty	and	perversion	(	see	hurt	's	performance	in	the	bbc	's	'i	,	claudius	to	see	how	a	good	actor	handles	this	character	.	)
in	short	,	reading	the	new	testament	or	going	to	a	church	sermon	is	far	more	entertaining	and	historically	faithful	than	watching	this	garbage	film	.
just	the	fact	that	victor	'manure	is	in	this	film	says	it	all	.
ignore	this	pathetic	film	and	watch	'spartacus	,	'barabas	,	or	even	the	fantasized	'gladiator	for	some	good	gladiatorial	films	.
as	for	biblical	films	,	'ben	hur	,	'the	ten	commandments	or	'quo	vadis	are	films	that	are	worth	watching	much	more	than	this	putrid	hollywood	junk	.