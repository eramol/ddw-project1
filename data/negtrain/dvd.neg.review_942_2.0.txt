a	friend	and	i	tried	watching	this	film	once	again	recently	,	but	still	we	both	find	it	irritating	and	unpleasant	.
from	the	beginning	,	when	katharine	hepburn	's	character	destroys	other	people	's	cars	without	a	care	,	we	found	one	scene	after	another	tedious	and	unfunny	.
the	acting	is	fine	,	as	could	be	expected	from	such	greats	as	hepburn	and	cary	grant	.
but	the	characters	are	not	sympathetic	,	fun	or	interesting	.
grant	's	character	is	an	unassertive	,	spineless	person	who	is	easily	dominated	.
hepburn	's	character	is	blindly	selfish	.
who	could	care	what	happens	to	them	?
the	one	interesting	element	is	the	acting	that	is	done	in	proximity	to	a	live	leopard	.