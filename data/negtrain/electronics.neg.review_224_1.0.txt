before	you	buy	this	drive	,	be	sure	to	google	cypress	at2lp	rc42	and	western	digital	.until	yesterday	,	i	recommended	this	drive	to	everyone	.
then	my	drive	failed	,	prompting	me	for	new	hardware	and	to	install	a	driver	for	cypress	at2lp	rc42	.
it	appears	that	there	is	a	chip	in	the	drive	housing	manufactured	by	cypress	that	is	failing	in	drives	left	and	right	.
(	class	action	suit	anyone	?
)	the	western	digital	support	staff	is	no	help	.
act	like	they	have	never	heard	of	this	error	.
offer	no	option	to	just	switch	out	the	housing	.
will	replace	drive-	but	you	lose	all	the	data	.
there	are	$	NUMBER	replacement	housings	available	-	but	you	void	your	warranty	.
bottom	line	-	if	you	buy	this	drive	,	be	prepared	for	it	to	fail	.
keep	everything	backed	up	,	because	there	will	come	a	day	that	this	drive	will	fail