i	ca	not	remember	the	last	time	i	failed	to	finish	reading	a	novel	.
i	abandoned	this	one	with	NUMBER	pages	to	go	.
with	another	implausible	plot	twist	,	i	've	had	enough	.
maybe	i	've	been	spoiled	by	the	quality	of	character	driven	fiction	from	george	pelecanos	,	james	lee	burke	,	robert	b	parker	,	james	sallis	,	scott	phillips	,	and	robert	crais	but	i	suspect	that	this	is	not	just	a	failure	to	connect	with	the	characters	and	more	to	do	with	poor	writing	.
i	've	read	quite	a	number	of	deaver	's	novels	and	enjoyed	the	complex	plots	and	the	exploration	of	psychologically/psyciatrically	devient	chbaracters	,	but	this	is	just	over	the	top	.
when	i	came	to	the	chapter	that	excplained	how	the	magician	faked	his	death	while	in	custody	,	that	was	enough	.
i	have	another	fifty	novels	lined	up	waiting	to	be	read	and	i	will	be	damned	if	i	am	going	to	waste	any	more	time	on	this	poor	one	.
for	that	matter	,	after	six	or	so	lincoln	rhyme	novels	,	i	ca	not	say	that	i	really	know	or	care	anything	about	lincoln	or	saches