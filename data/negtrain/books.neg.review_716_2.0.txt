i	like	john	grisham	,	but	the	king	of	torts	was	a	slow-moving	disappointment	.
grisham	is	obviously	making	an	argument	for	tort	reform	in	this	novel	,	which	is	fine	,	but	he	ends	up	putting	the	message	above	the	story	.
grisham	devotes	much	of	this	novel	to	a	young	plaintiffs	lawyer	and	his	new	law	firm	dedicated	to	class	action	lawsuits	.
this	provides	grisham	with	an	excuse	to	explain	,	in	agonizing	detail	,	how	tort	lawyers	sue	big	companies	and	collect	huge	fees	.
grisham	also	produces	scene	after	scene	describing	the	enormous	wealth	of	class	action	lawyers	,	and	how	they	put	their	own	greed	above	the	interests	of	their	clients	.
in	the	end	,	none	of	this	adds	up	to	an	engaging	storyline	.
it	does	not	help	that	the	young	lawyer	in	this	novel	is	a	rather	unlikable	person	that	grisham	is	obviously	setting	up	for	a	fall	.
since	i	did	not	care	for	this	character	,	i	found	the	storyline	of	the	king	of	torts	to	be	largely	uninvolving	.
this	novel	is	also	rather	preachy	and	heavy-handed	in	tone	.
i	personally	dislike	class	action	lawsuits	,	but	i	read	novels	for	entertainment	,	not	to	validate	my	own	political	beliefs	.
although	this	book	was	decently	written	,	i	just	did	not	find	the	story	interesting	enough	to	recommend	.
for	a	better	version	of	this	story	,	i	would	suggest	a	civil	action	by	jonathan	harr	.