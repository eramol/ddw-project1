in	terms	of	sound	quality	,	noise	cancelling	,	and	general	comfort	-	thise	headphones	are	top	of	the	line	.
i	absolutely	love	the	way	the	sound	and	the	ammount	of	noise	cancelling	that	they	provide	.
i	used	them	regularly	when	mowing	the	lawn	and	when	flying	,	and	i	always	recieved	the	highest	quality	sound	and	noise	cancelleation	.
i	was	even	able	to	listen	to	drum-n-bass	while	mowing	the	lawn	and	hear	all	of	the	appropriate	frequencies	on	the	low	end	.
however	DOTS	inside	of	NUMBER	months	,	i	have	gone	through	NUMBER	pairs	of	these	headphones	because	the	right	ear	headphone	has	gone	out	on	them	.
the	worst	part	is	-	i	ca	not	even	begin	to	imagine	why	it	goes	out	.
i	keep	my	heaphones	in	a	locked	cabinet	in	my	office	every	night	.
last	friday	,	i	placed	back	into	the	nice	little	carry	bag	that	they	come	with	,	and	placed	them	into	this	cabinet	.
this	morning	when	i	pulled	them	out	for	use	,	the	right	ear	bud	is	out	again	DOTS	no	physical	damage	is	apparent	anywhere	on	the	headphones	,	and	there	is	not	a	broken	wire	anywhere	.
in	fact	,	the	wiring	is	perfectly	fine	and	still	functional	.
it	is	the	physical	earbud	-	the	hard	plastic	peice	-	that	has	gone	out	.
if	i	squeeze	the	earbud	hard	enough	,	it	will	turn	on	again	.
something	inside	of	the	earbud	is	obviously	designed	improperly	and	i	would	highly	recommend	that	noone	purchase	these	headphones	because	of	this