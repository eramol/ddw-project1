i	came	to	blowback	by	way	of	a	year-long	research	project	on	globalization	.
over	the	past	year	i	have	read	dozens	of	books	,	both	scholary	and	popular	,	on	this	topic	.
right	-	left	-	love	america	-	hate	america	-	multi-cultural	-	eurocentric	;	i	've	suffered	through	them	all	(	happily	,	there	is	as	yet	no	feminist	perspective	on	the	subject	)	.
first	let	me	state	that	chalmers	johnson	,	this	regrettable	book	notwithstanding	,	is	not	by	profession	the	village	diot	.
johnson	is	a	highly	respected	economic	and	political	analyst	,	a	man	who	used	to	be	worth	listening	to	.
what	happened	?
how	did	he	come	to	cobble	together	this	breathtakingly	unanalytical	smorgasborg	?
where	did	the	imperialist	conspiracy	rant	come	from	?
johnson	,	where	did	you	go	?	well	,	the	easy	answer	is	that	he	delves	into	complex	areas	of	which	he	has	no	knowledge	.
his	take	on	america	's	military	policies	is	ludicrous	.
anyone	who	questions	why	the	u.s.	army	is	still	in	korea	after	50+	years	need	only	point	to	seoul	on	a	map	and	drag	his	finger	a	couple	of	inches	north	,	where	he	will	discover	north	korea	.
of	coures	once	an	idealogue	veers	into	unknown	territory	there	is	no	other	path	left	for	him	than	that	of	conspiracy	.
conspiracy	theory	is	all	fine	and	good	for	the	lazy	and	uneducated	;	it	's	their	sanctuary	;	but	for	a	scholar	of	johnson	's	caliber	to	go	there	is	incomprehensible	.
unti	you	see	what	i	saw	.
the	professional	works	that	i	read	on	globalization	were	pretty	much	uniform	in	that	they	addressed	one	or	another	facet	of	free-trade	and	concluded	more-or-less-nothing	,	all	in	scholarly	language	.
for	the	most	part	the	popular	books	were	not	nominally	about	globalization	at	all	,	although	globalization	was	the	only	thread	holding	the	books	together	,	and	to	each	other	.
brushing	aside	the	fact	that	most	of	the	authors	of	the	popular	group	are	ageing	oddballs	from	the	YEAR	(	you	underestimate	the	mustard-gas-like	effect	of	that	era	at	your	peril	)	,	the	only	possible	reason	for	these	immensely	angry	,	nonsensical	books	is	this	:	no	one	fully	understands	globalization	.
no	one	.
even	when	you	define	the	thing	narrowly	it	is	a	cantankerous	beast	,	too	young	to	tame	;	growing	unpredictably	every	day	like	the	blob	.
they	are	frustrated	,	these	authors	.
i	ca	not	blame	them	,	but	for	some	reason	they	must	have	a	scapegoat	and	the	've	rounded	up	the	usual	suspect	,	america	,	as	in	why	does	america	impose	-	dominate	-	ignore	,	etc	.
the	third	world	?
silly	question	,	really	.
you	are	at	the	top	of	the	food	chain	,	what	do	they	expect	you	to	do	?
when	bad	things	happen	you	want	to	be	sure	they	happen	to	someone	else	.