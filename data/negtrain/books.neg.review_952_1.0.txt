i	do	not	understand	why	anyone	would	publish	a	book	anymore	with	black	and	white	illustrations	.
i	know	this	is	supposed	to	be	a	classic	and	all	that	so	it	's	my	own	fault	for	not	checking	when	i	read	the	description	.
i	do	not	even	know	why	i	am	keeping	it	;	i	will	never	read	it	.
you	have	to	wade	through	tons	of	technical	jargon	and	lengthy	chapters	that	would	be	much	better	served	by	some	large	color	photographs	.
maybe	that	makes	me	shallow	,	i	do	not	know	,	but	there	are	much	more	'complete	guides	out	there	on	this	topic	these	days