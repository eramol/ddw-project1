i	bought	this	phone	for	a	work-at-home	situation	.
i	then	discovered	that	the	maximum	number	of	characters	i	could	enter	into	a	speed	dial	entry	(	the	number	itself	,	not	the	name	)	is	NUMBER	characters	.
a	telephone	calling	card	number	is	NUMBER	characters	long	,	NUMBER	if	you	program	a	pause	(	a	space	,	for	this	phone	)	between	the	phone	number	and	the	card	id/pin	.
when	i	called	polycom	about	it	,	i	was	told	that	not	everyone	uses	calling	cards	.
funny	.
i	've	been	issued	a	calling	card	at	every	company	for	which	i	've	worked	(	going	back	NUMBER	years	)	.
this	is	the	only	phone	i	've	ever	owned	(	well	,	since	the	NUMBER	's	)	into	which	i	could	not	program	my	calling	cards	.
pretty	basic	stuff	for	product	marketing	and	development	people	to	miss	.
on	top	of	that	,	when	i	try	and	enter	the	calling	card	number	and	card	id	by	hand	,	it	never	takes	on	the	first	try	:	the	network	does	not	recognize	the	card	id	and	pin	the	first	time	i	enter	them	.
i	have	to	do	it	twice	(	i	've	tried	this	a	number	of	times	,	and	it	's	consistent	)	.for	that	matter	,	the	sound	quality	,	especially	through	the	headset	(	also	purchased	from	polycom	)	is	not	that	good	.
of	course	,	that	could	be	because	the	headset	jack	is	loose	.
you	can	easily	move	the	jack	from	side	to	side	,	which	makes	the	sound	quality	even	worse	.
i	regret	having	made	this	purchase	.
i	am	going	to	scrap	it	and	look	for	a	new	one	from	a	different	manufacturer	.
the	only	reason	i	gave	it	one	star	is	because	the	site	interface	here	wo	not	let	me	give	it	no	stars