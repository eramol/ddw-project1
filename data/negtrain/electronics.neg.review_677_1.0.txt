i	bought	this	for	my	80-year-old	mother	,	who	is	disabled	and	ca	not	cope	with	a	computer	.
every	time	the	typewriter	is	turned	on	,	we	find	that	it	either	types	the	wrong	letter	(	press	a	q	,	it	prints	an	s	;	press	e	and	get	p	,	etc	.	)
or	else	it	prints	the	correct	letter	but	they	are	not	spaced	correctly	(	some	overlapping	,	some	huge	spaces	)	.
there	is	some	esoteric	combination	of	keystrokes	that	,	supposedly	,	will	reset	the	programming	.
but	this	is	just	too	much	for	mom	.
also	,	every	time	it	's	turned	on	,	you	have	to	re-program	the	line	spacing	,	the	tabs	and	everything	else	.
i	ended	up	buying	a	40-year-old	manual	typewriter	on	e-bay	and	mom	is	much	happier	with	it