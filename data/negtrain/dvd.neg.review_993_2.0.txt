this	is	a	wonderful	film	.
some	may	say	it	's	a	bit	slow	paced	and	perhaps	it	's	not	geared	for	the	mtv	generation	's	pace	of	visual	stimulation	.
nonetheless	,	patience	has	it	's	rewards	for	anyone	who	cares	to	take	a	chance	.
it	appears	that	every	last	detail	in	this	film	was	molded	by	the	director	,	tati	.
unfortunately	,	the	video	transfer	on	this	edition	is	so	terribly	soft	,	as	though	the	telecine	was	not	in	focus	.
it	's	a	crime	that	such	a	great	film	could	not	have	been	treated	with	greater	care	.
one	only	hopes	that	a	dvd	transfer	of	superior	quality	is	in	the	pipes