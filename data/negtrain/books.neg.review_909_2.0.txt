i	held	great	hopes	for	this	book-duveen	has	long	been	of	interest	to	me	because	of	the	pivotal	role	he	played	in	the	creation	of	some	of	the	greatest	art	collections	in	this	country	.
however	,	secrest	in	her	drive	to	capture	the	essence	of	the	man	has	so	mangled	the	story	of	his	life	and	career	that	reading	her	work	is	more	chore	than	delight	.
to	say	the	book	is	disorganized	is	to	deal	in	serious	understatement	.
but	worse	than	that	are	the	inaccuracies	,	especially	when	she	writes	about	duveen	's	customers	.
just	for	starters	,	apparently	she	did	not	recognize	the	need	to	differentiate	between	john	d.	rockefeller	,	jr.	and	his	father	(	or	maybe	she	did	not	know	there	has	been	more	than	one	jdr	!	)	.
you	wo	not	learn	much	from	this	tome	that	you	do	not	know	to	begin	,	and	getting	through	it	will	be	a	struggle	.