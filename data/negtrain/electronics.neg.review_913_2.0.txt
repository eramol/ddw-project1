it	is	a	great	idea	,	with	a	few	qualifiers	.
i	have	found	that	the	forward	and	reverse	buttons	do	not	function	for	my	video	ipod	or	for	my	niece	's	nano	(	maybe	it	would	for	a	non-apple	mp3	player	?	)	.
i	have	to	pair	the	device	to	the	ipod	every	time	i	turn	it	on	.
the	bluetooth	transponder	will	drain	the	ipod	's	battery	even	though	it	has	it	's	own	battery	.
sound	quality	is	decent	.