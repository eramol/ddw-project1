this	book	is	an	nice	collection	of	differing	points	of	views	about	latinos/hispanics	.
however	it	's	primarily	philisophical	.
it	's	more	confusing	than	inspiring	.
it	's	great	for	those	studying	for	a	phd	,	but	will	not	inspire	average	latinos	to	action	.
this	book	tilts	slightly	more	towards	the	assimilationist	point	of	view	,	although	a	few	writers	do	express	some	pan-latino	thoughts	.