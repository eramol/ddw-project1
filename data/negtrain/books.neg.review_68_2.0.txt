DOTS	are	incorrect	and	that	his	fractal	models	are	much	better	.
of	course	if	his	models	were	worth	more	than	the	paper	they	are	written	on	mandelbrot	would	not	have	to	write	books	like	this	because	he	'd	be	cleaning	out	everyone	else	's	wallets	on	the	stock	market	.
in	particular	,	if	it	's	true	that	extreme	events	are	more	unlikely	than	most	people	think	then	he	could	easily	exploit	this	with	a	suitable	derivative	.
but	the	fact	is	,	mandelbrot	does	not	know	anything	that	countless	other	traders	do	not	already	know	.
so	instead	mandelbrot	is	forced	to	resort	to	telling	people	how	smart	he	is	through	his	books	rather	than	actually	being	smart	enough	to	make	a	killing	on	wall	street	.
but	i	am	glad	i	read	this	book	.
having	seen	mandelbrot	'on	tour	a	few	years	ago	i	developed	a	strong	prejudice	against	him	.
but	reading	this	book	has	convinced	me	that	my	prejudice	is	entirely	justified	.
he	can	never	just	state	a	fact	.
instead	he	always	has	a	to	phrase	it	as	my	work	shows	that	DOTS	or	i	demonstrated	that	DOTS	even	if	he	makes	the	same	claim	again	and	again	.
if	he	can	try	to	take	credit	for	other	people	's	work	he	will	do	so	.
he	even	managed	to	find	someone	to	write	an	introduction	for	the	book	who	was	prepared	to	refer	to	the	levy	distribution	as	the	mandelbrot-levy	distribution	.
the	most	egregious	example	of	self-aggrandisement	has	to	be	the	caption	to	a	picture	of	the	brooks-metelsky-mandelbrot	set	where	he	mentions	that	two	mathematicians	only	scaled	part	way	up	his	'everest	of	this	set	and	received	fields	medals	for	this	work	-	the	implication	of	course	being	that	mandelbrot	has	actually	seen	the	view	from	the	summit	.
disgustingly	he	does	not	even	deign	to	mention	the	names	of	these	mathematicians	.
one	time	i	criticised	mandelbrot	publicly	and	someone	responded	by	pointing	out	how	many	peer	reviewed	papers	he	had	published	so	he	ca	not	be	all	bad	.
but	in	this	book	mandelbrot	actually	reveals	to	us	how	he	used	his	social	network	to	work	around	the	peer	review	system	.
astonishing	!	i	think	it	's	also	worth	pointing	out	that	there	are	some	interesting	ideas	in	this	book	and	so	it	's	not	completely	valueless	.
but	i	found	the	discussion	of	multifractal	time	methods	(	which	make	up	a	large	part	of	the	book	)	to	generate	plausible	looking	but	fake	price	charts	to	be	a	bit	pointless	.
i	work	in	computer	graphics	and	use	similar	methods	to	generate	random	surface	detail	all	the	time	,	but	it	does	not	mean	i	have	a	deep	understanding	of	the	statistics	of	random	surfaces	.
it	just	means	i	know	how	to	fool	the	eye