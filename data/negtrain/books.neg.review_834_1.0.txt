dr.	freeman	's	justly	esteemed	lee	's	lieutenants	-	a	three	volume	work	-	still	stands	(	despite	the	discovery	of	materials	unavailable	to	dr.	freeman	when	he	first	wrote	in	the	YEAR	and	YEAR	)	as	the	baseline	for	any	study	of	the	army	of	northern	virginia	.
the	cheap	attempt	by	a	modern	publisher	to	squeeze	a	few	cents	out	of	the	franchise	,	offered	here	,	throws	out	two	of	every	three	words	dr.	freeman	wrote	to	allow	for	a	one-volume	abridgement	for	those	presumably	too	intimidated	by	the	thought	of	reading	three	entire	books	.
the	three-volume	work	,	as	dr.	freeman	wrote	it	,	was	reprinted	fairly	recently	.
if	that	is	no	longer	available	on	amazon	,	any	decent	sized	used	book	store	will	have	it	in	stock	.
whatever	you	do	,	avoid	this	butchered	,	one	volume	version	like	the	plague