someone	reported	all	my	NUMBER	negative	reviews	of	daniel	silva	books	and	got	them	removed	!
hmm	,	i	wonder	who	that	could	be	.
i	am	rather	annoyed	at	this	because	i	spent	quite	a	lot	of	time	detailing	why	i	did	not	like	one	of	the	books	,	reasonably	,	i	thought	.
apparently	,	someone	had	decided	that	they	did	not	follow	the	review	guidelines	.
i	used	no	profanities	.
ok	,	there	were	some	spoilers	,	but	not	more	than	i	usually	read	in	other	reviews	.
not	single	worded	,	no	phone	numbers	,	no	solicitations	,	etc	.
maybe	whoever	it	was	just	did	not	like	my	negative	review	.
well	,	the	last	time	i	checked	,	the	ORDINAL	amendment	still	applies	in	this	country	.
ok	,	so	let	me	try	to	post	another	review	of	why	i	did	not	like	this	book	without	violating	any	review	guidelines	,	and	i	am	not	going	to	spend	NUMBER	mins	on	it	like	last	time	.
i	thought	the	book	was	very	poorly	written	and	very	boring	.
i	struggled	through	1/3	of	the	book	but	the	author	could	not	capture	my	attention	and	i	decided	not	to	continue	to	the	end	.
i	've	read	a	couple	of	other	daniel	silva	books	,	but	neither	one	of	them	could	change	my	mind	that	he	is	an	author	that	i	do	not	like	.
i	will	not	be	buying	any	more	of	his	books