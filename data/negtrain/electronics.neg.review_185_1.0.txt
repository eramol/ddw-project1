i	had	this	for	approximately	NUMBER	years	.
it	worked	fine	for	couple	of	months	but	suddenely	started	to	activate	the	alarm	without	any	disturbances	.
one	day	i	left	my	laptop	attached	the	alarm	in	my	office	for	about	half	an	hour	.
during	this	period	,	the	alarm	was	activated	and	i	was	embarrased	to	face	my	office	workers	.
this	is	without	increasing	the	sensitivity	of	the	alarm	.
i	sent	it	back	to	targus	and	they	were	nice	enough	to	give	me	a	new	alarm	.
but	after	several	months	,	it	started	the	same	problem	.
so	,	now	i	am	afraid	to	use	this	security	system	with	my	laptop	.
but	i	did	not	face	problems	like	changing	the	combination	by	its	own	.
you	can	use	it	without	the	alarm	activation	(	i.e	:	without	batteries	)	but	the	cable	is	so	thin	,	it	is	not	recommended	to	protect	any	valubles