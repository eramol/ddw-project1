i	bought	this	item	after	seeing	the	almost	uniformly	good	reviews	including	the	one	that	says	there	is	good	macintosh	support	.
unfortunately	the	player	does	not	work	well	with	the	macintosh	.
the	included	software	often	crashes	and	does	not	seem	to	be	able	to	import	dss	files	.
the	software	can	move	the	files	between	the	recorder	and	the	computer	but	the	files	do	not	appear	in	the	software	window	.
the	use	interface	is	also	fairly	crude	and	looks	as	if	it	was	ported	directly	from	windows	NUMBER	.
the	ds-2	appears	to	the	finder	as	a	standard	usb	disk	and	i	have	been	able	to	copy	the	dss	files	from	the	player	to	the	compuer	using	the	finder	.
it	is	then	possible	to	play	the	dss	files	by	double	clicking	them	,	but	it	will	only	play	one	file	.
if	you	want	to	play	another	file	it	is	necessary	to	completely	quit	the	dss	player	and	relanch	it	.
exporting	the	files	to	a	format	usable	by	other	software	such	as	aiff	is	also	nearly	impossible	.
there	is	an	export	function	in	the	menu	,	when	it	is	selected	it	asks	were	the	new	file	should	be	saved	but	no	file	ever	appears	.
unfortunately	there	is	no	other	macintosh	software	available	to	read	or	convert	dss	files	as	olympus	refuses	to	allow	others	use	the	the	format	on	the	macintosh	.
search	of	the	internet	reveals	that	others	have	found	extremely	painful	ways	of	converting	the	dss	files	such	as	playing	the	entire	file	and	re-recording	it	with	another	program