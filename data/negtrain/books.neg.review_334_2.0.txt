there	are	two	things	which	tickle	me	about	this	book	.
one	is	the	sample	zack	's	report	at	the	back	which	has	a	strong	buy	#	NUMBER	rating	on	a	-.3	%	return	on	equity	stock	.
the	other	is	the	section	on	practical	use	of	the	system	.
it	,	of	course	,	is	for	any	type	of	investor	including	long	term	investors	even	though	the	effects	that	the	system	measures	lasts	only	for	NUMBER	-	NUMBER	months	.
and	do	not	forget	about	growth	investors	and	value	investors	either	.
this	section	is	so	short	as	to	be	laughable	.
also	notice	that	in	the	NUMBER	a	list	there	were	NUMBER	stocks	.
most	are	going	to	have	to	buy	a	subset	.
but	what	subset	?
no	info	is	provided	on	the	performance	of	typical	subsets	for	each	investor	class	.
perhaps	an	mvo	addict	or	a	monte	carlo	hack	would	have	the	answer