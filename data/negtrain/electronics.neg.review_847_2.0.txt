when	realtime	traffic	monitoring	came	to	my	city	,	i	immediately	started	looking	at	solutions	to	help	my	wife	's	daily	business	driving	(	2-500	miles	weekly	,	crisscrossing	a	50-mile	area	through	major	traffic	routes	)	.
i	quickly	settled	on	an	automotive	gps	with	realtime	traffic	monitoring	.
there	are	not	yet	many	choices	in	this	area	,	so	the	options	boiled	down	to	this	unit	and	the	garmin	NUMBER	.
(	these	use	fm	radio	for	the	traffic	info	;	the	xm-radio	units	like	the	tomtom	NUMBER	and	garmin	NUMBER	had	unacceptably	high	monthly	costs	since	i	do	not	want	xm	radio	.
)	i	chose	the	cobra	for	its	large	screen	and	positive	reviews	in	areas	that	were	important	to	me	.
its	flawed	routing	ability	was	not	a	major	issue	as	it	was	to	be	used	in	a	well-known	region	.
address	lookup	and	traffic	monitoring	were	the	primary	needs	.
in	short	,	i	do	not	know	much	about	how	the	cobra	performs	because	i	was	only	able	to	use	it	for	a	few	short	test	trips	.
this	is	because	the	mounting	hardware	is	one	of	the	most	deficient	pieces	of	engineering	i	have	ever	encountered	in	higher-end	electronics	gear	:	-	the	only	mounting	option	is	via	suction	cup	,	which	in	tests	would	adhere	reliably	only	to	nearly	flat	glass	.
a	stick-on	disc	for	attaching	the	suction	cup	is	also	provided	,	but	i	did	not	attempt	to	use	it	and	would	not	trust	it	to	be	able	to	hold	this	heavy	(	NUMBER	pound	)	unit	over	the	long	run	.
if	your	vehicle	does	not	have	a	flat	section	of	windshield	in	a	convenient	location	,	you	will	have	very	few	options	for	mounting	this	unit.-	california	law	prohibits	attaching	things	to	the	windshield	,	meaning	that	california	users	either	need	to	risk	a	minor	ticket	or	find	an	acceptable	alternate	mounting	point	.
(	good	luck	to	you	:	i	was	unable	to	find	one	in	three	different	vehicles	!	)
compounding	the	problem	is	that	the	mount	has	a	very	limited	positioning	range	and	easily	runs	out	of	adjustment	room	for	most	potential	mounting	alternatives.-	cobra	does	not	offer	any	other	mounts	or	mounting	accessories	at	all	-	no	beanbag	or	friction	mount	,	no	clamp	,	no	fixed-base	mount	,	nothing	.
if	there	is	any	aftermarket	option	(	ram	mount	,	etc	.	)
i	was	unable	to	find	it	.
you	are	stuck	with	cobra	's	proprietary	mount	unless	you	are	willing	to	modify	the	unit	or	its	mounting	pieces	and	fabricate	an	adapter	for	a	garmin	or	ram	mount	base	.
i	was	*almost*	willing	to	go	the	extra	mile	to	make	this	unit	work	in	my	vehicle	,	even	though	the	best	solution	would	have	been	very	awkward	to	dismount	and	remount	,	making	it	a	potential	theft	target	.
the	final	straw	was	when	i	found	that	the	traffic	receiver	was	faulty	and	i	was	unable	to	get	a	reply	from	the	manufacturer	in	a	timely	manner	(	two	days	now	and	counting	DOTS	)	enough	's	enough	;	something	was	telling	me	that	this	unit	was	not	the	right	choice	.
so	this	unit	is	being	returned	and	i	've	already	ordered	a	garmin	NUMBER	with	gtm11	traffic	receiver	in	replacement	.
the	prices	for	the	units	through	amazon	are	nearly	the	same	(	especially	when	factoring	in	the	costs	for	NUMBER	months	of	traffic	monitoring	-	the	cobra	comes	with	three	months	and	gets	$	NUMBER	per	year	after	that	,	while	the	garmin	comes	with	NUMBER	months	and	has	the	same	annual	cost	after	that	-	so	you	have	to	add	$	NUMBER	to	the	cobra	to	get	a	truly	equivalent	price	)	.
garmin	also	has	excellent	mounts	with	a	range	of	attachment	options	,	some	of	the	best	map	and	routing	tools	,	and	a	long	track	record	with	gps	units	.
it	does	have	a	smaller	screen	and	some	complaints	about	the	user	interface	,	but	at	least	i	can	mount	it	properly	and	be	able	to	tuck	it	away	and	remount	it	easily	to	prevent	theft	.
this	unit	gets	two	stars	only	because	my	brief	experience	with	its	function	appears	to	live	up	to	all	the	positive	reviews	-	but	overall	,	it	's	like	a	high-performance	sports	car	with	no	wheels	!
i	NUMBER	%	recommend	against	it	for	california	users	and	suggest	that	others	try	to	lay	hands	on	a	sample	unit	to	see	if	it	can	be	properly	mounted	in	your	vehicle	before	laying	out	$	NUMBER