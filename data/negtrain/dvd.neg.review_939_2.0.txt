heavyweights	was	a	quite	a	disappointment	,	considering	the	presence	of	ben	stiller	.
the	movie	sheds	light	on	a	popular	summer	camp	for	fat	children	that	unexpectedly	finds	itself	under	different	management	.
the	new	administration	will	make	it	clear	very	soon	that	some	major	changes	are	to	take	place	DOTS
the	plot	and	the	acting	are	surprisingly	(	!	)
average	,	while	the	humor	and	the	dialogues	are	below	average	.
ben	stiller	's	character	,	a	cross	between	zoolander	and	dodgeball	(	both	being	amazing	movies	)	,	was	unexpectedly	plain	and	bland	.
as	for	the	rest	of	the	cast	,	this	film	does	not	seem	to	be	their	thing	.
though	the	potential	for	a	great	movie	was	definitely	there	it	fails	to	take	off	.
a	shame	really	DOTS	no	masterpiece	here	.