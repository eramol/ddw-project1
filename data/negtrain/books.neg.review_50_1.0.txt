this	is	a	moderately	well-spun	book	about	the	no	spin	zone	.
should	be	called	the	know	spin	zone	since	really	nothing	in	it	is	objective	.
o'reilly	is	a	case-study	in	the	typesubjective	journalism	that	is	prevalent	in	the	media	these	days	and	the	book	shows	this	.
in	other	words	it	's	a	must	read	for	everyone	with	the	ability	to	see	through	it	.
if	you	ca	not	see	through	o'reilly	's	bias	then	this	book	is	poison