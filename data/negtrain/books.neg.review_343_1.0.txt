gary	zukav	states	in	his	introduction	i	had	never	studied	physics	.
in	fact	i	did	not	like	science	and	i	had	no	mathematical	aptitude	.
on	opra	(	tv	)	he	admitted	that	he	does	not	have	a	tv	which	may	explain	his	lack	of	contemporary	physics	let	alone	any	depth	.
i	noticed	that	some	reviewers	refer	to	the	new	physics	is	that	like	new	age	or	voodoo	physics	?
NUMBER	%	of	the	matter	must	be	missing	from	this	book	.
or	else	how	can	he	take	physics	out	of	context	and	make	such	fantastic	leaps	to	religions	parallels	that	he	knows	little	of	.
he	even	twists	the	religion	around	to	serve	some	unknown	purpose	.
many	people	say	they	did	not	understand	physics	until	this	book	;	surprises	,	you	still	do	not	.
you	now	know	what	zukav	wished	physics	was	.
take	anti-mater	for	instants	that	does	not	mean	the	opposite	of	mater	.
and	the	relationship	between	particles	has	no	correlation	with	the	relationship	of	dogs	and	cats	.
at	least	get	it	straight	before	mixing	it	up	.
try	reading	some	of	these	:	the	ascent	of	man	by	jacob	bronowski	.
the	book	available	everywhere	the	dvd	's	available	from	the	uk	.
the	upanishads	by	eknath	easwaran	(	editor	)	,	michael	n.	nagler	(	photographer	)	or	just	about	any	mainstream	material	on	physics	and	religion	.
then	if	you	still	want	to	mix	worlds	into	one	read	someone	saner	:	the	tao	of	physics	:	an	exploration	of	the	parallels	between	modern	physics	and	eastern	mysticism	by	fritjof	capra	.
also	on	the	fringe	but	not	as	radical	as	zukav	is	a	series	called	what	the	bleep	.
(	both	in	books	and	on	video	)