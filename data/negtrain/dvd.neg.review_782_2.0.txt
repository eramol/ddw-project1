every	once	in	a	while	someone	comes	up	with	a	movie	and	makes	the	terrible	assumption	that	as	long	as	they	put	a	well	known	actor/actress	in	it	that	everything	will	turn	out	good	.
blue	steel	is	one	of	those	movies	.
jamie	lee	curtis	plays	a	rookie	cop	who	is	being	held	responsible	for	civilian	casualties	(	or	should	i	say	murders	)	which	she	did	not	commit	.
sounds	like	an	interesting	plot	,	right	?
it	would	be	had	the	script	not	been	so	poorly	written	.
the	supporting	actors	give	such	a	bland/typical	performance	that	not	even	jamie	lee	could	have	saved	the	dialogue	.
aside	from	the	acting	,	the	film	does	not	give	the	action	packed	performance	that	it	promises	.
some	scenes	are	quite	dull	and	as	you	are	watching	you	wonder	half	way	through	the	movie	when	the	cop	(	jamie	lee	)	is	going	to	finally	kill	the	psychopath	whose	been	after	her	.
so	why	do	i	give	blue	steel	an	extra	star	?
well	,	i	will	just	say	that	if	you	are	a	fan	of	jamie	lee	curtis	you	might	want	to	watch	this	if	it	comes	on	television	.
just	do	not	pay	over	$	NUMBER	to	rent	it	and	certainly	do	not	buy	it