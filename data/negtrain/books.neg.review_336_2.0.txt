this	book	contains	some	useful	historical	information	and	some	insights	into	the	mass	psychology	of	booms	,	bubbles	,	and	busts	.
prechter	should	have	left	it	at	that	.
instead	,	he	offers	up	predictions	.
big	mistake	.
this	guy	bases	his	forecasts	on	fibonacci	numbers	and	so-called	elliott	wave	counts	.
he	labels	all	market	patterns	so	that	they	fit	the	preordained	elliott	wave	5-up	,	3-down	pattern	.
if	you	see	more	or	fewer	waves	than	he	does	in	any	given	market	move	,	you	just	do	not	understand	that	there	can	only	be	NUMBER	waves	to	the	top	of	a	bull	market	.
he	's	imposing	his	theory	on	the	market	,	but	surprisingly	enough	,	the	market	does	not	care	.
it	does	whatever	it	wants	,	in	however	many	waves	it	wants	,	and	bob	has	been	very	frustrated	at	the	market	's	aberrant	behavior	lately	.
he	got	lucky	early	in	his	career	,	but	it	seems	he	's	been	fairly	consistently	getting	the	market	wrong	since	missing	the	'87	crash	.
any	superstitution	can	seem	to	work	if	you	only	look	at	its	happenstance	apparent	accuracies	and	disregard	its	abundant	failures	.
keep	looking	for	certain	numbers	of	waves	and	fibonacci	ratios	and	you	will	find	what	you	are	looking	for	.
pick	any	set	of	numbers	or	patterns	or	wave	counts	,	and	you	will	find	those	too	.
the	market	does	not	care	about	any	of	those	things	,	though	.
it	's	nothing	more	than	an	aggregation	of	billlions	of	individual	choices	,	which	are	influenced	by	an	infinite	number	of	factors	none	of	us	can	ever	understand	fully	.
university	studies	of	various	investing	methods	have	found	that	elliott	wave	,	like	most	,	is	no	better	than	random	stock	selection	.
if	prechter	had	chosen	the	random	method	,	he	and	his	followers	would	at	least	have	a	good	chance	of	keeping	pace	with	the	market	.
unfortunately	,	he	has	been	getting	everything	from	the	dow	,	to	oil	(	he	thought	it	was	going	to	$	NUMBER	)	,	to	gold	(	he	did	not	think	it	could	break	$	NUMBER	)	,	to	deflation	(	the	deflation	scare	ended	shortly	after	this	book	came	out	)	wrong	in	the	past	couple	of	years	.
he	apparently	still	believes	that	the	dow	will	go	to	NUMBER	(	not	NUMBER	but	four	hundred	)	because	his	charts	tell	him	it	should	(	they	've	been	telling	him	that	since	about	YEAR	,	actually	)	.
prechter	does	not	understand	demographics	or	economics	or	markets	or	the	monetary	system	,	all	of	which	render	dow	NUMBER	among	the	goofiest	of	forecasting	absurdities	-	unless	you	believe	a	giant	asteroid	will	hit	and	wipe	out	two	thirds	of	the	population	(	i	do	not	think	prechter	is	counting	on	that	)	.our	debt-heavy	economy	may	have	a	significant	crisis	soon	,	perhaps	by	the	end	of	the	decade	.
it	will	most	likely	be	spurred	on	by	inflation	and	rising	interest	rates	.
but	it	's	not	happening	now	and	it	's	not	happening	in	the	way	this	author	has	been	prematurely	forecasting	for	over	a	decade	.
this	book	came	out	near	the	bottom	of	the	2000-2002	stock	bear	market	,	just	in	time	for	a	reader	to	short	stocks	as	they	began	a	new	bull	market	.
how	typical	.
while	promoting	the	book	,	prechter	said	it	was	definite	that	stocks	would	crash	in	NUMBER	.
he	said	that	NUMBER	would	be	the	best	year	yet	for	bears	(	short	sellers	)	.
late	into	NUMBER	,	when	it	was	clear	that	the	market	had	proven	him	wrong	,	prechter	declared	on	a	radio	show	that	by	the	end	of	NUMBER	,	the	dow	would	certainly	fall	below	NUMBER	.
wrong	again	!
since	his	fibonacci	turn	dates	and	elliott	wave	counts	fail	to	portend	the	future	,	maybe	next	he	will	tell	us	he	's	found	the	secret	code	to	the	stock	market	from	the	text	count	of	nostradamus	writings	.
bob	prechter	could	not	call	the	market	if	it	was	listed	in	the	yellow	pages	.
this	guy	could	not	time	the	market	if	you	gave	him	a	stopwatch	.
he	's	throwing	out	guesses	and	misleading	people	by	describing	them	as	definite	and	certain	.
he	's	either	a	liar	or	a	true	believer	in	his	own	irrational	superstitions	.
in	either	case	,	he	's	not	credible	.
here	's	all	you	really	need	to	know	:	buy	dividend	stocks	.
buy	short-term	bonds	.
buy	gold	.
hang	on	to	them	.
do	that	and	you	will	do	better	than	you	will	trying	to	follow	the	prognostications	of	market	gurus	.
plus	,	you	will	have	a	lot	more	time	to	devote	to	family	,	fun	,	and	productive	activities