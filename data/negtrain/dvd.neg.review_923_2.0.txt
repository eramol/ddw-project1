the	weight	of	water	(	kathryn	bigelow	,	NUMBER	)	this	movie	should	have	been	a	big	production-	an	adaptation	of	an	anita	shreve	novel	directed	by	near	dark	's	kathryn	bigelow	,	starring	a	handful	of	big-name	actors	.
so	why	does	it	seem	like	a	lifetime	original	movie	(	tm	)	on	steroids	?	jean	janes	(	shadow	of	the	vampire	's	catherine	mccormack	)	is	obsessed	with	a	pair	of	murders	that	occurred	on	the	isles	of	shoals	in	YEAR	,	when	louis	wagner	(	rome	's	ciaran	hinds	)	was	convicted	of	the	murders	of	two	women	and	executed	.
jean	is	not	sure	they	got	the	right	person	.
she	and	her	poet	husband	thomas	(	sean	penn	)	set	off	for	the	islands	on	a	boat	with	thomas	brother	rich	(	josh	lucas	,	recently	of	poseidon	)	and	rich	's	new	girlfriend	adaline	(	elizabeth	hurley	,	who	went	from	this	right	on	to	film	bedazzled	)	.
the	interpersonal	relationships	on	the	boat	cause	tensions	to	flare	,	as	everyone	seems	to	want	pretty	much	everyone	else	,	and	the	situation	on	the	boat	brings	jean	to	a	possible	revelation	of	an	alternative	theory	of	the	crime	.
it	's	an	interesting	premise	,	and	from	everything	i	've	heard	it	's	carried	off	quite	successfully	in	the	book	,	which	i	have	not	read	.
but	oh	,	it	does	not	work	well	at	all	in	the	film	.
the	links	between	the	two	stories	are	shown	only	by	the	way	the	two	stories	are	intercut	;	no	work	at	all	was	done	on	trying	to	parallel	the	two	in	any	structural	way	.
most	of	the	actors	are	wasted	here	;	even	the	vastly	talented	mccormack	seems	only	a	shadow	of	her	usual	irrepressible	self	,	while	sarah	polley	,	whose	story	takes	place	over	in	the	nineteenth	century	time	frame	,	seems	as	if	she	's	worn	down	by	a	lot	more	than	inimical	primitive	island	life	.
about	the	only	actor	who	's	really	effective	here	is	hinds	,	who	plays	his	role	to	the	hilt	.
it	's	not	the	worst	movie	i	've	seen	in	the	past	month	,	but	it	's	pretty	close	.