the	glorious	cause	is	the	sequel	to	rise	to	rebellion	,	and	since	the	first	novel	was	so	good	i	eagerly	dove	into	this	one	,	but	was	disappointed	to	find	it	had	nowhere	near	the	same	depth	and	quality	of	characterization	.
on	a	scale	of	NUMBER	to	NUMBER	,	i	give	it	a	NUMBER	,	as	compared	to	the	NUMBER	i	would	give	rise	to	rebellion	.
the	events	recounted	in	rise	to	rebellion	involving	the	lead	up	to	the	revolutionary	war	seemed	so	much	more	detailed	and	interesting	,	with	a	vast	array	of	characters	that	were	so	colorful	;	john	adams	,	paul	revere	,	samuel	adams	,	ben	franklin	,	and	others	.
the	glorious	cause	was	not	without	its	merits	,	but	it	seemed	so	much	drier	and	less	appealing	,	with	a	more	narrow	focus	.
it	was	far	too	long	in	some	parts	,	to	the	point	my	attention	wandered	,	and	getting	through	it	was	a	chore	in	some	sections	,	unlike	the	preceding	novel	which	seemed	to	turn	its	own	pages	.
i	listened	to	this	book	on	audio	tape	during	my	commute	,	and	often	found	myself	impatiently	trying	to	keep	my	focus	upon	the	narrative	while	controlling	my	urge	to	fast	forward	to	a	livelier	scene	.
one	element	i	felt	truly	slighted	by	was	the	coverage	of	benedict	arnold	and	his	defection	to	the	british	cause	.
i	assumed	this	would	be	one	of	the	lengthier	and	more	rewarding	sections	of	the	book	,	but	it	zipped	by	fairly	rapidly	with	little	genuine	immersion	into	the	subject	material	.
a	mark	of	a	good	book	is	how	i	feel	when	i	have	finished	reading	it	-	if	i	am	disappointed	i	have	reached	the	end	of	the	story	,	then	i	know	it	was	time	well	spent	.
getting	to	the	end	of	the	glorious	cause	felt	more	like	finishing	some	particularly	grueling	outdoor	project	;	it	was	uncomfortable	work	,	and	now	that	i	am	done	i	feel	i	can	do	something	a	bit	more	interesting	.
because	shaara	did	such	a	good	job	with	rise	to	rebellion	i	do	not	feel	my	lack	of	appeal	for	this	book	is	his	responsibility	;	perhaps	the	subject	matter	did	not	complement	my	taste	as	well	.
i	prefer	interesting	historial	detail	over	tepid	,	droning	battle	descriptions	,	and	i	feel	the	differences	between	those	two	categories	represent	perfectly	the	differences	between	rise	to	rebellion	and	the	glorious	cause	.