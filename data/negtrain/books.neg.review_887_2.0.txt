earlier	this	year	i	reviewed	what	many	will	see	as	the	companion	volume	to	this	book	(	photoshop	blending	modes	cookbook	for	digital	photographers	)	,	written	by	the	same	author	.
unfortunately	,	the	newer	publication	is	less	useful	.
it	seems	to	have	been	written	on	a	pretext	that	it	's	clever	to	be	able	to	duplicate	what	traditional	artists	can	do	.
this	seems	-	from	my	own	personal	viewpoint	-	to	be	greatly	undervaluing	the	power	of	photoshop	(	and	similar	software	)	.
practitioners	of	digital	fine	art	should	(	really	,	constructively	)	be	looking	to	explore	what	the	principles	of	prior	and	traditional	art	can	mean	within	a	new	domain	.
plus	,	the	book	gets	off	to	a	definitely	poor	start	.
the	second	and	longer	of	two	introductory	sections	is	titled	the	tricks	of	the	trade	.
well	it	would	be	better	if	just	some	of	the	tricks	had	been	explained	in	full	and	more	accurately	.
say	,	how	to	make	a	selection	in	photoshop	from	the	best	available	precursor	(	a	black-and-white	alpha	channel	)	.
or	say	again	,	how	to	make	tonal	corrections	to	the	original	photograph	using	a	luminance	mask	.
then	again	,	the	first	(	and	shorter	)	of	the	introductory	chapters	,	titled	the	artist	's	eye	,	is	just	a	teaser	.
this	topic	-	pre-visualizing	what	can	be	achieved	as	an	output	image	when	composing	the	original	photographic	input	-	could	have	benefited	from	a	much	more	detailed	explanation/argument	.
indeed	,	it	could	even	merit	an	expansive	concluding	chapter	(	but	the	book	does	not	have	one	of	those	at	all	DOTS	.	)	.
this	is	,	after	all	,	at	the	very	core	of	what	the	user	could	harness	to	any	given	artistic	objective	.
additionally	,	i	think	that	it	's	strange	that	a	book	such	as	this	simply	makes	no	reference	at	all	to	what	could	be	printed	from	the	recipes	it	contains	.
some	of	the	finished	(	output	)	images	might	look	quite	intriguing	as	NUMBER	by	NUMBER	inch	reproductions	in	the	book	-	but	does	the	methodology	hold	up	if	you	are	targeting	a	NUMBER	by	NUMBER	inch	output	(	say	)	on	a	large	format	printer	?
and	what	to	do	if	that	's	not	the	case	?
finally	,	and	in	common	with	the	earlier	companion	volume	,	this	book	suffers	from	strange	and	inconsistent	layouts	of	screenshots	and	text	,	plus	all	sorts	of	technical	and	editing	omissions/errors	(	which	include	,	for	example	,	having	the	wrong	screenshot	in	the	wrong	recipe	-	see	p.108	)