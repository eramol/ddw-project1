mission	:	impossible	,	the	tv	series	,	told	the	adventures	of	the	impossible	missions	force	-	an	elite	team	of	covert	operatives	who	were	sent	to	overthrow	mob	kingpins	,	foreign	dictators	,	terrorists	and	others	who	threatened	the	security	of	the	united	states	.
rather	than	engage	in	james	bond-like	shoot	outs	,	the	imf	team	worked	together	to	covertly	neutralize	threats	,	leaving	little	or	no	trace	of	their	involvement	.
although	the	imf	had	a	leader	(	steven	hill	as	dan	briggs	and	peter	graves	as	jim	phelps	)	,	there	was	no	star	of	the	group	-	just	a	bunch	of	individuals	working	together	to	achieve	its	objective	.
a	truly	groundbreaking	show	.
along	comes	brain	de	palma	's	catastrophe	of	a	remake	,	starring	that	couch-jumping	scientologist	tom	cruise	.
within	half	an	hour	of	the	film	starting	,	the	entire	imf	team	,	save	for	cruise	,	is	killed	off	,	jim	phelps	(	jon	voight	)	is	exposed	as	a	double	agent	and	basically	the	entire	concept	of	the	original	mission	:	impossible	is	shot	to	hell	.
granted	,	ving	rhames	and	jean	reno	are	present	as	cruise	's	new	team	,	but	mission	:	impossible	is	quickly	bastardized	as	yet	another	tom	cruise	vehicle	where	he	preens	and	postures	.
making	a	film	called	mission	:	impossible	into	a	star	vehicle	goes	against	the	spirit	of	the	original	tv	series	.
it	's	obscene	what	tom	cruise	's	ego	has	done	to	bruce	geller	's	original	concept	.
this	is	just	another	action	film	with	mr.	cruise	flashing	his	toothy	smile	and	saving	the	world	all	by	himself	.
see	the	original	tv	series	and	skip	this	piece	of	bastardized	trash