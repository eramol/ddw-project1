i	picked	this	book	up	after	reading	both	books	in	the	bartimeus	trilogy	and	was	excited	to	see	what	else	jonathan	stroud	could	conjure	up	.
this	book	was	a	let	down	.
i	just	could	not	get	into	the	plot	.
i	even	found	the	story	somewhat	disturbing	and	eerie	,	especially	for	young	children	to	read	.
there	are	very	evil	characters	in	this	book	who	would	be	much	better	suited	for	an	adult	sci-fi	novel	,	rather	than	a	book	marketed	for	children	and	young	teens	.
the	characters	were	unlikeable	,	and	the	plot	just	never	seemed	to	grab	my	attention	.
i	was	very	dissapointed	that	this	is	from	the	same	author	who	wrote	the	amazing	bartimeus	trilogy	books	!
well	,	i	figured	i	would	add	my	two	cents	in	and	let	people	know	i	just	did	not	like	this	book