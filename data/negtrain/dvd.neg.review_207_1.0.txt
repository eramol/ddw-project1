how	high	is	a	film	that	lends	nothing	,	i	repeat	,	nothing	to	the	film	community	at	large	.
i	have	read	some	critics	that	say	that	this	film	introduced	a	whole	new	generation	to	the	cheech	&	chong	of	the	new	millennium	with	the	redman/method	man	pairing	.
while	most	seem	to	agree	that	this	is	a	horrible	film	,	very	few	of	them	mention	how	utterly	racist	the	overall	experience	happens	to	be	.
when	i	use	the	word	racist	,	i	mean	that	it	builds	upon	the	degenerate	clich�s	that	african	americans	struggle	with	on	a	daily	basis	with	our	society	.
while	there	were	elements	in	this	film	that	seemed	like	there	was	some	reverse	racism	occurring	,	what	struck	me	hardest	in	the	face	was	how	embarrassing	method	man	and	redman	must	be	to	both	the	film	community	,	but	also	to	their	own	culture	.
i	have	a	very	open	mind	when	it	comes	to	social	issues	as	well	as	films	,	but	this	one	left	my	jaw	on	the	floor	several	times	.
the	blatant	swings	at	african	american	stereotypes	were	meant	to	be	a	source	of	humor	for	director	jesse	dylan	,	but	instead	fell	flat	on	their	embarrassed	face	.
humor	was	in	very	short	supply	in	this	film	due	to	the	ethnic	stereotype	clich�s	that	seemed	to	be	used	nearly	every	second	of	this	film	.
nothing	was	original	.
nothing	was	exciting	.
was	the	pairing	of	redman	and	method	man	an	idea	of	comedic	proportions	?
i	think	not	.
as	you	can	tell	from	their	short	lived	series	after	the	release	of	this	film	,	the	corporate	world	wanted	to	cash	in	on	these	two	rappers	,	but	soon	realized	that	they	were	not	as	bankable	as	they	thought	.
why	?
method	man	and	redman	,	who	completely	different	people	,	were	nothing	more	than	the	same	character	in	this	film	.
with	the	very	subtle	hints	of	method	man	being	smarter	,	nothing	could	separate	the	equality	of	these	two	characters	.
that	is	why	they	failed	as	a	comic	team	.
think	of	matthau	and	lemmon	or	the	above	example	of	cheech	&	chong	,	what	made	these	teams	work	while	the	how	high	team	crashed	and	burned	?
what	worked	the	best	were	the	differences	that	uniquely	drew	the	pair	together	.
their	differences	made	us	laugh	and	sympathetically	brought	them	together	,	but	with	method	man	and	redman	,	they	were	the	same	.
i	was	watching	the	same	two	characters	fight	through	the	same	two	challenges	in	this	film	.
what	tried	to	be	funny	instead	transformed	into	repetitive	.
what	dylan	did	to	counter	the	similarity	between	redman/method	man	was	bring	in	obscure	secondary	characters	that	added	nothing	to	the	flimsy	plot	or	helped	us	feel	emotion	for	our	two	main	characters	.
the	clich�	jock	,	the	clich�	frat	boy	,	the	clich�	asian	(	which	was	again	another	racist	moment	)	,	and	the	clich�	rich	white	boy	,	just	felt	and	were	old	.
watch	college	films	from	the	YEAR	and	you	will	see	these	same	combinations	,	but	what	makes	the	films	from	the	YEAR	different	is	that	they	used	them	with	originality	.
how	high	,	from	beginning	to	end	,	was	anything	but	original	.
i	must	admit	,	after	watching	this	film	of	which	harvard	was	willing	to	loan	their	name	to	,	i	do	not	want	to	go	to	harvard	.
they	should	have	pondered	the	effects	this	film	would	have	upon	their	prestige	heritage	.
how	high	is	a	dark	spot	in	harvard	's	history	.
do	you	know	what	makes	it	even	worse	?
this	film	was	not	even	created	at	or	anywhere	near	harvard	.
again	,	dylan	was	cutting	corners	to	create	a	false	sense	of	realism	that	continued	to	hurt	this	film	.
how	high	has	no	redeeming	qualities	.
ooops	,	i	stand	corrected	.
the	only	smile	that	was	ever	created	on	my	face	during	this	film	was	when	spalding	gray	spoke	.
how	did	they	get	him	and	fred	willard	to	do	this	film	?
i	have	said	it	once	,	i	will	say	it	again	,	some	actors	just	need	their	bills	paid	.
inconsistency	,	incompetence	,	and	aggravation	seemed	to	be	the	hidden	themes	of	this	dud	.
what	destroyed	this	film	further	is	that	if	you	take	the	actors	away	and	look	at	the	story	in	general	,	it	is	nothing	.
there	is	no	original	story	at	all	with	how	high	.
going	to	college	,	working	for	your	grades	,	winning	over	the	heart	of	the	president	right	at	the	last	minute	-	this	was	nothing	short	of	a	cut	and	paste	film	that	had	no	backbone	and	horrid	acting	!	i	do	not	like	jesse	dylan	's	style	of	directing	.
while	i	think	kicking	and	screaming	is	his	prized	film	(	due	greatly	to	will	ferrell	)	,	all	of	his	films	seem	to	be	extremely	choppy	with	the	editing	and	are	randomly	interjected	with	an	overdose	of	unnecessary	scenes	.
the	john	adams	scene	in	how	high	is	a	prime	example	.
the	benjamin	franklin	moments	is	another	.
the	unconcluded	truth	serum	can	be	considered	another	.
how	high	was	just	scene	after	scene	of	inconsistency	.
lumpy	oatmeal	would	win	first	place	before	this	film	would	ever	be	congratulated	!
i	blame	a	majority	of	the	issues	with	this	film	on	jesse	dylan	.
if	he	would	have	tightened	the	script	,	created	unique	and	cult-like	characters	,	and	eliminated	the	racism	,	he	may	have	had	the	quintessential	weed	film	in	hollywood	.
now	,	all	he	has	is	a	violent	case	of	the	munchies	and	nothing	to	show	for	.
sad	.
overall	,	this	was	one	of	the	worst	films	that	i	have	witnessed	.
never	have	i	been	so	emotionally	charged	about	the	racist	moments	in	this	film	as	i	have	with	how	high	.
the	actors	,	all	around	,	were	horrible	.
the	story	had	no	legs	to	stand	on	and	it	became	very	obvious	after	the	opening	credits	that	unless	you	were	either	NUMBER	to	NUMBER	years	of	age	,	this	film	would	never	be	considered	a	comedy	.
nothing	was	funny	.
nothing	will	make	you	laugh	.
the	only	part	worth	enjoying	in	this	film	is	the	ending	credits	.
the	ending	credits	give	you	a	chance	to	walk	away	and	forget	this	part	of	your	cinematic	life	.