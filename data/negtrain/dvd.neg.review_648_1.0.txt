yes	,	this	is	a	supernatural	psycological	thriller	DOTS	but	far	from	a	good	one	as	far	as	i	am	concerned	.
i	love	these	kinds	of	stories	and	i	went	to	see	this	film	with	a	lot	of	excitement	DOTS	first	because	it	has	a	interesting	subject	.
second	because	it	has	a	great	actor	.
i	thought	denzel	washington	would	never	associate	himself	with	a	bad	script	DOTS	but	i	was	wrong	.
but	denzel	is	always	top	notch	.
the	rest	is	not	.
by	now	you	already	know	the	story	.
a	good	detective	DOTS	a	killer	who	happens	to	me	a	demon	who	happens	to	pass	from	person	to	person	just	by	touch	DOTS	the	hunt	DOTS	blah	blah	blah	DOTS	.the	script	is	idiot	from	page	one	.
the	killer	(	as	we	will	see	as	the	story	develops	)	travels	by	simple	touch	.
so	DOTS	excuse	me	DOTS	why	does	not	the	killer	passes	to	a	guard	as	he	is	going	to	be	executed	?	?
i	mean	DOTS
the	guy	is	sooo	powerful	(	and	mobile	)	that	i	find	it	unbelievable	that	he	gets	to	be	executed	in	the	first	place	.
but	DOTS	ok	DOTS	let	's	give	the	screenwriters	a	break	.
somewhere	in	the	middle	of	the	film	,	the	demon	enters	a	cat	.
ok	DOTS	so	he	can	enter	all	kinds	of	animals	.
fine	.
later	on	,	the	main	character	learns	that	in	order	to	defeat	the	demon	,	he	must	kill	his	body	in	a	place	where	there	is	nobody	around	.
his	brilliant	idea	:	the	forest	.
full	of	animals	.
was	i	the	only	one	who	did	not	know	the	ending	before	it	came	?
?	want	more	?	in	an	interesting	twist	,	the	protagonist	kills	an	innocent	man	and	later	on	DOTS	his	own	brother	.
and	no	one	will	believe	the	truth	!
how	is	he	gon	na	get	out	of	such	a	mess	?
?	in	the	world	of	storytelling	,	there	are	only	two	ways	of	getting	out	of	such	an	impossibly	difficult	situation	:	either	the	screenwriter	is	brilliant	and	finds	a	believable	way	to	save	his	character	from	the	master	mess	built	by	the	screenwriter	himself	-	so	he	should	know	!
(	and	then	we	get	some	sort	of	positive	ending	despite	everything	DOTS	)	or	something	deep	inside	the	protagonist	fails	and	he	loses	the	battle	against	evil	(	despite	the	fact	that	there	was	a	way	out	)	.first	line	of	solution	:	die	hard	,	the	fugitive	.
second	line	of	solution	:	s7ven	,	chinatown	.
we	have	none	of	that	here	.
instead	we	have	an	evil	god	against	a	simple	man	(	great	starter	for	drama	,	right	?	)
DOTS	but	the	man	gets	weaker	and	weaker	and	weaker	DOTS	and	the	demon	gets	stronger	and	stronger	DOTS	to	the	point	where	we	get	the	fact	that	the	demon	is	invincible	long	before	he	actually	wins	.
and	the	great	turnaround	never	comes	.
obviously	we	all	hope	he	will	be	defeated	DOTS	and	that	's	why	i	kept	myself	seated	all	through	the	film	DOTS	because	if	there	was	a	way	out	DOTS	it	would	be	brilliant	.
if	the	character	dropped	the	towel	DOTS	it	would	be	thought	provoking	to	say	the	least	.
but	no	DOTS	the	ending	was	easy	and	predictable	.
the	screenwriter	could	not	achieve	one	of	the	two	possible	lines	of	solutions	.
he	wrote	the	first	thing	that	came	to	his	mind	and	hid	himself	under	the	story	ends	bad	label	.
without	any	depth	.
every	villain	has	a	weak	point	.
it	is	up	to	the	hero	to	explore	it	(	may	he	loose	or	win	)	.
the	only	problem	the	hero	faces	here	is	a	stupid	lack	of	data	.
we	have	the	data	the	moment	the	demon	enters	a	cat	.
that	's	why	we	know	how	's	gon	na	end	.
what	about	the	girl	who	helps	denzel	?
what	about	his	nephew	?
should	not	they	be	part	of	the	ending	too	?
do	not	come	with	the	this	is	an	open-ending	kind	of	talk	!	do	i	sense	a	smell	of	desire	for	a	sequel	?	quick	!
gas	masks	!
or	DOTS	as	a.c.	doyle	would	write	:	quick	,	dr.	watson	,	the	needle	!
despite	a	stupid	screenplay	.
denzel	is	always	great	.