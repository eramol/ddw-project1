my	understanding	is	that	this	book	is	an	accessible	summary	of	the	ideas	catsells	presents	in	his	three	volume	magnum	opus	,	the	information	age	(	which	starts	with	the	network	society	)	.
if	this	is	so	,	i	am	definitely	not	missing	much	by	not	having	read	the	trilogy	.
this	is	a	really	bad	piece	of	sociology	,	characterized	by	a	technologically	deterministic	analysis	.
why	did	i	give	it	two	stars	instead	of	one	then	?
well	,	it	does	have	some	ok	parts	.
catsell	's	analysis	of	the	origins	of	the	internet	is	an	interesting	bit	of	the	sociology	of	technology	and	what	saves	the	book	from	pure	technological	determinism	.
he	also	presents	some	convincing	data	(	gathered	by	other	people	)	that	use	of	the	internet	for	socializing	does	not	suck	people	into	an	on-line	world	,	alienating	them	from	the	world	of	face-to-face	interaction	;	this	happens	in	the	case	of	a	few	troubled	people	,	but	most	people	use	the	internet	to	enhance	their	already	existing	off-line	relationships	.
the	rest	of	the	book	basically	argues	that	the	network	format	of	the	internet	is	reshaping	the	rest	of	society	in	its	image	,	with	everything	from	big	business	to	governments	to	social	movements	adopting	a	network	form	in	response	to	the	rise	of	this	new	technology	.
this	is	,	frankly	,	ludicrous	reductionism	.
it	does	not	even	stand	up	to	a	simple	test	of	chronology-a	lot	of	the	developments	that	castells	argues	are	driven	by	the	internet	predate	the	explosion	of	ist	usage	in	the	mid-1990s	.
as	castells	himself	admits	,	businesses	were	already	taking	on	more	of	a	network	form	before	the	internet	appeared	big	time	on	the	scene	,	and	social	movement	scholars	have	shown	the	same	is	true	of	transnational	social	movements	.
on	top	of	this	,	castells	shows	an	effusive	enthusiasm	for	all	things	networked	,	whether	they	be	transnational	corporations	or	the	transnational	social	movements	that	oppose	these	same	corporations	.
i	am	really	at	a	loss	to	understand	how	one	can	enthuse	about	both	of	these	opposed	phenomena	.
castells	does	see	some	of	the	problems	with	the	new	network	society-loss	of	job	security	and	the	digital	divide	,	for	instance-but	he	tends	to	downplay	these	.
and	his	solution	to	these	problems	tends	to	come	down	to	more	of	the	same-more	internet	access	,	more	network	social	organization	.
talk	about	a	narrow	vision