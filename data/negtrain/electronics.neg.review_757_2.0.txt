i	love	the	basic	design	,	great	hardware/software	linkage	,	and	software	interface	of	this	mac-happy	scanner	.
however	,	i	value	my	time	too	much	to	babysit	the	feed	mechanism	of	the	unit	i	received	last	week	.
it	inconsistently	graps	multiple	pages	as	other	reviewers	have	disclosed	.
so	,	if	you	are	scanning	a	10-page	doc	and	it	misses	one	somewhere	along	the	way	,	have	you	really	accomplished	anything	?
no	.
you	must	either	try	the	whole	thing	again	(	with	likely	the	same	result	)	,	or	you	must	figure	out	what	page	(	s	)	is	missing	,	scan	it	separately	,	and	go	thru	a	simple	yet	not	expedient	acrobat	process	to	insert	the	omitted	page	.
in	the	end	,	this	machine	made	it	+90	%	of	the	way	to	being	a	really	great	product	,	but	the	shortcoming	fouls	half	the	machine	's	overall	utility	.
swing	a	little	bit	harder	next	time	fujitsu