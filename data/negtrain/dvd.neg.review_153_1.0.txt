you	want	realism	of	the	old	west	?
try	showing	people	frequently	blowing	big	wads	of	snot	out	their	nose	and	hawking	green	loogies	onto	the	floor	or	into	their	sleeves	because	they	did	not	have	tissues	.
try	having	the	actors	and	actresses	go	for	a	couple	of	months	without	washing	their	hair	,	and	not	brushing	their	teeth	with	toothpaste	,	so	their	hair	is	oily	and	their	mouths	are	scuzzy	and	black	.
that	would	be	realistic	,	but	it	's	a	lot	more	repellant	to	television	audiences	than	gratuitous	profanity	.
so	this	realistic	show	has	characters	with	clean	,	fluffy	hair	and	pretty	teeth	spew	garbage	langauge	constantly	-	and	sucker	viewers	and	reviewers	buy	this	as	realistic	!
every	great	writer	of	every	age	and	era	,	be	it	mark	twain	,	owen	wister	,	william	shakespeare	,	or	john	steinbeck	,	or	any	other	storyteller	whose	works	have	survived	for	generations	,	told	realistic	stories	of	their	era	but	who	understood	what	realistic	trivia	to	keep	in	,	and	what	to	leave	out	,	in	order	to	tell	the	story	.
that	is	why	their	work	has	remained	timeless	.
deadwood	's	writing	shows	a	huge	insecurity	complex	in	choosing	to	rely	on	a	gimmick	(	and	a	juvenile	one	at	that	)	like	over-the-top	profanity	.
whether	or	not	the	calamity	janes	of	the	time	actually	used	the	words	ignorant	f****ng	c***s	is	something	we	will	never	know	and	is	also	absolutely	irrelevant	.
there	are	an	awful	lot	of	sheeple	in	tv	land