if	you	admired	the	young	mccourt	boy	who	survived	misery	and	a	drunken	father	in	angela	's	ashes	(	a'sa	)	,	this	book	is	sure	a	huge	deception	.
the	first	NUMBER	pages	are	a	great	continuation	to	a'sa	,	but	suddenly	the	rhythm	of	the	book	stops	,	as	if	mccourt	had	suffered	from	a	severe	drought	of	ideas	.
the	storyline	becomes	chaotic	,	changing	the	day-today	timeline	we	were	used	to	to	a	mixture	of	anecdotes	that	take	place	NUMBER	years	forward	and	jump	back	again	,	no	order	or	sense	whatsoever	.
what	's	worse	,	nice	and	naive	mccourt	transforms	into	a	copy	of	his	drunken	father	,	constant	beating	up	of	women	included	.
it	's	a	huge	shock	(	hey	this	story	's	real	,	folks	!
)	;	it	's	as	if	snow	white	suddenly	turned	into	the	role	of	the	queen	.
mccourt	wo	not	be	the	same	nice	,	brave	and	funny	guy	for	me	nevermore	.