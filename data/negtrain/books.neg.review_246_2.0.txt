most	books	that	set	out	to	explain	why	organisms	behave	as	they	do	describe	observations	of	behaviour	on	almost	every	page	.
the	books	of	richard	dawkins	,	whom	rose	selects	as	his	special	target	,	illustrate	this	well	:	readers	can	reject	all	of	his	interpretations	while	remaining	fascinated	by	the	purely	factual	information	that	they	contain	.
how	one	can	hope	to	convince	anyone	of	the	truth	of	a	theory	without	supporting	it	with	abundant	facts	?
yet	hard	biological	information	is	extremely	sparse	in	rose	's	book	.
there	is	a	great	deal	about	what	he	thinks	of	other	biologists	opinions	,	but	almost	no	observations	from	behavioural	biology	.
nonetheless	,	in	his	preface	he	aligns	himself	with	the	practising	biologists	who	spend	a	significant	part	of	every	working	day	thinking	about	and	designing	experiments	,	dismissing	dawkins	and	daniel	dennett	as	people	who	either	no	longer	do	science	or	never	did	it	.
what	a	pity	,	therefore	,	that	he	chose	to	include	so	little	of	the	experimental	basis	of	his	ideas	in	his	book	.
there	are	a	few	vague	remarks	about	how	chicks	behave	,	and	that	's	about	it	.