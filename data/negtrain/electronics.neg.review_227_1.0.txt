i	recently	purchased	this	item	from	sharper	image	and	was	very	disappointed	.
the	video	is	faint	,	grainy	(	even	in	strong	lighting	)	,	and	out	of	focus	once	you	are	more	than	NUMBER	feet	away	from	the	camera	.
colors	all	but	disappear	.
it	's	practically	black-and-white	video	.
the	power	supply	weighs	more	than	the	device	itself	(	one	pound	,	including	cord	)	and	is	huge	.
it	also	gets	very	warm	after	an	hour	or	so	.
i	've	tested	this	product	in	every	type	of	lighting	situation	and	the	results	have	been	unsatisfactory	in	all	cases	.
the	only	thing	the	device	does	well	is	detect	motion	.