julia	roberts	plays	ed	harris	young	new	wife	.
susan	sarandon	plays	harris	ex-wife	.
ex-wife	does	not	like	new	wife	.
new	wife	does	not	like	ex-wife	.
ex-wife	's	cutsey	,	but	bratty	kids	make	new	wife	's	life	a	living	hell	.
ex-wife	thinks	new	wife	is	a	stupid	bimbo	.
new	wife	thinks	ex-wife	is	a	nagging	battle	axe	.
new	wife	and	ex-wife	fight	and	yell	at	each	other	ad	nauseum	.
how	does	the	film	solve	this	conflict	?
ex-wife	is	diagnosed	with	terminal	cancer	-	everyone	reconciles	,	ex-wife	and	new	wife	suddenly	love	each	other	,	the	end	.
this	is	one	of	the	most	cliched	,	saccharine	,	hackneyed	and	shamelessly	manipulative	pieces	of	garbage	put	out	by	hollywood	in	the	past	decade	.
if	you	like	julia	roberts	,	susan	sarandon	or	ed	harris	,	please	see	erin	brokovich	,	dead	man	walking	and	the	right	stuff	and	run	screaming	from	this	nauseating	cinematic	catastrophe