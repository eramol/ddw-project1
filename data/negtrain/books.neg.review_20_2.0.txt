i	was	disappointed	in	this	book-especially	after	reading	the	other	stellar	reviews	(	which	are	all	,	weirdly	,	from	texas	,	where	the	author	lives	)	.when	it	's	a	book	about	logic	and	clear	thinking	,	a	reader	expects	that	book	to	be	free	of	typographical	errors	,	imprecise	logic	,	and	muddled	thinking	.
needless	to	say	,	i	almost	put	the	book	down	after	reading	the	introduction	because	of	the	missing	words	and	typographical	errors	.
exampe	from	page	NUMBER	:	because	the	real	is	out	there	,	we	must	effectively	with	it	DOTS	.	here	's	another	gem	from	the	same	page	:	i	am	sorry	to	have	to	tell	you	that	fact	,	but	that	is	the	way	things	are	.
that	is	the	nature	of	nature	.
that	is	the	reality	of	reality	.
look	at	page	NUMBER	.
the	author	states	,	why	when	one	exception	is	uncovered	,	multiple	other	exceptions	surface	almost	right	away	is	not	entirely	clear	DOTS	.
probably	this	has	something	to	do	with	the	way	we	humans	view	reality	.
very	disappointing-has	not	this	author	studied	any	cognitive	psychology	?
it	's	called	confirmation	bias	and	it	's	entirely	clear	.
one	more	example	!
on	page	NUMBER	,	the	author	states	that	barbara	boxer	,	a	senator	from	california	,	does	not	know	the	difference	between	a	revolver	and	a	semiautomatic	pistol	and	is	unlikely	to	shed	intelligent	light	on	the	relative	safety	of	either	gun	.
ouch	.
how	does	he	know	what	barbara	boxer	knows	or	does	not	know	?
she	's	actually	very	well-versed	on	the	difference	between	the	two	;	has	he	heard	her	speak	on	the	subject	?
it	comes	across	as	more	of	an	ad	hominem	attack	than	anything	.
anyway	,	save	your	money	on	this	book	and	instead	buy	jamie	whyte	's	crimes	against	logic	.
now	that	's	a	well-written	book	!