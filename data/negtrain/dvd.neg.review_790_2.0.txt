kiefer	sutherland	stars	in	`dead	heat	a	crime	drama	in	which	the	star	of	hit	tv	series	`24	can	show	more	tender	side	of	the	character	than	he	is	often	associated	with	.
he	plays	pally	,	35-year-old	boston	cop	who	is	forced	to	retire	because	of	the	heart	problem	,	and	his	acting	is	very	good	.
overall	,	however	,	there	is	not	much	that	i	can	recommend	in	`dead	heat	except	some	nice	touch	in	showing	the	characters	.
the	film	tries	to	include	as	many	things	as	possible	,	but	few	of	them	really	materialize	.
the	story	looks	rather	like	a	familiar	one	.
because	of	his	early	retirement	,	pally	starts	to	drink	,	and	even	thinks	of	committing	suicide	.
his	relationship	with	his	estranged	wife	charlotte	(	rhada	mitchell	)	is	not	going	well	and	he	is	not	happy	to	know	that	she	is	now	going	out	with	someone	else	.
but	charlotte	is	also	nice	and	caring	,	so	she	persuades	pally	's	brother	ray	(	anthony	lapaglia	)	to	visit	him	.
ray	,	who	is	accused	of	not	being	a	law-abiding	citizen	by	his	too	earnest	brother	,	has	a	good	plan	to	make	money	easily	,	and	offers	it	to	pally	.
it	is	about	buying	a	racehorse	cheap	,	he	says	,	and	it	is	an	easy	job	because	he	happens	to	know	that	the	horse	,	which	has	not	win	any	races	recently	,	is	actually	misdiagnosed	,	only	suffering	from	a	polyp	a	small	operation	can	remove	.
when	everything	is	going	well	,	things	get	suddenly	complicated	because	of	a	gambling-addicted	jockey	tony	(	lothaire	bluteau	)	.
from	here	,	the	story	snowballs	into	blackmail	,	caper	and	even	murder	,	but	the	film	's	overall	tone	is	always	light	,	not	taking	itself	very	seriously	.
unfortunately	,	for	all	their	effective	acting	from	sutherland	,	lapaglia	,	mitchell	,	and	bluteau	,	the	weak	and	unsure	direction	totally	fails	to	give	momentum	to	the	otherwise	unremarkable	film	.
it	is	not	funny	when	it	tries	to	make	us	funny	,	and	it	is	not	romantic	when	it	tries	to	be	romantic	.
feeble	direction	simply	misses	several	good	opportunities	to	make	good	use	of	the	jockey	's	sullen	little	daughter	sam	(	kay	panabaker	,	her	feature	debut	)	,	who	could	have	been	a	little	surprise	(	or	gem	)	in	this	unconvincing	plot	.
it	is	true	that	kiefer	sutherland	shows	in	`dead	heat	another	side	of	acting	talent	,	which	is	less	eccentric	and	intense	than	most	of	the	roles	he	had	played	,	or	he	would	play	.
that	is	good	news	,	and	the	only	good	news	here