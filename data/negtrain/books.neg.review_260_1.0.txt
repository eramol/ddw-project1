the	title	grabbed	me	because	i	have	been	noticing	for	,	oh	about	NUMBER	years	now	,	the	decline	of	the	truth	in	this	country	.
unfortunately	,	this	book	is	written	precisely	by	the	kind	of	person	who	has	lost	touch	with	reality	and	what	truth	is	(	definition	of	irony	i	believe	)	.the	reason	it	is	#	NUMBER	on	the	bestseller	list	is	because	there	is	a	ready	audience	out	there	,	of	similarly	detached	people	,	who	want	anything	(	and	i	mean	anything	-	a	website	with	a	good	conspiracy	story	and	a	glass	of	warm	milk	before	bed	everynight	is	their	ideal	)	that	backs	up	their	point	of	view	.
it	is	precisely	why	the	truth	is	so	out	of	fashion	.
new	media	streams	like	the	internet	,	blogs	,	etc	.
offer	up	news	without	accountability	.
they	allow	this	segment	of	society	to	avoid	confronting	the	truth	and	instead	construct	their	own	alternative	realities	.
it	makes	people	like	frank	rich	,	rich	,	with	very	little	effort	.
why	bother	checking	facts	,	his	core	readership	does	not	care	whether	it	is	true	or	not	,	just	tell	them	bush	is	evil