hmm	,	quite	a	disappointment	,	especially	after	splurging	on	the	hardcover	edition	.
set	as	a	kind	of	rockies	rear	window	,	this	is	one	slow	story	.
endless	pages	of	crazy-making	when	i	'd	already	guessed	the	outcome	about	halfway	through	.
finally	skipped	ahead	to	finish	the	book	.
i	had	really	looked	forward	to	angels	fall	,	too	:	(	if	you	are	a	roberts	fan	,	do	not	spend	the	money	on	the	hardcover	,	wait	for	the	paperback	,	go	to	the	library	,	or	borrow	from	a	friend	.
not	worth	the	price	.