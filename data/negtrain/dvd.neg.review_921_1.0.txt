this	is	a	potentially	very	good	film	.
interesting	idea	(	bearing	some	resemblance	to	angel	heart	)	.
sadly	marred	by	a	quite	appauling	transfer	.
i	do	not	think	i	've	ever	bought	(	dvd	or	video	)	or	seen	(	tv	or	cinema	)	quite	such	an	awful	copy	of	a	movie	.
it	is	clearly	stuck	together	from	more	than	one	original	,	with	the	logo	delta	appearing	now	and	again	in	the	bottom	right	hand	corner	,	but	do	not	be	misled	into	thinking	this	is	the	most	complete	version	.
the	term	special	edition	seems	to	refer	only	to	the	fact	that	it	has	been	released	!
tony	curtis	does	an	incredibly	poor	intro	and	epilogue	(	clearly	from	some	late	night	cheap	tv	series	)	.do	not	buy	it	.
maybe	there	's	a	better	version	(	i	read	talk	of	a	critereon	disc	)	,	maybe	not	.
still	,	it	really	is	not	worth	(	any	)	money