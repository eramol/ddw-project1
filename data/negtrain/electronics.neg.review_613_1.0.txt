i	had	this	adapter	running	in	my	the	cardbus	slot	of	my	windows	NUMBER	computer	and	it	ran	fine	.
i	installed	it	on	my	windows	NUMBER	laptop	.
well	,	my	first	sign	of	trouble	was	a	warning	popup	when	i	installed	the	drivers	that	the	drivers	were	not	certified	by	microsoft	.
so	,	imagine	my	shock	when	my	brand	new	computer	constantly	got	blue	screen	of	death	(	bsod	)	crashes	.
big	blue	screen	,	loss	of	data	,	computer	freezes	tight	,	power	button	inoperative	,	and	when	you	have	to	use	a	pin	to	press	that	tiny	little	laptop	reset	button	on	a	brand	new	computer	,	well	,	we	all	know	that	's	when	you	are	in	computer	hell	.
i	am	a	microsoft	certified	professional	so	i	was	able	to	narrow	down	the	source	of	the	problem	.
after	definitely	identifying	the	fa511	as	the	culprit	,	i	contacted	netgear	's	online	tech	support	.
and	that	's	when	things	really	started	to	get	ugly	.
it	took	forever	for	netgear	to	finally	give	me	an	answer	.
this	was	after	days	of	inactivity	,	wildly	inappropriate	responses	,	and	repeatedly	requesting	same	information	from	me	.
finally	,	netgear	escalated	the	problem	to	their	next	level	of	tech	support	and	i	was	shocked	to	read	their	solution	:	this	is	a	windows	issue	regarding	permissions	and	***16	bit***	compatibility	mode	while	they	were	vague	as	to	the	exact	nature	of	the	problem	,	and	contradicted	themselves	as	to	whether	compatibility	mode	had	to	be	enabled	or	disabled	,	their	solution	was	straightforward	:	i	was	directed	to	a	microsoft	knowledge	base	article	which	tells	you	how	to	enable	compatibility	mode	-	and	for	16-bit	compatibility	mode	i	had	to	make	my	nice	clean	windows	NUMBER	computer	compatible	with	windows	NUMBER	(	yes	,	ninety-five	)	.so	,	while	the	fa511	hardware	is	a	32-bit	cardbus	adapter	,	apparently	their	drivers	are	16-bit	!
no	wonder	the	fa511	was	never	certified	for	windows	NUMBER	.
and	i	would	not	trust	it	for	windows	xp	either	.
there	are	other	ethernet	adapters	out	there	by	3com	,	belkin	,	linksys	,	smc	,	and	xircom	that	work	flawlessly	with	windows	2000/xp	by	companies	that	will	respond	to	your	questions	.
go	get	them	.
avoid	the	fa511