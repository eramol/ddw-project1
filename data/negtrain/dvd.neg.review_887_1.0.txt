picasso	is	one	of	the	giants	in	the	history	of	art	and	surely	the	artistic	genious	of	the	ORDINAL	century	,	but	this	renders	him	boring	and	repetitive	.
it	was	done	while	he	was	alive	,	and	has	him	painting	little	cartoon-like	works	that	were	done	only	for	this	dvd	DOTS	one	DOTS	after	DOTS	the	DOTS	.other	ad	nauseum	.
this	is	picasso	as	celebrity	entertainer	cartoonist	,	not	the	edgy	artist	genius	of	les	demoiselles	d'avignon	and	guernica	.
it	looks	as	if	he	's	painting	with	magic	markers	on	screen	and	the	action	is	speeded	up	so	it	looks	like	a	cartoonist	at	work	.
i	turned	it	off	after	30+	interminable	minutes	.
fortunately	i	rented	it	on	netflix	,	which	i	'd	recommend	,	rather	than	buying	it