this	is	the	sixth	book	in	the	left	behind	series	.
it	is	also	the	first	one	in	the	series	i	did	not	finish	in	one	sitting	.
it	did	not	flow	as	well	as	the	others	,	so	as	to	lend	itself	to	a	complete	reading	in	one	evening	.
the	book	also	spends	a	lot	of	time	re-telling	what	we	read	in	previous	books	.
i	felt	this	time	like	i	read	half	a	novel	.
the	book	starts	in	the	thirty-eighth	month	into	the	tribulation	.
but	most	of	the	characters	seem	a	little	too	self	serving	in	this	book	.
allowing	their	own	desires	to	guide	their	actions	.
whether	that	desire	be	derived	from	rage	,	revenge	,	self	pity	,	or	even	guilt	.
rayford	spends	most	of	his	time	focusing	on	hattie	or	his	desire	to	kill	the	anti-christ	.
though	i	do	like	the	description	of	the	handgun	that	rayford	procures	.
and	when	the	time	comes	for	the	assassination	,	there	are	a	few	characters	to	chose	from	.
the	book	ends	in	an	old	time	cliff	hanger	where	you	are	left	guessing	who	it	is	that	assassinated	the	anti-christ	.
stay	tuned	for	the	next	volume	.
i	have	read	this	far	,	so	i	will	finish	the	series	.
though	most	people	my	find	this	book	suspensful	,	i	did	not