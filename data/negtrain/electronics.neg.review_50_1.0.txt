here	is	the	original	review	of	this	machine	i	wrote	on	june	NUMBER	,	NUMBER	:	we	bought	this	machine	just	over	a	year	ago	(	a	year	and	two	weeks	,	to	be	specific	)	.
we	had	another	very	similar	panasonic	dvd	recorder	(	dmr-e55	)	in	a	different	room	and	wanted	to	be	able	to	record	and	watch	dvd-rams	on	both	.
for	a	long	time	,	the	es10s	recorder	worked	fine	.
recently	,	though	,	it	has	begun	making	noises	when	starting	up	and	shutting	down	,	and	now	it	occasionally	has	problems	reading	the	disks	,	ejecting	them	,	and	even	playing	them	.
at	first	we	thought	this	might	be	because	we	'd	recorded	over	the	dvd-rams	so	many	times	.
but	today	we	tested	one	of	them	in	the	other	machine	,	which	has	never	made	any	similar	noises	,	and	the	e55	had	no	problems	with	it	.
needless	to	say	,	we	are	not	happy	that	the	es10s	seems	to	be	dying	,	especially	since	the	1-year	warranty	just	ended	.
if	you	buy	or	own	this	machine	,	follow	up	on	any	strange	noises	as	soon	as	you	hear	them	if	you	are	within	the	warranty	period	,	because	the	noises	and	problems	just	get	worse	.
panasonic	is	usually	a	reliable	brand	,	one	we	've	used	for	many	years	,	but	this	machine	seems	to	be	a	lemon	.
we	are	actually	going	to	write	to	panasonic	about	it-i	will	try	to	post	again	with	the	results	.
here	is	my	update	,	which	i	wrote	november	NUMBER	:	this	is	a	follow-up	review	to	the	one	above	,	which	i	wrote	on	june	NUMBER	.
i	wrote	to	panasonic	after	my	machine	had	stopped	working	just	after	the	warranty	ended	.
they	sent	me	a	nice	letter	authorizing	a	free	repair	,	including	labor	,	of	the	machine	at	one	of	three	places	they	listed	.
the	places	were	all	within	a	half-hour	drive	of	my	home	.
after	doing	some	research	,	i	chose	one	and	brought	in	the	machine	.
they	fixed	it	by	replacing	the	entire	internal	mechanism	(	i	do	not	know	what	that	means	)	.
we	got	the	machine	back	from	the	repair	shop	near	the	end	of	july	;	it	's	now	the	beginning	of	november	.
the	fixed	machine	worked	fine	for	a	while	,	but	in	the	past	few	weeks	,	we	've	been	hearing	the	same	old	noises	that	were	the	beginning	of	the	end	for	it	before	.
i	am	going	to	try	writing	to	panasonic	again	and	asking	for	a	refund	,	because	this	machine	really	seems	to	have	something	fundamentally	wrong	with	it	.
it	's	still	working	,	but	i	know	from	the	last	time	with	these	noises	and	efforts	to	read	the	disks	that	soon	it	wo	not	.
avoid	this	machine	.
again	,	the	previous	panasonic	model	(	dmr-e55	)	we	bought	has	not	had	any	of	these	difficulties	and	continues	to	work	great	.
we	are	still	big	panasonic	fans	,	but	this	particular	model	is	a	lemon	.