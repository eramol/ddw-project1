first	off	,	i	bought	this	headset	for	gaming	,	which	anyone	who	enjoys	the	pastime	will	tell	you	how	quickly	the	hours	can	melt	away	.
gamers	are	dependant	on	an	ultra	comfortable	pair	of	headphones	that	we	should	not	be	able	to	feel	.
i	have	been	searching	high	and	low	for	the	holy	grail	of	comfort	and	i	was	certain	paying	$	90+	for	a	gaming	headset	would	be	the	end	of	my	search	.
sorry	,	these	guys	are	going	back	.
i	've	tried	a	couple	cheap	best-buy	headsets	that	i	found	painful	to	wear	after	an	hour	or	so	.
then	i	decided	to	spring	for	top	of	the	line	.
i	wore	these	for	NUMBER	minutes	before	it	became	obvious	this	headset	was	not	the	holy	grail	i	've	been	looking	for	.
the	headset	itself	feels	flimsy	and	cheap	.
the	padding	is	made	of	an	itchy	fabric	,	and	even	on	my	small	head	,	it	feels	tight	.
the	earphones	do	not	cover	my	ears	and	i	can	feel	them	slowly	being	crushed	.
i	am	not	impressed	with	the	sound	,	but	i	could	overlook	that	if	they	were	comfortable	to	wear	.
i	did	not	even	bother	to	try	the	mic	.
if	this	is	how	sennheiser	makes	their	high	end	gaming	equipment	,	i	think	it	's	safe	to	say	i	will	avoid	these	guys	in	the	future	.
the	$	NUMBER	pair	of	aiwa	's	i	wear	at	work	are	more	comfortable	than	this	and	sound	the	same	DOTS	if	only	that	pair	had	a	mic	.
*my	ears	still	feel	uncomfortable	after	taking	the	set	off	and	then	writing	this	review-	i	could	not	imagine	wearing	them	for	a	few	hours	while	gaming	.
if	i	find	the	perfect	pair	i	will	let	you	all	know