while	this	book	contains	a	ton	of	data	,	do	not	get	it	if	you	want	calorie	counts	on	anything	homemade	.
if	you	look	up	french	toast	,	for	example	,	it	only	gives	information	on	name	brand	items	.
much	of	the	information	in	the	book	is	available	on	the	packaging	of	the	products	.
i	suppose	that	if	you	wanted	to	check	it	before	shopping	,	or	use	it	before	eating	out	,	it	would	be	helpful	.
but	if	you	want	to	find	out	the	caloric	content	of	any	homemade	items	,	you	are	out	of	luck	with	this	book