i	was	thrilled	when	i	saw	there	was	a	new	sesame	street	christmas	dvd	out	as	my	NUMBER	and	NUMBER	year	olds	have	the	other	three	memorized	.
and	,	as	much	as	i	enjoy	being	a	sesame	street	kid	(	NUMBER	,	just	like	the	show	)	,	i	was	tiring	of	the	same	stuff	over	and	over	.
imagine	my	surprise	,	then	,	when	virtually	all	of	this	dvd	is	just	the	highlights	of	the	others	(	classic	ernie	,	bert	and	the	beloved	mr.	hooper	from	_christmas	eve_	,	hanukkah	from	_happy	holidays_	,	and	bob	singing	all	though	the	year	from	_elmo	saves	christmas	among	others	)	.
the	only	new	part	is	christmas	future	and	it	is	a	poor	cartoon	instead	of	new	muppet	footage	.
if	you	do	not	have	the	other	three	dvds	this	would	probably	earn	NUMBER	stars	.
if	you	do	have	the	other	dvds	this	is	not	a	necessity	.
hopefully	next	time	the	budget	will	allow	sesame	workshop	to	be	more	creative