DOTS	essentially	you	get	what	you	pay	for	.
this	book	is	certanly	not	representative	of	what	i	have	come	to	expect	from	national	geographic	.
i	had	eagerly	anticipated	the	arrival	of	my	book	,	but	was	dissappointed	to	find	within	the	first	few	pages	that	it	is	uninspiring	at	best	.
most	of	the	pictures	are	poorly	executed	and	at	times	seem	amateurish	.
in	the	past	i	've	found	photographs	by	professionals	like	steve	mccurry	(	'portraits	)	visually	arresting	.
not	so	here	.
at	least	it	only	cost	me	$	NUMBER	oredering	from	amazon	.
if	i	had	been	able	to	preview	it	beforehand	,	i	would	not	have	thought	it	worth	even	the	discounted	price