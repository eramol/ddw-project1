of	course	the	criterion	collection	has	wonderful	extras	and	a	great	transfer	.
the	problem	is	the	film	itself	.
preston	sturges	made	some	genuinely	profound	films	such	as	the	lady	eve	,	unfaithfully	yours	,	and	hail	the	conquering	hero	that	are	rich	and	complex	experiences	.
but	sullivan	's	travels	is	simplistic	,	pretentious	,	and	false	.
as	the	film	's	characters	themselves	point	out	,	poverty	is	only	intersting	to	the	rich	,	whose	view	of	it	is	distorted	.
odd-faced	character	actors	in	dingy	sets	make	a	rich	man	's	view	of	poverty	and	not	at	all	related	to	the	real	thing	.
sturges	was	one	of	the	most	highly-paid	men	in	america	,	so	his	view	of	the	subject	matter	is	(	like	characters	say	in	the	film	)	based	on	cliche	.
the	climax	with	its	chain-gang	laughing	at	a	cartoon	,	is	pretty	dopey	and	a	fairly	unconvincing	argument	for	comedy	.
sturges	would	do	better	to	point	to	how	comedy	in	his	best	films	allows	him	to	examine	self-deception	,	angry	love	,	role	playing	and	a	host	of	other	themes-in	other	words	how	humor	gets	into	the	darkest	and	most	ambigous	areas	of	human	life	that	serious	drama	can	not	so	easily	reach	.
this	film	is	exactly	the	kind	of	film	that	sullivan	wants	to	make-which	is	why	it	is	so	overearnest	and	uninteresting	.
if	you	want	to	see	the	genious	of	preston	sturges	,	watch	any	of	his	other	films	(	even	harold	diddlebock	)	in	their	entirety	.
after	the	first	NUMBER	minutes	a	simplemindedness	takes	over	sullivan	's	travels	that	make	makes	it	a	dispiriting	film	.