i	was	delighted	with	my	toughskin	for	all	of	two	hours	.
that	's	how	long	it	took	for	the	belt	clip	to	break	,	rendering	my	new	purchase	worthless	to	me	.
the	toughskin	itself	is	rugged	;	i	loved	it	.
i	should	have	waited	before	buying	it	,	though	DOTS	on	my	last	visit	to	an	apple	dealership	,	they	had	a	bin	full	of	*just	the	toughskins	,	*	for	sale	at	a	discount	.
when	i	asked	about	these	,	i	was	told	,	the	belt	clips	break	,	and	customers	bring	the	toughskins	back	.
i	should	note	i	am	very	careful	with	my	ipod	and	accessories	.
i	am	not	rough	on	them	.
the	clip	just	fell	apart	while	i	was	walking	down	the	street	.
given	other	comments	about	the	flimsy	clips	,	this	is	clearly	a	design	issue	that	speck	products	needs	to	address