i	am	a	longtime	user	of	wacom	intuos	tablets	and	i	was	looking	for	something	similar	with	wireless	capabilities	for	my	new	dual	2.5ghz-g5	.
there	were	no	wireless	intuos	products	available	at	the	time	of	purchase	,	so	i	went	with	the	graphire	bluetooth	.
unfortunately	,	the	product	leaves	a	lot	to	be	desired	and	after	a	couple	of	weeks	of	use	,	i	plan	on	returning	it	.
the	major	problem	lies	with	the	pen-both	in	design	and	responsiveness	.
design	:	the	graphire	series	is	cheaper	than	the	intuos	,	and	there	's	a	good	reason	why	:	the	graphire	pen	is	thinner	,	lighter	,	and	has	no	grip	.
if	you	have	very	small	hands	this	might	be	acceptable	;	in	my	hand	this	feels	like	a	toothpick	.
it	's	very	difficult	to	manipulate	a	tool	effectively	when	it	does	not	feel	like	it	has	any	durability	.
additionally	,	the	graphire	pen	stand	is	little	more	than	a	piece	of	transparent	molded	plastic	and	slides	around	the	desk	whenever	i	put	the	pen	back	into	it	.
responsiveness	:	there	is	a	half-second	delay	whenever	using	the	pen	to	click	in	any	finder-related	options-opening	windows	,	selecting	files	,	etc	.
1/2	second	may	sound	acceptable	,	but	if	you	,	like	me	,	use	the	pen	for	everything	,	not	just	drawing	,	believe	me	it	is	utterly	maddening	.
on	the	other	hand	,	the	mouse	behaves	perfectly	with	no	delay	at	all	,	so	i	know	it	's	the	pen	and	not	my	computer	.
the	tablet	is	attractive	and	the	mouse	works	fine	,	but	the	bottom	line	is	that	it	's	more	important	for	me	to	have	a	responsive	,	durable	pen	than	a	wireless	tablet	.
if	you	've	never	used	an	intuos	you	may	not	recognize	the	deficiencies	,	but	for	those	who	have	,	this	is	like	going	from	a	mercedes	to	a	honda	civic	.
you	will	get	better	gas	mileage	with	the	civic	but	the	benz	gives	you	the	best	overall	driving	experience	.
i	need	the	driving	experience	,	so	i	am	going	back	to	the	intuos-wire	and	all	.