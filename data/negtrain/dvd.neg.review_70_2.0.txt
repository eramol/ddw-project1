i	am	a	big	fan	of	frank	miller	's	work	and	i	loved	reading	the	graphic	novels	the	movie	was	adapted	from	.
when	i	saw	this	film	i	was	blown	away	by	the	visuals	.
it	had	an	amazing	graphic	style	that	was	very	different	than	anything	i	had	seen	in	a	movie	before	.
having	mentioned	it	's	good	points	i	have	to	say	i	was	enormously	disappointed	with	the	dialogue	and	some	of	the	story	adaptation	to	screen	.
it	seems	to	me	those	involved	in	the	screenplay	and	production	were	too	concerned	with	adapting	this	in	an	overly	faithful	manner	from	the	graphic	novel	to	the	big	screen	.
for	the	most	part	the	bad	dialogue	and	some	of	the	terrible	acting	in	parts	were	just	too	painful	to	sit	through	.
i	had	to	keep	checking	to	see	if	my	ears	were	bleeding	yet	.
each	extraordinarily	bad	line	or	horribly	acted	sequence	made	we	wince	like	i	was	being	mugged	.
what	works	in	a	graphic	novel	does	not	necessarily	work	as	well	when	adapted	directly	to	screen	without	a	bit	more	adaptation	than	you	will	see	here	.
all	the	characters	were	severely	and	tragically	2-dimensional	with	none	of	the	depth	you	get	from	reading	the	graphic	novel	and	very	little	attention	was	paid	to	developing	better	dialogue	suitable	for	a	big	screen	experience	.
also	,	many	of	the	big	name	actors	in	this	movie	were	capable	of	much	better	performances	than	they	displayed	here	.
purists	and	rabid	fans	of	the	graphic	novel	may	be	capable	of	somehow	finding	a	way	to	look	past	all	the	terrible	acting	and	awful	dialogue	that	is	rife	throughout	the	film	.
however	,	my	bottom	line	opinion	is	it	's	a	good	film	to	watch	for	the	visual	look	only	,	but	you	will	almost	certainly	enjoy	it	far	more	if	you	keep	the	'mute	button	on	.
this	film	had	the	potential	to	be	truly	great	but	it	is	extremely	disappointing	that	it	fell	so	far	short	of	that	mark