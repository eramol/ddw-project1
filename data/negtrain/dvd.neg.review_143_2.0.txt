therese	,	lux	,	mary	,	bonnie	and	cecilia	make	up	the	five	lisbon	girls	,	ages	NUMBER	,	NUMBER	,	NUMBER	,	NUMBER	and	NUMBER	.
cecilia	,	the	youngest	,	lives	in	her	sisters	shadows	because	they	are	all	older	and	of	much	more	interest	to	their	male	neighbors	.
as	a	cry	for	help	,	she	slits	her	wrists	and	lays	down	in	the	bathtub	,	she	survived	the	incident	.
after	a	psychiatrist	highly	impresses	upon	the	strict	mr.	and	mrs.	lisbon	that	it	would	be	benificial	for	cecilia	to	be	exposed	to	environments	which	have	boys	her	own	age	,	mr.	and	mrs.	lisbon	decide	to	allow	the	girls	to	throw	the	one	and	only	lisbon	party	.
some	of	the	neighborhood	's	boys	are	invited	over	and	while	the	party	is	in	session	,	cecilia	is	pretty	inanimate	,	sitting	on	the	couch	while	her	sisters	flirt	,	talk	and	laugh	.
midway	through	the	party	,	cecilia	asks	to	be	excused	and	mrs.	lisbon	-	though	somewhat	reluctantly	-	allows	it	.
cecilia	goes	up	to	her	room	(	on	the	second	floor	of	a	two-story	house	)	and	jumps	out	the	window	,	landing	on	the	spiked	fence	on	the	front	lawn	of	the	lisbon	house	.
in	this	way	,	cecilia	lisbon	commited	suicide	.
only	four	girls	remain	.
the	act	which	cecilia	committed	did	not	go	at	all	unnoticed	.
the	father	saw	her	immediately	,	the	party	ended	there	and	all	of	the	boys	went	home	rather	glum	.
four	of	the	lisbon	's	neighbors	,	four	young	boys	,	suffered	a	rapidly	growing	obsession	with	the	lisbon	girls	,	who	were	beautiful	and	a	total	mystery	to	the	outside	world	.
they	got	hold	of	cecilia	's	diary	and	tried	to	explain	why	she	committed	suicide	.
the	eldest	decided	cecilia	was	a	dreamer	,	and	she	did	not	think	she	would	fall	when	she	jumped	out	her	window	,	she	thought	she	would	fly	.
with	their	youngest	sister	gone	,	the	remaining	lisbon	girls	grow	closer	to	each	other	,	stranding	themselves	further	from	the	outside	world	.
when	school	starts	again	,	they	attempt	to	go	on	acting	as	if	nothing	happened	,	not	ever	mentioning	their	youngest	sister	.
the	run-of-the-mill	awkward	act	is	taken	part	by	the	rest	of	the	school	however	.
the	usual	profuse	apology	to	the	mention	of	the	word	death	and	the	like	.
trip	fontaine	is	the	most	popular	guy	in	school	,	the	object	of	desire	to	all	of	the	girls	at	school	DOTS
except	for	the	lisbon	girls	,	which	is	of	no	importance	to	trip	until	he	sets	eyes	on	lux	.
trip	can	not	help	but	be	enchanted	by	her	flawless	beauty	.
in	the	beginning	,	he	is	unsuccessful	in	his	quest	to	even	catch	her	attention	for	she	bluntly	shows	she	's	not	interested	.
but	after	his	consistent	courtship	,	lux	falls	for	his	charming	ways	.
trip	then	attempts	to	convince	mr.	lisbon	to	let	him	and	three	of	his	football	team	pals	take	the	lisbon	daughters	to	homecoming	.
after	mr.	lisbon	talks	to	his	wife	,	he	agrees	to	trip	's	request	,	knowing	nothing	but	trip	's	sincerity	and	claimed	honorable	intentions	,	on	the	understanding	that	trip	have	all	of	the	lisbon	girls	returned	to	the	house	by	curfew	.
after	trip	explains	the	situation	to	his	football	peers	,	he	gives	in	to	those	who	bribe	him	the	most	handsomely	.
meanwhile	,	the	lisbon	girls	are	picking	out	fabric	for	the	dresses	they	shall	wear	to	the	homecoming	dance	.
though	one	pattern	is	used	for	all	four	dresses	,	each	dress	looks	unique	when	concentrating	on	detail	.
once	they	pick	up	the	girls	,	they	stop	midway	to	the	homecoming	in	order	to	smoke	,	lux	being	the	only	girl	volunteering	to	smoke	.
they	then	drive	to	the	dance	.
trip	and	lux	sneak	away	and	sneak	swallows	of	alcohol	.
another	one	of	the	lisbon	sisters	and	her	date	follow	,	but	leave	soon	afterwards	,	not	being	as	wild	nor	daring	as	trip	and	lux	.
at	the	time	they	all	decided	to	meet	back	at	the	car	so	they	could	make	the	girls	curfew	,	they	are	all	there	DOTS
except	for	trip	and	lux	,	who	at	that	time	were	alone	in	the	middle	of	the	football	stadium	field	.
deciding	they	must	make	their	own	curfews	,	therese	,	bonnie	,	mary	and	their	dates	leave	without	them	.
after	fooling	around	,	the	intoxicated	trip	and	lux	fall	asleep	in	the	middle	of	the	football	stadium	.
trip	awakens	in	the	middle	of	the	night	and	leaves	lux	there	alone	.
when	lux	wakes	up	early	the	next	morning	she	is	confused	as	to	why	trip	is	not	there	.
she	takes	a	taxi	home	and	her	parents	were	worried	sick	(	of	course	)	.
but	they	take	measures	as	far	as	pulling	the	girls	out	of	school	and	stranding	them	inside	the	lisbon	residence	.
the	girls	,	in	a	desperate	attempt	to	hold	onto	a	connection	to	the	outside	world	,	begin	using	morse	code	to	interact	with	the	four	boys	who	so	closely	follow	their	lives	.
one	of	which	boys	lived	across	the	street	and	the	rest	which	came	over	daily	after	this	in	order	to	contact	the	girls	.
soon	following	,	the	boys	begin	talking	to	the	lisbon	girls	via	phone	.
one	day	,	the	girls	invite	the	boys	to	come	over	once	mr.	and	mrs.	lisbon	are	asleep	.
the	plan	being	that	the	boys	were	to	drive	the	girls	somewhere	,	the	boys	hastily	agree	.
they	sneak	over	and	lux	invites	them	to	wait	inside	for	her	sisters	and	lux	goes	to	wait	in	the	car	.
the	boys	wander	down	to	the	basement	while	waiting	for	therese	,	bonnie	and	mary	,	verbally	expressing	their	hopes	for	the	night	.
but	then	they	realize	they	have	stumbled	upon	one	of	the	sisters	.
she	had	hung	herself	.
the	boys	,	scared	to	death	,	run	all	the	way	home	.
time	passed	,	things	changed	,	girls	came	and	went	DOTS
but	no	matter	what	,	those	four	boys	never	forgot	the	lisbon	suicides	.
ultimately	,	the	movie	was	nothing	short	of	queer	.
when	trying	to	sum	up	the	story	in	a	few	lines	(	such	as	follow	the	lives	of	the	lisbon	sisters	and	how	they	connect	to	their	neighbors	.
a	story	full	of	obsession	,	gossip	,	lust	,	love	,	desire	and	isolation	.	)
the	story	sounds	quite	unique	and	intriguing	,	but	the	story	fell	short	of	that	for	me	personally	.
partly	because	the	lisbon	sisters	really	committed	suicide	for	a	reason	not	much	deeper	than	isolation	.
the	movie	,	in	places	,	is	also	extremely	flawed	.
i	do	not	believe	mr.	and	mrs.	lisbon	could	've	removed	their	daughters	from	school	for	so	long	without	their	daughters	being	taken	from	them	by	social	services	.
in	many	parts	of	the	movie	,	they	should	've	used	upbeat	music	to	fill	the	deafening	silences	.
the	whole	movie	was	filmed	in	a	style	that	i	can	truthfully	say	makes	me	rather	bored	.
i	would	only	suggest	this	film	to	,	perhaps	,	people	who	like	movies	such	as	elizabethtown	which	kirsten	dunst	is	also	in	.
but	i	personally	disliked	this	movie	which	was	not	very	tasteful	and	will	not	be	watching	it	again	in	the	future