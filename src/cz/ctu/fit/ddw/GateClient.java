
package cz.ctu.fit.ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.creole.gazetteer.Gazetteer;
import gate.util.GateException;
import gate.opennlp.OpenNLPNameFin;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    public void run(){
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            
            
            //Added
            ProcessingResource annieGazeteer = (Gazetteer) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            //Added
            ProcessingResource openNLPGazeteer = (OpenNLPNameFin) Factory.createResource("gate.opennlp.OpenNLPNameFin");
            
            ProcessingResource taggerPR = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");
            
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("/Users/Milan/Desktop/jape-example.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(taggerPR);            
//            annotationPipeline.add(japeTransducerPR);
            annotationPipeline.add(annieGazeteer);
//            annotationPipeline.add(openNLPGazeteer);
            
            // create a document
            Document document = Factory.newDocument("Sometext,\nwhat are Japan you");

            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();
                
                
                FeatureMap featureMap = null;
                // get all Token annotations
                AnnotationSet annSetTokens = as_default.get("Lookup",featureMap);
                System.out.println("Number of Lookup annotations: " + annSetTokens.size());
                System.out.println("" + as_default.toString());
                ArrayList tokenAnnotations = new ArrayList(annSetTokens);

                // looop through the Token annotations
                for(int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation)tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    System.out.println("Token: " + underlyingString);
                    
                    // get the features of the token
                    FeatureMap annFM = token.getFeatures();
                    
                    // get the value of the "string" feature
                    String value = (String)annFM.get((Object)"string");
                    
                    
                    System.out.println("Token: " + annFM.toString());
                }
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. //home/martin/GATE_Developer_8.1
            File gateHomeFile = new File("//home/martin/GATE_Developer_8.1");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. //home/martin/GATE_Developer_8.1/plugins            
            File pluginsHome = new File("//home/martin/GATE_Developer_8.1/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. //home/martin/GATE_Developer_8.1/user.xml
//            Gate.setUserConfigFile(new File("//home/martin/GATE_Developer_8.1", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            URL onlpHome = new File(pluginsHome, "OpenNLP").toURL();
            System.out.println("" + onlpHome);
            register.registerDirectories(onlpHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}