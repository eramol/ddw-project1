/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author martin
 */
public class FileOperations {
    
    public static List<String> loadFolderFilesContents(String foldername) throws IOException {
        List<String> contents = new ArrayList<>();
        File folder = new File(foldername);
        File[] files = folder.listFiles();
        for(File f : files) {
            contents.add(FileUtils.readFileToString(f));
        }
        return contents;
    }
        
    
}
