/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.FeatureMap;
import gate.Node;
import gate.util.InvalidOffsetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.xhtmlrenderer.util.Util;

/**
 *
 * @author martin
 */
public class BayesFactory {

    /**
     * static metod to create a basic token-based naive bayes classificator
     *
     * @param folderPath
     * @return
     */
    static public BayesClassificator createBasicBayes(String folderPath) {
        Corpus corpus = GATEOperations.createCorpus(folderPath);
        GATEOperations.runFullPipeline(corpus);
        
        Map<String, Integer> counts = wordTokenCounts(corpus);
        
        Set<String> tooFrequent = new HashSet<>();
        filterHapaxAndRubbish(counts, tooFrequent);
        
        Map<String, Double> prob = createLogProbabilities(counts);
        
        return new BayesClassificator(prob, tooFrequent);
    }
    
    static public BayesClassificator createAdvancedBayes(String folderPath) {
        Corpus corpus = GATEOperations.createCorpus(folderPath);
        GATEOperations.runFullPipeline(corpus);
        
        Map<String, Integer> counts = wordLookupTypesCounts(corpus);
        Set<String> tooFrequent = new HashSet<>();
        filterHapaxAndRubbish(counts, tooFrequent);
        
        Map<String, Double> prob = createLogProbabilities(counts);
        System.out.println("psize " + prob.size());
        
        
        for(String s : prob.keySet())
            System.out.println("" + s + " " + prob.get(s));
        
        return new BayesClassificator(prob, tooFrequent);
        
    }
    
    static public BayesClassificator createCategoryBayes(String folderPath) {
        Corpus corpus = GATEOperations.createCorpus(folderPath);
        GATEOperations.runFullPipeline(corpus);
        
        Map<String, Integer> counts = wordCategoryCounts(corpus);
        Set<String> tooFrequent = new HashSet<>();
        
        Map<String, Double> prob = createLogProbabilities(counts);
        System.out.println("psize " + prob.size());
        return new BayesClassificator(prob, tooFrequent);
    }

    /**
     * create log probabilities from counts also adds UNKNOWN token and applies
     * basic smoothing
     *
     * @param counts map of tokens and counts
     * @return
     */
    static public Map<String, Double> createLogProbabilities(Map<String, Integer> counts) {
        Map<String, Double> logProb = new HashMap<>();
        
        counts.put("UNKNOWN", 0);
        int acc = counts.size();
        for (String key : counts.keySet()) {
            acc += counts.get(key);
        }
        
        for (String key : counts.keySet()) {
            logProb.put(key, Math.log((counts.get(key) + 1) * 1.0 / acc));
        }
        
        return logProb;
    }

    /**
     * removes 30 most used tokens and those that were used just once from the
     * map and evides too frequent words without meaning
     *
     * @param counts input/output parameter
     * @param tooFrequent output parameter, 30 most frequent words
     */
    static public void filterHapaxAndRubbish(Map<String, Integer> counts, Set<String> tooFrequent) {
        List< Pair<String, Integer>> words = new ArrayList<>();
        for (String key : counts.keySet()) {
            words.add(new Pair<>(key, counts.get(key)));
        }
        
        Collections.sort(words, Comparator.comparing(p -> -p.getValue()));

        //filter
        Iterator<Pair<String, Integer>> it = words.iterator();
        for (int i = 0; i < 30; i++) {
            String key = it.next().getKey();
            tooFrequent.add(key);
            counts.remove(key);
            it.remove();
        }
        while (it.hasNext()) {
            Pair<String, Integer> pair = it.next();
            if (pair.getValue() < 2) {
                counts.remove(pair.getKey());
                it.remove();
            }
        }
        
    }

    /**
     * counts a number of tokens
     *
     * @param corpus
     * @return
     */
    static public Map<String, Integer> wordTokenCounts(Corpus corpus) {
        Map<String, Integer> cmap = new HashMap<>();
        for (int i = 0; i < corpus.size(); i++) {
            FeatureMap fm = null;
            AnnotationSet as = corpus.get(i).getAnnotations().get("Token", fm);
            List<Annotation> tokenAnnotations = new ArrayList<>(as);
            
            for (Annotation token : tokenAnnotations) {
                
                FeatureMap annFM = token.getFeatures();
                
                String kind = (String) annFM.get((Object) "kind");
                
                if (!kind.equals("word")) {
                    continue;
                }
                
                String string = (String) annFM.get((Object) "string");
                string = string.toLowerCase();
                Integer val = cmap.get(string);
                cmap.put(string, val == null ? 1 : val + 1);
                
            }
        }
        return cmap;
    }
    
    static public Map<String, Integer> wordCategoryCounts(Corpus corpus) {
        Map<String, Integer> cmap = new HashMap<>();
        for (int i = 0; i < corpus.size(); i++) {
            FeatureMap fm = null;
            AnnotationSet as = corpus.get(i).getAnnotations().get("Token", fm);
            List<Annotation> tokenAnnotations = new ArrayList<>(as);
            
            for (Annotation token : tokenAnnotations) {
                
                FeatureMap annFM = token.getFeatures();
                
                String kind = (String) annFM.get((Object) "kind");
                
                if (!kind.equals("word")) {
                    continue;
                }
                
                String string = (String) annFM.get((Object) "category");
                string = string.toLowerCase();
                Integer val = cmap.get(string);
                cmap.put(string, val == null ? 1 : val + 1);
                
            }
        }
        return cmap;
    }

    /**
     * counts a number of tokens
     *
     * @param corpus
     * @return
     */
    static public Map<String, Integer> wordLookupTypesCounts(Corpus corpus) {
        Map<String, Integer> cmap = new HashMap<>();
        for (int i = 0; i < corpus.size(); i++) {
            FeatureMap fm = null;
            AnnotationSet as = corpus.get(i).getAnnotations().get("Lookup", fm);
            List<Annotation> tokenAnnotations = new ArrayList<>(as);
            
            for (Annotation token : tokenAnnotations) {
                
                try {
                    FeatureMap annFM = token.getFeatures();
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = corpus.get(i).getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    
                    underlyingString = underlyingString.toLowerCase();
                    
                    Integer val = cmap.get(underlyingString);
                    cmap.put(underlyingString.toLowerCase(), val == null ? 1 : val + 1);
                } catch (InvalidOffsetException ex) {
                    Logger.getLogger(BayesFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
        return cmap;
    }
}
