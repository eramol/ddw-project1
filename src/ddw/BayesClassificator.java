/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author martin
 */
public class BayesClassificator {
    
    private final Map<String, Double> logProbabilities;
    private final Set<String> tooFrequent;

    public BayesClassificator(Map<String, Double> logProbabilities, Set<String> tooFrequent) {
        this.logProbabilities = logProbabilities;
        this.tooFrequent = tooFrequent;
    }
    
    /**
     * instead of probability and very low numbers the log probability is used
     * @param words
     * @return 
     */
    public double estimateLogProbability(List<String> words) {
        double acc = 0;
        for(String word : words) {
            if(tooFrequent.contains(word))
                continue;
            Double p = logProbabilities.get(word);
            acc += p == null? logProbabilities.get("UNKNOWN") : p;
        }
        return acc;
    }
    
}
