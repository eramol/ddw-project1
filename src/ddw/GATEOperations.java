/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.creole.gazetteer.Gazetteer;
import gate.opennlp.OpenNLPNameFin;
import gate.util.GateException;
import gate.util.InvalidOffsetException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public class GATEOperations {

    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;

    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    private static ProcessingResource documentResetPR;
    private static ProcessingResource tokenizerPR;
    private static ProcessingResource sentenceSplitterPR;
    private static ProcessingResource annieGazeteer;
    private static ProcessingResource taggerPR;

    /**
     * initialises the standard pipeline and runs it for the given corpus,
     * annotating it in the process
     *
     * @param corpus
     */
    public static void runFullPipeline(Corpus corpus) {
        initCheck();

        try {
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");
        } catch (ResourceInstantiationException ex) {
            Logger.getLogger(GATEOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        annotationPipeline.setCorpus(corpus);
        annotationPipeline.add(documentResetPR);
        annotationPipeline.add(tokenizerPR);
        annotationPipeline.add(sentenceSplitterPR);
        annotationPipeline.add(taggerPR);
        annotationPipeline.add(annieGazeteer);

        try {
            annotationPipeline.execute();
        } catch (ExecutionException ex) {
            Logger.getLogger(GATEOperations.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * creates a corpus from folder
     *
     * @param folderPath folder path
     * @return created corpus or null if there was an error
     */
    public static Corpus createCorpus(String folderPath) {
        initCheck();

        try {

            List<String> data = null;
            data = FileOperations.loadFolderFilesContents(folderPath);
            Corpus corpus = Factory.newCorpus("");
            for (String s : data) {

                corpus.add(Factory.newDocument(s));
            }
            return corpus;

        } catch (IOException e) {
            System.err.println("Corpus not created, IO error: " + e.getMessage());
        } catch (ResourceInstantiationException e) {
            System.err.println("Corpus not created, resource instantiation error: " + e.getMessage());
        }
        return null;
    }

    /**
     * checks whether GATE is initialised
     */
    private static void initCheck() {
        if (!isGateInitilised) {
            initialiseGate();
        }
    }

    /**
     * self explanatory
     */
    public static void initialiseGate() {
        try {
            // set GATE home folder
            File gateHomeFile = new File("//home/martin/GATE_Developer_8.1");
            Gate.setGateHome(gateHomeFile);

            // set GATE plugins folder            
            File pluginsHome = new File("//home/martin/GATE_Developer_8.1/plugins");
            Gate.setPluginsHome(pluginsHome);

            Gate.init();

            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);

            // flag that GATE was successfuly initialised
            isGateInitilised = true;

            documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            annieGazeteer = (Gazetteer) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            taggerPR = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");
        } catch (ResourceInstantiationException e) {
            System.err.println("Resource instantiation failed: " + e.getMessage());
        } catch (GateException e) {
            System.err.println("GATE not initialised: " + e.getMessage());
        } catch (MalformedURLException e) {
            System.err.println("Malformed url: " + e.getMessage());
        }

        isGateInitilised = true;
    }

    /**
     * extraction subfunction for Tokens
     * @param d
     * @param param feature name
     * @return 
     */
    private static List<String> extractFromTokens(Document d, String param) {
        FeatureMap fm = null;
        AnnotationSet as = d.getAnnotations().get("Token", fm);
        List<Annotation> tokenAnnotations = new ArrayList<>(as);
        List<String> tokens = new ArrayList<>();

        for (Annotation token : tokenAnnotations) {

            FeatureMap annFM = token.getFeatures();

            String kind = (String) annFM.get((Object) "kind");

            if (!kind.equals("word")) {
                continue;
            }

            String string = (String) annFM.get((Object) param);
            string = string.toLowerCase();
            tokens.add(string);
        }

        return tokens;
    }

    /**
     * extract names from Tokens
     * @param d
     * @return 
     */
    public static List<String> extractTokens(Document d) {
        return extractFromTokens(d, "string");
    }

    /**
     * extract grammatical categories from Tokens
     * @param d
     * @return 
     */
    public static List<String> extractCategories(Document d) {
        return extractFromTokens(d, "category");
    }

    /**
     * method to extract Lookup labeled tokens and get relevant string
     * @param d document to be read
     * @return 
     */
    public static List<String> extractLookup(Document d) {
        FeatureMap fm = null;
        AnnotationSet as = d.getAnnotations().get("Lookup", fm);
        List<Annotation> tokenAnnotations = new ArrayList<>(as);
        List<String> tokens = new ArrayList<>();

        for (Annotation token : tokenAnnotations) {

            try {
                FeatureMap annFM = token.getFeatures();
                Node isaStart = token.getStartNode();
                Node isaEnd = token.getEndNode();
                String underlyingString = d.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                
                underlyingString = underlyingString.toLowerCase();
                tokens.add(underlyingString);
            } catch (InvalidOffsetException ex) {
                Logger.getLogger(GATEOperations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return tokens;
    }
}
