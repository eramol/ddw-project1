/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import ddw.BayesClassificator;
import ddw.BayesFactory;
import ddw.GATEOperations;
import gate.Corpus;
import java.util.List;

/**
 *
 * @author martin
 */
public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            basicBayes(new String[]{"p"});
        } else if (args.length == 1) {
            if (null != args[0]) switch (args[0]) {
                case "b":
                    basicBayes(new String[]{"p"});
                    break;
                case "c":
                    categoricalBayes(new String[]{"p"});
                    break;
                case "a":
                    advancedBayes(new String[]{"p"});
                    break;
                default:
                    System.out.println("Wrong argument. Expected a|b|c p|n");
                    break;
            }
        } else if (args.length == 2) {
            if (null != args[0]) switch (args[0]) {
                case "b":
                    basicBayes(args);
                    break;
                case "c":
                    categoricalBayes(args);
                    break;
                case "a":
                    advancedBayes(args);
                    break;
                default:
                    System.out.println("Wrong argument. Expected a|b|c p|n");
                    break;
            }

        }



    }

    /**
     * designed to use the words recognized by Lookup from ANNIE gazetteer
     *
     * @param args
     */
    public static void advancedBayes(String[] args) {
        BayesClassificator pos = BayesFactory.createAdvancedBayes("./data/postrain");
        BayesClassificator neg = BayesFactory.createAdvancedBayes("./data/negtrain");

        String path = "./data/postest";
        if (args.length > 1 && args[1].equals("n")) {
            path = "./data/negtest";
        }

        Corpus c = GATEOperations.createCorpus(path);
        GATEOperations.runFullPipeline(c);

        int T = 0, F = 0;
        for (int i = 0; i < c.size(); i++) {
            List<String> data = GATEOperations.extractLookup(c.get(i));
            double tt = pos.estimateLogProbability(data);
            double ff = neg.estimateLogProbability(data);
            System.out.println("" + tt + " : " + ff);
            if(tt == ff)
                continue;
            
            
            int tmp = (tt > ff) ? T++ : F++;
        }
        System.out.println("T: " + T + " F:" + F);
    }

    /**
     * instead of word tokens uses categories
     *
     * @param args
     */
    public static void categoricalBayes(String[] args) {
        BayesClassificator pos = BayesFactory.createCategoryBayes("./data/postrain");
        BayesClassificator neg = BayesFactory.createCategoryBayes("./data/negtrain");

        String path = "./data/postest";
        if (args.length > 1 && args[1].equals("n")) {
            path = "./data/negtest";
        }

        Corpus c = GATEOperations.createCorpus(path);
        GATEOperations.runFullPipeline(c);

        int T = 0, F = 0;
        for (int i = 0; i < c.size(); i++) {
            List<String> data = GATEOperations.extractCategories(c.get(i));
            double tt = pos.estimateLogProbability(data);
            double ff = neg.estimateLogProbability(data);
            System.out.println("" + tt + " : " + ff);
            
            if(tt == ff)
                continue;
            
            int tmp = (tt > ff) ? T++ : F++;
        }
        System.out.println("T: " + T + " F:" + F);
    }

    
    /**
     * the very basic bayes naive classificator
     * @param args 
     */
    public static void basicBayes(String[] args) {
        BayesClassificator pos = BayesFactory.createBasicBayes("./data/postrain");
        BayesClassificator neg = BayesFactory.createBasicBayes("./data/negtrain");

        String path = "./data/postest";
        if (args.length > 1 && args[1].equals("n")) {
            path = "./data/negtest";
        }

        Corpus c = GATEOperations.createCorpus("./data/postest");
        GATEOperations.runFullPipeline(c);

        int T = 0, F = 0;
        for (int i = 0; i < c.size(); i++) {
            List<String> data = GATEOperations.extractTokens(c.get(i));
            double tt = pos.estimateLogProbability(data);
            double ff = neg.estimateLogProbability(data);
            System.out.println("" + tt + " : " + ff);
            if(tt == ff)
                continue;
            
            int tmp = (tt > ff) ? T++ : F++;
        }
        System.out.println("T: " + T + " F:" + F);
    }

}
